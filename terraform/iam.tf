data "aws_iam_policy_document" "sakura_web" {
  statement {
    sid = "AllObjectActions"
    actions = [
      "s3:*",
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.buzzlink-public-file.id}",
      "arn:aws:s3:::${aws_s3_bucket.buzzlink-private-file.id}",
      "arn:aws:s3:::${aws_s3_bucket.buzzlink-public-file.id}/*",
      "arn:aws:s3:::${aws_s3_bucket.buzzlink-private-file.id}/*"
    ]
  }

  statement {
    sid    = "DenyDeleteObject"
    effect = "Deny"

    actions = [
      "s3:DeleteObjectVersion*",
      "s3:DeleteBucket*"
    ]
    resources = [
      "arn:aws:s3:::${aws_s3_bucket.buzzlink-public-file.id}",
      "arn:aws:s3:::${aws_s3_bucket.buzzlink-private-file.id}",
      "arn:aws:s3:::${aws_s3_bucket.buzzlink-public-file.id}/*",
      "arn:aws:s3:::${aws_s3_bucket.buzzlink-private-file.id}/*"
    ]
  }
}

resource "aws_iam_policy" "sakura_web" {
  name        = "sakura_web_${terraform.workspace}"
  path        = "/"
  description = "sakura_web"
  policy      = data.aws_iam_policy_document.sakura_web.json
}

resource "aws_iam_user" "sakura_web" {
  name = "sakura_web_${terraform.workspace}"
  tags = local.tags
}

resource "aws_iam_user_policy_attachment" "sakura_web" {
  user       = aws_iam_user.sakura_web.name
  policy_arn = aws_iam_policy.sakura_web.arn
}


resource "aws_iam_policy" "backup" {
  count        = "${terraform.workspace == "stg" ? 1 : 0}"
  name        = "backup"
  path        = "/"
  description = "for upload backup object"
  policy      = data.aws_iam_policy_document.buzzlink_backup[count.index].json
}

resource "aws_iam_user" "backup" {
  count        = "${terraform.workspace == "stg" ? 1 : 0}"
  name = "backup"
  # tags = local.tags
}

resource "aws_iam_user_policy_attachment" "backup" {
  count        = "${terraform.workspace == "stg" ? 1 : 0}"
  user       = aws_iam_user.backup[count.index].name
  policy_arn = aws_iam_policy.backup[count.index].arn
}

data "aws_iam_policy_document" "buzzlink_backup" {
  count        = "${terraform.workspace == "stg" ? 1 : 0}"
  statement {
    sid = "AllObjectActions"
    actions = [
      "s3:*",
    ]
    resources = [
      "arn:aws:s3:::buzzlink-*-dump", 
      "arn:aws:s3:::buzzlink-*-image",
      "arn:aws:s3:::buzzlink-*-dump/*", 
      "arn:aws:s3:::buzzlink-*-image/*"
    ]
  }

  statement {
    sid    = "DenyDeleteObject"
    effect = "Deny"

    actions = [
      "s3:DeleteObjectVersion*",
      "s3:DeleteBucket*"
    ]
    resources = [
      "arn:aws:s3:::buzzlink-*-dump", 
      "arn:aws:s3:::buzzlink-*-image",
      "arn:aws:s3:::buzzlink-*-dump/*", 
      "arn:aws:s3:::buzzlink-*-image/*"
    ]
  }
}
