variable "bucket_name" {}
variable "tags" {}


resource "aws_s3_bucket" "buzzlink-backup-bucket" {
  bucket = var.bucket_name
  acl    = "private"
  lifecycle_rule {
    enabled = true
    expiration {
      days = 90
    }
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "aws:kms"
      }
    }
  }
  tags = var.tags
}

resource "aws_s3_bucket_public_access_block" "buzzlink-backup-access-block" {
  bucket                  = aws_s3_bucket.buzzlink-backup-bucket.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

output "backup_bucket_id" {
  value = aws_s3_bucket.buzzlink-backup-bucket.id
}
