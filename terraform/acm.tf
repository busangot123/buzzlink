resource "aws_acm_certificate" "cloudfront_cert" {
  domain_name       = local.subdomain_name[terraform.workspace]
  validation_method = "DNS"
  tags              = local.tags

  lifecycle {
    create_before_destroy = true
  }
  provider = "aws.virginia"
}
