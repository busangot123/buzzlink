provider "aws" {
  region  = "ap-northeast-1"
  profile = "buzzlink"
}

provider "aws" {
  alias  = "virginia"
  region = "us-east-1"
  profile = "buzzlink"
}

terraform {
  required_version = ">= 0.15.0"
  backend "s3" {
    bucket  = "buzzlink-terraform"
    profile = "buzzlink"
    region  = "ap-northeast-1"
    key     = "terraform.tfstate"
  }
}
