locals {
  account_id     = "691266396091"
  project_name   = "buzzlink"
  default_region = "ap-northeast-1"

  cloudfront_domain = {
    stg = "storage.buzzlink.info"
    prd = "storage.buzzlink.jp"
  }

  subdomain_name = {
    stg = "*.buzzlink.info"
    prd = "*.buzzlink.jp"
  }

  tags = {
    Environment = "${terraform.workspace}"
    Project     = local.project_name
  }
}
