locals {
  backup_bucket_name = {
    stg = [
      "buzzlink-${terraform.workspace}-db-dump",
      "buzzlink-${terraform.workspace}-web-image",
      "buzzlink-${terraform.workspace}-db-image",
    ]
    prd = [
      "buzzlink-${terraform.workspace}-db-dump",
      "buzzlink-${terraform.workspace}-web-image",
      "buzzlink-${terraform.workspace}-db-image",
      "buzzlink-log-image",
    ]
  }
}


resource "aws_s3_bucket" "buzzlink-public-file" {
  bucket = "buzzlink-${terraform.workspace}-public-file"
  acl    = "public-read"
  versioning {
    enabled = true
  }
  tags = local.tags
}


resource "aws_s3_bucket" "buzzlink-private-file" {
  bucket = "buzzlink-${terraform.workspace}-private-file"
  acl    = "private"
  versioning {
    enabled = true
  }
  tags = local.tags
}

resource "aws_s3_bucket_public_access_block" "buzzlink-private-file" {
  bucket                  = aws_s3_bucket.buzzlink-private-file.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

module "backup_bucket" {
  for_each    = toset(local.backup_bucket_name[terraform.workspace])
  source      = "./s3_module"
  bucket_name = each.key
  tags        = local.tags
}
