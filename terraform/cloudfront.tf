resource "aws_cloudfront_distribution" "public-file-cloudfront" {
  enabled = true

  aliases = [local.cloudfront_domain[terraform.workspace]]
  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate.cloudfront_cert.arn
    ssl_support_method  = "sni-only"
  }
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  origin {
    domain_name = aws_s3_bucket.buzzlink-public-file.bucket_regional_domain_name
    origin_id   = aws_s3_bucket.buzzlink-public-file.id
  }

  default_cache_behavior {
    target_origin_id       = aws_s3_bucket.buzzlink-public-file.id
    viewer_protocol_policy = "allow-all"
    allowed_methods        = ["GET", "HEAD", "OPTIONS", "PUT", "POST", "PATCH", "DELETE"]
    cached_methods         = ["GET", "HEAD"]
    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  tags = merge(local.tags, { "Name" = "${local.project_name}-${terraform.workspace}-public-file-cloudfront" })
}
