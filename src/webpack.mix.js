const mix = require('laravel-mix');
require('dotenv').config();

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .sass('resources/sass/app.scss', 'public/css');

mix.styles([
    'resources/css/lp-styles.css',
    'resources/css/lp-add.css',
    'resources/css/slick-theme.css',
    'resources/css/slick.css',
    'resources/css/lp-styles-common.css',
    ], 
    'public/css/lp.css');

mix.copy('resources/css/ajax-loader.gif', 'public/css/ajax-loader.gif')

mix.copy('resources/js/lp/slick.min.js', 'public/js/slick.min.js')

mix.js('resources/js/lp/jquery.min.1.8.3.js',
    'public/js/jquery.js')
    .js([
        'resources/js/lp/common.js',
        'resources/js/lp/zdo_drawer_menu.js',
        'resources/js/lp/index.js',
        'resources/js/lp/omitted.js',
    ], 'public/js/lp.js');
    