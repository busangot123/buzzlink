<?php


$finder = PhpCsFixer\Finder::create()
    ->in([
        __DIR__ . '/app',
        __DIR__ . '/config',
        __DIR__ . '/database/factories',
        __DIR__ . '/database/seeders',
        __DIR__ . '/routes',
        __DIR__ . '/tests',
    ]);

$config = new PhpCsFixer\Config();

return $config
    ->setRiskyAllowed(false)
    ->setRules([
        '@PHP71Migration:risky' => false,
        '@PHPUnit75Migration:risky' => false,
        '@PhpCsFixer' => true,
        '@PhpCsFixer:risky' => false,
        'declare_strict_types' => false,
    ])
    ->setFinder($finder);
