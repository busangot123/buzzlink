<?php

namespace Database\Seeders;

use App\Models\Project;
use App\Models\ProjectCategory;
use Illuminate\Database\Seeder;

class ProjectTableSeeder extends Seeder
{
    public function run()
    {
        Project::factory()->count(50)
            ->has(ProjectCategory::factory(), 'projectCategory')
            ->create()
        ;
    }
}
