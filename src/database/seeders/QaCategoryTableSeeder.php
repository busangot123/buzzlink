<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QaCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('qa_categories')->insert([
            0 => [
                'id' => 1,
                'name' => 'BuzzLinkについて',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
            1 => [
                'id' => 2,
                'name' => '支払い・報酬',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
            2 => [
                'id' => 3,
                'name' => 'クライアント向け',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
            3 => [
                'id' => 4,
                'name' => 'インフルエンサー向け',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
        ]);
    }
}
