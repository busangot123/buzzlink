<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class QaTitleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('qa_titles')->insert([
            0 => [
                'id' => 1,
                'qa_category_id' => 1,
                'title' => 'BuzzLinkとは',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
            1 => [
                'id' => 2,
                'qa_category_id' => 1,
                'title' => '会員情報',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
            2 => [
                'id' => 3,
                'qa_category_id' => 1,
                'title' => 'サイト機能',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
            3 => [
                'id' => 4,
                'qa_category_id' => 2,
                'title' => '支払い・返金',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
            4 => [
                'id' => 5,
                'qa_category_id' => 2,
                'title' => 'クレジット決済',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
            5 => [
                'id' => 6,
                'qa_category_id' => 2,
                'title' => '報酬（インフルエンサー）',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
            6 => [
                'id' => 7,
                'qa_category_id' => 3,
                'title' => '依頼全般',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
            7 => [
                'id' => 8,
                'qa_category_id' => 3,
                'title' => '支払い関連',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
            8 => [
                'id' => 9,
                'qa_category_id' => 3,
                'title' => '取引銀行',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
            9 => [
                'id' => 10,
                'qa_category_id' => 3,
                'title' => 'クライアントについて',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
            10 => [
                'id' => 11,
                'qa_category_id' => 4,
                'title' => '案件全般',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
            11 => [
                'id' => 12,
                'qa_category_id' => 4,
                'title' => '報酬関連',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
            12 => [
                'id' => 13,
                'qa_category_id' => 4,
                'title' => '取引銀行',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
            13 => [
                'id' => 14,
                'qa_category_id' => 4,
                'title' => 'クライアントについて',
                'updated_user' => 'user1',
                'created_at' => '2021-07-09 17:19:17',
                'updated_at' => '2021-07-09 17:19:17',
            ],
        ]);
    }
}
