<?php

namespace Database\Seeders;

use App\Models\QaContent;
use Illuminate\Database\Seeder;

class QaContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        QaContent::factory()->count(15)->create();
    }
}
