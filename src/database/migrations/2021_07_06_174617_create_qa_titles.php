<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQaTitles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qa_titles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('qa_category_id')->unsigned();
            $table->string('title', 200);
            $table->string('updated_user', 45);
            $table->timestamps();

            $table->foreign('qa_category_id')->references('id')->on('qa_categories');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qa_titles');
    }
}
