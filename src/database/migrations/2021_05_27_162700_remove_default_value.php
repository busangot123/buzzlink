<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveDefaultValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_accounts', function (Blueprint $table) {
            DB::statement('ALTER TABLE bank_accounts CHANGE account_type account_type TINYINT(1) DEFAULT NULL;');
            DB::statement('ALTER TABLE bank_accounts CHANGE code code CHAR(4) DEFAULT NULL;');
            DB::statement('ALTER TABLE bank_accounts CHANGE branch_code branch_code CHAR(4) DEFAULT NULL;');
            $table->dateTime('account_checked_at')->nullable()->change();
            $table->string('created_user', 45)->nullable()->change();
            $table->string('updated_user', 45)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_accounts', function (Blueprint $table) {
            //
        });
    }
}
