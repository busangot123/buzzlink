<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('client_user_id')->unsigned();
            $table->tinyInteger('status_code');
            $table->string('categories');
            $table->string('title');
            $table->string('contents', 2500);
            $table->char('post_sns_code', 1);
            $table->string('notification_url', 2048);
            $table->integer('reward_amount');
            $table->integer('request_people_number');
            $table->dateTime('application_deadline_at');
            $table->dateTime('report_deadline_at');
            $table->dateTime('application_start_at');
            $table->dateTime('selection_at');
            $table->string('created_user', 45);
            $table->string('updated_user', 45);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('client_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
