<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('uuid');
            $table->string('email')->unique();
            $table->string('password', 64)->nullable();
            $table->string('remember_token')->nullable();
            $table->string('email_verify_token', 300)->nullable();
            $table->string('name', 100)->nullable();
            $table->string('last_name', 64)->nullable();
            $table->string('first_name', 64)->nullable();
            $table->string('last_name_kana', 64)->nullable();
            $table->string('first_name_kana', 64)->nullable();
            $table->string('phone_number', 15)->nullable();
            $table->tinyInteger('default_role')->default(1)->comment('1：クライアント　2：インフルエンサー');
            $table->string('avatar_image')->nullable();
            $table->string('self_introduction', 2500)->nullable();
            $table->integer('postcode')->nullable();
            $table->string('prefecture', 8)->nullable();
            $table->string('address1', 500)->nullable();
            $table->string('address2', 500)->nullable();
            $table->date('birthday')->nullable();
            $table->tinyInteger('sex')->nullable()->comment('1：男性　2：女性');
            $table->json('sns_accounts')->nullable();
            $table->json('genres')->nullable();
            $table->tinyInteger('is_profile_update')->default(0)->comment('0：未更新　1：更新済');
            $table->tinyInteger('email_verified')->default(0)->comment('0：未確認　1：確認済');
            $table->dateTime('last_password_change_at')->nullable();
            $table->tinyInteger('is_information_read')->default(0)->comment('0:未読　1:既読');
            $table->dateTime('last_login_at')->nullable();
            $table->SoftDeletes();
            $table->string('created_user', 45)->default('temp_user');
            $table->string('updated_user', 45)->default('temp_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
