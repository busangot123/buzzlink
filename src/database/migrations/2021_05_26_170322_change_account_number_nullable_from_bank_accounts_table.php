<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Doctrine\DBAL\Types\StringType;
use Doctrine\DBAL\Types\Type;

class ChangeAccountNumberNullableFromBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Type::hasType('char')) {
            Type::addType('char', StringType::class);
        }
        Schema::table('bank_accounts', function (Blueprint $table) {
            //
            $table->char('account_number',7)->nullable()->change();
            $table->char('account_name', 100)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Type::hasType('char')) {
            Type::addType('char', StringType::class);
        }
        Schema::table('bank_accounts', function (Blueprint $table) {
            //
            $table->char('account_number', 7)->after('account_type')->change();
            $table->char('account_name', 100)->after('account_number')->change();
        });
    }
}
