<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeeRate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_rate', function (Blueprint $table) {
          $table->id();
          $table->date('start_date');
          $table->date('end_date');
          $table->decimal('fee_rate', 4, 2);
          $table->string('created_user', 45);
          $table->string('updated_user', 45);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_rate');
    }
}
