<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class RemoveUserProjectTableAndCreateProjectUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('user_project');

        Schema::create('project_users', function (Blueprint $table) {
            $table->id();
            $table->integer('project_id')->unsigned();
            $table->integer('influencer_user_id')->unsigned();
            $table->tinyInteger('application_status')->default(1)->comment('1:応募中　2:選定中　3:対応中(作業中) 4:結果確認中　5:完了 6:選定漏れ 9:応募取消');
            $table->string('self_promotion', 2500)->nullable();
            $table->dateTime('applied_at')->nullable();
            $table->tinyInteger('client_eva')->comment('1:Good 2:Bad')->nullable();
            $table->string('client_comment', 2500)->nullable();
            $table->tinyInteger('influencer_eva')->comment('1:Good 2:Bad')->nullable();
            $table->string('influencer_comment', 2500)->nullable();
            $table->string('sns_url', 2048)->nullable();
            $table->dateTime('reported_at')->nullable();
            $table->dateTime('evaluated_at')->nullable();
            $table->string('created_user', 45)->nullable();
            $table->string('updated_user', 45)->nullable();
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('project_users');

        Schema::create('user_project', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
        });

    }
}
