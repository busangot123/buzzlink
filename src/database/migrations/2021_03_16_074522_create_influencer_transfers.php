<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfluencerTransfers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencer_transfers', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('influencer_user_id')->unsigned();
            $table->date('bank_transfer_date');
            $table->integer('export_amount');
            $table->integer('fee_amount');
            $table->integer('payment_amount');
            $table->char('is_transfer', 1);
            $table->integer('closing_date');
            $table->date('closing_processing_date');
            $table->string('created_user', 45);
            $table->string('updated_user', 45);
            $table->timestamps();

            $table->foreign('influencer_user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencer_transfers');
    }
}
