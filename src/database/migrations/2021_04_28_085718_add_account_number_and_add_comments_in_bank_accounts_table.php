<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Doctrine\DBAL\Types\StringType;
use Doctrine\DBAL\Types\Type;
use Illuminate\Support\Facades\DB;

class AddAccountNumberAndAddCommentsInBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Type::hasType('char')) {
            Type::addType('char', StringType::class);
          }
        Schema::table('bank_accounts', function (Blueprint $table) {
            //
            $table->char('account_number', 7)->after('account_type');
            DB::statement('ALTER TABLE bank_accounts CHANGE account_type account_type TINYINT(1) DEFAULT 1 COMMENT "1:普通　2:当座";');
            DB::statement('ALTER TABLE bank_accounts CHANGE is_auto_pay is_auto_pay TINYINT(1) DEFAULT 0 COMMENT "0:自動出金OFF　1:自動出金ON";');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_accounts', function (Blueprint $table) {
            //
            $table->dropColumn('account_number');
            DB::statement('ALTER TABLE bank_accounts CHANGE account_type account_type TINYINT(1) DEFAULT NULL;');
            DB::statement('ALTER TABLE bank_accounts CHANGE is_auto_pay is_auto_pay TINYINT(1) DEFAULT NULL;');
        });
    }
}
