<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnInFeeAndProjectUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fee_rate', function (Blueprint $table) {
            $table->integer('min_rate')->after('fee_rate');
        });

        Schema::table('project_users', function (Blueprint $table) {
            $table->dateTime('selected_at')->after('applied_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
