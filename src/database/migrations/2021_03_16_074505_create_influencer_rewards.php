<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfluencerRewards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('influencer_rewards', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('project_user_id')->unsigned();
            $table->dateTime('get_reward_at');
            $table->integer('reward_amount');
            $table->integer('closing_month');
            $table->date('closing_processing_date');
            $table->string('created_user', 45);
            $table->string('updated_user', 45);
            $table->timestamps();

            $table->foreign('project_user_id')->references('id')->on('user_project');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('influencer_rewards');
    }
}
