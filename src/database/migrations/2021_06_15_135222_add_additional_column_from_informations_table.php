<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdditionalColumnFromInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('informations', function (Blueprint $table) {
            //
            $table->tinyInteger('important_flg')->after('notification_at')->default(0)->comment('0:普通　1:大切なお知らせ   ※大切なお知らせはメール受信必須');
            $table->dateTime('publish_date')->after('important_flg')->nullable();
            $table->dateTime('close_date')->after('publish_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('informations', function (Blueprint $table) {
            //
            $table->dropColumn(['important_flg','publish_date','close_date']);
        });
    }
}
