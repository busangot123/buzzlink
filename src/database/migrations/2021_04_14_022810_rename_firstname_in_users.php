<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameFirstnameInUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('first_name', 'full_name');
            $table->renameColumn('first_name_kana', 'full_name_kana');
            $table->dropColumn('last_name');
            $table->dropColumn('last_name_kana');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
          $table->renameColumn('full_name', 'first_name');
          $table->renameColumn('full_name_kana', 'first_name_kana');
        });
    }
}
