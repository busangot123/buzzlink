<?php

namespace Database\Factories;

use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Project::class;

    public function definition()
    {
        $created_date = $this->faker->dateTimeBetween('-3 days', '+3 days');
        $application_start_at = $created_date->format('Y-m-d H:i:s');
        $application_deadline_at = $created_date->modify('+7 day')->format('Y-m-d H:i:s');
        $selection_deadline_at = $created_date->modify('+7 day')->format('Y-m-d H:i:s');
        $report_dead_line_at = $created_date->modify('+14 day')->format('Y-m-d H:i:s');

        return [
            'client_user_id' => function () {
                return User::factory()->create()->id;
            },
            'status_code' => 1,
            'title' => $this->faker->realText(50),
            'content' => $this->faker->realText(200),
            'post_sns_code' => $this->faker->numberBetween(1, 4),
            'notification_url' => $this->faker->url,
            'reward_amount' => $this->faker->numberBetween(3000, 30000),
            'request_people_number' => $this->faker->numberBetween(1, 5),
            'application_start_at' => $application_start_at,
            'application_deadline_at' => $application_deadline_at,
            'selection_deadline_at' => $selection_deadline_at,
            'report_deadline_at' => $report_dead_line_at,
        ];
    }
}
