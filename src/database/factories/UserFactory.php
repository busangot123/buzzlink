<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $image_category = [
            'abstract', 'animals', 'business', 'cats', 'city', 'food', 'nightlife', 'fashion', 'people', 'nature', 'sports', 'technics', 'transport',
        ];

        return [
            'uuid' => 'BLC-'.$this->faker->regexify('[0-9A-Z]{6}'),
            'email' => $this->faker->email,
            'password' => '$2y$10$muocUBFQVTeal/0BOqXVmeOXNshaBn3GI9JY7t8CwFzg/brcKgIKO', // password02
            'remember_token' => Str::random(10),
            'name' => $this->faker->name,
            'email_verified_at' => now(),
            'full_name' => $this->faker->name(),
            'full_name_kana' => $this->faker->kanaName(),
            'phone_number' => $this->faker->randomNumber(9, true),
            'default_role' => $this->faker->numberBetween(0, 1),
            'avatar_image' => $this->faker->imageUrl(200, 200, $this->faker->randomElement($image_category), false),
            'self_introduction' => $this->faker->realText(200),
            'postcode' => $this->faker->postcode,
            'prefecture' => $this->faker->numberBetween(1, 46),
            'address1' => $this->faker->address,
            'address2' => $this->faker->address,
            'birthday' => $this->faker->dateTimeBetween('-50 years', '-20years'),
            'sex' => $this->faker->numberBetween(0, 1),
            'sns_accounts' => null,
            'genres' => null,
            'is_profile_update' => $this->faker->boolean,
            'last_password_change_at' => null,
            'last_login_at' => null,
        ];
    }
}
