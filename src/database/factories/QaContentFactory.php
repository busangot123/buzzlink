<?php

namespace Database\Factories;

use App\Models\QaContent;
use App\Models\QaTitle;
use Illuminate\Database\Eloquent\Factories\Factory;

class QaContentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = QaContent::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $qaTitle = new QaTitle();
        $total_titles = $qaTitle->count('id');

        return [
            'qa_title_id' => rand(1, $total_titles),
            'question' => $this->faker->sentence(5).'?',
            'answer' => $this->faker->sentence(5),
            'updated_user' => 'user1',
        ];
    }
}
