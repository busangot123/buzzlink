<?php

namespace Database\Factories;

use App\Models\Genre;
use App\Models\Project;
use App\Models\ProjectCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProjectCategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProjectCategory::class;

    public function definition()
    {
        return [
            'project_id' => Project::factory()->create()->id,
            'name' => Genre::find(rand(1, 15))->name,
        ];
    }
}
