export default Object.freeze({
  application_status: {
    applied: 1,
    selection: 2,
    working: 3,
    checking: 4,
    done: 5,
    completed: 6,
    missing: 9
  },
  status_role: {
    influencer: 1,
    client: 2
  },
  project_user: {
    display_limit: 50
  },
  char_map: {
    ァ: 'ア',
    ィ: 'イ',
    ゥ: 'ウ',
    ェ: 'エ',
    ォ: 'オ',
    ヵ: 'カ',
    ヶ: 'ケ',
    ッ: 'ツ',
    ャ: 'ヤ',
    ュ: 'ユ',
    ョ: 'ヨ',
    ヮ: 'ワ',
    '(': '（',
    ')': '）',
    'ｰ': '－',
    'ー': '－',
    '-': '－',
    '−': '－',
  },
  attribute: {
    user: {
      email: "メールアドレス",
      password: "パスワード",
      passwordConfirm: "パスワード確認",
      name: " 表示用ユーザー名",
      fullName: "氏名",
      fullNameKana: "氏名（カナ）",
      phoneNumber: "電話番号",
      defaultRole: "主な利用用途",
    }
  },
  maxInput: {
    user: {
      email: 255,
      name: 100,
      fullName: 64,
      fullNameKana: 64,
      phoneNumber: 15,
    },
    project: {
      genres: 3,
      title: 200,
      content: 2500,
      notificationURL: 200,
      rewardAmount: 7,
    }
  },
  httpCode: {
    OK: 200,
    Created: 201,
    BadRequest: 400,
    UnprocessableEntity: 422,
    InternalServerError: 500,
  },
  responseMessage: {
    InternalServerError: "予期しないエラーが発生しました。"
  },
  social_followers: [
    { id: 1, content: "1,000未満" },
    { id: 2, content: "1,000以上～5,000未満" },
    { id: 3, content: "5,000以上～10,000未満" },
    { id: 4, content: "10,000以上～50,000未満" },
    { id: 5, content: "50,000以上～100,000未満" },
    { id: 6, content: "100,000以上～1,000,000未満" },
    { id: 7, content: "1,000,000以上" },
  ],
  CustomValidationMessage: {
    MaxFileSize: "添付ファイルは10Mバイト以下にしてください。"
  },
  successMessage: {
    emailRejectSetting: "メール配信設定が更新されました。"
  }
});
