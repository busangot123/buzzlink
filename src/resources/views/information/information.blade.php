@extends('include.layout')
@section('header')
@include('include.auth_header')
@endsection
@section('content')
    <div class="mainCont">
        <div class="mainCont__Inner">
            <div class="mainCont__Wrap mainCont__Wrap2">
            @include('include.user.profile')
                <div class="mainCont__Right">
                    <h1 class="mainCont__Title2">
                        @if (isset($title))
                            {{$title}}
                        @else
                            運営からのお知らせ一覧
                        @endif
                    </h1>
                    <div class="mainCont__Box">
                            <div class="mainCont__UneiBox">
                            @if(isset($title))
                                <p class="mainCont__UneiDate">{{japaneseDateFormat($information->notification_at)}}</p>
                                <span>{!! $information->contents !!}</span>
                            @else
                            @foreach ($information as $key => $info)
                            @if ($key > 0)
                                <br>
                                <p class="mainCont__UneiDate">{{japaneseDateFormat($info->notification_at)}}</p>
                            @else
                                <p class="mainCont__UneiDate">{{japaneseDateFormat($info->notification_at)}}</p>
                            @endif
                                <a href="/information/{{$info->id}}" class="mainCont__UneiLink">
                                    <span>{{$info->title}}</span>
                                </a>

                            @endforeach
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
