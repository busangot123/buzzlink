<!doctype html>
<html>

<head>

    <!-- Google Tag Manager -->
    <!-- Google Tag Manager -->

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>バズリンク｜インフルエンサー様と企業を結び付けるマッチングプラットフォーム</title>
    <meta name="keywords" content="" />
    <meta name="description" content="BuzzLinkはインフルエンサーと広告主となる企業が出会う接点の創出から、
関係深化までを支援する新時代のインフルエンサーマーケティングプラットフォームです。" />

    <!-- favicon -->
    <link rel="shortcut icon" href="#">
    <link rel="apple-touch-icon" href="#" />

    <!-- ogp -->
    <meta property="og:title" content="バズリンク｜インフルエンサー様と企業を結び付けるマッチングプラットフォーム" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="#" />
    <meta property="og:image" content="#" />
    <meta property="og:site_name" content="#" />
    <meta property="og:description" content="#" />


    <!-- css -->
    <link rel="stylesheet" href="{{ asset('css/lp.css') }}">

</head>

<body>

    <div id="entrance" class="entrance">
        <div class="entrance_Cont">
            <p>「広告主」×「インフルエンサー」</p>
            <h1 class="ttl">
                "自由に出会える"<br>
                新時代クラウドマッチング
            </h1>
            <div class="entrance_Btns">
                <a href="/top_client">
                    広告主の方はこちら
                </a>
                <a href="/top_influencer">
                    インフルエンサーの方はこちら
                </a>
            </div>
        </div>
        <p class="entrance_Logo">
            <img src="{{ asset('assets/logo3.png') }}" alt="バズリンク" />
        </p>
        <p class="entrance_Copy">インフルエンサー様と企業を結び付けるマッチングプラットフォーム<br class="inline_s">「バズリンク」｜&copy; 2021 BuzzLink<br><a
                href="{{ route('guide') }}" target="_blank">特定商取引法に基づく表記</a></p>
    </div>

</body>

</html>