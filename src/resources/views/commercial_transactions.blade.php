@extends('include.layout')

@section('header')
@include('include.headerNone')
@endsection

@section('content')
<div class="tokuteiLead">
    <div id="">
        <div class="">
            <h3 class="tokutei_Ttl">特定商取引法に基づく表記</h3>
        </div>
        <div class="">
            <div class="">
                <table class="gtable3">
                    <tbody>
                        <tr>
                            <td><span class="t5">販売業者</span></td>
                            <td>エイチキューブ株式会社</td>
                        </tr>
                        <tr>
                            <td><span class="t5">運営責任者</span></td>
                            <td>堀部 謙二</td>
                        </tr>
                        <tr>
                            <td><span class="t5">住所</span></td>
                            <td>〒460-0003 愛知県名古屋市中区錦１丁目７番３４号</td>
                        </tr>
                        <tr>
                            <td><span class="t5">電話番号</span></td>
                            <td>052-228-6994</td>
                        </tr>
                        <tr>
                            <td><span class="t5">FAX番号</span></td>
                            <td>052-308-3573</td>
                        </tr>
                        <tr>
                            <td><span class="t5">メールアドレス</span></td>
                            <td><a href="mailto:info@hcube3.com">info@hcube3.com</a></td>
                        </tr>
                        <tr>
                            <td><span class="t5">URL</span></td>
                            <td><a href="http://www.hcube3.com">www.hcube3.com</a></td>
                        </tr>
                        <tr>
                            <td><span class="t5">商品以外の必要代金</span></td>
                            <td>なし</td>
                        </tr>
                        <tr>
                            <td><span class="t5">注文方法</span></td>
                            <td>当サイトからのお申し込み</td>
                        </tr>
                        <tr>
                            <td><span class="t5">支払方法</span></td>
                            <td>クレジットカード決済</td>
                        </tr>
                        <tr>
                            <td><span class="t5">支払期限</span></td>
                            <td>申し込み時</td>
                        </tr>
                        <tr>
                            <td><span class="t5">引渡し時期</span></td>
                            <td>登録審査完了時</td>
                        </tr>
                        <tr>
                            <td><span class="t5">返品・交換について</span></td>
                            <td>弊社が別途規定した場合を除き、決済後の返金には応じられません。</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection