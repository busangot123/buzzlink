@extends('include.layout')

@section('header')
@include('include.headerNone')
@endsection

@section('content')
<div class="error footer__fixed">
    <div class="error__base">
        <div class="error__Inner">
            <p class="error__Title">アクセスしようとしたページを表示できませんでした。<br>しばらく時間を置いてからやり直してください。</p>
            <div class="error__Content">
                <div class="error__CauseTitle">
                    <p class="error__CauseTitleP">以下の原因が考えられます。</p>
                </div>
                <div class="error__detail">
                    <ul class="error__detailUl">
                        <li class="error__detailLi">Webサイトをメンテナンスしています。</li>
                        <li class="error__detailLi">Webサイトへのアクセスが集中しています。</li>
                        <li class="error__detailLi">Webサイトに問題が発生しています。</li>
                        </ui>
                </div>
            </div>

            <div class="err__Btn">
                <a href="/" class="err__BtnA">
                    ホームへ
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
