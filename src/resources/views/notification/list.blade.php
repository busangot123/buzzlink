@extends('include.layout')

@section('content')
@include('include.auth_header')
    <div class="mainCont">
        <div class="mainCont__Inner">
            <div class="mainCont__Wrap mainCont__Wrap2">
                @include('include.user.profile')

                <div class="mainCont__Right">
                    <h1 class="mainCont__Title2">
                        お知らせ一覧
                    </h1>

                    <table class="table">
                        <tbody>
                            <tr>
                                <th class="table__th">日付</th>
                                <th class="table__th">お知らせ</th>
                            </tr>
                            @foreach($notifications as $notification)
                                <tr>
                                    <td class="table__td table__Center">{{ \Carbon\Carbon::parse($notification->notification_at)->format('Y/m/d') }}</td>
                                    <td class="table__td"><a href="{{ $notification->destination_url }}">{{ $notification->notification_contents }}</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                  </div>
            </div>
        </div>
    </div>
@endsection
