@unless ($breadcrumbs->isEmpty())
    <div class="breadcrumb breadcrumb2">
        <ul class="breadcrumb__List">
            @foreach ($breadcrumbs as $breadcrumb)
                @if (!is_null($breadcrumb->url))
                    <li class="breadcrumb__Li">
                        <a href="{{ $breadcrumb->url }}">
                            {{ $breadcrumb->title }}
                        </a>
                    </li>
                @else
                    <li class="breadcrumb__Li">
                        {{ $breadcrumb->title }}
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
@endunless
