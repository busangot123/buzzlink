@extends('include.layout')
@section('header')
@include('include.auth_header')
@endsection
@section('content')
<div class="mainCont">
    <div class="mainCont__Inner">

        <div class="faq">
            <h1 class="faq__PageTitle">
                よくある質問
            </h1>
            <div class="faq__SearchWrap">
                <p class="faq__SearchTxt">調べたいキーワードを入力してください。</p>
                <form class="searchWrap" method="GET" action="{{ route('faq.search') }}">
                    <div class="searchWrap__SearchKeywordWrap searchWrap__SearchKeywordWrap2">
                        <input placeholder="検索キーワード" class="searchWrap__SearchKeyword" value="" name="keyword">
                        <button type="submit" class="searchWrap__SearchKeywordBtn"></button>
                    </div>
                </form>
            </div>
            <div class="faq__Wrap faq__Wrap2">
                <div class="faq__Left block_p">
                    <ul class="faq__LeftList">
                        @foreach ($categories as $category)
                        <li class="faq__LeftListTitle">{{ $category->name }}</li>
                        @foreach ($category->titles as $qa_title)
                        <li class="faq__LeftListLi">
                            <a href="{{ route('faq.search_title_id', ['title_id' => $qa_title->id]) }}">{{ $qa_title->title }}</a>
                        </li>
                        @endforeach
                        @endforeach
                    </ul>
                </div>
                <div class="faq__Right">
                    @if (isset($title))
                        <h2 class="faq__RightTitle">{{ $title }}</h2>
                    @endif
                    @forelse ($titleWithContents as $key => $titleWithContent)
                        @if (empty($title))
                            @if ($key > 0)
                                <br>
                                <h2 class="faq__RightTitle">{{ $titleWithContent->title }}</h2>
                            @else
                                <h2 class="faq__RightTitle">{{ $titleWithContent->title }}</h2>
                            @endif
                        @endif
                    <dl class="faq__RightBox">
                        @if (empty($titleWithContent->contents))
                            <dt class="faq__Dt">{{ $titleWithContent->question }}</dt>
                            <dd class="faq__Dd">{!! $titleWithContent->answer !!}</dd>
                        @else
                        @foreach ($titleWithContent->contents as $content)
                            <dt class="faq__Dt">{{ $content->question }}</dt>
                            <dd class="faq__Dd">{!! $content->answer !!}</dd>
                        @endforeach
                        @endif
                    </dl>
                    @empty
                        <p><i>キーワードが見つかりませんでした。</i></p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
