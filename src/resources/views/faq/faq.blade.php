@extends('include.layout')
@section('header')
@include('include.auth_header')
@endsection
@section('content')
    <div class="mainCont">
        <div class="mainCont__Inner">
            <div class="faq">
                <div class="faq__Cont">
                    <h1 class="faq__PageTitle">
                        よくある質問
                    </h1>
                    <div class="faq__SearchWrap">
                        <p class="faq__SearchTxt">調べたいキーワードを入力してください。</p>
                        <form class="searchWrap" method="GET" action="{{ route('faq.search') }}">
                            <div class="searchWrap__SearchKeywordWrap" >
                                <input placeholder="検索キーワード" class="searchWrap__SearchKeyword" value="" name="keyword">
                                <button type="submit" class="searchWrap__SearchKeywordBtn"></button>
                            </div>
                        </form>
                    </div>
                    <div class="faq__Wrap">
                        @foreach ($categories as $category)
                            <div class="faq__Box">
                                <h2 class="faq__BoxTitle">
                                    {{ $category->name }}
                                </h2>
                                <ul class="faq__BoxListWrap">
                                    @foreach ($category->titles as $qa_title)
                                        <li class="faq__BoxList">
                                            <a href="{{ route('faq.search_title_id', ['title_id' => $qa_title->id]) }}">{{ $qa_title->title }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
