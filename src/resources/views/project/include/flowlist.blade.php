<div class="flowlist flowlist2">
    <p class="flowlist__Flow {{ $flow_active == 1 ? 'flowlist__Flow2 flowlist__FlowActive' : 'flowlist__Flow flowlist__Flow2'}}">
        <span class="flowlist__FlowNumber {{ $flow_active == 1 ?  'flowlist__FlowNumberActive' : '' }}">FLOW1</span>
        募集内容<br>入力
    </p>
    <p class="flowlist__Flow {{ $flow_active == 2 ? 'flowlist__Flow2 flowlist__FlowActive' : 'flowlist__Flow flowlist__Flow2'}}">
        <span class="flowlist__FlowNumber {{ $flow_active == 2 ?  'flowlist__FlowNumberActive' : '' }}">FLOW2</span>
        募集内容<br>確認
    </p>
    <p class="flowlist__Flow {{ $flow_active == 3 ? 'flowlist__Flow2 flowlist__FlowActive' : 'flowlist__Flow flowlist__Flow2'}}">
        <span class="flowlist__FlowNumber {{ $flow_active == 3 ?  'flowlist__FlowNumberActive' : '' }}">FLOW3</span>
        募集登録<br>完了
    </p>
</div>
