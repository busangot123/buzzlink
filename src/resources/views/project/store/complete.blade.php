@extends('include.layout')

@section('header')
@include('include.headerNone')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner mainCont__Inner2">
        <div class="mainCont__Wrap">
            <div class="mainCont__Right mainCont__Right3">
                <div class="mainCont__Box test">
                    <h1 class="mainCont__Title2 mainCont__Title3">
                        新しい仕事を募集する
                    </h1>
                    @include('project.include.flowlist', ['flow_active' => 3])
                </div>
                <div class="register__Cont">
                    <h2 class="register__Title register__Title2">ご登録ありがとうございます。<br>
                        仕事の募集を開始しました。</h2>

                    <h2 class="register__Title">募集期間が終了するまで、インフルエンサーからの応募をお待ちください。</h2>

                    <div class="mainCont__Btns">
                        <ul class="mainCont__BtnsList">
                            <li class="mainCont__BtnsLi">
                                <a href="{{ route('mypage.project_management') }}" class="mainCont__Btn2 mainCont__BtnCon">
                                    仕事管理を表示する
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
