@extends('include.layout')

@section('header')
@include('include.headerNone')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner mainCont__Inner2">
        <div class="mainCont__Wrap">
            <div class="mainCont__Right mainCont__Right3">
                <div class="mainCont__Box test">
                    <h1 class="mainCont__Title2 mainCont__Title3">
                        新しい仕事を募集する
                    </h1>
                    <div class="mainCont__Box">
                        @include('project.include.flowlist', ['flow_active' => 2])
                    </div>
                    <div class="mainCont__Box">
                        <div class="mainCont__GrayBox">
                            <p class="mainCont__GrayBoxP">
                                登録はまだ完了していません。<br>
                                下記のプレビュー内容が公開されます。問題がなければ、ページ下の「この内容で登録する」をクリック（タップ）してください。<br>
                                <span class="mainCont__GrayBoxRed">
                                    公開後は、依頼内容の修正ができなくなるため、必ずもう一度依頼内容をご確認ください。<br>
                                    ※報酬にはシステム利用料は含まれておりません。
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        プレビュー
                    </h2>
                    <div class="mainCont__TitleWrap">
                        <div class="mainCont__TitleWrapBox">
                            <p class="mainCont__StIcon">募集中</p>
                            <h1 class="mainCont__Title2">
                                {{ $input['title'] }}
                            </h1>
                            <ul class="tag">
                                @if(isset($input['genres']))
                                @foreach($input['genres'] as $genre)
                                <li class="tag__Inner">
                                    {{ App\Models\Genre::getGenreName($genre) }}
                                </li>
                                @endforeach
                                @endif

                                @if(isset($input['other_category']))
                                <li class="tag__Inner">
                                    {{ $input['other_category'] }}
                                </li>
                                @endif
                            </ul>
                            <p class="mainCont__Date">掲載日：{{ japaneseDateFormat($input['application_start_at']) }}</p>
                        </div>
                        <div class="mainCont__UserWrap">
                            <div class="mainCont__UserPic mainCont__UserPic2">
                                <img src="{{ auth()->user()->avatar_image }}" class="mainCont__UserPicImg">
                            </div>
                            <div class="mainCont__UserName">
                                <h2>
                                    {{ auth()->user()->name }}
                                </h2>
                                <p class="mainCont__UserSt2">
                                    評価<span class="mainCont__UserStScore3">{{ auth()->user()->client_rate }}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        案件内容
                    </h2>
                    <div class="mainCont__BlockWrap">
                        <div class="mainCont__Block">
                            <span class="mainCont__BlockTitle">
                                1人あたりの報酬
                            </span>
                            {{ number_format($input['reward_amount']) }}円
                        </div>
                        <div class="mainCont__Block">
                            <span class="mainCont__BlockTitle">
                                依頼予定人数
                            </span>
                            {{ $input['request_people_number'] }}
                        </div>
                    </div>
                </div>

                <div class="mainCont__Box">
                    <h3 class="mainCont__Title4">
                        投稿希望SNS
                    </h3>

                    <div class="mainCont__BlockSnsWrap">
                        @switch($input['post_sns_code'])
                        @case(1)
                        <div class="mainCont__BlockSns">
                            <i class="fab fa-youtube" aria-hidden="true"></i>
                            Youtube
                        </div>
                        @break
                        @case(2)
                        <div class="mainCont__BlockSns">
                            <i class="fab fa-instagram" aria-hidden="true"></i>
                            Instagram
                        </div>
                        @break
                        @case(3)
                        <div class="mainCont__BlockSns">
                            <i class="fab fa-facebook" aria-hidden="true"></i>
                            Facebook
                        </div>
                        @break
                        @case(4)
                        <div class="mainCont__BlockSns">
                            <i class="fab fa-twitter" aria-hidden="true"></i>
                            Twitter
                        </div>
                        @break
                        @case(5)
                        <div class="mainCont__BlockSns">
                            <i class="fab fa-tiktok" aria-hidden="true"></i>
                            TikTok
                        </div>
                        @break
                        @default
                        @endswitch
                    </div>
                </div>

                <div class="mainCont__Box">
                    <h3 class="mainCont__Title4">
                        依頼内容
                    </h3>
                    <div class="mainCont__GrayBox">
                        <p class="mainCont__GrayBoxP">
                            {!! nl2br(e($input['content'])) !!}
                        </p>
                    </div>
                </div>

                <div class="mainCont__Box">
                    @if($input['notification_url'])
                    <h3 class="mainCont__Title4">
                        告知URL
                    </h3>
                    <div class="mainCont__GrayBox">
                        <p class="mainCont__GrayBoxP">
                            <a href="{{$input['notification_url']}}">
                                {{ $input['notification_url'] }}
                            </a>
                        </p>
                    </div>
                    @endif
                </div>

                <div class="mainCont__Box mainCont__Box--confirm">
                    @if(sizeof($input['uploaded_files']) > 0)
                    <h3 class="mainCont__Title4">
                        添付ファイル
                    </h3>
                    <div class="mainCont__GrayBox">
                        @foreach($input['uploaded_files'] as $file)
                        <div class="mainCont__File">
                            <span class="file_name">{{ $file['name'] }}</span>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>

                <form action="{{route('project.store.confirm.post')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="register__BtnWrap">
                        <button class="register__BtnConfirm2" type="button" onclick="history.back()">入力内容を修正する</button>
                        <button class="register__BtnRegister2" type="submit">この内容で登録する</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection