@extends('include.layout')

@section('header')
@include('include.headerNone')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner mainCont__Inner2">
        <div class="mainCont__Wrap">
            <div class="mainCont__Right mainCont__Right3">
                <div class="mainCont__Box test">
                    <h1 class="mainCont__Title2 mainCont__Title3">
                        新しい仕事を募集する
                    </h1>
                    @include('project.include.flowlist', ['flow_active' => 1])
                </div>
                <project-store :input='@json($session_input)' :genres='@json($genres)' :tax-rate='{{$tax_rate}}' :fee-rate='{{$fee_rate}}' :csrf='@json(csrf_token())' post-url="{{ route('project.store.form.post') }}">
                </project-store>
            </div>
        </div>
    </div>
</div>
@endsection
