@extends('include.layout')

@section('content')
@include('include.headerNone')
<div class="mainCont">
    <div class="mainCont__Inner">
        {{ Breadcrumbs::render('management_details', route('mypage.project_management')) }}
        @include('project.checking.flowlist', ['flow_active' => 1])
        <div class="mainCont__Wrap">
            <div class="mainCont__Right mainCont__Right3">
                @include('include.project_details', [
                'project' => $project_user->project,
                'project_user_status' => $project_user->project_status_label
                ])

                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        選定したインフルエンサー
                    </h2>

                    <h3 class="mainCont__Title4">選定日時 {{
                        \Carbon\Carbon::parse($project_user->selected_at)->format('Y/m/d H:i:s') }}</h3>
                    @include('include.project_application_user_details', ['project_user' => $project_user])
                </div>
                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        インフルエンサーからクライアントへの完了報告
                    </h2>
                    <h3 class="mainCont__Title4">SNS投稿情報　/　完了報告日時 {{
                        \Carbon\Carbon::parse($project_user->reported_at)->format('Y/m/d H:i:s') }}</h3>
                    <p>
                        投稿URL：{{ $project_user->sns_url }}
                    </p>
                    <div class="mainCont__EmbedSnsWrap">
                        @if($project_user->sns_type == 'twitter')
                        <blockquote class="twitter-tweet">
                            <a href="{{ $project_user->sns_url }}"></a>
                        </blockquote>
                        <script type="application/javascript" async src="https://platform.twitter.com/widgets.js"
                            charset="utf-8"></script>
                        @elseif($project_user->sns_type == 'facebook')
                        <div class="mainCont__EmbedSns">
                            <div id="fb-root"></div>
                            <script type="application/javascript" async defer
                                src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v12.0&autoLogAppEvents=1"></script>
                            <div class="fb-post" data-href="{{ $project_user->sns_url }}"></div>
                        </div>
                        @else
                        <div class="mainCont__EmbedSns">
                            <iframe class="sns_frame {{ $project_user->sns_type }}"
                                src="{{ $project_user->sns_frame_url }}" frameborder="0" scrolling="no"></iframe>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        クライアントからインフルエンサーへの結果評価
                    </h2>
                    <p class="mainCont__RedBox">
                        インフルエンサーについての評価を入力してください
                    </p>
                    <form
                        action="{{ route('project_management.client.confirmation', ['project_user_id' => $project_user->id]) }}"
                        method="POST">
                        @csrf
                        <div class="register__FormBox">
                            <dl class="register__FormList">
                                <dt class="register__FormListLeft">
                                    結果の評価
                                </dt>
                                <dd class="register__FormListRight">
                                    <input hidden value="{{ $project->id }}" name="project_id">
                                    <p class="register__RadioBox">
                                        <input id="radio3" class="register__RadioBtnGood" name="client_eva" type="radio"
                                            value="1" @if(old('client_eva')==1 || $client_eva==1) checked @endif>
                                        <label for="radio3" class="register__Label register__Label3">Good!</label>
                                    </p>
                                    <p class="register__RadioBox">
                                        <input id="radio4" class="register__RadioBtnBad" name="client_eva" type="radio"
                                            value="2" @if(old('client_eva')==2 || $client_eva==2) checked @endif>
                                        <label for="radio4" class="register__Label register__Label4">Bad</label>
                                    </p>
                                    @error('client_eva')
                                    <div class="error__message">{{ $message }}</div>
                                    @enderror
                                </dd>
                                <dt class="register__FormListLeft">
                                    コメント
                                </dt>
                                <dd class="register__FormListRight">
                                    <textarea id="text-editor" name="client_comment" placeholder=""
                                        class="register__TextArea3">{{ old('client_comment') ?: $client_comment }}</textarea>
                                    <span class="register__FormListStxt" style="float:right;width:35%;"><span
                                            id="character_count">0</span>/2500</span>
                                    @error('client_comment')
                                    <div class="error__message">{{ $message }}</div>
                                    @enderror
                                </dd>
                            </dl>

                        </div>
                        <div class="mainCont__Btns">
                            <ul class="mainCont__BtnsList">
                                <li class="mainCont__BtnsLi">
                                    <button type="submit" class="report__submitBtn mainCont__Btn2 mainCont__BtnCon2">
                                        入力内容を確認する
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
