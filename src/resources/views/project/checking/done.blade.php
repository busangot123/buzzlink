@extends('include.layout')

@section('content')
@include('include.headerNone')
    <div class="mainCont">
        <div class="mainCont__Inner">
            @include('project.checking.flowlist', ['flow_active' => 3])
            <div class="mainCont__Wrap">
                <div class="register__Cont">
                    <h2 class="register__Title register__Title2">ご登録ありがとうございます。<br>
                        結果確認が完了しました
                    </h2>
                    <h2 class="register__Title">この案件のタスクはすべて完了しました。<br>引き続きBuzzLinkをご利用ください。</h2>

                    <div class="mainCont__Btns">
                        <ul class="mainCont__BtnsList">
                            <li class="mainCont__BtnsLi">
                                <a href="{{ route('mypage.project_management') }}" class="mainCont__Btn2 mainCont__BtnCon">
                                    仕事管理を表示する
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
