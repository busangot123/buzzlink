@extends('include.layout')

@section('content')
@include('include.headerNone')
<div class="mainCont">
    <div class="mainCont__Inner">
        {{ Breadcrumbs::render('management_details', route('mypage.project_management')) }}
        <div class="mainCont__Wrap">
            <div class="mainCont__Right mainCont__Right3">
                @include('include.project_details', [
                'project' => $project_user->project,
                'project_user_status' => $project_user->project_status_label
                ])

                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        選定したインフルエンサー
                    </h2>

                    <h3 class="mainCont__Title4">選定日時 {{
                        \Carbon\Carbon::parse($project_user->selected_at)->format('Y/m/d H:i:s') }}</h3>

                    @include('include.project_application_user_details', ['project_user' => $project_user])
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
