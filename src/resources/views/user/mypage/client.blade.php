@extends('include.layout')

@section('header')
@include('include.auth_header')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner">
        <div class="mainCont__Wrap mainCont__Wrap2">
            @include('include.user.profile')
            <div class="mainCont__Right">
                <h1 class="mainCont__Title2">
                    マイページ　クライアント用
                </h1>

                <div class="mainCont__Box">
                    <div class="mainCont__BlockWrap2">
                        <div class="mainCont__Block2">
                            <p class="mainCont__Block2Title">
                                <i class="fas fa-tasks"></i> 現在進行中の<br class="inline_s">案件
                            </p>
                            <dl class="mainCont__Block2List">
                                <dt class="mainCont__Block2ListDt">
                                    募集中
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <a href="{{ route('mypage.project_management') . '#募集中' }}"
                                        class="mainCont__Block2Number">{{$data['recruiting_count']}}</a>件
                                </dd>
                                <dt class="mainCont__Block2ListDt">
                                    選定中
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <a href="{{ route('mypage.project_management') . '#選定中' }}"
                                        class="mainCont__Block2Number">{{$data['selection_count']}}</a>件
                                </dd>
                                <dt class="mainCont__Block2ListDt">
                                    依頼中
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <a href="{{ route('mypage.project_management') . '#依頼中' }}"
                                        class="mainCont__Block2Number">{{$data['completed_count']}}</a>件
                                </dd>
                                <dt class="mainCont__Block2ListDt">
                                    結果確認中
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <a href="{{ route('mypage.project_management') . '#結果確認中' }}"
                                        class="mainCont__Block2Number">{{$data['checking_count']}}</a>件
                                </dd>
                            </dl>
                        </div>
                        <div class="mainCont__Block2">
                            <p class="mainCont__Block2Title">
                                <i class="fas fa-medal"></i> これまでの<br class="inline_s">案件実績
                            </p>
                            <dl class="mainCont__Block2List">
                                <dt class="mainCont__Block2ListDt">
                                    募集実績
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <span href="#"
                                        class="mainCont__Block2Number2">{{$data['client']->client_project_post_count}}</span>件
                                </dd>
                                <dt class="mainCont__Block2ListDt">
                                    依頼実績
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <span href="#" class="mainCont__Block2Number2">{{
                                        $data['client']->client_project_user_count }}</span>件
                                </dd>
                                <dt class="mainCont__Block2ListDt">
                                    発注率
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <span href="#" class="mainCont__Block2Number2">{{
                                        $data['client']->client_order_rate }}</span>%
                                </dd>
                            </dl>
                        </div>
                        <div class="mainCont__Block2">
                            <p class="mainCont__Block2Title">
                                <i class="fas fa-medal"></i> これまでの<br class="inline_s">評価
                            </p>
                            <dl class="mainCont__Block2List">
                                <dt class="mainCont__Block2ListDt">
                                    評価
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <span href="#"
                                        class="mainCont__Block2Number2">{{$data['client']->client_rate}}</span>
                                </dd>
                                <dt class="mainCont__Block2ListDt">
                                    評価件数
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <span href="#" class="mainCont__Block2Number2">{{ $data['evaluation_count']
                                        }}</span>件
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <a href="{{route('project.store.form')}}" class="mainCont__Btn2 mainCont__Btn2_2">
                        案件を募集する（無料）
                    </a>
                </div>

                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        実行中のタスク
                    </h2>
                    @if(!$data['project']->isEmpty() || !$data['project_users']->isEmpty())
                    @foreach($data['project'] as $project)
                    <a href="{{ \App\Helpers\Utils::ProjectRoutePerStatus($project) }}" class="matter">
                        <div href="#" class="matter__Box">
                            <div class="matter__OtherBox matter__OtherBox2">
                                <p class="matter__OtherTitle">{{$project->statusName}}</p>
                                <p class="matter__OtherTxt">あと<span class="matter__OtherDate">{{
                                        $project->project_time_details['hours_left']
                                        }}</span>{{ $project->project_time_details['hours_left_sign'] }}</p>
                                <span class="matter__OtherSmall">({{ $project->project_time_details['deadline_at']
                                    }})</span>
                            </div>
                            <div class="matter__Detail">
                                <h3 class="matter__Title">{{ $project->title }}</h3>
                                <ul class="tag">
                                    @foreach($project->projectCategory as $category)
                                    <li class="tag__Inner">
                                        <span class="matter__hash">
                                            # {{ $category->name }}
                                        </span>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="matter__PriceBox matter__PriceBox2">
                                <p class="matter__PriceTitle">1人あたりの報酬</p>
                                <p class="matter__Price">
                                    {{ number_format($project->reward_amount) }}<span class="matter__PriceYen">円</span>
                                </p>
                                <p class="matter__PriceNinzu">
                                    募集人数 <span class="matter__OtherDate">{{ $project->request_people_number }}</span>人
                                </p>
                            </div>
                            <p class="matter__Date">
                                掲載日：{{ $project->published }}
                            </p>
                        </div>
                    </a>
                    @endforeach
                    @foreach($data['project_users'] as $project)
                    <a href="{{ \App\Helpers\Utils::ProjectRoutePerStatus($project) }}" class="matter">
                        <div href="#" class="matter__Box">
                            <div class="matter__OtherBox matter__OtherBox2">
                                <p class="matter__OtherTitle">{{$project->statusName}}</p>
                                <p class="matter__OtherTxt">あと<span class="matter__OtherDate">{{
                                        $project->project_time_details['hours_left']
                                        }}</span>{{ $project->project_time_details['hours_left_sign']
                                    }}</p>
                                <span class="matter__OtherSmall">({{
                                    $project->project_time_details['deadline_at']
                                    }})</span>
                            </div>
                            <div class="matter__Detail">
                                <h3 class="matter__Title">{{ $project->project->title }}</h3>
                                <ul class="tag">
                                    @foreach($project->projectCategory as $category)
                                    <li class="tag__Inner">
                                        <span class="matter__hash">
                                            # {{ $category->name }}
                                        </span>
                                    </li>
                                    @endforeach
                                </ul>
                                <div class="matter__UserBox">
                                    <p class="matter__UserPic">
                                        <img src="{{$project->user->avatar_image != null ? $project->user->avatar_image : '/assets/user.png'}}"
                                            alt="" class="matter__UserPicImg">
                                    </p>
                                    <span href="#" class="matter__UserName">
                                        {{$project->user->name}}
                                    </span>
                                </div>
                            </div>
                            <div class="matter__PriceBox">
                                <p class="matter__PriceTitle">報酬</p>
                                <p class="matter__Price">
                                    {{ number_format($project->project->reward_amount) }}<span
                                        class="matter__PriceYen">円</span>
                                </p>
                            </div>
                            <p class="matter__Date">
                                掲載日：{{ $project->project_time_details['published_at'] }}
                            </p>
                        </div>
                    </a>
                    @endforeach
                    @else
                    <div class="mainCont__GrayBox">
                        <p class="mainCont__GrayBoxP">
                            実行中のタスクはありません。
                        </p>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection