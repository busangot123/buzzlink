@extends('include.layout')

@section('header')
@include('include.auth_header')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner">
        <div class="mainCont__Wrap mainCont__Wrap2">
            @include('include.user.profile')
            <project-management-form :status='@json($status)'></project-management-form>
        </div>
    </div>
</div>
@endsection