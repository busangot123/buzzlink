@extends('include.layout')

@section('header')
@include('include.auth_header')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner">
        <div class="mainCont__Wrap mainCont__Wrap2">
            @include('include.user.profile')
            <edit-information :user="{{ $user }}" :prefecture_list="{{ $prefectures }}" :genre_list="{{ $genres }}"
                :sns_list='@json($user->sns_list)'></edit-information>
        </div>
    </div>
</div>
@endsection