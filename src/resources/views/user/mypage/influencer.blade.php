@extends('include.layout')

@section('header')
@include('include.auth_header')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner">
        <div class="mainCont__Wrap mainCont__Wrap2">
            @include('include.user.profile')
            <div class="mainCont__Right">
                <h1 class="mainCont__Title2">
                    マイページ　インフルエンサー用
                </h1>
                <div class="mainCont__Box">
                    <div class="mainCont__MoneyBox">
                        <div class="mainCont__MoneyBoxInner">
                            <h3 class="mainCont__MoneyTitle"><i class="fas fa-yen-sign"></i> 現在の報酬残高</h3>
                            <p class="mainCont__Money">¥ {{$data['reward_balance']}}</p>
                        </div>
                        <a href="{{ route('mypage.edit_information') . '#振込先口座情報' }}">出金方式の確認・変更はこちら</a>
                    </div>

                    <div class="mainCont__BlockWrap2">
                        <div class="mainCont__Block2">
                            <p class="mainCont__Block2Title">
                                <i class="fas fa-tasks"></i> 現在進行中の<br class="inline_s">案件
                            </p>
                            <dl class="mainCont__Block2List">
                                <dt class="mainCont__Block2ListDt">
                                    応募中
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <a href="{{ route('mypage.work_management') . '#応募中' }}"
                                        class="mainCont__Block2Number">{{$data['applied_count']}}</a>件
                                </dd>
                                <dt class="mainCont__Block2ListDt">
                                    対応中
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <a href="{{ route('mypage.work_management') . '#対応中' }}"
                                        class="mainCont__Block2Number">{{$data['working_count']}}</a>件
                                </dd>
                                <dt class="mainCont__Block2ListDt">
                                    結果確認中
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <a href="{{ route('mypage.work_management') . '#結果確認中' }}"
                                        class="mainCont__Block2Number">{{$data['checking_count']}}</a>件
                                </dd>
                            </dl>
                        </div>
                        <div class="mainCont__Block2">
                            <p class="mainCont__Block2Title">
                                <i class="fas fa-medal"></i> これまでの<br class="inline_s">案件実績
                            </p>
                            <dl class="mainCont__Block2List">
                                <dt class="mainCont__Block2ListDt">
                                    対応実績
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <span href="#"
                                        class="mainCont__Block2Number2">{{$data['client']->users_in_contact}}</span>件
                                </dd>
                                <dt class="mainCont__Block2ListDt">
                                    完了実績
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <span href="#"
                                        class="mainCont__Block2Number2">{{$data['client']->completed_project}}</span>件
                                </dd>
                                <dt class="mainCont__Block2ListDt">
                                    完了率
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <span href="#"
                                        class="mainCont__Block2Number2">{{$data['client']->completion_rate}}</span>%
                                </dd>
                            </dl>
                        </div>
                        <div class="mainCont__Block2">
                            <p class="mainCont__Block2Title">
                                <i class="fas fa-medal"></i> これまでの<br class="inline_s">評価
                            </p>
                            <dl class="mainCont__Block2List">
                                <dt class="mainCont__Block2ListDt">
                                    評価
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <span href="#" class="mainCont__Block2Number2">{{ $data['client']->influencer_rate
                                        }}</span>
                                </dd>
                                <dt class="mainCont__Block2ListDt">
                                    評価件数
                                </dt>
                                <dd class="mainCont__Block2ListDd">
                                    <span href="#" class="mainCont__Block2Number2">{{$data['evaluation_count']}}</span>件
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <a href="{{ route('search') }}" class="mainCont__Btn2 mainCont__Btn2_2">
                        案件に応募する（無料）
                    </a>
                </div>

                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        実行中のタスク
                    </h2>

                    @if(!$data['project_users']->hasManyProjectUsers->isEmpty())
                    @foreach($data['project_users']->hasManyProjectUsers as $project)
                    <div href="#" class="matter matter__Box">
                        <div class="matter__OtherBox matter__OtherBox2">
                            <p class="matter__OtherTitle">{{$project->statusName}}</p>
                            <p class="matter__OtherTxt">あと<span class="matter__OtherDate">{{
                                    $project->project_time_details['hours_left']
                                    }}</span>{{ $project->project_time_details['hours_left_sign']
                                }}</p>
                            <span class="matter__OtherSmall">({{
                                $project->project_time_details['deadline_at']
                                }})</span>
                        </div>
                        <div class="matter__Detail">
                            <h3 class="matter__Title">
                                <a href="{{ App\Helpers\Utils::ProjectUserRoutePerStatus($project) }}"
                                    class="matter__TitleA">{{ $project->project->title }}
                                </a>
                            </h3>
                            <ul class="tag">
                                @foreach($project->projectCategory as $category)
                                <li class="tag__Inner">
                                    <span class="matter__hash">
                                        # {{ $category->name }}
                                    </span>
                                </li>
                                @endforeach
                            </ul>
                            <div class="matter__UserBox">
                                <p class="matter__UserPic">
                                    <a target="_blank"
                                        href="{{ route('profile.client', ['uuid' => $project->project->user->uuid]) }}">
                                        <img src="{{$project->project->user->avatar_image != null ? $project->project->user->avatar_image : '/assets/user.png'}}"
                                            alt="" class="matter__UserPicImg">
                                    </a>
                                </p>
                                <a target="_blank"
                                    href="{{ route('profile.client', ['uuid' => $project->project->user->uuid]) }}"
                                    class="matter__UserName">
                                    {{$project->project->user->name}}
                                </a>
                            </div>
                        </div>
                        <div class="matter__PriceBox matter__PriceBox2">
                            @if ($project->application_status == 1 || $project->application_status == 2)
                            <p class="matter__PriceTitle">1人あたりの報酬</p>
                            @endif
                            @if ($project->application_status == 3 || $project->application_status == 4)
                            <p class="matter__PriceTitle">報酬</p>
                            @endif
                            <p class="matter__Price">
                                {{ number_format($project->project->reward_amount) }}<span
                                    class="matter__PriceYen">円</span>
                            </p>
                            @if ($project->application_status == 1 || $project->application_status == 2)
                            <p class="matter__PriceNinzu">
                                募集人数 <span class="matter__OtherDate">{{ $project->project->request_people_number
                                    }}</span>人
                            </p>
                            @endif
                        </div>
                        <p class="matter__Date">
                            掲載日：{{$project->project->parse_created_at_in_japanese}}
                        </p>
                    </div>
                    @endforeach
                    @else
                    <div class="mainCont__GrayBox">
                        <p class="mainCont__GrayBoxP">
                            実行中のタスクはありません。
                        </p>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection