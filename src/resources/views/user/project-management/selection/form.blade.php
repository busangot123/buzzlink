@extends('include.layout')

@section('header')
@include('include.headerNone')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner">
        @include('user.project-management.selection.top', ['step' => 1])
        <div class="mainCont__Wrap">
            <div class="mainCont__Right mainCont__Right3">
                <div class="register register2">
                    @include('include.project_details', [
                        'project' => $project_data['project'], 
                        'project_user_status' => null,
                        'client_rating' => $project_data['project']->user->client_rate, 
                        'applied_user' => $project_data['applied_user'], 
                        'is_applied' => $project_data['is_applied'], 
                        'contacted_user' => $project_data['contacted_user']
                    ])
                    <selection-form
                        :project='@json($project)'
                        :project_users='@json($project_users)'
                        :selected_users_array='@json($selected_users_array)'
                        :csrf='@json(csrf_token())' 
                        :posturl='@json($post_url)'
                    />
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
