{{ Breadcrumbs::render('management_details', route('mypage.project_management')) }}

<div class="flowlist">
    <p class="{{ $step == 1 ? 'flowlist__Flow flowlist__FlowActive' : 'flowlist__Flow' }}">
        <span
            class="{{ $step == 1 ? 'flowlist__FlowNumber flowlist__FlowNumberActive' : 'flowlist__FlowNumber' }}">FLOW.1</span>
        選定<br>入力
    </p>
    <p class="{{ $step == 2 ? 'flowlist__Flow flowlist__FlowActive' : 'flowlist__Flow' }}">
        <span
            class="{{ $step == 2 ? 'flowlist__FlowNumber flowlist__FlowNumberActive' : 'flowlist__FlowNumber' }}">FLOW.2</span>
        内容<br>確認
    </p>
    <p class="{{ $step == 3 ? 'flowlist__Flow flowlist__FlowActive' : 'flowlist__Flow' }}">
        <span
            class="{{ $step == 3 ? 'flowlist__FlowNumber flowlist__FlowNumberActive' : 'flowlist__FlowNumber' }}">FLOW.3</span>
        選定<br>完了
    </p>
</div>

