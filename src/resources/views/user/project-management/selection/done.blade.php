@extends('include.layout')

@section('header')
@include('include.headerNone')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner">
        @include('user.project-management.selection.top', ['step' => 3])
        <div class="mainCont__Wrap">
            <div class="mainCont__Right mainCont__Right3">
                <div class="register register2">
                    <section>
                        <div class="register__Cont">
                            <h2 class="register__Title register__Title2">ご登録ありがとうございます。<br>
                            インフルエンサーの選定が完了しました。</h2>
                            <h2 class="register__Title">選定したインフルエンサーに対してシステムより選定通知を送信します。<br>インフルエンサーからの完了報告をお待ちください。</h2>
                            <div class="mainCont__Btns">
                            <ul class="mainCont__BtnsList">
                                <li class="mainCont__BtnsLi">
                                <a href="{{ route('mypage.project_management') }}" class="mainCont__Btn2 mainCont__BtnCon">
                                    仕事管理を表示する
                                </a>
                                </li>
                            </ul>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
