@extends('include.layout')

@section('header')
@include('include.headerNone')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner">
        @include('user.project-management.selection.top', ['step' => 2])
        <div class="mainCont__Wrap">
            <div class="mainCont__Right mainCont__Right3">
                <div class="register register2">
                    <div class="mainCont__Box">
                        <div class="mainCont__GrayBox">
                            <p class="mainCont__GrayBoxP">下記の内容で問題なければ、クレジットカード情報を入力し「この内容で確定する」をクリックしてください。</p>
                            <p class="mainCont__GrayBoxRed">本ページのご入力が完了することで、クレジットカード決済を行い、インフルエンサーへ案件が依頼されます。<br>
                            確定後は修正ができなくなるため、必ずもう一度依頼内容をご確認ください。</p>
                        </div>
                    </div>
                    @include('include.project_details', [
                        'project' => $project_data['project'],
                        'project_user_status' => null,
                        'client_rating' => $project_data['project']->user->client_rate,
                        'applied_user' => $project_data['applied_user'],
                        'is_applied' => $project_data['is_applied'],
                        'contacted_user' => $project_data['contacted_user']
                    ])
                    <selection-payment
                        :project='@json($project)'
                        :project_users='@json($project_users)'
                        :selected_users_array='@json($selected_users_array)'
                        :payment='@json($payment)'
                        :csrf='@json(csrf_token())'
                        :posturl='@json($post_url)'
                    />
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
