@extends('include.layout_message')

@section('content')
@include('include.headerNone')
    @if(isset($active_messages))
        <div class="message__wrapper">
            <chat-room
            :message_users='@json($message_users)'
            :message_list='@json($active_messages)'
            :active_chat='@json($active_chat)'
            :user='@json(auth()->user())'
            ></chat-room>
        </div>
    @else
        <div class="message__wrapper">
            <chat-room
            :message_users='@json($message_users)'
            :user='@json(auth()->user())'
            ></chat-room>
        </div>
    @endif
@endsection
