@extends('include.layout')

@section('header')
@include('include.auth_header')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner">
        <div class="mainCont__Wrap mainCont__Wrap2">
            @include('include.user.profile')
            <div class="mainCont__Right">
                <h1 class="mainCont__Title2">
                    支払管理
                </h1>
                <p>報酬、出金、クレジット明細が確認できます。</p>

                <div class="mainCont__MoneyBox">
                    <div class="mainCont__MoneyBoxInner">
                        <h3 class="mainCont__MoneyTitle"><i class="fas fa-yen-sign"></i> 現在の報酬残高</h3>
                        <p class="mainCont__Money">¥{{ $reward_balance == null ? 0 :
                            number_format($reward_balance->balance) }}</p>
                    </div>
                    <a href="{{route('mypage.edit_information').'#振込先口座情報'}}">出金方式の確認・変更はこちら</a>
                </div>

                <div class="tabs">
                    <input id="housyu" type="radio" name="tab_item">
                    <label class="tab_item" for="housyu">報酬明細</label>

                    <input id="furikomi" type="radio" name="tab_item">
                    <label class="tab_item" for="furikomi">振込明細</label>

                    <input id="credit" type="radio" name="tab_item">
                    <label class="tab_item" for="credit">クレジット明細</label>

                    <div class="tab_content" id="housyu_content">
                        <div class="tab_content_description">
                            過去24ケ月における、獲得した報酬を表示しています。
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th class="table__th">日付</th>
                                        <th class="table__th">概要</th>
                                        <th class="table__th">獲得報酬</th>
                                    </tr>
                                    @foreach($influencer_rewards_list as $influencer_reward)
                                    <tr>
                                        <td class="table__td table__Center">{{
                                            dateTimeSlashFormat($influencer_reward->get_reward_at) }}</td>
                                        <td class="table__td"><a
                                                href="{{ route('work_management.complete', ['project_id' => $influencer_reward->project_id]) }}">{{
                                                $influencer_reward->project_title }}</a></td>
                                        <td class="table__td table__Right">¥{{
                                            number_format($influencer_reward->reward_amount) }}</td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab_content" id="furikomi_content">
                        <div class="tab_content_description">
                            過去24ケ月における、報酬を登録口座へ振込（出金）した明細を表示しています。
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th class="table__th">日付(予定含む)</th>
                                        <th class="table__th">出金額</th>
                                        <th class="table__th">手数料</th>
                                        <th class="table__th">振込額</th>
                                    </tr>

                                    @foreach($influencer_transfer_list as $influencer_transfer)
                                    <tr>
                                        <td class="table__td table__Center">{{
                                            dateTimeSlashFormat($influencer_transfer->bank_transfer_date) }}</td>
                                        <td class="table__td table__Right">¥{{
                                            number_format($influencer_transfer->export_amount) }}</td>
                                        <td class="table__td table__Right">¥{{ $influencer_transfer->fee_amount }}</td>
                                        <td class="table__td table__Right">¥{{
                                            number_format($influencer_transfer->payment_amount) }}</td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab_content" id="credit_content">
                        <div class="tab_content_description">
                            過去24ケ月における、案件を依頼したことにより発生した費用を表示しています。
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th class="table__th">日付</th>
                                        <th class="table__th">概要</th>
                                        <th class="table__th">費用</th>
                                    </tr>
                                    @foreach($client_payment_list as $client_payment)
                                    <tr>
                                        <td class="table__td table__Center">{{
                                            dateTimeSlashFormat($client_payment->created_at) }}</td>
                                        <td class="table__td"><a
                                                href="{{ route('project.details', ['project_id' => $client_payment->project_id]) }}">{{
                                                $client_payment->project_title }}</a></td>
                                        <td class="table__td table__Right">¥{{
                                            number_format($client_payment->total_cost) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript">
    $(function () {
        function getUrlHashParams() {
            const hash = decodeURIComponent(window.location.hash.split("#").filter(hash => hash != "")[0]);
            if (hash == 'undefined') return false;
            return hash;
        }

        if (typeof (getUrlHashParams()) == 'string' && getUrlHashParams() == '報酬明細') {
            localStorage.setItem("open_tab_id", 'housyu');
        }

        $("input[type=\"radio\"]").click(function (event) {
            localStorage.setItem("open_tab_id", event.target.id);
        });

        var open_tab_id = localStorage.getItem("open_tab_id");
        if (open_tab_id) {
            $("input[id=\"" + open_tab_id + "\"]").click();
        } else {
            $("input[id=\"housyu\"]").click();
        }
    });
</script>
@endsection
