@extends('include.layout')

@section('header')
@include('include.auth_header')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner">
        {{ Breadcrumbs::render('profile', $influencer->name) }}
        <div class="mainCont__Wrap">
            <div class="mainCont__Left">
                <div class="mainCont__Left">
                    <div class="mainCont__UserBox">
                        <p class="mainCont__UserTitle">
                            {{$influencer->name}}
                        </p>
                        <div class="mainCont__UserPic mainCont__UserPic3">
                            <img src="{{ $influencer->avatar_image }}" class="mainCont__UserPicImg">
                        </div>
                    </div>

                    <div class="mainCont__UserSt">

                        <ul class="mainCont__UserStList">
                            <li class="mainCont__UserStLi">
                                評価<br class="inline_s">
                                <div class="mainCont__UserStBox">
                                    <span class="mainCont__UserStScore">{{$influencer->influencer_rate}}</span>
                                </div>
                                <span class="mainCont__UserStScore2">評価件数　{{$influencer->influencer_rate_count}}件</span>
                            </li>
                            <li class="mainCont__UserStLi">対応実績<br class="inline_s">
                                <div class="mainCont__UserStBox">
                                    <span class="mainCont__UserStScore">{{$influencer->users_in_contact}}</span>件
                                </div>
                            </li>
                            <li class="mainCont__UserStLi">完了率<br class="inline_s">
                                <div class="mainCont__UserStBox">
                                    <span class="mainCont__UserStScore">{{$influencer->completion_rate}}</span>%
                                </div>
                            </li>
                        </ul>

                        <ul class="mainCont__UserStOtherList">
                            <li class="mainCont__UserStOtherLi">
                                <span class="mainCont__UserStOtherTtl">登録日</span>{{$influencer->registration_date}}
                            </li>
                            @if(!is_null($influencer->prefectureData))
                            <li class="mainCont__UserStOtherLi">
                                <span class="mainCont__UserStOtherTtl">都道府県</span>{{ $influencer->prefectureData->name
                                ?? '' }}
                            </li>
                            @endif
                            @if (!is_null($influencer->age) || (!is_null($influencer->sex) && $influencer->sex != 9))
                            <li class="mainCont__UserStOtherLi">
                                <span class="mainCont__UserStOtherTtl">{{$influencer->age}}</span>
                                {{ (is_null($influencer->sex_text) || $influencer->sex_text == "公開しない") ? '' :
                                $influencer->sex_text }}
                            </li>
                            @endif
                        </ul>
                        <a href="{{route('profile.client', ['uuid'=> $influencer->uuid])}}"
                            class="mainCont__UserStLinkTxt">クライアント情報を表示</a>
                    </div>

                </div>
            </div>
            <div class="mainCont__Right">
                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        自己紹介
                    </h2>
                    <div class="mainCont__GrayBox">
                        <p class="mainCont__GrayBoxP">
                            {!! nl2br($influencer->self_introduction) !!}
                        </p>
                    </div>
                </div>
                <div class="mainCont__Box">

                    <h2 class="mainCont__Title1">
                        得意なジャンル
                    </h2>

                    @if (!$influencer->hasManyGenres->isEmpty())
                    <ul class="mainCont__Genre">
                        @foreach ($influencer->hasManyGenres as $genres)
                        <li class="mainCont__GenreLi">
                            {{$genres->name}}
                        </li>
                        @endforeach
                    </ul>
                    @else
                    <div class="mainCont__GrayBox">
                        <p class="mainCont__GrayBoxP">
                            登録されていません。
                        </p>
                    </div>
                    @endif
                </div>

                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        依頼可能なSNS
                    </h2>

                    @if (sizeof($influencer->sns_list) > 0)
                    @foreach($influencer->sns_list as $info)
                    <div class="mainCont__SnsBox">
                        @if($info['type'] == 'twitter')
                        <p class="mainCont__SnsIcon">
                            Twitter<br class="inline_p">
                            <i class="fab fa-twitter"></i>
                        </p>
                        @endif
                        @if($info['type'] == 'youtube')
                        <p class="mainCont__SnsIcon">
                            YouTube<br class="inline_p">
                            <i class="fab fa-youtube"></i>
                        </p>
                        @endif
                        @if($info['type'] == 'instagram')
                        <p class="mainCont__SnsIcon">
                            Instagram<br class="inline_p">
                            <i class="fab fa-instagram"></i>
                        </p>
                        @endif
                        @if($info['type'] == 'facebook')
                        <p class="mainCont__SnsIcon">
                            Facebook<br class="inline_p">
                            <i class="fab fa-facebook"></i>
                        </p>
                        @endif
                        @if($info['type'] == 'tiktok')
                        <p class="mainCont__SnsIcon">
                            TikTok<br class="inline_p">
                            <i class="fab fa-tiktok"></i>
                        </p>
                        @endif
                        <dl class="mainCont__SnsDetail">
                            @if ($info['type'] == 'youtube')
                            <dt class="mainCont__SnsTitle">チャンネル登録者数</dt>
                            @else
                            <dt class="mainCont__SnsTitle">フォロワー数</dt>
                            @endif
                            <dd class="mainCont__SnsTxt">{{ $info['follower_label'] }}</dd>
                            @if ($info['type'] == 'youtube')
                            <dt class="mainCont__SnsTitle">チャンネル再生回数</dt>
                            <dd class="mainCont__SnsTxt">{{ $info['views'] }}</dd>
                            <dt class="mainCont__SnsTitle">アップロード動画数</dt>
                            <dd class="mainCont__SnsTxt">{{ $info['videos'] }}</dd>
                            @endif
                            @if ($info['type'] == 'facebook')
                            <dt class="mainCont__SnsTitle">投稿数</dt>
                            <dd class="mainCont__SnsTxt">{{ $info['likes'] }}</dd>
                            @endif
                            @if ($info['type'] == 'twitter')
                            <dt class="mainCont__SnsTitle">投稿数</dt>
                            <dd class="mainCont__SnsTxt">{{ $info['posts'] }}</dd>
                            @endif
                            @if (in_array($info['type'], ['instagram','tiktok']))
                            <dt class="mainCont__SnsTitle">投稿数</dt>
                            <dd class="mainCont__SnsTxt">{{ $info['posts'] }}</dd>
                            @endif
                        </dl>
                    </div>
                    @endforeach
                    @else
                    <div class="mainCont__GrayBox">
                        <p class="mainCont__GrayBoxP">
                            登録されていません。
                        </p>
                    </div>
                    @endif
                </div>

                <div class="mainCont__Box">

                    <h2 class="mainCont__Title1">
                        直近の契約実績
                    </h2>

                    @if (!$influencer->project_influencer->isEmpty())
                    @foreach ($influencer->project_influencer as $project)
                    <a href="{{ route('project.details', ['project_id' => $project->project->id, 'limit' => 50]) }}">
                        <div class="matter">
                            <div class="matter__Box">
                                <div class="matter__Detail">
                                    <h3 class="matter__Title">{{$project->project->title}}</h3>
                                    <ul class="matter__TagList">
                                        @foreach ($project->project->projectCategory as $category)
                                        <li class="tag__Inner">
                                            # {{ $category->name }}
                                        </li>
                                        @endforeach
                                    </ul>
                                    <ul class="matter__SnsIcons">
                                        <li class="matter__SnsIconsLi">
                                            <a href="#" class="matter__SnsIcon">
                                                <i class="fab fa-tiktok"></i>
                                            </a>
                                        </li>
                                        <li class="matter__SnsIconsLi">
                                            <a href="#" class="matter__SnsIcon">
                                                <i class="fab fa-youtube"></i>
                                            </a>
                                        </li>
                                        <li class="matter__SnsIconsLi">
                                            <a href="#" class="matter__SnsIcon">
                                                <i class="fab fa-instagram"></i>
                                            </a>
                                        </li>
                                        <li class="matter__SnsIconsLi">
                                            <a href="#" class="matter__SnsIcon">
                                                <i class="fab fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="matter__SnsIconsLi">
                                            <a href="#" class="matter__SnsIcon">
                                                <i class="fab fa-twitter"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="matter__PriceBox">
                                    <p class="matter__PriceTitle">1人あたりの報酬</p>
                                    <p class="matter__Price">
                                        {{ number_format($project->project->reward_amount) }}<span
                                            class="matter__PriceYen">円</span>
                                    </p>
                                </div>
                                <p class="matter__Date">
                                    掲載日：{{$project->project->parse_created_at_in_japanese}}
                                </p>
                            </div>
                        </div>
                    </a>
                    @endforeach
                    @else
                    <div class="mainCont__GrayBox">
                        <p class="mainCont__GrayBoxP">
                            案件の契約実績はありません。
                        </p>
                    </div>

                    @endif
                </div>

                <div class="mainCont__Box">

                    <h2 class="mainCont__Title1">
                        直近の評価実績
                    </h2>

                    @if (!$influencer->project_influencer_comment->isEmpty())
                    @foreach ($influencer->project_influencer_comment as $influencer)
                    <div class="mainCont__Evaluation">
                        <div
                            class="{{$influencer->client_eva == 1 ? 'mainCont__EvaluationIconGood' : 'mainCont__EvaluationIconBad'}}">
                            {{$influencer->client_eva == 1 ? 'Good!' : 'Bad!'}}
                        </div>
                        <div class="mainCont__EvaluationTxt">
                            <p>
                                {{$influencer->client_comment}}
                            </p>
                            <span class="mainCont__EvaluationDate">{{$influencer->parse_reported_at_in_japanese}}</span>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div class="mainCont__GrayBox">
                        <p class="mainCont__GrayBoxP">
                            案件を評価された実績はありません。
                        </p>
                    </div>
                    @endif
                </div>
            </div>
        </div>

    </div>
</div>
@endsection