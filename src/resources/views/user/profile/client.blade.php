@extends('include.layout')

@section('header')
@include('include.auth_header')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner">
        {{ Breadcrumbs::render('profile', $client->name) }}
        <div class="mainCont__Wrap">
            <div class="mainCont__Left">
                <div class="mainCont__Left">
                    <div class="mainCont__UserBox">
                        <p class="mainCont__UserTitle">
                            {{$client->name}}
                        </p>

                        <div class="mainCont__UserPic mainCont__UserPic3">
                            <img src="{{ $client->avatar_image }}" class="mainCont__UserPicImg">
                        </div>
                    </div>

                    <div class="mainCont__UserSt">

                        <ul class="mainCont__UserStList">
                            <li class="mainCont__UserStLi">
                                評価<br class="inline_s">
                                <div class="mainCont__UserStBox">
                                    <span class="mainCont__UserStScore">{{$client->client_rate}}</span>
                                </div>
                                <span class="mainCont__UserStScore2">評価件数　{{$client->client_rate_count}}件</span>
                            </li>
                            <li class="mainCont__UserStLi">募集実績<br class="inline_s">
                                <div class="mainCont__UserStBox">
                                    <span class="mainCont__UserStScore">{{$client->client_project_post_count}}</span>件
                                </div>
                            </li>
                            <li class="mainCont__UserStLi">依頼実績<br class="inline_s">
                                <div class="mainCont__UserStBox">
                                    <span class="mainCont__UserStScore">{{$client->client_project_user_count}}</span>件
                                </div>
                            </li>
                        </ul>

                        <ul class="mainCont__UserStOtherList">
                            <li class="mainCont__UserStOtherLi">
                                <span class="mainCont__UserStOtherTtl">登録日</span>{{$client->registration_date}}
                            </li>
                            @if(!is_null($client->prefectureData))
                            <li class="mainCont__UserStOtherLi">
                                <span class="mainCont__UserStOtherTtl">都道府県</span>{{ $client->prefectureData->name ?? ''
                                }}
                            </li>
                            @endif
                            @if (!is_null($client->age) || (!is_null($client->sex) && $client->sex != 9))
                            <li class="mainCont__UserStOtherLi">
                                <span class="mainCont__UserStOtherTtl">{{$client->age}}</span> {{
                                (is_null($client->sex_text) || $client->sex_text == "公開しない") ? '' :
                                $client->sex_text }}
                            </li>
                            @endif
                        </ul>
                        <a href="{{route('profile.influencer', ['uuid'=> $client->uuid])}}"
                            class="mainCont__UserStLinkTxt">インフルエンサー情報を表示</a>
                    </div>

                </div>
            </div>
            <div class="mainCont__Right">

                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        自己紹介
                    </h2>
                    <div class="mainCont__GrayBox">
                        <p class="mainCont__GrayBoxP">
                            {!! nl2br($client->self_introduction) !!}
                        </p>
                    </div>
                </div>

                <div class="mainCont__Box">

                    <h2 class="mainCont__Title1">
                        直近の募集実績
                    </h2>

                    @if (!$client->projects->isEmpty())
                    @foreach ($client->projects as $project)

                    <a href="{{ route('project.details', ['project_id' => $project->id, 'limit' => 50]) }}"
                        class="matter">
                        <div class="matter__Box">
                            <div class="matter__Detail">

                                <h3 class="matter__Title">
                                    {{$project->title}}
                                </h3>
                                <ul class="tag">
                                    @foreach($project->projectCategory as $category)
                                    <li class="tag__Inner">
                                        # {{$category->name}}
                                    </li>
                                    @endforeach
                                </ul>
                                <ul class="matter__SnsIcons">
                                    @if(in_array(1,explode(',',$project->post_sns_code)))
                                    <li class="matter__SnsIconsLi">
                                        <span class="matter__SnsIcon">
                                            <i class="fab fa-tiktok"></i>
                                        </span>
                                    </li>
                                    @elseif (in_array(2,explode(',',$project->post_sns_code)))
                                    <li class="matter__SnsIconsLi">
                                        <span class="matter__SnsIcon">
                                            <i class="fab fa-youtube"></i>
                                        </span>
                                    </li>
                                    @elseif (in_array(3,explode(',',$project->post_sns_code)))
                                    <li class="matter__SnsIconsLi">
                                        <span class="matter__SnsIcon">
                                            <i class="fab fa-instagram"></i>
                                        </span>
                                    </li>
                                    @elseif (in_array(4,explode(',',$project->post_sns_code)))
                                    <li class="matter__SnsIconsLi">
                                        <span class="matter__SnsIcon">
                                            <i class="fab fa-facebook"></i>
                                        </span>
                                    </li>
                                    @else
                                    <li class="matter__SnsIconsLi">
                                        <span class="matter__SnsIcon">
                                            <i class="fab fa-twitter"></i>
                                        </span>
                                    </li>
                                    @endif
                                </ul>
                            </div>
                            <div class="matter__PriceBox">
                                <p class="matter__PriceTitle">1人あたりの報酬</p>
                                <p class="matter__Price">
                                    {{ number_format($project->reward_amount) }}<span class="matter__PriceYen">円</span>
                                </p>
                            </div>
                            <p class="matter__Date">
                                掲載日：{{$project->parse_created_at_in_japanese}}
                            </p>
                        </div>
                    </a>

                    @endforeach
                    @else
                    <div class="mainCont__GrayBox">
                        <p class="mainCont__GrayBoxP">
                            案件の募集実績はありません。
                        </p>
                    </div>
                    @endif
                </div>

                <div class="mainCont__Box">

                    <h2 class="mainCont__Title1">
                        直近の評価実績
                    </h2>
                    @if (!$client->project_client_comment->isEmpty())
                    @foreach ($client->project_client_comment as $client)
                    <div class="mainCont__Evaluation">
                        <div
                            class="{{$client->influencer_eva == 1 ? 'mainCont__EvaluationIconGood' : 'mainCont__EvaluationIconBad'}}">
                            {{$client->influencer_eva == 1 ? 'Good!' : 'Bad!'}}
                        </div>
                        <div class="mainCont__EvaluationTxt">
                            <p>
                                {!! nl2br($client->influencer_comment) !!}
                            </p>
                            <span class="mainCont__EvaluationDate">{{$client->parse_reported_at_in_japanese}}</span>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div class="mainCont__GrayBox">
                        <p class="mainCont__GrayBoxP">
                            案件を評価された実績はありません。
                        </p>
                    </div>
                    @endif

                </div>
            </div>

        </div>
    </div>
    @endsection