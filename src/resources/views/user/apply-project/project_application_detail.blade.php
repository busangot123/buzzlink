@extends('include.layout')

@section('header')
@include('include.auth_header')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner">
        {{ Breadcrumbs::render('management_details', route('mypage.work_management')) }}
        <div class="mainCont__Wrap">
            <div class="mainCont__Right mainCont__Right3">
                @include('include.project_details', [
                'project' => $project,
                'project_user_status' => $project_users->status_label
                ])
                @if ($project_users->application_status != 6)
                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        {{ $project_users->user->name }}さんの応募内容
                    </h2>
                    @include('include.project_application_user_details', ['project_user' => $project_users])
                    <project-application-detail :work_details='@json($work_details)'></project-application-detail>
                </div>
                @endif
                <div class="mainCont__Box" style="margin-bottom: 0px">
                    @if ($project_users->application_status != 6)
                    <h2 class="mainCont__Title1">
                        応募したインフルエンサー
                    </h2>
                    @else
                    <h2 class="mainCont__Title1">
                        選定したインフルエンサー
                    </h2>
                    <h3 class="mainCont__Title4">
                        選定日時 {{ $project_users->project->selection_deadline_at }}
                    </h3>
                    @endif
                    @include('include.project_application_other_user_details', ['project' => $project])
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
