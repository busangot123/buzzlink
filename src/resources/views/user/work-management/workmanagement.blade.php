@extends('include.layout')

@section('header')
@include('include.auth_header')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner">
        <div class="mainCont__Wrap mainCont__Wrap2">
            @include('include.user.profile')
            <div class="mainCont__Right">
                <work-management-page :projects='@json($projects)' :count='@json($count)'></work-management-page>
            </div>
        </div>
    </div>
</div>
@endsection