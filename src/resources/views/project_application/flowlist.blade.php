<div class="mainCont__Box">
    <div class="flowlist flowlist2">
        <p class="flowlist__Flow flowlist__Flow2 {{ $flow_active == 1 ?  'flowlist__FlowActive' : '' }}">
            <span class="flowlist__FlowNumber {{ $flow_active == 1 ?  'flowlist__FlowNumberActive' : '' }}">FLOW1</span>
            応募内容<br>入力
        </p>
        <p class="flowlist__Flow flowlist__Flow2 {{ $flow_active == 2 ?  'flowlist__FlowActive' : '' }}">
            <span class="flowlist__FlowNumber {{ $flow_active == 2 ?  'flowlist__FlowNumberActive' : '' }}">FLOW2</span>
            応募内容<br>確認
        </p>
        <p class="flowlist__Flow flowlist__Flow2 {{ $flow_active == 3 ?  'flowlist__FlowActive' : '' }}">
            <span class="flowlist__FlowNumber {{ $flow_active == 3 ?  'flowlist__FlowNumberActive' : '' }}">FLOW3</span>
            応募登録<br>完了
        </p>
    </div>
</div>
