@extends('include.layout')

@section('content')
@include('include.headerNone')
    <div class="mainCont mainCont--projectComplete">
        <div class="mainCont__Inner">
          @include('project_application.flowlist', ['flow_active' => 3])
          <div class="mainCont__Wrap">
            <div class="register__Cont">
              <h2 class="register__Title register__Title2">応募ありがとうございます。</h2>
              <h2 class="register__Title">案件の募集期間終了後、クライアント様がインフルエンサーを選定します。<br>選定結果の通知をお待ちください。</h2>

              <div class="mainCont__Btns">
                <ul class="mainCont__BtnsList">
                  <li class="mainCont__BtnsLi">
                    <a href="{{ route('mypage.work_management') }}" class="mainCont__Btn2 mainCont__BtnCon">
                      仕事管理を表示する
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
  @endsection
