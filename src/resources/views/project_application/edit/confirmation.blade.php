@extends('include.layout')

@section('content')

@include('include.headerNone')
<div class="mainCont">
    <div class="mainCont__Inner">
        {{ Breadcrumbs::render('project_application_edit', $project->title, route('workmanagement.details',
        ['project_id' => $project->id])) }}
        @include('project_application.flowlist', ['flow_active' => 2])
        <div class="mainCont__Box">
            <div class="mainCont__GrayBox">
                <p class="mainCont__GrayBoxP">
                    登録はまだ完了していません。<br>
                    下記の内容で問題がなければ、ページ下の「この内容で登録する」をクリックしてください。<br>
                </p>
            </div>
        </div>
        <div class="mainCont__Box">
            <div class="mainCont__TitleWrap">
                <div class="mainCont__TitleWrapBox">
                    <p class="mainCont__StIcon">{{ $project->status }}</p>
                    <h1 class="mainCont__Title2">
                        {{ $project->title }}
                    </h1>
                    <ul class="tag">
                        @foreach( $project->projectCategory as $tag)
                        <li class="tag__Inner">
                            # {{ $tag->name }}
                        </li>
                        @endforeach
                    </ul>
                    <p class="mainCont__Date">掲載日：{{ $project->published }}</p>
                    @if($project->is_applied_user && $project->status_code < 9) <p class="mainCont__ContirmBox2">
                        <a href="{{ route('chat.room.project', ['project_id' => $project->id]) }}" target="_blank"
                            class="mainCont__Btn3">
                            メッセージ画面を表示する
                        </a>
                        </p>
                        @endif
                </div>
                <div class="mainCont__UserWrap">
                    <div class="mainCont__UserPic mainCont__UserPic3">
                        <img src="{{ $project->user->avatar_image }}" class="mainCont__UserPicImg">
                    </div>
                    <div class="mainCont__UserName">
                        <h2>
                            <a href="#">{{ $project->user->name }}</a>
                        </h2>
                        <p class="mainCont__UserSt2">
                            評価<span class="mainCont__UserStScore3">{{ $project->user->client_rate
                                }}</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="mainCont__Wrap">
            <div class="mainCont__Right mainCont__Right3">

                <form action="{{ route('project.edit.update', ['project_id' => $project->id]) }}" method="POST"
                    class="register register2">
                    @csrf
                    <input type="hidden" value="{{ $self_promotion }}" name="self_promotion">
                    <input type="hidden" value="{{ $project->id }}" name="project_id">
                    <div class="mainCont__Box">
                        <h2 class="mainCont__Title1">
                            応募する
                        </h2>
                        <dl class="register__FormList">
                            <dt class="register__FormListLeft">
                                自己PR・実績<span class="register__Required">必須</span>
                            </dt>
                            <dd class="register__FormListRight">
                                <div class="register__GrayBox">
                                    <p class="register__GrayBoxP">
                                        {!! nl2br(e($self_promotion)) !!}
                                    </p>
                                </div>
                                <span
                                    class="register__FormListStxt register__FormListStxtRed">※個人情報・機密情報等を含めないようにご注意ください。</span>
                            </dd>
                        </dl>
                    </div>
                    <div class="register__BtnWrap">
                        <a href="{{ route('project.edit.application', ['project_id' => $project->id]) }}"
                            class="register__BtnConfirm2" type="button">入力画面に戻る</a>
                        <button class="register__BtnRegister2" type="submit">この内容で登録する</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection