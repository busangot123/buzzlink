@extends('include.layout')

@section('content')
@include('include.headerNone')
    <div class="mainCont">
        <div class="mainCont__Inner">
            {{ Breadcrumbs::render('project_application', $project->title) }}
            <div class="m-top--30">
                @include('project_application.flowlist', ['flow_active' => 1])
            </div>
            <div class="mainCont__Wrap">
                <div class="mainCont__Right mainCont__Right3">
                    @include('include.project_details', [
                        'project' => $project,
                        'project_user_status' => null
                    ])
                    <form action="{{ route('project.confirmation', ['project_id' => $project->id]) }}" method="POST">
                        @csrf
                        <div class="mainCont__Box">
                            <h2 class="mainCont__Title1">
                                応募する
                            </h2>
                            <p class="mainCont__Box">自己PR・実績は具体的に書くほうが選ばれやすくなります。</p>
                            <dl class="register__FormList">
                                <dt class="register__FormListLeft">
                                    自己PR・実績<span class="register__Required">必須</span>
                                    <p class="register__FormListStxt">2500文字まで</p>
                                </dt>
                                <dd class="register__FormListRight">
                                    <input hidden name="project_id" value="{{ $project->id }}">
                                    <textarea id="text-editor" name="self_promotion" class="register__TextArea" maxlength="2500">{!! old('self_promotion') ?: $self_promotion !!}</textarea>
                                    <span class="register__FormListStxt" style="float:right;"><span id="character_count">0</span>/2500</span>
                                    <span class="register__FormListStxt">※個人情報・機密情報等を含めないようにご注意ください。</span>
                                @if($errors->any())
                                    @foreach ($errors->all() as $error)
                                        <span class="error__message">{{ $error }}</span>
                                    @endforeach
                                @endif
                                </dd>
                            </dl>
                            <button class="register__BtnConfirm" type="submit">内容を確認</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
