@extends('include.layout')
@section('header')
@include('include.auth_header')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner">
        {{ Breadcrumbs::render('project_details_search') }}
        <div class="mainCont__Result">
            <h2 class="mainCont__ResultTitleBox">
                <span class="mainCont__ResultTitle">{{ Request::get('keyword') ?: 'すべて' }}</span>の検索結果
            </h2>
            <p class="mainCont__ResultTxt">
                @if($projects->total() > 0)
                {{ $projects->total() }}件のうち、{{ $projects->firstItem() }}〜{{ $projects->lastItem() }}件を表示
                @endif
            </p>
        </div>

        <div class="mainCont__Wrap mainCont__Wrap2">
            <div class="mainCont__Left mainCont__Left2">
                <div class="mainCont__CategoryBox">
                    <h3 class="mainCont__CategoryTitle">
                        カテゴリ
                    </h3>

                    <div class="mainCont__FormSelectBox block_s">
                        <select name="" class="mainCont__FormSelect">
                            @foreach($categories as $category)
                            <option value="">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <ul class="mainCont__CategoryList block_p">
                        <li class="mainCont__CategoryLi">
                            <a href="{{ route('search', ['title' => Request::get('title'), 'category' => '', 'sns' => is_array(Request::get('sns')) ? join(',',Request::get('sns')) : Request::get('sns'), 'min_reward' => Request::get('min_reward'), 'max_reward' => Request::get('max_reward'), 'sort' => Request::get('sort')]) }}"
                                class="mainCont__Category"><i class="far fa-smile"></i> 全てのカテゴリ</a>
                        </li>
                        @foreach($categories as $category)
                        <li class="mainCont__CategoryLi">
                            <a href="{{ route('search', ['title' => Request::get('title'), 'category' => $category->name, 'sns' => is_array(Request::get('sns')) ? join(',',Request::get('sns')) : Request::get('sns'), 'min_reward' => Request::get('min_reward'), 'max_reward' => Request::get('max_reward'), 'sort' => Request::get('sort')]) }}"
                                class="mainCont__Category">{!! $category->icon !!} {{ $category->name }}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>

                <form method="GET" action="{{ route('search') }}" class="mainCont__NarrowBox">
                    <h3 class="mainCont__NarrowTitle">
                        絞り込み
                    </h3>
                    <div class="mainCont__Formbox">
                        <p class="mainCont__NarrowSubTitle">広告媒体</p>
                        <input type="checkbox" id="youtube" class="mainCont__Narrowinput" name="sns[]" value="1">
                        <label for="youtube" class="mainCont__Narrowinput">YouTube</label>
                        <input type="checkbox" id="instagram" class="mainCont__Narrowinput" name="sns[]" value="2">
                        <label for="instagram" class="mainCont__Narrowinput">Instagram</label>
                        <input type="checkbox" id="facebook" class="mainCont__Narrowinput" name="sns[]" value="3">
                        <label for="facebook" class="mainCont__Narrowinput">Facebook</label>
                        <input type="checkbox" id="twitter" class="mainCont__Narrowinput" name="sns[]" value="4">
                        <label for="twitter" class="mainCont__Narrowinput">Twitter</label>
                        <input type="checkbox" id="tiktok" class="mainCont__Narrowinput" name="sns[]" value="5">
                        <label for="tiktok" class="mainCont__Narrowinput">TikTok</label>
                    </div>
                    <div class="mainCont__Formbox">
                        <p class="mainCont__NarrowSubTitle">報酬額</p>
                        <div class="rewardBox">
                            <input type="text" name="reward_min" id="reward_min" class="rewardPrice"
                                value="{{ Request::get('reward_min') }}">
                            <span class="reward__currency">円以上〜</span>
                        </div>
                        <p align="center">｜</p>
                        <div class="rewardBox">
                            <input type="text" name="reward_max" id="reward_max" class="rewardPrice"
                                value="{{ Request::get('reward_max')}}">
                            <span class="reward__currency">円以下</span>
                        </div>
                    </div>
                    <input type="hidden" name="category" value="{{Request::get('category')}}">
                    <input type="hidden" name="keyword" value="{{Request::get('keyword')}}">
                    <input type="hidden" name="sort" value="{{Request::get('sort')}}">
                    <p>
                        <input type="submit" class="mainCont__NarrowBtn" value="絞り込む">
                    </p>
                </form>
            </div>

            <div class="mainCont__Right mainCont__Right2">
                <div class="mainCont__Box">
                    <div class="mainCont__BoxWrap">
                        <form method="GET" action="{{ route('search') }}" class="searchWrap">
                            <div class="searchWrap__SearchKeywordWrap">
                                <input type="hidden" name="category" value="{{Request::get('category')}}">
                                <input type="hidden" name="sns"
                                    value="{{ is_array(Request::get('sns')) ? join(',',Request::get('sns')) : Request::get('sns') }}">
                                <input type="hidden" name="min_reward" value="{{Request::get('reward_min')}}">
                                <input type="hidden" name="max_reward" value="{{Request::get('reward_max')}}">
                                <input type="hidden" name="sort" value="{{Request::get('sort')}}">
                                <input placeholder="検索キーワード" name="keyword" class="searchWrap__SearchKeyword"
                                    value="{{ Request::get('keyword')}}">
                                <button type="submit" class="searchWrap__SearchKeywordBtn" />
                            </div>
                        </form>
                        <div class="mainCont__NextPrevBtns mainCont__NextPrevBtns3">
                            <a href="{{$projects->appends(request()->query())->previousPageUrl()}}"
                                class="mainCont__NextPrevBtn {{ $projects->appends(request()->query())->previousPageUrl() ?: 'disabled' }}">
                                <i class="fas fa-chevron-left"></i>前へ
                            </a>
                            <a href="{{$projects->appends(request()->query())->nextPageUrl()}}"
                                class="mainCont__NextPrevBtn {{ $projects->appends(request()->query())->nextPageUrl() ?: 'disabled' }}">
                                次へ<i class="fas fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="mainCont__BoxWrap">
                        <form class="searchWrap" method="GET" action="{{ route('search') }}">
                            <input type="hidden" name="category" value="{{Request::get('category')}}">
                            <input type="hidden" name="sns"
                                value="{{ is_array(Request::get('sns')) ? join(',',Request::get('sns')) : Request::get('sns') }}">
                            <input type="hidden" name="min_reward" value="{{Request::get('reward_min')}}">
                            <input type="hidden" name="max_reward" value="{{Request::get('reward_max')}}">
                            <input type="hidden" name="keyword" value="{{Request::get('keyword')}}">
                            <div class="searchWrap__SearchSelectWrap">
                                <select name="sort" id="sorting" class="searchWrap__SearchSelect">
                                    <option value="">新着順</option>
                                    <option value="lowest_time">残り時間が少ない順</option>
                                    <option value="highest_price">報酬が高い順</option>
                                    <option value="big_to_small">応募が多い順</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="mainCont__Box">
                    @if($projects->total() == 0)
                    <div class="mainCont__GrayBox matter">
                        <p class="mainCont__GrayBoxP center__text">
                            条件に合う案件が見つかりませんでした。
                        </p>
                    </div>
                    @endif
                    @foreach($projects as $project)
                    <div class="matter">
                        <div class="matter__Box">
                            <div class="matter__Detail">
                                <h3 class="matter__Title"><a
                                        href="{{ route('project.details', ['project_id' => $project->id, 'limit' => 50]) }}"
                                        class={{ $project->application_hours_left['duration'] > 0 ? 'matter__TitleA' :
                                        'matter__TitleE' }}>{{ $project->title }}</a></h3>
                                <ul class="tag">
                                    @foreach($project->projectCategory as $tag)
                                    <li class="tag__Inner">
                                        <a href="#" class="matter__hash"># {{ $tag->name }}</a>
                                    </li>
                                    @endforeach
                                </ul>
                                <ul class="matter__SnsIcons">
                                    @foreach($project->sns_code_list as $code)
                                    <li class="matter__SnsIconsLi">
                                        <span class="matter__SnsIcon">
                                            <i class="fab fa-{{ $code }}"></i>
                                        </span>
                                    </li>
                                    @endforeach
                                </ul>
                                <p class="ellipsis">
                                    {!! nl2br($project->content) !!}
                                </p>
                                <div class="matter__UserBox">
                                    <div class="matter__UserPic">
                                        <a target="_blank"
                                            href="{{ route('profile.client', ['uuid' => $project->user->uuid])}}">
                                            <div class="matter__UserPicImg"
                                                style="background-image: url({{ $project->user->avatar_image }})"></div>
                                        </a>
                                    </div>
                                    <a target="_blank"
                                        href="{{ route('profile.client', ['uuid' => $project->user->uuid])}}"
                                        class="matter__UserName">
                                        {{ $project->user->name }}
                                    </a>
                                </div>
                            </div>
                            <div class="matter__PriceBox">
                                <p class="matter__PriceTitle">1人あたりの報酬</p>
                                <p class="matter__Price">
                                    {{ number_format($project->reward_amount) }}<span class="matter__PriceYen">円</span>
                                </p>
                                <p>
                                    募集人数{{ $project->request_people_number }}人
                                </p>
                            </div>
                            @if($project->application_hours_left['duration'] > 0)
                            <div class="matter__OtherBox">
                                <p class="matter__OtherTitle">募集終了まで</p>
                                <p class="matter__OtherTxt">あと<span class="matter__OtherDate">{{
                                        $project->application_hours_left['duration'] }}</span>{{
                                    $project->application_hours_left['sign'] }}</p>
                                <span class="matter__OtherSmall">({{ $project->application_deadline_at }}まで)</span>
                            </div>
                            @else
                            <div class="matter__OtherBox">
                                <p class="matter__OtherTxt matter__OtherTxt2">募集終了</p>
                            </div>
                            @endif
                            <p class="matter__Date">
                                掲載日：{{ $project->published }}
                            </p>
                        </div>
                    </div>
                    @endforeach

                    <div class="mainCont__Box">
                        <div class="mainCont__PageFeedBox">
                            @for ($i = 1; $i <= $projects->lastPage(); $i++)
                                <a href="{{$projects->appends(request()->query())->url($i)}}"
                                    class="mainCont__PageFeedBtn {{ $projects->currentPage() == $i ? 'mainCont__PageFeedBtnAc' : '' }}">{{
                                    $i }}</a>
                                @endfor
                        </div>

                        <div class="mainCont__NextPrevBtns mainCont__NextPrevBtns2">
                            <a href="{{$projects->appends(request()->query())->previousPageUrl()}}"
                                class="mainCont__NextPrevBtn {{ $projects->appends(request()->query())->previousPageUrl() ?: 'disabled' }}">
                                <i class="fas fa-chevron-left"></i>前へ
                            </a>
                            <a href="{{$projects->appends(request()->query())->nextPageUrl()}}"
                                class="mainCont__NextPrevBtn {{ $projects->appends(request()->query())->nextPageUrl() ?: 'disabled' }}">
                                次へ<i class="fas fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="application/javascript">
    var elements = document.getElementsByClassName('ellipsis')
    for (let i = 0; i < elements.length; i++) {
        $clamp(elements[i], {
            clamp: 2,
            useNativeClamp: false,
            truncationChar: ' ...'
        });
    }

    $(function () {
        let queryString = window.location.search;
        let urlParams = new URLSearchParams(queryString)

        if (urlParams.getAll('sns[]').length > 0 || urlParams.get('sns')) {
            let sns_list = urlParams.getAll('sns[]').length > 0 ? urlParams.getAll('sns[]') : urlParams.get('sns').split(',')
            for (let i = 0; i < sns_list.length; i++) {
                $(`input[value=${sns_list[i]}]`).prop('checked', true)
            }
        }

        $('#sorting').val(urlParams.get('sort'))
        $('#sorting').on('change', function () {
            this.form.submit();
        })
    })
</script>
@endsection