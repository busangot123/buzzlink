@extends('include.layout')

@section('header')
@include('include.auth_header')
@endsection

@section('content')
<div class="mainCont">
    <div class="mainCont__Inner">
        @if(Request::input('management'))
        {{ Breadcrumbs::render('management_details', route('mypage.project_management')) }}
        @else
        {{ Breadcrumbs::render('project_details', $project->title) }}
        @endif
        <div class="mainCont__Wrap">
            <div class="mainCont__Right mainCont__Right3">
                @include('include.project_details', [
                'project' => $project,
                'project_user_status' => null
                ])

                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        応募したインフルエンサー
                    </h2>

                    @include('include.project_application_other_user_details', ['project' => $project])

                    @if(Auth::check())
                    @if(!Request::input('management'))
                    @if((!$project->is_applied_user || $project->is_already_applied_user) && $project->user->id !=
                    Auth::user()->id)
                    @if($project->status_code == 1)
                    <div class="mainCont__BtnsList3">
                        <a href="{{ route('project.application', ['project_id' => $project->id]) }}"
                            class="mainCont__Btn2 mainCont__BtnCon">
                            この案件に応募する（無料)
                        </a>
                    </div>
                    @endif
                    @endif
                    @endif
                    @else
                    <div class="mainCont__BtnsList3">
                        <a href="{{ route('email.verification_page') }}" class="mainCont__Btn2 mainCont__BtnCom">
                            まずは会員登録（無料）
                        </a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endsection
