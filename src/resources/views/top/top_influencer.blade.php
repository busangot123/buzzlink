@extends('include.top_layout')

@section('content')

<div class="topMv">
    <div class="topMv_Left">
        <div class="topMv_Box">
            <h2 class="topMv_Ttl">
                <span>
                    「広告主」×「インフルエンサー」
                </span>
                "自由に出会える"<br>
                新時代クラウド<br class="inline_s">マッチング
            </h2>
            <p>
                BuzzLinkを使って、簡単に案件に応募！<br class="inline_p">
                案件の依頼を受けてSNSでPRすることで報酬を獲得！
            </p>
            <ul class="topMv_SnsIcons">
                <li class="topMv_SnsIconsLi">
                    <span class="topMv_SnsIcon">
                        <i class="fab fa-tiktok"></i>
                    </span>
                </li>
                <li class="topMv_SnsIconsLi">
                    <span class="topMv_SnsIcon">
                        <i class="fab fa-youtube"></i>
                    </span>
                </li>
                <li class="topMv_SnsIconsLi">
                    <span class="topMv_SnsIcon">
                        <i class="fab fa-instagram"></i>
                    </span>
                </li>
                <li class="topMv_SnsIconsLi">
                    <span class="topMv_SnsIcon">
                        <i class="fab fa-facebook"></i>
                    </span>
                </li>
                <li class="topMv_SnsIconsLi">
                    <span class="topMv_SnsIcon">
                        <i class="fab fa-twitter"></i>
                    </span>
                </li>
            </ul>
            <a href="{{route('email.verification_page')}}" class="topMv_Btn topMv_Btn2">
                まずは会員登録（無料）
            </a>
        </div>
    </div>
    <div class="topMv_Right">
        <p class="block_s">
            <img src="{{ asset('assets/top_mv.jpg') }}" alt="">
        </p>
    </div>
</div>

<section class="top_Q top_Q1 top_Clientbox1 top_Infulbox1">
    <div class="top_Inner">
        <p class="block_s img">
            <img src="{{ asset('assets/bg_inful.png') }}" alt="">
        </p>
        <div class="box2 fadein">
            <p class="subttl">PR投稿やフォロワー数を増やして</p>
            <h3 class="question_Ttl">
                <span class="question_Ttl_Line">収益を増やしたい</span>
            </h3>
            <p>
                だけど....
                <span>
                    どうしたらいいかわからない
                </span>
            </p>
            <ul>
                <li>・どうやって案件を獲得するか</li>
                <li>・広告主との接点がない</li>
            </ul>
        </div>
    </div>
</section>

<section class="top_Q top_Q2 top_Clientbox4 top_Infulbox2">
    <div class="top_Inner">
        <div class="box box2">
            <div class="box_Left">
                <h3 class="question_Ttl">
                    <span class="question_Ttl_Line">BuzzLinkを使って</span><br>
                    <span class="question_Ttl_Line">簡単に案件に応募！</span>
                </h3>
                <p>クライアントからの多くの案件に応募可能！
                    <br class="inline_p">フォロワー数の制限なし。<br>ナノインフルエンサーでもOK！！
                </p>
                <p>依頼を受けてSNSでPRすることで報酬を獲得！</p>
            </div>
            <div class="box_Right fadein">
                <p>
                    <img src="{{ asset('assets/img_inful01.png') }}" alt="">
                </p>
            </div>
        </div>
    </div>
</section>

<section class="top3step">
    <div class="top_Inner">
        <h3 class="question_Ttl">
            わずか3STEPで<br class="inline_s">報酬獲得が可能
        </h3>
        <div class="top3step_Box">
            <div class="box fadein">
                <img src="{{ asset('assets/img_step01_inful.png') }}" alt="">
                <p>
                    案件への応募は無料!<br>
                    やってみたい案件に応募して<br>
                    広告主からの案件依頼を待ちます。
                </p>
            </div>
            <div class="box fadein">
                <img src="{{ asset('assets/img_step2_inful.png') }}" alt="">
                <p>
                    広告主からの依頼の後<br>
                    SNSでPRを行います。<br>
                    広告主に完了報告します。
                </p>
            </div>
            <div class="box fadein">
                <img src="{{ asset('assets/img_step03_inful.png') }}" alt="">
                <p>
                    広告主が結果を確認することで<br>
                    報酬を獲得します。
                </p>
            </div>
        </div>
        <a href="{{route('email.verification_page')}}" class="btn">
            まずは会員登録（無料）
        </a>
    </div>
</section>

<section class="topMatter topMatter--ipad">
    <div class="top_Inner">
        <h3 class="question_Ttl">
            おすすめ案件
        </h3>
    </div>
    <div class="topMatter_Box slide ">
        @foreach($projects as $project)

        <div class="topMatter_Box_Inner">
            <div href="{{ route('project.details', ['project_id' => $project->id]) }}" class="matter">
                <p class="matter__Category">
                    {!! $project->projectCategory[0]->icon !!} {{ $project->projectCategory[0]->name }}
                </p>
                <div class="matter__Box">
                    <div class="matter__Detail">
                        <a href="{{ route('project.details', ['project_id' => $project->id]) }}" class="matter__TitleA">
                            <h3 class="matter__Title">{{ Str::limit($project->title, 25) }}</h3>
                        </a>
                        <ul class="matter__SnsIcons">
                            @foreach($project->sns_code_list as $code)
                            <li class="matter__SnsIconsLi">
                                <span class="matter__SnsIcon">
                                    <i class="fab fa-{{ $code }}"></i>
                                </span>
                            </li>
                            @endforeach
                        </ul>
                        <p class="matter__DetailTxt">{!! $project->content !!}</p>
                        <div class="matter__UserBox">
                            <div class="matter__UserPic">
                                <div class="matter__UserPicImg"
                                    style="background-image:url({{ $project->user->avatar_image}})"></div>
                            </div>
                            <a href="{{ route('profile.client', ['uuid' => $project->user->uuid])}}"
                                class="matter__UserName">
                                {{ $project->user->name }}
                            </a>
                        </div>
                    </div>
                    <div class="matter__PriceBox">
                        <p class="matter__PriceTitle">1人あたりの報酬</p>
                        <p class="matter__Price">
                            {{ number_format($project->reward_amount) }}<span class="matter__PriceYen">円</span>
                        </p>
                        <p>
                            募集人数{{ $project->request_people_number }}人
                        </p>
                    </div>
                    @if($project->application_hours_left['duration'] > 0)
                    <div class="matter__OtherBox">
                        <p class="matter__OtherTitle">募集終了まで</p>
                        <p class="matter__OtherTxt">あと<span class="matter__OtherDate">{{
                                $project->application_hours_left['duration'] }}</span>{{
                            $project->application_hours_left['sign'] }}</p>
                        <span class="matter__OtherSmall">({{ $project->application_deadline_at }}まで)</span>
                    </div>
                    @else
                    <div class="matter__OtherBox">
                        <p class="matter__OtherTxt matter__OtherTxt2">募集終了</p>
                    </div>
                    @endif
                    <p class="matter__Date">
                        掲載日：{{ $project->published }}
                    </p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <a href="{{ route('guide') }}" target="_blank" class="btn">
        特定商取引法に基づく表記
    </a>

</section>

<script type="application/javascript">
    var elements = document.getElementsByClassName('ellipsis')
    for (let i = 0; i < elements.length; i++) {
        $clamp(elements[i], {
            clamp: 2,
            useNativeClamp: false,
            truncationChar: ' ...'
        });
    }

    $(function () {
        let queryString = window.location.search;
        let urlParams = new URLSearchParams(queryString)

        if (urlParams.getAll('sns[]').length > 0 || urlParams.get('sns')) {
            let sns_list = urlParams.getAll('sns[]').length > 0 ? urlParams.getAll('sns[]') : urlParams.get('sns').split(',')
            for (let i = 0; i < sns_list.length; i++) {
                $(`input[value=${sns_list[i]}]`).prop('checked', true)
            }
        }

        $('#sorting').val(urlParams.get('sort'))
        $('#sorting').on('change', function () {
            this.form.submit();
        })
    })
</script>
@endsection