@extends('include.top_layout')

@section('content')


<div class="topMv">
    <div class="topMv_Left">
        <div class="topMv_Box">
            <h2 class="topMv_Ttl">
                <span>
                    「広告主」×「インフルエンサー」
                </span>
                "自由に出会える"<br>
                新時代クラウド<br class="inline_s">マッチング
            </h2>
            <p>
                BuzzLinkを使って、簡単にインフルエンサーへ依頼！<br class="inline_p">
                インフルエンサーマーケティングで効率的に納品・サービスをPR！
            </p>
            <ul class="topMv_SnsIcons">
                <li class="topMv_SnsIconsLi">
                    <span class="topMv_SnsIcon">
                        <i class="fab fa-tiktok"></i>
                    </span>
                </li>
                <li class="topMv_SnsIconsLi">
                    <span class="topMv_SnsIcon">
                        <i class="fab fa-youtube"></i>
                    </span>
                </li>
                <li class="topMv_SnsIconsLi">
                    <span class="topMv_SnsIcon">
                        <i class="fab fa-instagram"></i>
                    </span>
                </li>
                <li class="topMv_SnsIconsLi">
                    <span class="topMv_SnsIcon">
                        <i class="fab fa-facebook"></i>
                    </span>
                </li>
                <li class="topMv_SnsIconsLi">
                    <span class="topMv_SnsIcon">
                        <i class="fab fa-twitter"></i>
                    </span>
                </li>
            </ul>
            <a href="{{route('email.verification_page')}}" class="topMv_Btn topMv_Btn2">
                まずは会員登録（無料）
            </a>
        </div>
    </div>
    <div class="topMv_Right topMv_Right2">
        <p class="block_s">
            <img src="{{ asset('assets/bg_client.jpg') }}" alt="">
        </p>
    </div>
</div>

<section class="top_Q top_Q1 top_Clientbox1">
    <div class="top_Inner">
        <div class="box">
            <div class="box_Left">
                <h3 class="question_Ttl">
                    SNS広告がきっかけで<br>
                    商品ページやブランドサイト<br>
                    を見た経験は？
                </h3>
            </div>
            <div class="box_Right fadein">
                <p>
                    <img src="{{ asset('assets/img_client01.png') }}" alt="">
                </p>
                <span class="smalltxt">※ 自社調べ</span>
            </div>
        </div>
    </div>
</section>

<section class="top_Q top_Q2 top_Clientbox2">
    <div class="top_Inner">
        <div class="box">
            <div class="box_Left fadein">
                <p>
                    <img src="{{ asset('assets/img_client002.png') }}" alt="">
                </p>
            </div>
            <div class="box_Right">
                <h3 class="question_Ttl">
                    SNSの投稿を見て、<br>
                    インターネットで商品を<br>
                    購入したことがありますか？
                </h3>
                <p class="img">
                    <img src="{{ asset('assets/img_client5.png') }}" alt="">
                </p>
                <span class="smalltxt">※ 自社調べ</span>
            </div>
        </div>
    </div>
</section>

<section class="top_Q top_Q1 top_Clientbox3">
    <div class="top_Inner">
        <div class="box">

            <div class="box_Left">
                <p>
                    <img src="{{ asset('assets/img_client3.png') }}" alt="">
                </p>
            </div>
            <div class="box_Right fadein">
                <h3 class="question_Ttl">
                    集客を増やし<br>売り上げを伸ばすためには<br>
                    <span class="question_Ttl_Line">インフルエンサーを活用して<br class="inline_s">SNSで広めることが有効</span>
                </h3>
            </div>
        </div>
    </div>
</section>


<section class="top_Q top_Q2 top_Clientbox4">
    <div class="top_Inner">
        <div class="box">
            <div class="box_Left">
                <h3 class="question_Ttl">
                    <span class="question_Ttl_Line">BuzzLinkを使って<br>簡単に・低コストで</span><br class="inline_p">
                    <span class="question_Ttl_Line">インフルエンサーへ依頼！</span>
                </h3>
                <p>
                    インフルエンサーマーケティングで<br class="inline_s">効率的に<br class="inline_p">
                    商品・サービスをPR!!
                </p>
            </div>
            <div class="box_Right fadein">
                <p>
                    <img src="{{ asset('assets/img_client4.png') }}" alt="">
                </p>
            </div>
        </div>
    </div>
</section>

<section class="top_Q top_Q1 top_Clientbox2">
    <div class="top_Inner">
        <div class="box">
            <div class="box_Left box_Left2 fadein">
                <p>
                    <img src="{{ asset('assets/img_client6.png') }}" alt="">
                </p>
            </div>
            <div class="box_Right box_Right2">
                <h3 class="question_Ttl">
                    インフルエンサーへの報酬は<br>
                    自分で設定するため低コストを実現可能！
                </h3>
                <p>
                    かかるコストは「報酬」と「サービス利用料」
                    で分かりやすい。</p>
            </div>
        </div>
    </div>
</section>

<section class="top3step">
    <div class="top_Inner">
        <h3 class="question_Ttl">
            わずか3STEPで<br class="inline_s">商品・サービスのPRが可能
        </h3>
        <div class="top3step_Box">
            <div class="box fadein">
                <img src="{{ asset('assets/img_step1.png') }}" alt="">
                <p>
                    案件の募集は無料!<br>
                    案件の内容、条件を設定して<br>
                    インフルエンサーを募集します。
                </p>
            </div>
            <div class="box fadein">
                <img src="{{ asset('assets/img_step2.png') }}" alt="">
                <p>
                    応募のあった中から希望とマッチする<br>
                    インフルエンサーを選定します。
                </p>
            </div>
            <div class="box fadein">
                <img src="{{ asset('assets/img_step3.png') }}" alt="">
                <p>
                    インフルエンサーがSNSでPRを実施します。<br>
                    実施した内容を確認します。
                </p>
            </div>
        </div>
        <a href="{{route('email.verification_page')}}" class="btn">
            まずは会員登録（無料）
        </a>
    </div>
</section>

<section class="topMatter topMatter--ipad">
    <div class="top_Inner">
        <h3 class="question_Ttl">
            おすすめ案件
        </h3>
    </div>
    <div class="topMatter_Box slide">
        @foreach($projects as $project)

        <div class="topMatter_Box_Inner">
            <div href="{{ route('project.details', ['project_id' => $project->id]) }}" class="matter">
                <p class="matter__Category">
                    {!! $project->projectCategory[0]->icon !!} {{ $project->projectCategory[0]->name }}
                </p>
                <div class="matter__Box">
                    <div class="matter__Detail">
                        <a href="{{ route('project.details', ['project_id' => $project->id]) }}" class="matter__TitleA">
                            <h3 class="matter__Title">{{ Str::limit($project->title, 25) }}</h3>
                        </a>
                        <ul class="matter__SnsIcons">
                            @foreach($project->sns_code_list as $code)
                            <li class="matter__SnsIconsLi">
                                <span class="matter__SnsIcon">
                                    <i class="fab fa-{{ $code }}"></i>
                                </span>
                            </li>
                            @endforeach
                        </ul>
                        <p class="matter__DetailTxt">{!! $project->content !!}</p>
                        <div class="matter__UserBox">
                            <div class="matter__UserPic">
                                <div class="matter__UserPicImg"
                                    style="background-image:url({{ $project->user->avatar_image}})"></div>
                            </div>
                            <a href="{{ route('profile.client', ['uuid' => $project->user->uuid])}}"
                                class="matter__UserName">
                                {{ $project->user->name }}
                            </a>
                        </div>
                    </div>
                    <div class="matter__PriceBox">
                        <p class="matter__PriceTitle">1人あたりの報酬</p>
                        <p class="matter__Price">
                            {{ number_format($project->reward_amount) }}<span class="matter__PriceYen">円</span>
                        </p>
                        <p>
                            募集人数{{ $project->request_people_number }}人
                        </p>
                    </div>
                    @if($project->application_hours_left['duration'] > 0)
                    <div class="matter__OtherBox">
                        <p class="matter__OtherTitle">募集終了まで</p>
                        <p class="matter__OtherTxt">あと<span class="matter__OtherDate">{{
                                $project->application_hours_left['duration'] }}</span>{{
                            $project->application_hours_left['sign'] }}</p>
                        <span class="matter__OtherSmall">({{ $project->application_deadline_at }}まで)</span>
                    </div>
                    @else
                    <div class="matter__OtherBox">
                        <p class="matter__OtherTxt matter__OtherTxt2">募集終了</p>
                    </div>
                    @endif
                    <p class="matter__Date">
                        掲載日：{{ $project->published }}
                    </p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <a href="{{ route('guide') }}" target="_blank" class="btn">
        特定商取引法に基づく表記
    </a>

</section>
@endsection