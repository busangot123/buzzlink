@extends('include.layout')

@section('header')
@include('include.headerNone')
@endsection

@section('content')
<div>
    <div class="register">
        <div class="register__Inner">
            <h1 class="register__PageTitle">会員登録</h1>
            @include('auth.include.flowlist', ['flow_active' => 3])
            <div class="register__FormCont">
                <h2 class="register__Title">登録情報を確認してください</h2>
                <div class="register__FormBox">
                    <h3 class="register__FormTitle">ログイン情報について</h3>
                    <dl class="register__FormList">
                        <dt class="register__FormListLeft">
                            メールアドレス
                            <br class="inline_s">
                            <span class="register__Hikoukai">非公開</span>
                        </dt>
                        <dd class="register__FormListRight">
                            {{ $email }}
                        </dd>
                        <dt class="register__FormListLeft">
                            パスワード
                            <br class="inline_s">
                            <span class="register__Hikoukai">非公開</span>
                        </dt>
                        <dd class="register__FormListRight">
                            {{ $input['password_num'] }}
                        </dd>
                    </dl>
                </div>
                <div class=" register__FormBox">
                    <h3 class="register__FormTitle">会員情報について</h3> 
                    <dl class="register__FormList">
                        <dt class="register__FormListLeft">
                            表示用ユーザー名<br class="inline_s">
                            <span class="register__Koukai">公　開</span>
                        </dt>
                        <dd class="register__FormListRight">
                            {{ $input['name'] }}
                        </dd>
                        <dt class="register__FormListLeft">
                            氏名<br class="inline_s">
                            <span class="register__Hikoukai">非公開</span>
                        </dt>
                        <dd class="register__FormListRight">
                            {{ $input['full_name'] }}
                        </dd>
                        <dt class="register__FormListLeft">
                            氏名（カナ）<br class="inline_s">
                            <span class="register__Hikoukai">非公開</span>
                        </dt>
                        <dd class="register__FormListRight">
                            {{ $input['full_name_kana'] }}
                        </dd>
                        <dt class="register__FormListLeft">
                            電話番号<br class="inline_s">
                            <span class="register__Hikoukai">非公開</span>
                        </dt>
                        <dd class="register__FormListRight">
                            {{ $input['phone_number'] }}
                        </dd>
                        <dt class="register__FormListLeft">
                            主な利用用途<br class="inline_s">
                            <span class="register__Hikoukai">非公開</span>
                        </dt>
                        <dd class="register__FormListRight">
                            {{ $input['default_role'] == 1 ? 'クライアント' : 'インフルエンサー' }}
                        </dd>
                    </dl>
                </div>

                <div class="register__BtnWrap">
                    <button class="register__BtnConfirm2" type="button" onclick="history.back()">
                    入力内容を修正する
                    </button>
                    <form method="POST" action="{{ $patch_url }}" class="register__form--alt">
                        @csrf
                        @method('patch')
                        <button class="register__BtnRegister2" type="submit">
                        上記内容で登録する
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</template>
@endsection
