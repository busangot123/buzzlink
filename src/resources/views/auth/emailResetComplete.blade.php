@extends('include.layout')

@section('header')
@include('include.headerNone')
@endsection

@section('content')
<div class="resetPassword">
    <div class="resetPassword__Inner">
        <h1 class="resetPassword__PageTitle">メールアドレスの再設定</h1>
        <div class="resetPassword__Cont">
            <p class="resetPassword__Txt">
                メールアドレスの変更が完了しました。<br>
                新しいメールアドレスでログインしてください。
            </p>
            <div class="mainCont__Btns">
                <ul class="mainCont__BtnsList">
                    <li class="mainCont__BtnsLi3">
                        <a href="{{route('login.page')}}" class="resetPassword__BtnLogin">
                            ログインする
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
