@extends('include.layout')

@section('header')
@include('include.headerNone')
@endsection

@section('content')
<div class="register register__fixedHeight">
    <div class="register__Inner">
        <h1 class="register__PageTitle">会員登録</h1>
        @include('auth.include.flowlist', ['flow_active' => 4])
        <div class="register__Cont">
            <h2 class="register__Title">ご登録ありがとうございます。<br>
                会員登録が完了しました。</h2>

            <div class="mainCont__Btns">
                <ul class="mainCont__BtnsList">
                    <li class="mainCont__BtnsLi3">
                        <a href="{{route('login.page')}}" class="resetPassword__BtnLogin" type="button">ログインして始める</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</template>
@endsection
