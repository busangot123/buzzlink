@extends('include.layout')

@section('header')
@include('include.headerNone')
@endsection

@section('content')
<div class="register">
    <div class="register__Inner">
        <h1 class="register__PageTitle">会員登録</h1>
        @include('auth.include.flowlist', ['flow_active' => 2])
        <div class="register__FormCont">
            <h2 class="register__Title">情報を入力してください</h2>
            <register-info-form :email='@json($email)' :input='@json($session_input)' :csrf='@json(csrf_token())' :posturl='@json($post_url)'> </register-info-form>
        </div>
    </div>
</div>
@endsection
