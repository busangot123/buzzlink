<div class="flowlist">
    <p class="flowlist__Flow {{ $flow_active == 1 ?  'flowlist__FlowActive' : '' }}">
        <span class="flowlist__FlowNumber {{ $flow_active == 1 ?  'flowlist__FlowNumberActive' : '' }}">FLOW.1</span>
        メール<br />送信
    </p>
    <p class="flowlist__Flow {{ $flow_active == 2 ?  'flowlist__FlowActive' : '' }}">
        <span class="flowlist__FlowNumber {{ $flow_active == 2 ?  'flowlist__FlowNumberActive' : '' }}">FLOW.2</span>
        情報<br />入力
    </p>
    <p class="flowlist__Flow {{ $flow_active == 3 ?  'flowlist__FlowActive' : '' }}">
        <span class="flowlist__FlowNumber {{ $flow_active == 3 ?  'flowlist__FlowNumberActive' : '' }}">FLOW.3</span>
        登録内容<br />確認
    </p>
    <p class="flowlist__Flow {{ $flow_active == 4 ?  'flowlist__FlowActive' : '' }}">
        <span class="flowlist__FlowNumber {{ $flow_active == 4 ?  'flowlist__FlowNumberActive' : '' }}">FLOW.4</span>
        登録<br />完了
    </p>
</div>
