@extends('include.layout')

@section('header')
@include('include.headerNone')
@endsection

@section('content')
<div>

    <div class="register">
        <div class="register__Inner">
            <h1 class="register__PageTitle">会員登録</h1>
            @include('auth.include.flowlist', ['flow_active' => 1])

            <div class="register__Cont">
                <h2 class="register__Title">
                    メールアドレスをご入力のうえ<br />
                    ボタンをクリック（タップ）してください
                </h2>
                <p>
                    ご入力いただいたメールアドレスへお手続きのご案内を送信します。<br />
                    メール内容をご確認いただき、会員登録をお進めください。
                </p>
                <form action="{{route('email.send_verification')}}" method="POST">
                    @csrf
                    <input type="input" placeholder="例）info@buzzlink.jp" class="register__Email" name="email" />
                    @error('email')
                    <div class="error__message">{{ $message }}</div>
                    @enderror

                    <button class=" register__BtnRegister3" type="submit">
                        メールを送信する
                    </button>
                </form>
                <p class="register__Txt">
                    ※会員登録をすることで<a href="#">利用規約</a>、<br class="inline_s" />
                    <a href="#">個人情報保護方針</a>に同意したものとします
                </p>
            </div>
            <p class="register__Txt">
                <a href="/login">既にご登録済の方はこちら</a>
            </p>
        </div>
    </div>
</div>
@endsection
