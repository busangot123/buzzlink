@extends('include.layout')
@section('header')
@include('include.headerLogin')
@endsection
@section('content')
<div class="login login__fixedHeight">
    <h1 class="login__PageTitle">ログイン画面</h1>
    <form action="{{route('login.post')}}" method="POST" novalidate>
        @csrf
        <div class="login__Inner">

            <p class="login__Title">メールアドレス</p>
            <input type="email" placeholder="例）info@buzzlink.jp" class="login__Email" value="{{ old('email') }}"  name="email">
            @error('email')
            <div class="error__message">{{ $message }}</div>
            @enderror

            <p class="login__Title">パスワード</p>
            <input type="password" class="login__Password" name="password">
            @error('password')
            <div class="error__message">
                {{ $message }}
            </div>
            @enderror

            <div class="login__BtnLoginWrap">
                <button class="login__BtnLogin" type="submit">ログインする</button>
                <p>パスワードを忘れた方は<a href="{{route('password.request')}}">こちら</a></p>
            </div>
            <div class="login__BtnMemberWrap">
                <p>まだアカウントをお持ちでない方は会員登録をお済ませください</p>
                <a href="{{route('email.verification_page')}}" class="login__BtnMemberRegistration" type="button" style="text-decoration: none; color: #FFF">まずは会員登録（無料）</a>
            </div>
        </div>
    </form>
</div>
@endsection
