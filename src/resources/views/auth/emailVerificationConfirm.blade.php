@extends('include.layout')

@section('header')
@include('include.headerNone')
@endsection

@section('content')
<div>

    <div class="register">
        <div class="register__Inner">
            <h1 class="register__PageTitle">会員登録</h1>
            @include('auth.include.flowlist', ['flow_active' => 1])

            <div class="register__Cont">
                <h2 class="register__Title">
                    下記メールアドレスにメールを送信しました。
                </h2>
                <p>
                    会員登録のお手続きは、まだ完了しておりません。メール本文に記載のURLをクリック（タップ）していただき、会員登録のお手続きをお進めください。
                </p>
                <div class="register__Mail">
                    <p>{{ $email }}</p>
                    </div$>
                </div>
                <p>
                    ※メールに記載された登録用URLの有効期限は24時間です。<br />
                    有効期限が切れた場合は、再度メールアドレスの入力からやり直してください。
                </p>
                <div class="register__Confirm">
                    <h3 class="register__SubTitle">
                        メールを受信しない場合は、以下の点をご確認ください
                    </h3>
                    <ul class="register__List">
                        <li class="register__ListLi">
                            ・ご入力いただいたメールアドレスが正しいか
                        </li>
                        <li class="register__ListLi">
                            ・迷惑メールフォルダに振り分けられていないか
                        </li>
                        <li class="register__ListLi">
                            ・「@buzzlink.jp」ドメインのメールが受信可能な設定か
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @endsection
