@extends('include.layout')
@section('content')
@include('include.headerResetPass')

<div class="resetPassword resetPassword__fixedHeight">
    <div class="resetPassword__Inner">
        <h1 class="resetPassword__PageTitle">パスワードの再設定</h1>
        <div class="resetPassword__Cont">
            <p>
            <h2 class="register__Title">ご入力いただいたメールアドレス宛にメールを送信しました。</h2>
            メールを確認し、24時間以内にパスワードの再設定をお願いします。<br><br>
            24時間を経過した場合パスワードの再設定はできなくなります。<br><br>
            もしメールが届かない場合は、入力したメールアドレスに誤りがないかご確認下さい。<br>迷惑メールとして削除されている場合もありますので、メールソフトの設定もあわせてご確認ください。
            </p>
        </div>
    </div>
</div>
@endsection
