@extends('include.layout')
@section('content')
@include('include.headerResetPass')

<div class="resetPassword resetPassword__fixedHeight">
    <div class="resetPassword__Inner">
        <h1 class="resetPassword__PageTitle">パスワードの再設定</h1>
        <div class="resetPassword__Cont">
            <p>
                ご登録いただいたメールアドレスを入力してください。<br class="inline_p">
                メールアドレス宛にパスワード変更ページのURLが記載されたメールを送信します。
            </p>
            <form action="{{route('password.email')}}" method="POST">
                @csrf
                <input type="input" placeholder="例）info@buzzlink.jp" class="resetPassword__Email" name="email">
                @error('email')
                <div class="error__message">{{$message}}</div>
                @enderror
                <button class="resetPassword__BtnMail" type="submit">再設定用メールを送信する</button>
            </form>
        </div>
    </div>
    <p class="resetPassword__Txt">まだアカウントをお持ちでない方は<br class="inline_s"><a href="{{route('email.verification_page')}}">会員登録</a>をお済ませください</p>
</div>
@endsection
