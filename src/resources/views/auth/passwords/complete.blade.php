@extends('include.layout')
@section('content')
@include('include.headerResetPass')
<div class="resetPassword resetPassword__fixedHeight">
    <div class="resetPassword__Inner">
        <h1 class="resetPassword__PageTitle">パスワードの再設定</h1>
        <div class="resetPassword__Cont">
            <p class="resetPassword__Txt">
                パスワードの変更が完了しました。<br>
                新しいパスワードでログインしてください。
            </p>
            <div class="mainCont__Btns">
                <ul class="mainCont__BtnsList">
                    <li class="mainCont__BtnsLi3">
                        <a href="{{route('login.page')}}" class="resetPassword__BtnLogin">
                            ログインする
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
