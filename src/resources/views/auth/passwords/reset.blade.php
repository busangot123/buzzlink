@extends('include.layout')
@section('content')
@include('include.headerResetPass')
<div class="resetPassword resetPassword__fixedHeight">
    <div class="resetPassword__Inner">
        <h1 class="resetPassword__PageTitle">パスワードの再設定</h1>
        <div class="resetPassword__Cont">
            <p>
                パスワードの設定を行います。<br>
                新しいパスワードを設定してください。
            </p>
            <form action="{{ route('password.update')}}" method="POST">
                @csrf
                <input type="hidden" name="token" value="{{ $token }}">
                <input id="email" type="hidden" name="email" value="{{ $email ?? old('email') }}">
                <div class="resetPassword__Wrap">
                    <p class="resetPassword__Title">パスワード</p>
                    <input type="password" class="resetPassword__PassWord" name="password">
                    <span class="resetPassword__Caution">※半角英数字8～32文字</span>
                    @error('password')
                    <span class="error__message">{{ $message }}</span>
                    @enderror
                </div>
                <div class="resetPassword__Wrap">
                    <p class="resetPassword__Title">パスワード(確認)</p>
                    <input type="password" class="resetPassword__PassWord" name="password_confirmation">
                </div>
                <button class="resetPassword__BtnRegistration" type="submit">パスワードを変更する</button>
            </form>
        </div>
    </div>
</div>
@endsection
