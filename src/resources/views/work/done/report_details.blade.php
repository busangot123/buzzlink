@extends('include.layout')

@section('content')
@include('include.headerNone')
<div class="mainCont">
    <div class="mainCont__Inner">
        {{ Breadcrumbs::render('management_details', route('mypage.work_management')) }}
        <div class="mainCont__Wrap">
            <div class="mainCont__Right mainCont__Right3">
                @include('include.project_details', [
                'project' => $project_user->project,
                'project_user_status' => $project_user->status_label
                ])

                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        選定したインフルエンサー
                    </h2>

                    <h3 class="mainCont__Title4">選定日時 {{
                        \Carbon\Carbon::parse($project_user->selected_at)->format('Y/m/d H:i:s') }}</h3>
                    @include('include.project_application_user_details', ['project_user' => $project_user])
                </div>
                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        インフルエンサーからクライアントへの完了報告
                    </h2>
                    <h3 class="mainCont__Title4">SNS投稿情報　/　完了報告日時 {{
                        \Carbon\Carbon::parse($project_user->reported_at)->format('Y/m/d H:i:s') }}</h3>
                    <p>
                        投稿URL：{{ $project_user->sns_url }}
                    </p>
                    <div class="mainCont__EmbedSnsWrap">
                        @if($project_user->sns_type == 'twitter')
                        <blockquote class="twitter-tweet">
                            <a href="{{ $project_user->sns_url }}"></a>
                        </blockquote>
                        <script type="application/javascript" async src="https://platform.twitter.com/widgets.js"
                            charset="utf-8"></script>
                        @elseif($project_user->sns_type == 'facebook')
                        <div class="mainCont__EmbedSns">
                            <div id="fb-root"></div>
                            <script type="application/javascript" async defer
                                src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v12.0&autoLogAppEvents=1"></script>
                            <div class="fb-post" data-href="{{ $project_user->sns_url }}"></div>
                        </div>
                        @else
                        <div class="mainCont__EmbedSns">
                            <iframe class="sns_frame {{ $project_user->sns_type }}"
                                src="{{ $project_user->sns_frame_url }}" frameborder="0" scrolling="no"></iframe>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="mainCont__Box">
                    <h3 class="mainCont__Title4">
                        {{ $project->user->name }}さんについての評価
                    </h3>
                    <div class="register__FormBox">
                        <dl class="register__FormList">
                            <dt class="register__FormListLeft">
                                結果の評価
                            </dt>
                            <dd class="register__FormListRight">
                                <p class="register__RadioBox">
                                    <input id="radio3" disabled="" class="register__RadioBtnGood" name="hoge"
                                        type="radio" value="Good">
                                    <label for="radio3"
                                        class="register__Label {{ $project_user->influencer_eva == 1 ? 'register__Label5' : 'register__Label6' }}">{{
                                        $project_user->influencer_eva == 1 ? 'Good!' : 'Bad' }}</label>
                                </p>
                            </dd>
                            <dt class="register__FormListLeft">
                                コメント
                            </dt>
                            <dd class="register__FormListRight">
                                <div class="mainCont__GrayBox">
                                    <p class="mainCont__GrayBoxP">{!! $project_user->influencer_comment !!}</p>
                                </div>
                            </dd>
                        </dl>
                    </div>
                </div>
                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        クライアントからインフルエンサーへの結果評価
                    </h2>
                    <h3 class="mainCont__Title4">結果評価日時 {{
                        \Carbon\Carbon::parse($project_user->evaluated_at)->format('Y/m/d H:i:s') }}</h3>
                    <div class="register__FormBox">
                        <dl class="register__FormList">
                            <dt class="register__FormListLeft">
                                結果の評価
                            </dt>
                            <dd class="register__FormListRight">
                                <p class="register__RadioBox">
                                    <input id="radio6" disabled class="register__RadioBtnBad" name="hoge" type="radio"
                                        value="Bad">
                                    <label for="radio3"
                                        class="register__Label {{ $project_user->client_eva == 1 ? 'register__Label5' : 'register__Label6' }}">{{
                                        $project_user->client_eva == 1 ? 'Good!' : 'Bad' }}</label>
                                </p>
                            </dd>
                            <dt class="register__FormListLeft">
                                コメント
                            </dt>
                            <dd class="register__FormListRight">
                                <div class="mainCont__GrayBox">
                                    <p class="mainCont__GrayBoxP">{!! $project_user->client_comment !!}</p>
                                </div>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
