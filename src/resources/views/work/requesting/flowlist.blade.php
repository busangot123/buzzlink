<div class="flowlist flowlist2">
    <p class="flowlist__Flow flowlist__Flow2 {{ $flow_active == 1 ?  'flowlist__FlowActive' : '' }}">
        <span class="flowlist__FlowNumber {{ $flow_active == 1 ?  'flowlist__FlowNumberActive' : '' }}">FLOW1</span>
        完了報告<br>入力
    </p>
    <p class="flowlist__Flow flowlist__Flow2 {{ $flow_active == 2 ?  'flowlist__FlowActive' : '' }}">
        <span class="flowlist__FlowNumber {{ $flow_active == 2 ?  'flowlist__FlowNumberActive' : '' }}">FLOW2</span>
        完了報告<br>確認
    </p>
    <p class="flowlist__Flow flowlist__Flow2 {{ $flow_active == 3 ?  'flowlist__FlowActive' : '' }}">
        <span class="flowlist__FlowNumber {{ $flow_active == 3 ?  'flowlist__FlowNumberActive' : '' }}">FLOW3</span>
        完了報告<br>完了
    </p>
</div>
