@extends('include.layout')

@section('content')
@include('include.headerNone')
<div class="mainCont">
    <div class="mainCont__Inner">
        {{ Breadcrumbs::render('management_details', route('mypage.work_management')) }}
        @include('work.requesting.flowlist', ['flow_active' => 1])
        <div class="mainCont__Wrap">
            <div class="mainCont__Right mainCont__Right3">
                @include('include.project_details', [
                'project' => $project_user->project,
                'project_user_status' => $project_user->status_label
                ])

                <div class="mainCont__Box">
                    <h2 class="mainCont__Title1">
                        選定したインフルエンサー
                    </h2>

                    <h3 class="mainCont__Title4">選定日時 {{
                        \Carbon\Carbon::parse($project_user->selected_at)->format('Y/m/d H:i:s') }}</h3>
                    @include('include.project_application_user_details', ['project_user' => $project_user])
                </div>
                <form action="{{ route('work_management.influencer.confirmation', ['project_id' => $project->id]) }}"
                    method="POST">
                    @csrf
                    <input hidden name="project_id" value="{{ $project->id }}">
                    <div class="mainCont__Box">
                        <h2 class="mainCont__Title1">
                            インフルエンサーからクライアントへの完了報告
                        </h2>
                        <p class="mainCont__RedBox">
                            投稿したSNSへのURLを入力してください
                        </p>
                        <h3 class="mainCont__Title4">SNS投稿情報</h3>
                        <dl class="register__FormList">
                            <dt class="register__FormListLeft">
                                投稿SNS
                            </dt>
                            <dd class="register__FormListRight">
                                <input type="text" name="sns_url" value="{{ old('sns_url') ?: $sns_url }}"
                                    placeholder="https://xxxxxx.com" class="register__Url2">
                                <span class="register__FormListStxt">※半角英数字、記号</span>
                                @error('sns_url')
                                <div class="error__message">{{ $message }}</div>
                                @enderror
                            </dd>
                        </dl>
                    </div>

                    <div class="mainCont__Box">
                        <h3 class="mainCont__Title4">
                            {{ $project->user->name }}さんについての評価
                        </h3>
                        <p class="mainCont__RedBox">
                            クライアントについての評価を入力してください
                        </p>
                        <div class="register__FormBox">
                            <dl class="register__FormList">
                                <dt class="register__FormListLeft">
                                    クライアントの評価
                                </dt>
                                <dd class="register__FormListRight">
                                    <p class="register__RadioBox">
                                        <input id="radio3" class="register__RadioBtnGood" name="influencer_eva"
                                            type="radio" value="1" @if(old('influencer_eva')==1 || $influencer_eva==1)
                                            checked @endif>
                                        <label for="radio3" class="register__Label register__Label3">Good</label>
                                    </p>
                                    <p class="register__RadioBox">
                                        <input id="radio4" class="register__RadioBtnBad" name="influencer_eva"
                                            type="radio" value="2" @if(old('influencer_eva')==2 || $influencer_eva==2)
                                            checked @endif>
                                        <label for="radio4" class="register__Label register__Label4">Bad</label>
                                    </p>
                                    @error('influencer_eva')
                                    <div class="error__message">{{ $message }}</div>
                                    @enderror
                                </dd>
                                <dt class="register__FormListLeft">
                                    コメント
                                </dt>
                                <dd class="register__FormListRight">
                                    <textarea id="text-editor" name="influencer_comment"
                                        class="register__TextArea3">{!! old('influencer_comment') ?: $influencer_comment !!}</textarea>
                                    <span class="register__FormListStxt register__FormListStxtSp"><span
                                            id="character_count">0</span>/2500</span>
                                    @error('influencer_comment')
                                    <div class="error__message">{{ $message }}</div>
                                    @enderror
                                </dd>
                            </dl>
                        </div>
                        <div class="mainCont__Btns">
                            <ul class="mainCont__BtnsList">
                                <li class="mainCont__BtnsLi">
                                    <button type="submit" class="report__submitBtn mainCont__Btn2 mainCont__BtnCon2">
                                        入力内容を確認する
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
