@extends('include.layout')

@section('content')
@include('include.headerNone')
    <div class="mainCont">
        <div class="mainCont__Inner">
            @include('work.requesting.flowlist', ['flow_active' => 3])
            <div class="mainCont__Wrap">
                <div class="register__Cont">
                    <h2 class="register__Title register__Title2">完了報告の登録ありがとうございます。</h2>
                    <h2 class="register__Title">報酬の獲得は、クライアントの結果確認登録後となりますのでご了承ください。</h2>
                    <div class="mainCont__Btns">
                        <ul class="mainCont__BtnsList">
                            <li class="mainCont__BtnsLi">
                                <a href="{{ route('mypage.work_management') }}" class="mainCont__Btn2 mainCont__BtnCon">
                                    仕事管理を表示する
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
