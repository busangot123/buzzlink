@extends('include.layout')

@section('content')
@include('include.headerNone')
<div class="mainCont">
    <div class="mainCont__Inner">
        {{ Breadcrumbs::render('management_details', route('mypage.work_management')) }}
        @include('work.requesting.flowlist', ['flow_active' => 2])
        <div class="mainCont__Wrap">
            <div class="mainCont__Right mainCont__Right3">
                <div class="register register2">
                    <div class="mainCont__Box">
                        <div class="mainCont__GrayBox">
                            <p class="mainCont__GrayBoxP">下記の内容で問題がなければ、ページ下の「この内容で確定する」をクリックしてください。</p>
                            <p class="mainCont__GrayBoxRed">確定後は修正ができなくなるため、必ずもう一度内容をご確認ください。</p>
                        </div>
                    </div>
                    @include('include.project_details', [
                    'project' => $project_user->project,
                    'project_user_status' => $project_user->status_label
                    ])

                    <div class="mainCont__Box">
                        <h2 class="mainCont__Title1">
                            選定したインフルエンサー
                        </h2>

                        <h3 class="mainCont__Title4">選定日時 {{
                            \Carbon\Carbon::parse($project_user->selected_at)->format('Y/m/d H:i:s') }}</h3>
                        @include('include.project_application_user_details', ['project_user' => $project_user])
                    </div>
                    <form action="{{ route('work_management.requesting.store', ['project_id' => $project->id]) }}"
                        method="POST">
                        @csrf
                        <div class="mainCont__Box">
                            <h2 class="mainCont__Title1">
                                インフルエンサーからクライアントへの完了報告
                            </h2>
                            <h3 class="mainCont__Title4">SNS投稿情報</h3>
                            <input hidden name="sns_url" value="{{ $sns_url }}">
                            <p style="word-break: break-all;">
                                投稿URL：{{ $sns_url }}
                            </p>
                            <div class="mainCont__EmbedSnsWrap">
                                @if($sns_iframe_data['domain'] == 'twitter')
                                <blockquote class="twitter-tweet">
                                    <a href="{{ $sns_url }}"></a>
                                </blockquote>
                                <script type="application/javascript" async
                                    src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                                @elseif($sns_iframe_data['domain'] == 'facebook')
                                <div class="mainCont__EmbedSns">
                                    <div id="fb-root"></div>
                                    <script type="application/javascript" async defer
                                        src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v12.0&autoLogAppEvents=1"></script>
                                    <div class="fb-post" data-href="{{ $sns_url }}"></div>
                                </div>
                                @else
                                <div class="mainCont__EmbedSns">
                                    <iframe class="sns_frame {{ $sns_iframe_data['domain'] }}"
                                        src="{{ $sns_iframe_data['url'] }}" frameborder="0" scrolling="no"
                                        allowfullscreen="true"></iframe>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="mainCont__Box">
                            <h3 class="mainCont__Title4">
                                {{ $project->user->name }}さんについての評価
                            </h3>
                            <div class="register__FormBox">
                                <dl class="register__FormList">
                                    <dt class="register__FormListLeft">
                                        結果の評価
                                    </dt>
                                    <dd class="register__FormListRight">
                                        <p class="register__RadioBox">
                                            <input hidden name="project_id" value="{{ $project->id }}">
                                            <input hidden name="influencer_eva" value="{{ $influencer_eva }}">
                                            <input id="radio3" disabled class="register__RadioBtnGood" name="hoge"
                                                type="radio" value="Good">
                                            <label for="radio3"
                                                class="register__Label {{ $influencer_eva == 1 ? 'register__Label5' : 'register__Label6' }}">{{
                                                $influencer_eva == 1 ? 'Good!' : 'Bad' }}</label>
                                        </p>
                                    </dd>
                                    <dt class="register__FormListLeft">
                                        コメント
                                    </dt>
                                    <dd class="register__FormListRight">
                                        <div class="mainCont__GrayBox">
                                            <input hidden name="influencer_comment"
                                                value="{!! nl2br($influencer_comment) !!}">
                                            <p class="mainCont__GrayBoxP">{!! nl2br($influencer_comment) !!}</p>
                                        </div>
                                    </dd>
                                </dl>
                            </div>
                            <div class="mainCont__Btns">
                                <ul class="mainCont__BtnsList">
                                    <li class="mainCont__BtnsLi">
                                        <a href="{{ route('work_management.review', ['project_id' => $project->id]) }}"
                                            class="mainCont__Btn5 mainCont__BtnCon2">
                                            完了報告入力画面に戻る
                                        </a>
                                    </li>
                                    <li class="mainCont__BtnsLi">
                                        <button type="submit"
                                            class="report__submitBtn mainCont__Btn2 mainCont__BtnCon2">
                                            この内容で確定する
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection