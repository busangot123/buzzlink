<div>
    <header class="topHeader">
        <div class="topHeader_Logo">
            @include('include.headerLogo')
        </div>
        <div class="topHeader_Right mt-3">
            <form action="{{ route('search') }}" method="GET" class="topHeader_Search">
                <input type="text" name="keyword" placeholder="案件を検索する" class="topHeader_SearchText">
                <input type="submit" value="" class="topHeader_SearchBtn">
            </form>
            <ul class="topHeader_Btns">
                <li class="topHeader_BtnsLi">
                    <a href="{{route('login.page')}}" class="topHeader_Btn">
                        ログイン
                    </a>
                </li>
                <li class="topHeader_BtnsLi">
                    <a href="{{route('email.verification_page')}}" class="topHeader_Btn-2">
                        新規登録
                    </a>
                </li>
            </ul>
        </div>
        <nav class="topSpnav block_s">
            <div class="topSpnav_menu">
                <div class="topSpnav_bg"></div>
                <button type="button" class="topSpnav_button">
                    <span class="topSpnav_bar topSpnav_bar1"></span>
                    <span class="topSpnav_bar topSpnav_bar2"></span>
                    <span class="topSpnav_bar topSpnav_bar3"></span>
                </button>
                <nav class="topSpnav_nav_wrapper">
                    <ul class="topSpnav_nav">
                        <form action="{{ route('search') }}" method="GET" class="topSpnav_Search">
                            <input type="text" name="keyword" placeholder="案件を検索する" class="topSpnav_SearchText">
                            <input type="submit" value="" class="topSpnav_SearchBtn">
                        </form>
                        <ul class="topSpnav_Btns">
                            <li class="topSpnav_BtnsLi">
                                <a href="{{route('login.page')}}" class="topSpnav_Btn">
                                ログイン
                                </a>
                            </li>
                            <li class="topSpnav_BtnsLi">
                                <a href="{{route('email.verification_page')}}" class="topSpnav_Btn-2">
                                新規登録
                                </a>
                            </li>
                        </ul>
                    </ul>
                </nav>
            </div>
        </nav>
    </header>
</div>
