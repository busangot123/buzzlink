<div class="userBox">
    <div class="userBox__Box">
        <p class="userBox__Pic">
            <a target="_blank" href="{{ route('profile.influencer', ['uuid' => $project_user->user->uuid])}}">
                <img src="{{ $project_user->user->avatar_image ?: '/assets/user.png' }}" class="matter__UserPicImg">
            </a>
        </p>
        <h3 class="userBox__Title"><a target="_blank"
                href="{{ route('profile.influencer', ['uuid' => $project_user->user->uuid])}}">{{
                $project_user->user->name }}</a></h3>

        <div class="userBox__UserBox">
            <div class="userBox__UserSt">
                評価
                <span class="userBox__UserStScore">{{ $project_user->user->influencer_rate }}</span>
            </div>
            <div class="userBox__UserSt">
                完了実績
                <span class="userBox__UserStScore">{{ $project_user->user->completed_project }}</span>件
            </div>
            <div class="userBox__UserSt">
                完了率
                <span class="userBox__UserStScore">{{ $project_user->user->completion_rate }}</span>%
            </div>
        </div>
    </div>
    <div class="userBox__Box">
        @foreach($project_user->user->hasManyGenres as $tag)
        <ul class="tag">
            <li class="tag__Inner">
                # {{ $tag->name }}
            </li>
        </ul>
        @endforeach
    </div>
    <div class="userBox__Pr">
        <h4 class="userBox__SubTitle">
            自己PR・実績
        </h4>
        <p>
            {!! nl2br(e($project_user->self_promotion)) !!}
        </p>
    </div>
    <div class="userBox__Data">
        @foreach($project_user->user->sns_list as $info)
        <div class="userBox__DataBox">
            @if($info['type'] == 'youtube')
            <p class="userBox__DataTitle"><i class="fab fa-youtube"></i>YouTube</p>
            チャンネル登録者数<br>
            @endif
            @if($info['type'] == 'instagram')
            <p class="userBox__DataTitle"><i class="fab fa-instagram"></i> Instagram</p>
            フォロワー数<br>
            @endif
            @if($info['type'] == 'facebook')
            <p class="userBox__DataTitle"><i class="fab fa-facebook"></i> Facebook</p>
            フォロワー数<br>
            @endif
            @if($info['type'] == 'twitter')
            <p class="userBox__DataTitle"><i class="fab fa-twitter"></i> Twitter</p>
            フォロワー数<br>
            @endif
            @if($info['type'] == 'tiktok')
            <p class="userBox__DataTitle"><i class="fab fa-tiktok"></i> Tiktok</p>
            フォロワー数<br>
            @endif
            <span class="userBox__DataTxt">{{ $info['follower_label'] }}</span>
        </div>
        @endforeach
    </div>
    <div class="userBox__Date">
        <h4 class="userBox__SubTitle">
            応募日時
        </h4>
        <p>{{ \Carbon\Carbon::parse($project_user->applied_at)->format('Y/m/d H:i:s') }}</p>
    </div>
    @if ($project_user->project->client_user_id == Auth::user()->id && $project_user->application_status != 5)
    <a href="{{ route('chat.room', ['project_user_id' => $project_user->id]) }}" target="_blank" class="userBox__Btn">
        メッセージを送る
    </a>
    @endif
</div>