<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="UTF-8">
    <title>Buzzlink</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    <link rel="icon" href="{{asset('/assets/icon_logo.png') }}" type="image/x-icon" />
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/ui-lightness/jquery-ui.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Clamp.js/0.5.1/clamp.min.js"></script>
    <!-- <script src="https://kit.fontawesome.com/c558d8d24f.js" crossorigin="anonymous"></script> -->
    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    <script src="https://rawgit.com/jquery/jquery-ui/master/ui/i18n/datepicker-ja.js"></script>
    <script src="https://js.stripe.com/v3/"></script>
</head>

<body>
    <div id="app">
        @yield('header')
        @yield('content')
    </div>
    @include('include.footer')

    <script type="application/javascript">
        window.Laravel = {
            csrfToken: '{{ csrf_token() }}'
        }
    </script>
    <script type="application/javascript" src="{{ mix('js/app.js') }}" defer></script>
</body>

</html>
