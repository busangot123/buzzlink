<header class="header">
    <div class="header__wrap">
    <p class="header__Logo">
        @include('include.headerLogo')
    </p>
        <div class="header__Btns">
            <a class="header__BtnBeginner" href="{{route('email.verification_page')}}">
                はじめての方
            </a>
            <a class="header__BtnLogin" href="{{route('login.page')}}">
                ログイン
            </a>
        </div>
    </div>
</header>
