@if (!$project->hasManyProjectUsers->isEmpty())
@foreach($project->hasManyProjectUsers->take(Request::input('limit')) as $influencer)
<div class="userBox">
    <div class="userBox__Box">
        <p class="userBox__Pic">
            <a target="_blank" href="{{ route('profile.influencer', ['uuid' => $influencer->user->uuid])}}"><img
                    src="{{ $influencer->user->avatar_image ?: '/assets/user.png' }}" class="userBox__PicImg"
                    alt=""></a>
        </p>
        <h3 class="userBox__Title"><a target="_blank" href="{{ route('profile.influencer', ['uuid' => $influencer->user->uuid])}}">{{
                $influencer->user->name }}</a></h3>

        <div class="userBox__UserBox">
            <div class="userBox__UserSt">
                評価
                <span class="userBox__UserStScore">{{ $influencer->user->influencer_rate }}</span>
            </div>
            <div class="userBox__UserSt">
                完了実績
                <span class="userBox__UserStScore">{{ $influencer->user->completed_project }}</span>件
            </div>
            <div class="userBox__UserSt">
                完了率
                <span class="userBox__UserStScore">{{ $influencer->user->completion_rate }}</span>%
            </div>
        </div>
    </div>
    <div class="userBox__Box">
        <ul class="tag">
            @foreach($influencer->hasManyGenres as $tag)
            <li class="tag__Inner">
                # {{ $tag->name }}
            </li>
            @endforeach
        </ul>
    </div>
    <div class="userBox__Pr">
        <h4 class="userBox__SubTitle">
            自己PR・実績
        </h4>
        @if( (isset(Auth::user()->id) ? Auth::user()->id : 0) == $influencer->influencer_user_id ||
        (isset(Auth::user()->id) ? Auth::user()->id : 0) == $project->client_user_id)
        <p>
            <span>{!! nl2br(e($influencer->self_promotion)) !!}</span>
        </p>
        @else
        <p class="userBox__PrTxt">
            <span class="userBox__Blur">投稿者のみが閲覧できます。</span>
        </p>
        @endif
    </div>
    <div class="userBox__Data">
        @foreach($influencer->user->sns_list as $info)
        <div class="userBox__DataBox">
            @if($info['type'] == 'youtube')
            <p class="userBox__DataTitle"><i class="fab fa-youtube"></i>YouTube</p>
            チャンネル登録者数<br>
            @endif
            @if($info['type'] == 'instagram')
            <p class="userBox__DataTitle"><i class="fab fa-instagram"></i> Instagram</p>
            フォロワー数<br>
            @endif
            @if($info['type'] == 'facebook')
            <p class="userBox__DataTitle"><i class="fab fa-facebook"></i> Facebook</p>
            フォロワー数<br>
            @endif
            @if($info['type'] == 'twitter')
            <p class="userBox__DataTitle"><i class="fab fa-twitter"></i> Twitter</p>
            フォロワー数<br>
            @endif
            @if($info['type'] == 'tiktok')
            <p class="userBox__DataTitle"><i class="fab fa-tiktok"></i> Tiktok</p>
            フォロワー数<br>
            @endif
            <span class="userBox__DataTxt">{{ $info['follower_label'] }}</span>
        </div>
        @endforeach
    </div>
    <div class="userBox__Date">
        <h4 class="userBox__SubTitle">
            応募日時
        </h4>
        <p>{{ \Carbon\Carbon::parse($influencer->applied_at)->format('Y/m/d H:i:s') }}</p>
    </div>
    @if( Auth::user() && Auth::user()->id == $project->client_user_id && $project->status_code < 9) <a
        href="{{ route('chat.room', ['project_user_id' => $influencer->id] )}}" target="_blank" class="userBox__Btn">
        メッセージを送る
        </a>
        @endif
</div>
@endforeach
@else
<div class="mainCont__GrayBox" style="margin-bottom: 20px;">
    <p class="mainCont__GrayBoxP">
        応募したインフルエンサーはいません。
    </p>
</div>
@endif

@if(count($project->hasManyProjectUsers) > Request::input('limit') && Request::input('limit') != null)
<div class="mainCont__BtnsList2">
    <a href="{{ route('project.details', ['project_id' => Request::input('project_id'), 'limit' => Request::input('limit') + 50 ])}}"
        class="mainCont__Btn4">
        すべての応募を確認する
    </a>
</div>
@endif
