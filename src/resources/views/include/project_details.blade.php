<div class="register register2">
    <div class="mainCont__Box">
        <div class="mainCont__TitleWrap">
            <div class="mainCont__TitleWrapBox">
                <p class="mainCont__StIcon">{{ $project_user_status ?: $project->status }}</p>
                <h1 class="mainCont__Title2">
                    {{ $project->title }}
                </h1>
                <ul class="tag">
                    @foreach( $project->projectCategory as $tag)
                    <li class="tag__Inner">
                        # {{ $tag->name }}
                    </li>
                    @endforeach
                </ul>
                <p class="mainCont__Date">掲載日：{{ $project->published }}</p>
                @if($project->is_applied_user && $project->status_code < 9) <p class="mainCont__ContirmBox2">
                    <a href="{{ route('chat.room.project', ['project_id' => $project->id]) }}" target="_blank"
                        class="mainCont__Btn3">
                        メッセージ画面を表示する
                    </a>
                    </p>
                    @endif
            </div>
            <div class="mainCont__UserWrap">
                <div class="mainCont__UserPic mainCont__UserPic2">
                    <img src="{{ $project->user->avatar_image }}" class="mainCont__UserPicImg">
                </div>
                <div class="mainCont__UserName">
                    <h2>
                        <a target="_blank" href="{{ route('profile.client', ['uuid' => $project->user->uuid]) }}">{{
                            $project->user->name }}</a>
                    </h2>
                    <p class="mainCont__UserSt2">
                        評価<span class="mainCont__UserStScore3">{{ $project->user->client_rate }}</span>
                    </p>
                </div>
            </div>

        </div>
    </div>
    <div class="mainCont__Box">
        <h2 class="mainCont__Title1">
            案件内容
        </h2>
        <div class="mainCont__BlockWrap">
            <div class="mainCont__Block">
                <span class="mainCont__BlockTitle">
                    応募期限
                </span>
                {{ $project->application_deadline_inner }}
            </div>
            <div class="mainCont__Block">
                <span class="mainCont__BlockTitle">
                    完了報告制限
                </span>
                {{ $project->report_deadline_inner }}
            </div>
            <div class="mainCont__Block">
                <span class="mainCont__BlockTitle">
                    1人あたりの報酬
                </span>
                {{ number_format($project->reward_amount) }}円
            </div>
            <div class="mainCont__Block">
                <span class="mainCont__BlockTitle">
                    依頼予定人数
                </span>
                {{ $project->request_people_number }}
            </div>
        </div>
    </div>

    <div class="mainCont__Box">
        <h3 class="mainCont__Title4">
            投稿希望SNS
        </h3>
        <div class="mainCont__BlockSnsWrap">
            @foreach($project->sns_code_list as $code)
            <div class="mainCont__BlockSns">
                <i class="fab fa-{{ $code }}"></i>
                {{ ucfirst($code) }}
            </div>
            @endforeach
        </div>
    </div>

    <div class="mainCont__Box">
        <h3 class="mainCont__Title4">
            依頼内容
        </h3>
        <div class="mainCont__GrayBox">
            <p class="mainCont__GrayBoxP">
                {!! nl2br($project->content) !!}
            </p>
        </div>
    </div>

    <div class="mainCont__Box">
        @if($project->notification_url)
        <h3 class="mainCont__Title4">
            告知URL
        </h3>
        <div class="mainCont__GrayBox">
            <p class="mainCont__GrayBoxP">SNSへの投稿内容に掲示して欲しいURL</p>
            <p class="mainCont__GrayBoxP">
                <a href="{{ $project->notification_url }}">{{ $project->notification_url }}</a>
            </p>
        </div>
        @endif
    </div>
    <div class="mainCont__Box">
        @if(sizeof($project->projectAttachment) > 0)
        <h3 class="mainCont__Title4">
            添付ファイル
        </h3>
        <div class="mainCont__GrayBox">
            <p class="mainCont__GrayBoxP">
                @foreach($project->projectAttachment as $attachment)
                <span class="mainCont__File">
                    <a href="{{ route('download.project_attachment',['id' => $attachment->id]) }}">{{ $attachment->name
                        }}</a>
                </span>
                @endforeach
            </p>
        </div>
        @endif
    </div>
    <div class=" mainCont__Box">
        <h2 class="mainCont__Title1">
            応募状況
        </h2>
        <div class="mainCont__BlockWrap">
            <div class="mainCont__Block">
                <span class="mainCont__BlockTitle">
                    募集人数
                </span>
                {{ $project->request_people_number }}名
            </div>
            <div class="mainCont__Block">
                <span class="mainCont__BlockTitle">
                    応募人数
                </span>
                {{ $project->applied_user_count }}名
            </div>
            <div class="mainCont__Block">
                <span class="mainCont__BlockTitle">
                    契約人数
                </span>
                {{ $project->contacted_user_count }}名
            </div>
        </div>
    </div>
</div>
