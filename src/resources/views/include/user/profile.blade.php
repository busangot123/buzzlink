<div class="mainCont__Left">
    <div class="mainCont__UserBox">
        <p class="mainCont__UserTitle">
            {{ Auth::user()->name }}
        </p>
        <p class="mainCont__UserPic mainCont__UserPic3">
            <img src="{{ Auth::user()->avatar_image }}" alt="" class="mainCont__UserPicImg">
        </p>
        <p class="mainCont__UserId">
            ID：{{ Auth::user()->uuid }}
        </p>
    </div>
    <ul class="mainCont__CategoryList block_p">
        <li class="mainCont__CategoryLi"><span class="mainCont__Category"><i class="far fa-smile"
                    aria-hidden="true"></i> マイページ</span>
            <ul class="mainCont__CategoryChild">
                <li class="mainCont__CategoryLiChild">
                    <a href="{{route('mypage.client')}}" class="mainCont__Category">└ クライアントページ</a>
                </li>
                <li class="mainCont__CategoryLiChild">
                    <a href="{{route('mypage.influencer')}}" class="mainCont__Category">└ インフルエンサーページ</a>
                </li>
            </ul>
        </li>
        <li class="mainCont__CategoryLi"><a href="{{route('chat.messages')}}" class="mainCont__Category"
                target="_blank"><i class="far fa-comment-dots"></i> メッセージ</a></li>
        <li class="mainCont__CategoryLi"><span href="#" class="mainCont__Category"><i class="fas fa-user-tie"></i>
                仕事管理</span>
            <ul class="mainCont__CategoryChild">
                <li class="mainCont__CategoryLiChild">
                    <a href="{{route('mypage.project_management')}}" class="mainCont__Category">└
                        募集した案件を見る</a>
                </li>
                <li class="mainCont__CategoryLiChild">
                    <a href="{{route('mypage.work_management')}}" class="mainCont__Category">└
                        応募した案件を見る</a>
                </li>
            </ul>
        </li>
        <li class="mainCont__CategoryLi"><a href="/payment" class="mainCont__Category"><i
                    class="far fa-credit-card"></i> 支払管理</a></li>
        <li class="mainCont__CategoryLi"><a href="{{route('mypage.edit_information')}}" class="mainCont__Category"><i
                    class="fas fa-user-friends" aria-hidden="true"></i> ユーザー情報編集</a></li>
        <li class="mainCont__CategoryLi"><a href="/notifications" class="mainCont__Category"><i class="far fa-flag"></i>
                お知らせ</a></li>
        <li class="mainCont__CategoryLi"><a href="{{ route('faq.faq_page') }}" class="mainCont__Category"><i
                    class="far fa-question-circle"></i>
                ヘルプ</a></li>
    </ul>
    <div class="mainCont__CategoryBox block_s">
        <h3 class="mainCont__CategoryTitle">
            メニュー
        </h3>
        <div class="mainCont__FormSelectBox block_s">
            <select name="" class="mainCont__FormSelect" id="sidebarSP"
                onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                <option value="{{route('mypage.client')}}">クライアントページ</option>
                <option value="{{route('mypage.influencer')}}">インフルエンサーページ</option>
                <option value="{{route('chat.messages')}}">メッセージ</option>
                <option value="{{route('mypage.project_management')}}">募集した案件をみる</option>
                <option value="{{route('mypage.work_management')}}">応募した案件をみる</option>
                <option value="{{route('payment.list')}}">支払管理</option>
                <option value="{{route('mypage.edit_information')}}">ユーザー情報編集</option>
                <option value="{{route('notification.list')}}">お知らせ</option>
                <option value="{{route('faq.faq_page')}}">ヘルプ</option>
            </select>
        </div>
        <ul class="mainCont__CategoryList block_p">
            <li class="mainCont__CategoryLi"><a href="#" class="mainCont__Category"><i class="far fa-smile"
                        aria-hidden="true"></i> 全てのカテゴリ</a></li>
            <li class="mainCont__CategoryLi"><a href="#" class="mainCont__Category"><i class="fas fa-tshirt"
                        aria-hidden="true"></i> アパレル・ファッション</a></li>
            <li class="mainCont__CategoryLi"><a href="#" class="mainCont__Category"><i class="fas fa-utensils"
                        aria-hidden="true"></i> グルメ・スイーツ</a></li>
            <li class="mainCont__CategoryLi"><a href="#" class="mainCont__Category"><i class="fas fa-plane"
                        aria-hidden="true"></i> 旅行</a></li>
            <li class="mainCont__CategoryLi"><a href="#" class="mainCont__Category"><i class="fas fa-user-friends"
                        aria-hidden="true"></i> ファミリー（ママ・キッズ）</a></li>
            <li class="mainCont__CategoryLi"><a href="#" class="mainCont__Category"><i class="fas fa-basketball-ball"
                        aria-hidden="true"></i> スポーツ</a></li>
            <li class="mainCont__CategoryLi"><a href="#" class="mainCont__Category"><i class="fas fa-running"
                        aria-hidden="true"></i> フィットネス・ボディメイク</a></li>
            <li class="mainCont__CategoryLi"><a href="#" class="mainCont__Category"><i class="fas fa-fire-alt"
                        aria-hidden="true"></i> ダイエット</a></li>
            <li class="mainCont__CategoryLi"><a href="#" class="mainCont__Category"><i class="fas fa-couch"
                        aria-hidden="true"></i> 家具・インテリア</a></li>
            <li class="mainCont__CategoryLi"><a href="#" class="mainCont__Category"><i class="fas fa-desktop"
                        aria-hidden="true"></i> 家電・ガジェット</a></li>
            <li class="mainCont__CategoryLi"><a href="#" class="mainCont__Category"><i class="fas fa-pencil-ruler"
                        aria-hidden="true"></i> 生活雑貨</a></li>
            <li class="mainCont__CategoryLi"><a href="#" class="mainCont__Category"><i class="fas fa-heart"
                        aria-hidden="true"></i> 花・フラワーアレンジメント</a></li>
            <li class="mainCont__CategoryLi"><a href="#" class="mainCont__Category"><i class="fas fa-paw"
                        aria-hidden="true"></i> ペット・動物</a></li>
            <li class="mainCont__CategoryLi"><a href="#" class="mainCont__Category"><i class="fas fa-book"
                        aria-hidden="true"></i> アート・漫画・イラスト</a></li>
            <li class="mainCont__CategoryLi"><a href="#" class="mainCont__Category"><i class="fas fa-stethoscope"
                        aria-hidden="true"></i> 医療</a></li>
        </ul>
    </div>
</div>