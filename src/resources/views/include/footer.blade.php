<footer id="footer" class="footer footerCont">
    <div class="footer__Inner">
        <p class="footer__Logo">
            <img src="/assets/logo2.png" alt="BUZZLINK">
        </p>
        <p class="footer__Txt">
            インフルエンサー様と企業を結び付ける<br class="inline_s">マッチングプラットフォーム<br class="inline_s">「バズリンク」｜© 2021 BuzzLink
        </p>
    </div>
</footer>
