<!doctype html>
<html>

<head>

    <!-- Google Tag Manager -->
    <!-- Google Tag Manager -->

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>バズリンク｜インフルエンサー様と企業を結び付けるマッチングプラットフォーム</title>
    <meta name="keywords" content="" />
    <meta name="description" content="BuzzLinkはインフルエンサーと広告主となる企業が出会う接点の創出から、
関係深化までを支援する新時代のインフルエンサーマーケティングプラットフォームです。" />

    <!-- favicon -->
    <link rel="shortcut icon" href="#">
    <link rel="apple-touch-icon" href="#" />

    <!-- ogp -->
    <meta property="og:title" content="バズリンク｜インフルエンサー様と企業を結び付けるマッチングプラットフォーム" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="#" />
    <meta property="og:image" content="#" />
    <meta property="og:site_name" content="#" />
    <meta property="og:description" content="#" />

    <!-- css -->
    <link rel="stylesheet" href="{{ asset('css/lp.css') }}">
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/slick.min.js') }}"></script>
    <script src="https://kit.fontawesome.com/c558d8d24f.js" crossorigin="anonymous"></script>
    <style>
        .matter__TitleA::before {
            content: '';
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }

        .matter__UserName {
            position: relative;
        }

        .matter__DetailTxt {
            min-height: 90px;
        }

        @media only screen and (min-device-width: 768px) and (max-width: 1112px) and (orientation: portrait) {
            .topMatter--ipad {
                min-width: 1060px;
            }
        }

        @media only screen and (min-device-width: 1024px) and (max-device-width: 1024px) and (orientation: portrait) and (-webkit-min-device-pixel-ratio: 2) {
            .topMatter--ipad {
                min-width: 1060px;
            }
        }

        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 2) {
            .topMatter--ipad {
                min-width: 1060px;
            }
        }

        @media only screen and (min-device-width: 1024px) and (max-device-width: 1366px) and (orientation: landscape) and (-webkit-min-device-pixel-ratio: 2) {
            .topMatter--ipad {
                min-width: 1060px;
            }
        }
    </style>
</head>

<body>
    <header class="header">
        <div class="header__wrap">
            <p class="header__Logo">
                <a href="/">
                    <img src="{{ asset('assets/logo.png') }}" alt="BuzzLink">
                </a>
            </p>
            <form class="searchWrap searchWrapHeader" action="{{ route('search') }}" method="GET">
                <div class="searchWrap__SearchTxtWrap">
                    <p class="searchWrap__SearchSelectTitle">
                        案件をさがす
                    </p>
                </div>
                <div class="searchWrap__SearchHeaderKeywordWrap">
                    <input placeholder="検索キーワード" class="searchWrap__SearchKeyword" value="" name="keyword">
                    <button type="submit" class="searchWrap__SearchKeywordBtn"></button>
                </div>
            </form>
            <div class="searchWrap__Sp block_s">
                <p id="slidetoggle_button"><i class="fas fa-search"></i></p>
            </div>
            <div class="header__Btns">
                <a class="header__BtnLogin" href="{{route('login.page')}}">
                    ログイン
                </a>
                <a class="header__BtnMemberRegistration" href="{{route('email.verification_page')}}">
                    無料会員登録
                </a>
            </div>
        </div>
        <div id="slidetoggle_menu" class="searchWrap__SpFormWrap">
            <form class="searchWrap__SpForm" action="{{ route('search') }}" method="GET">
                <input placeholder="検索キーワード" class="searchWrap__SpFormKeyword" value="" name="keyword">
                <button type="submit" class="searchWrap__SpFormKeywordBtn"></button>
            </form>
        </div>
    </header>
    @yield('content')

    <footer class="topFooter">
        <p class="topFooter_Logo">
            <img src="{{ asset('assets/logo2.png') }}" alt="BuzzLink">
        </p>
        <p class="topFooter_Txt">
            インフルエンサー様と企業を結び付ける<br class="inline_s">マッチングプラットフォーム「バズリンク」｜&copy; 2021 BuzzLink<br><a
                href="{{ route('guide') }}" target="_blank" class="link">特定商取引法に基づく表記</a>
        </p>
    </footer>

    <!-- JS -->
    <script src="{{ asset('js/lp.js') }}"></script>
    <script>
        $(function () {
            $("#slidetoggle_button").on("click", function () {
                $("#slidetoggle_menu").slideToggle();
                $("#slidetoggle_menu").toggleClass("active");
            });
        });
    </script>
</body>

</html>