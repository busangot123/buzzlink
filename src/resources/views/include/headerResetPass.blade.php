<header class="header">
    <div class="header__wrap">
    <p class="header__Logo">
        @include('include.headerLogo')
    </p>
        <div class="header__Btns">
            <a class="header__BtnLogin" href="{{route('login.page')}}">
                ログイン
            </a>
            <a class="header__BtnMemberRegistration" href="{{route('email.verification_page')}}">
                無料会員登録
            </a>
        </div>
    </div>
</header>
