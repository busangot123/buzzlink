<header class="header">
    <div class="header__wrap">
        <p class="header__Logo">
            @include('include.headerLogo')
        </p>
        <form action="{{ route('search') }}" method="GET" class="searchWrap searchWrapHeader">
            <div class="searchWrap__SearchTxtWrap">
                <p class="searchWrap__SearchSelectTitle">
                    案件をさがす
                </p>
            </div>
            <div class="searchWrap__SearchHeaderKeywordWrap">
                <input placeholder="検索キーワード" name="keyword" class="searchWrap__SearchKeyword">
                <input type="hidden" name="category" value="{{Request::get('category')}}">
                <input type="hidden" name="sns"
                    value="{{ is_array(Request::get('sns')) ? join(',',Request::get('sns')) : Request::get('sns') }}">
                <input type="hidden" name="min_reward" value="{{Request::get('min_reward')}}">
                <input type="hidden" name="max_reward" value="{{Request::get('max_reward')}}">
                <input type="hidden" name="sort" value="{{Request::get('sort')}}">
                <button type="submit" class="searchWrap__SearchKeywordBtn" />
            </div>
        </form>
        <div class="searchWrap__Sp {{ Auth::check() ? 'searchWrap__Sp2' : '' }} block_s">
            <p id="slidetoggle_button"><i class="fas fa-search" aria-hidden="true"></i></p>
        </div>
        @auth
        @include('include.content.user')
        @endauth

        @guest
        <div class="header__Btns">
            <a href="{{route('login.page')}}" class="header__BtnLogin">
                ログイン
            </a>
            <a href="{{route('email.verification_page')}}" class="header__BtnMemberRegistration">
                無料会員登録
            </a>
        </div>
        @endguest
    </div>
    <div id="slidetoggle_menu" class="searchWrap__SpFormWrap">
        <form class="searchWrap__SpForm" action="{{ route('search') }}" method="GET">
            <input placeholder="検索キーワード" class="searchWrap__SpFormKeyword" value="" name="keyword">
            <button type="submit" class="searchWrap__SpFormKeywordBtn"></button>
        </form>
    </div>
</header>