<ul class="header__IconsList">
    <li class="header__IconsLi">
        <span class="header__Icon header__IconUser" href="#">
        </span>
        <div class="header__balloon header__balloonUser">
            <p class="header__balloonTitle">{{ Auth::user()->name }}</p>
            <ul>
                <li class="header__balloonList">
                    <a
                        href="{{ route(Auth::user()->default_role == 1 ? 'profile.client' : 'profile.influencer', ['uuid' => Auth::user()->uuid]) }}">
                        プロフィールを表示
                    </a>
                </li>
                <li class="header__balloonList">
                    <a href="{{ route('mypage.edit_information') }}">
                        設定
                    </a>
                </li>
                <li class="header__balloonList">
                    <a href="{{ route('logout') }}">
                        ログアウト
                    </a>
                </li>
            </ul>
        </div>
    </li>
    <li class="header__IconsLi">
        <span class="header__Icon header__IconMessage" href="#">
            @if (headerMessage()['count'] > 0)
            <span class="header__IconOn">{{ headerMessage()['count'] }}</span>
            @endif
        </span>
        <div class="header__balloon header__balloonMessage">
            <p class="header__balloonTitle">メッセージ</p>
            <ul>
                @foreach(headerMessage()['latest'] as $msg)
                <a href="{{ route('chat.room', ['project_user_id' => $msg->project_user_id]) }}" target="_blank">
                    <li class="header__balloonList">
                        <span class="header__ballonName">
                            {{ $msg->sentUser->name }}
                            <!-- massage sent user name -->
                        </span>
                        {{ \Illuminate\Support\Str::limit($msg->content, 30) }}
                        <!-- message content -->
                        <span class="header__ballonDate">
                            {{ japaneseDateFormat($msg->create_at) }}
                            <!-- message date-->
                        </span>
                    </li>
                </a>
                @endforeach
            </ul>
            <a class="header__ballonLink" href="{{route('chat.messages')}}" target="_blank">
                <i class="far fa-envelope"></i> 全てのメッセージを見る
            </a>
        </div>
    </li>
    <li class="header__IconsLi">
        <span class="header__Icon header__IconNotification">
            @if (headerNotification()['count'] > 0)
            <span class="header__IconOn">{{ headerNotification()['count'] }}</span>
            @endif
        </span>
        <div class="header__balloon header__balloonNotification">
            <p class="header__balloonTitle">お知らせ</p>
            <!-- からメッセージ  -->
            <!--<p>メッセージを受信すると、ここに表示されます。</p>-->
            <!-- からメッセージ  -->
            <ul>
                @foreach(headerNotification()['latest'] as $notif)
                <li class="header__balloonList">
                    <a href="{{ route('notification.read', ['id' => $notif->id]) }}">
                        {{ \Illuminate\Support\Str::limit($notif->notification_contents, 30) }}
                        <!--value.notification_contents-->
                        <span class="header__ballonDate">
                            {{ japaneseDateFormat($notif->notification_at) }}
                            <!-- value.notification_at -->
                        </span>
                    </a>
                </li>
                @endforeach
            </ul>
            <a class="header__ballonLink" href="{{route('notification.list')}}">
                <i class="fas fa-bell"></i> 全てのお知らせを見る
            </a>
        </div>
    </li>
    <li class="header__IconsLi">
        <span class="header__Icon header__IconNotice">
            @if (headerInformation()['is_user_info_read'] == 0)
            <span class="header__IconOn"></span>
            @endif
        </span>
        <div class="header__balloon header__balloonNotice">
            <p class="header__balloonTitle">運営からお知らせ</p>
            <!-- からメッセージ  -->
            <!--<p>メッセージを受信すると、ここに表示されます。</p>-->
            <!-- からメッセージ  -->
            <ul>
                @foreach(headerInformation()['info'] as $info)
                <li class="header__balloonList">
                    <a href="{{ route('information.show_details', ['info_id' => $info->id]) }}">
                        {{ \Illuminate\Support\Str::limit(strip_tags($info->contents), 30) }}
                        <span class="header__ballonDate">
                            {{ japaneseDateFormat($info->notification_at) }}
                            <!-- value.notification_at -->
                        </span>
                    </a>
                </li>
                @endforeach
            </ul>
            <a href="{{route('information.index')}}" class="header__ballonLink"><i class="far fa-flag"></i>
                全ての運営からお知らせを見る</a>
        </div>
    </li>

    <li class="header__IconsLi">
        <span class="header__Icon header__IconHelp" href="#">
        </span>
        <div class="header__balloon header__balloonHelp">
            <p class="header__balloonTitle">ヘルプ</p>
            <!-- からメッセージ  -->
            <!--<p>メッセージを受信すると、ここに表示されます。</p>-->
            <!-- からメッセージ  -->
            <ul>
                <li class="header__balloonList">
                    <a href="#">
                        ご利用ガイド
                    </a>
                </li>
                <li class="header__balloonList">
                    <a href="{{ route('faq.faq_page') }}">
                        よくある質問
                    </a>
                </li>

            </ul>
</ul>
<p class="header__bk-balloon"></p>
