import axios from "axios";
var promise;

export default class GenreService {

    list() {
        promise = axios.get('/genre_list');
        return promise;
    }

}
