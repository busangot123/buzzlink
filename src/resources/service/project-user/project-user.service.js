import axios from "axios";
var promise;

export default class ProjectUserService {
  list(filter) {
    promise = axios.get(
      `/project-user?page=${filter.page}&status=${filter.status}`
    );
    return promise;
  }

  store(data) {
    promise = axios.post("/project-user", data);
    return promise;
  }

  update(id, data) {
    promise = axios.put("/project-user/" + id, data);
    return promise;
  }

  show(id) {
    promise = axios.get(`/project-user/${id}`);
    return promise;
  }

  delete(id) {
    promise = axios.delete(`/project-user/${id}`);
    return promise;
  }

  checkSelfpromo(data) {
    promise = axios.post("/check-self-promo", data);
    return promise;
  }

  checkReportDetails(data) {
    promise = axios.post("/check-report-details", data);
    return promise;
  }

  applicationStatusCount(data) {
    promise = axios.post("/get-application-status-count", data);
    return promise;
  }

  statusCount() {
    promise = axios.get("/project-users-status-count");
    return promise;
  }

  getOtherProjectUsers(id) {
    promise = axios.post("/get-other-project-users", id);
    return promise;
  }

  getProjectUser(id) {
    promise = axios.post("/get-project-user", id);
    return promise;
  }

  getProjectUserDetails(id) {
    promise = axios.get("/get-project-user-details?project_id=" + id);
    return promise;
  }

  getCompletedProjectUserDetails(id) {
    promise = axios.post("/get-completed-project-user-details", id);
    return promise;
  }

  updateApplication(data) {
    promise = axios.post("/update-application", data);
    return promise;
  }
}
