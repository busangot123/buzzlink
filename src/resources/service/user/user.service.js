import axios from "axios";
var promise;

export default class UserService {
  getUser() {
    promise = axios.get("/init");
    return promise;
  }

  getUserByUuid(uuid) {
    promise = axios.get(`/get-profile/${uuid}`);
    return promise;
  }

  update(data, headers = null) {
    promise = axios.post(`/user/update`, data, headers);
    return promise;
  }

  updateInformation(data) {
    promise = axios.post("/update-user-informations", data);
    return promise;
  }

  updatePassword(data) {
    promise = axios.post("/update-user-password", data);
    return promise;
  }

  influencerDetails() {
    promise = axios.get("/get-user-influencer-details");
    return promise;
  }

  clientDetails() {
    promise = axios.get("/get-user-client-details");
    return promise;
  }
}
