import axios from "axios";

export default class SnsInfoService {
  listSnsInfo() {
    return axios.get("sns_info");
  }
  createSnsInfo(payload) {
    return axios.post("sns_info", payload);
  }
  updateSnsInfo(payload) {
    return axios.patch("sns_info", payload);
  }
  deleteSnsInfo(payload) {
    const { id, type } = { ...payload };
    return axios.delete(`sns_info?id=${id}&type=${type}`);
  }
}
