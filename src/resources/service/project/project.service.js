import axios from "axios";
var promise;

export default class ProjectService {

    getProject(id) {
        promise = axios.get(`/get-project?project_id=${id}`);
        return promise;
    }

}
