import axios from "axios";
let promise;

export default class StripeService {
  projectsSelectionPayment(data) {
    promise = axios.post("/selection/stripe_payment", data, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    });
    return promise;
  }
}
