import axios from "axios";
var promise;

export default class UserCategoryService {

    list() {
        promise = axios.get('/user-categories');
        return promise;
    }

    store(data) {
        promise = axios.post('/user-categories', data);
        return promise;
    }

    update(id, data) {
        promise = axios.put('/user-categories', data);
        return promise;
    }

    show(id) {
        promise = axios.get(`/user-categories/${id}`);
        return promise;
    }

    delete(id) {
        promise = axios.delete(`/user-categories/${id}`);
        return promise;
    }

}
