import axios from "axios";
var promise;

export default class NotificationService {

    list() {
        promise = axios.get('/get-notifications');
        return promise;
    }

    store(data) {
        promise = axios.post('/get-notifications', data);
        return promise;
    }

    update(id, data) {
        promise = axios.put('/get-notifications', data);
        return promise;
    }

    show(id) {
        promise = axios.get(`/get-notifications/${id}`);
        return promise;
    }

    delete(id) {
        promise = axios.delete(`/get-notifications/${id}`);
        return promise;
    }

}
