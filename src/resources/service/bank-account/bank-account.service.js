import axios from "axios";
var promise;

export default class BankAccountService {

    list() {
        promise = axios.get('/bank-account');
        return promise;
    }

    store(data) {
        promise = axios.post('/bank-account', data);
        return promise;
    }

    update(id, data) {
        promise = axios.put('/bank-account', data);
        return promise;
    }

    show(id) {
        promise = axios.get(`/bank-account/${id}`);
        return promise;
    }

    delete(id) {
        promise = axios.delete(`/bank-account/${id}`);
        return promise;
    }

}
