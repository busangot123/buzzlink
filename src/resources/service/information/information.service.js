import axios from "axios";
var promise;

export default class InformationService {

    list() {
        promise = axios.get('/get-informations');
        return promise;
    }

    store(data) {
        promise = axios.post('/get-informations', data);
        return promise;
    }

    update(id, data) {
        promise = axios.put('/get-informations', data);
        return promise;
    }

    show(id) {
        promise = axios.get(`/get-informations/${id}`);
        return promise;
    }

    delete(id) {
        promise = axios.delete(`/get-informations/${id}`);
        return promise;
    }

}
