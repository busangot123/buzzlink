import axios from "axios";
var promise;

export default class PrefectureService {

    list() {
        promise = axios.get('/prefectures');
        return promise;
    }

}
