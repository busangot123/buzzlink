$(function () {
  $('.header__IconUser').on('click', function () {
    $(".header__balloonUser").fadeIn();
    $(".header__bk-balloon").fadeIn();
  })
  $('.header__bk-balloon').on('click', function () {
    $(".header__balloonUser,.header__bk-balloon").fadeOut();
  });
  $('.header__IconMessage').on('click', function () {
    $(".header__balloonMessage").fadeIn();
    $(".header__bk-balloon").fadeIn();
  })
  $('.header__bk-balloon').on('click', function () {
    $(".header__balloonMessage,.header__bk-balloon").fadeOut();
  });
  $('.header__IconNotification').on('click', function () {
    $(".header__balloonNotification").fadeIn();
    $(".header__bk-balloon").fadeIn();
  })
  $('.header__bk-balloon').on('click', function () {
    $(".header__balloonNotification,.header__bk-balloon").fadeOut();
  });
  $('.header__IconNotice').on('click', function () {
    $(".header__balloonNotice").fadeIn();
    $(".header__bk-balloon").fadeIn();
  })
  $('.header__bk-balloon').on('click', function () {
    $(".header__balloonNotice,.header__bk-balloon").fadeOut();
  });
  $('.header__IconHelp').on('click', function () {
    $(".header__balloonHelp").fadeIn();
    $(".header__bk-balloon").fadeIn();
  })
  $('.header__bk-balloon').on('click', function () {
    $(".header__balloonHelp,.header__bk-balloon").fadeOut();
  });
});
//
$(function () {
  $(".faq__RightBox dt").on("click", function () {
    $(this).next().slideToggle();
  });
});
$(function() {
  let tabs = $(".message__UserWrap");
  $(".message__UserWrap").on("click", function () {
    $(".active").removeClass("active");
    $(this).addClass("active");
    const index = tabs.index(this);
    $(".message__Box").removeClass("show").eq(index).addClass("show");
  })
})
$(function () {
  $('.js-modal-open').on('click', function () {
    $('.js-modal').fadeIn();
    return false;
  });
  $('.js-modal-close').on('click', function () {
    $('.js-modal').fadeOut();
    return false;
  });
});

$(function () {
  $('.topSpnav_button').click(function () {
    $(this).toggleClass('active');
    $('.topSpnav_bg').fadeToggle();
    $('nav').toggleClass('open');
  })
  $('.topSpnav_bg').click(function () {
    $(this).fadeOut();
    $('.topSpnav_button').removeClass('active');
    $('nav').removeClass('open');
  });
})

$(function () {
  $("#slidetoggle_button").on("click", function () {
    $("#slidetoggle_menu").slideToggle();
    $("#slidetoggle_menu").toggleClass("active");
  });
});

$(function () {
  const textarea = $(".msg");
  const placeholderDiv = $("#placeholderDiv");
  textarea.on('keyup change', function () {
    let letterLength = textarea.val().length;
    if (letterLength !== 0) {
      placeholderDiv.addClass('none');
    } else {
      placeholderDiv.removeClass('none');
    }
  });
});

$(function () {
  const textarea = $(".msg");
  const placeholderDiv = $("#placeholderDiv");
  let letterLength = textarea.length == 0 ? 0 : textarea.val().length;
  if (letterLength !== 0) {
    placeholderDiv.addClass('none');
  } else {
    placeholderDiv.removeClass('none');
  }
});

function checkBoxSize() {
  var e = document.createElement("div");
  var defHeight = e.offsetHeight;
  if (defHeight != e.offsetHeight) {
    func();
    defHeight = e.offsetHeight;
  }
}

$.get("https://holidays-jp.github.io/api/v1/date.json", function (holidaysData) {
  var numberOfMonths;
  if (window.matchMedia && window.matchMedia('(max-device-width: 640px)').matches) {
    numberOfMonths = 1;
  } else {
    numberOfMonths = 2;
  }

  $("#input-datepicker").datepicker({
    dateFormat: 'yy/mm/dd',
    minDate: "+1d",
    maxDate: "+6m",
    numberOfMonths: numberOfMonths,
    beforeShowDay: function (date) {
      if (date.getDay() == 0) {
        return [true, 'day-sunday', null];
      } else if (date.getDay() == 6) {
        return [true, 'day-saturday', null];
      }

      var holidays = Object.keys(holidaysData);
      for (var i = 0; i < holidays.length; i++) {
        var holiday = new Date(Date.parse(holidays[i]));
        if (holiday.getYear() == date.getYear() &&
          holiday.getMonth() == date.getMonth() &&
          holiday.getDate() == date.getDate()) {
          return [true, 'day-holiday', null];
        }
      }
      return [true, 'day-weekday', null];
    }
  });
});

$(function () {
  $('#input-datepicker').change(function (e) {
    var year = $('.deadline_year');
    var month = $('.deadline_month');
    var day = $('.deadline_day');
    var formattedDate = new Date(e.target.value);
    var d = formattedDate.getDate();
    var m = formattedDate.getMonth() + 1;
    var y = formattedDate.getFullYear();

    var year_plus = $('.deadline_year_weeks');
    var month_plus = $('.deadline_month_weeks');
    var day_plus = $('.deadline_day_weeks');
    var formattedDateWeeks = new Date(e.target.value);
    formattedDateWeeks.setDate(formattedDateWeeks.getDate() + 14)
    var dp = formattedDateWeeks.getDate();
    var mp = formattedDateWeeks.getMonth() + 1;
    var yp = formattedDateWeeks.getFullYear();

    year.empty();
    month.empty();
    day.empty();
    year_plus.empty();
    month_plus.empty();
    day_plus.empty();

    year.append(y + "年");
    if (m < 10) {
      month.append("0" + m + "月");
    } else {
      month.append(m + "月");
    }

    if (d < 10) {
      day.append("0" + d + "日");
    } else {
      day.append(d + "日");
    }

    year_plus.append(y + "年");
    if (mp < 10) {
      month_plus.append("0" + mp + "月");
    } else {
      month_plus.append(mp + "月");
    }

    if (dp < 10) {
      day_plus.append("0" + dp + "日");
    } else {
      day_plus.append(dp + "日");
    }
  });
});

$(function () {
  var year = $('.deadline_year');
  var month = $('.deadline_month');
  var day = $('.deadline_day');
  var formattedDate = new Date($('#input-datepicker').val());
  var d = formattedDate.getDate();
  var m = formattedDate.getMonth() + 1;
  var y = formattedDate.getFullYear();

  var year_plus = $('.deadline_year_weeks');
  var month_plus = $('.deadline_month_weeks');
  var day_plus = $('.deadline_day_weeks');
  var formattedDateWeeks = new Date($('#input-datepicker').val());
  formattedDateWeeks.setDate(formattedDateWeeks.getDate() + 14)
  var dp = formattedDateWeeks.getDate();
  var mp = formattedDateWeeks.getMonth() + 1;
  var yp = formattedDateWeeks.getFullYear();

  year.empty();
  month.empty();
  day.empty();
  year_plus.empty();
  month_plus.empty();
  day_plus.empty();

  year.append(y + "年");
  if (m < 9) {
    month.append("0" + m + "月");
  } else {
    month.append(m + "月");
  }

  if (d < 9) {
    day.append("0" + d + "日");
  } else {
    day.append(d + "日");
  }

  year_plus.append(y + "年");
  if (mp < 9) {
    month_plus.append("0" + mp + "月");
  } else {
    month_plus.append(mp + "月");
  }

  if (dp < 9) {
    day_plus.append("0" + dp + "日");
  } else {
    day_plus.append(dp + "日");
  }
});

$(function () {
  if (document.getElementById("text-editor")) {
    $('#character_count').html($('#text-editor').val().length)
  }
  $('#text-editor').keyup(function () {
    var input_str;
    var text_input;
    var output_html = "";
    var counter;

    input_str = $('#text-editor').val();
    text_input = input_str.trim();
    if (text_input.length > 0) {
      for (counter = 0; counter < text_input.length; counter++) {
        switch (text_input[counter]) {
          case '\n':
            output_html += "<br>";
            break;

          case ' ':
            if (text_input[counter - 1] != ' ' && text_input[counter - 1] != '\t')
              output_html += " ";
            break;

          case '\t':
            if (text_input[counter - 1] != '\t')
              output_html += " ";
            break;

          default:
            output_html += text_input[counter];
        }
      }
    }

    $('#character_count').html($('#text-editor').val().length)
    document.getElementById('html-output').value = output_html;
  })
});

$(function () {
  var footerId = "footer";
  function checkFontSize(func) {
    var e = document.createElement("div");
    var s = document.createTextNode("S");
    e.appendChild(s);
    e.style.visibility = "hidden";
    e.style.position = "absolute";
    e.style.top = "0";
    document.body.appendChild(e);
    var defHeight = e.offsetHeight;

    function checkBoxSize() {
      if (defHeight != e.offsetHeight) {
        func();
        defHeight = e.offsetHeight;
      }
    }
    setInterval(checkBoxSize, 1000);
  }

  
  function footerFixed() {
    var dh = document.getElementsByTagName("body")[0].clientHeight;
    document.getElementById(footerId).style.top = "0px";
    var ft = document.getElementById(footerId).offsetTop;
    var fh = document.getElementById(footerId).offsetHeight;
    if (window.innerHeight) {
      var wh = window.innerHeight;
    } else if (document.documentElement && document.documentElement.clientHeight != 0) {
      var wh = document.documentElement.clientHeight;
    }

    if (ft + fh < wh) {
      document.getElementById(footerId).style.position = "relative";
      document.getElementById(footerId).style.top = (wh - fh - ft) + "px";
    }
    setInterval(checkBoxSize, 1000)
  }


  function addEvent(elm, listener, fn) {
    try {
      elm.addEventListener(listener, fn, false);
    } catch (e) {
      elm.attachEvent("on" + listener, fn);
    }
  }
  if (!$(".footer__fixed")[0]) {
    addEvent(window, "load", footerFixed);
    addEvent(window, "load", function () {
      checkFontSize(footerFixed);
    });
    addEvent(window, "resize", footerFixed);
  }
  addEvent(window, "load", function () {
    $("#sidebarSP > option").each(function() {
      var currURL = window.location.href;
      if (currURL.indexOf(this.value) > -1) { 
        this.selected = 'selected';
      }
    });
  });
})

// $(function(){
//   $(".switch").click(function(){
//   if($(this).hasClass("clicked")){
//     $(this).removeClass("clicked");
//     $(".on").removeClass("on2");
//     $(".off").removeClass("off2");
//   }
//   else{
//     $(this).addClass("clicked");
//     $(".on").addClass("on2");
//     $(".off").addClass("off2");
//   }
//   });
// });
