require("./bootstrap");
require("./script");
require("jquery");
require("jquery-ui-bundle");

import AppMixin from "./app.mixin";

import Axios from "axios";
Axios.defaults.baseURL = "/api";

window.Vue = require("vue").default;
import VueChatScroll from "vue-chat-scroll";
Vue.use(VueChatScroll);

import VueRouter from "vue-router";
import routes from "./routes";
Vue.use(VueRouter);

import { ValidationObserver, ValidationProvider } from "./util/vee-validator";
Vue.component("ValidationProvider", ValidationProvider);
Vue.component("ValidationObserver", ValidationObserver);

import VueCryptojs from "crypto-js";
Vue.use(VueCryptojs);

import VueEllipsis from "vue-ellipsis";
Vue.use(VueEllipsis);

Vue.use(require("vue-moment"));
Vue.mixin(AppMixin);

import LoadScript from "vue-plugin-load-script";
Vue.use(LoadScript);
Vue.loadScript("https://www.instagram.com/embed.js");

import RegisterInfo from "./components/guest/RegisterInfo";
import ProjectManagement from "./components/project/Management";
import WorkManagementPage from "./components/user/JobManagement";
import SelectionForm from "./components/project/selection/Form";
import SelectionPayment from "./components/project/selection/Payment";
import EditUser from "./components/user/EditUser";
import ProjectApplicationDetail from "./components/user/apply-project/ProjectApplicationDetail";
import ProjectStore from "./components/user/ProjectStore";
import Chat from "./components/user/Chat";

const app = new Vue({
  el: "#app",
  router: new VueRouter(routes),
  components: {
    "register-info-form": RegisterInfo,
    "project-management-form": ProjectManagement,
    "selection-form": SelectionForm,
    "selection-payment": SelectionPayment,
    "work-management-page": WorkManagementPage,
    "edit-information": EditUser,
    "project-application-detail": ProjectApplicationDetail,
    "project-store": ProjectStore,
    "chat-room": Chat
  }
});
