import Home from "./components/guest/Home";
import Login from "./components/guest/Login";
import Search from "./components/user/Search";
import MyPage from "./components/user/MyPage";
import Notice from "./components/user/Notice";
import Project from "./components/user/Project";
import JobManagement from "./components/user/JobManagement";
import ApplyProject from "./components/user/apply-project/ApplyProject";
import ConfirmApplyProject from "./components/user/apply-project/ConfirmProject";
import DoneApplyProject from "./components/user/apply-project/DoneProject";
import ResultDetails from "./components/user/job-management/ResultDetails";
import DoneDetails from "./components/user/job-management/DoneDetails";
import SelectionDetails from "./components/user/job-management/SelectionDetails";
import ChangeEmail from "./components/user/ChangeEmailPrompt";
import PageNotFound from "./components/guest/PageNotFound";
import Management from "./components/project/Management";
import Requesting from "./components/project/Requesting";
import Chat from "./components/user/Chat";

export default {
  mode: "history",
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 };
  },
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    },
    {
      path: "/email_change/success",
      name: "Change Email",
      component: ChangeEmail
    },
    {
      path: "/search",
      name: "Search Project",
      component: Search
    },
    {
      path: "/project_form/add",
      name: "project",
      component: Project
    },
    {
      path: "/mypage",
      name: "MyPage",
      component: MyPage
    },
    {
      path: "/mypage/project_management",
      name: "project management",
      component: Management
    },
    {
      path: "/mypage/project_management/requesting",
      name: "Project Requesting",
      component: Requesting
    },
    {
      path: "/notifications",
      name: "Notice",
      component: Notice
    },
    {
      path: "/project/apply_start",
      name: "apply.project",
      component: ApplyProject,
      props: true
    },
    {
      path: "/project/apply_confirm",
      name: "confirm.project",
      component: ConfirmApplyProject,
      props: true
    },
    {
      path: "/project/apply_done",
      name: "done.project",
      component: DoneApplyProject,
      props: true
    },
    {
      path: "/mypage/work_management",
      name: "job-management",
      component: JobManagement
    },
    {
      path: "/work_management/checking",
      name: "job-management-result-details",
      component: ResultDetails
    },
    {
      path: "/work_management/done",
      name: "job-management-done-details",
      component: DoneDetails
    },
    {
      path: "/work_management/unselected",
      name: "job-management-selection-details",
      component: SelectionDetails
    },
    {
      path: "/project_messages/:id",
      name: "project_user_message",
      component: Chat
    },
    {
      path: "*",
      name: "Page Not Found",
      component: PageNotFound
    }
  ]
};
