import moment from 'moment';
import Config from '../config/constant';
const config = Config;

export default {

    methods: {
        youtubeParser(url) {
            var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
            var match = url.match(regExp);
            return (match && match[7].length == 11) ? `http://youtube.com/embed/${match[7]}` : false;
        },

        instagramParser(url) {
            var url = url.endsWith('/') ? url : `${url}/`;
            return `${url}embed`;
        },

        facebookParser(url) {
            return `https://www.facebook.com/plugins/post.php?href=${url}&show_text=true&width=500`;
        },

        tiktokParser(url) {
            var url = url.endsWith('/') ? url.slice(0, -1) : url;
            return `http://www.tiktok.com/embed/${url.split('/').pop()}`
        },

        twitterParser(url) {
            return `https://twitframe.com/show?url=${url}`
        },

        getSocialDetails(url) {
            if (url != undefined) {
                if (url.includes('youtube.com')) return this.youtubeParser(url);
                if (url.includes('instagram.com')) return this.instagramParser(url);
                if (url.includes('facebook.com')) return this.facebookParser(url);
                if (url.includes('twitter.com')) return this.twitterParser(url);
                if (url.includes('tiktok.com')) return this.tiktokParser(url);
            } else {
                return ''
            }

        },

        checkStatusValue(id) {
            if (id == 1) return '募集終了まで'
            if (id == 2) return '選定終了まで'
            if (id == 3) return '完了報告期限まで'
            if (id == 4) return '結果確認終了まで'
            return ''
                // if(id == 5) return '募集終了まで'
                // if(id == 6) return '募集終了まで'
        },

        modifyDate(date) {
            return moment(date).format("YY/DD/MM h:mm:ss");
        },

        modifyDateWithAddedDays(days, date) {
            return moment(date).add('days', days).format('YY/DD/MM h:mm:ss');
        },

        getClientRate(data) {
            if (data == undefined || data.length < 1) {
                return '-';
            }
            var sum = data.map((o) => {
                return o.influencer_eva == 1 ? 5 : 1;
            }).reduce((a, c) => { return a + c });
            return (sum / data.length).toFixed(3)
        },

        getInfluencerRate(data) {
            if (data == undefined || data.length < 1) {
                return '-';
            }
            var sum = data.map((o) => {
                return o.client_eva == 1 ? 5 : 1;
            }).reduce((a, c) => { return a + c });
            return (sum / data.length).toFixed(3)
        },
        checkClientImage(data) {
            return data == null ? '/assets/user.png' : data;
        },
        getUrlHashParams() {
          const hash = decodeURIComponent(window.location.hash.split("#").filter(hash => hash != "")[0]);
          if (hash == 'undefined') return false;
          return hash;
        },
        getNumberFormat(number) {
          return Number(number).toLocaleString();
        },
        getErrorResponeMessages(errResponse) {
          switch (errResponse.status) {
            case config.httpCode.BadRequest:
              return [errResponse.data.message];
            case config.httpCode.UnprocessableEntity:
              let errMessages = Object.values(errResponse.data.errors);
              errMessages = errMessages.map((message) => message[0]);
              return errMessages;
            default:
              return [config.responseMessage.InternalServerError];
          }
        },
        getSnsIdLabel(sns) {
          switch (sns) {
            case 'instagram':
              return 'アカウント名（ユーザーネーム）';
            case 'facebook':
            case 'twitter':
              return 'ユーザーネーム';
            case 'tiktok':
              return 'ユーザー名';
            default:
              return 'チャンネルID';
          }
        },
        getSnsUrlLabel(sns) {
          switch (sns) {
            case 'instagram':
            case 'facebook':
            case 'twitter':
            case 'tiktok':
              return 'URL';
            default:
              return 'チャンネルURL';
          }
        },
    }
};
