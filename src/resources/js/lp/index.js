// slide
$(function() {
  $('.slide').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    //autoplay: true,
    //autoplaySpeed: 2000,
    arrows: false,
    autoplay:true,
    autoplaySpeed: 6000,
    variableWidth: true,
    responsive: [{
      breakpoint: 640,
      settings: {
      centerMode: true,
      slidesToShow: 1,
      centerPadding:'0',
    }}]
});
});

// fadein
$(function(){
    $(window).scroll(function (){
        $('.fadein').each(function(){
            var targetElement = $(this).offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > targetElement - windowHeight + 200){
                $(this).css('opacity','1');
                $(this).css('transform','translateY(0)');
            }
        });
    });
});


