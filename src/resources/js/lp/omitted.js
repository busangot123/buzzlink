// 案件タイトル
$(function() {
   var count = 30;
 $('.matter__Title').each(function() {
     var thisText = $(this).text();
      var textLength = thisText.length;
       if (textLength > count) {
          var showText = thisText.substring(0, count);
          var insertText = showText += '…';
          $(this).html(insertText);
      };
  });
});

// 概要テキスト
$(function() {
   var count = 100;
 $('.matter__DetailTxt').each(function() {
     var thisText = $(this).text();
      var textLength = thisText.length;
       if (textLength > count) {
          var showText = thisText.substring(0, count);
          var insertText = showText += '…';
          $(this).html(insertText);
      };
  });
});