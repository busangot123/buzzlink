$(function () {
	$('.topSpnav_button').click(function () {
		$(this).toggleClass('active');
		$('.topSpnav_bg').fadeToggle();
		$('nav').toggleClass('open');
	})
	$('.topSpnav_bg').click(function () {
		$(this).fadeOut();
		$('.topSpnav_button').removeClass('active');
		$('nav').removeClass('open');
	});
})