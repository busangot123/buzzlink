import { ValidationObserver, ValidationProvider, extend, localize } from 'vee-validate'
import * as originalRules from 'vee-validate/dist/rules';
import custom from './message.json';
import * as customeRules from './custome-rules'

for (let rule in originalRules) {
  extend(rule, {
    ...originalRules[rule],
  });
}

for (let rule in customeRules) {
  extend(rule, {
    ...customeRules[rule]
  })
}

localize('custom', custom);

export {
  ValidationObserver,
  ValidationProvider
}
