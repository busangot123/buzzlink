const between_string = {
  params: ['min', 'max'],
  validate(value, { min, max }) {
    return value.length >= min & value.length <= max
  }
};

const contain_alpha_numeric = {
  validate(value) {
    return /^(?=.*?[a-zA-Z])(?=.*?\d).+$/.test(value)
  }
}
const alpha_numeric_symbol = {
  validate(value) {
    return /^[a-zA-Z0-9!-/:-@¥[-`{-~]+$/.test(value)
  }
}



const password_confirmed = {
  params: [
    {
      name: 'password',
      isTarget: true
    }
  ],
  validate(value, { password }) {
    return value === password
  }
}

const katakana = {
  validate(value) {
    return /^[ァ-ヶー 　・]+$/.test(value)
  }
}


export {
  between_string,
  contain_alpha_numeric,
  alpha_numeric_symbol,
  password_confirmed,
  katakana,
}
