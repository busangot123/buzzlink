function persistedRefresh() {
  window.onpageshow = function (event) {
    if (event.persisted) {
      window.location.reload();
    }
  };
}

export {
  persistedRefresh
}
