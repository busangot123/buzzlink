
import ValidationMessage from './validation-message';
import ValidationError from './validation-error';

export default class ValidationMethod {

  constructor(attribute, input) {
    this.input = input;
    this.message = new ValidationMessage(attribute);
  }

  /**
   *
   * @param {Error} e
   * @returns string
   */
  static getValidationErrorMessage(e) {
    if (e instanceof ValidationError) {
      return e.message;
    } else {
      throw e;
    }
  }

  required() {
    if (!this.input || !this.input.match(/\S/g)) {
      throw new ValidationError(this.message.required())
    }
  }

  containAlphaNumeric() {
    if (!/^(?=.*?[a-zA-Z])(?=.*?\d).+$/.test(this.input)) {
      throw new ValidationError(this.message.containAlphaNumeric())
    }
  }

  onlyAlpheNumericSymbol() {
    if (!/^[a-zA-Z0-9!-/:-@¥[-`{-~]+$/.test(this.input)) {
      throw new ValidationError(this.message.onlyAlpheNumericSymbol())
    }
  }

  /**
   *
   * @param {number} maxNumber
   */
  max(maxNumber) {
    if (this.input.length > maxNumber)
      throw new ValidationError(this.message.max(maxNumber))
  }

  /**
   *
   * @param {number} start
   * @param {number} end
   */
  between(start, end) {
    if (this.input.length < start || this.input.length > end)
      throw new ValidationError(this.message.between(start, end))
  }

  /**
   * length check for password
   */
  betweenPassword() {
    const start = 8;
    const end = 32;
    if (this.input.length < start || this.input.length > end)
      throw new ValidationError(this.message.between(start, end))
  }

  /**
   *
   * validate confirm password, password_confirm
   * @param {string} password
   */
  confirmedPassword(password) {
    if (this.input !== password)
      throw new ValidationError(this.message.confirmedPassword())
  }

  /**
   * validate only katakana and space, ・
   */
  katakana() {
    if (!/^[ァ-ヶー 　・]+$/.test(this.input)) {
      throw new ValidationError(this.message.katakana())
    }
  }

  /**
   * validate email
   */
  email() {
    if (!/^([\w!#$%&'*+\-\/=?^`{|}~]+(\.[\w!#$%&'*+\-\/=?^`{|}~]+)*|"([\w!#$%&'*+\-\/=?^`{|}~. ()<>\[\]:;@,]|\\[\\"])+")@(([a-zA-Z\d\-]+\.)+[a-zA-Z]+|\[(\d{1,3}(\.\d{1,3}){3}|IPv6:[\da-fA-F]{0,4}(:[\da-fA-F]{0,4}){1,5}(:\d{1,3}(\.\d{1,3}){3}|(:[\da-fA-F]{0,4}){0,2}))\])$/.test(this.input)) {
      throw new ValidationError(this.message.email())
    }
  }
}
