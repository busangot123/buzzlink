export default class ValidationMessage {

  constructor(attribute) {
    this.attribute = attribute;
  }

  static staticRequired(attribute) {
    return attribute + "は必須です。";
  }

  static staticRequiredCheckbox() {
    return "利用規約、個人情報保護方針に同意されていません。同意する場合は、チェックを入れてください。";
  }

  required() {
    return this.attribute + "は必須です。";
  }

  /**
   *
   * @param {number} maxNumber
   * @returns  string
   */
  max(maxNumber) {
    return this.attribute + `は${maxNumber}文字以内で入力してください。`
  }
  /**
   *
   * @param {number} start
   * @param {number} end
   * @returns string
   */
  between(start, end) {
    return this.attribute + `は${start}~${end}文字で入力してください`;
  }

  /**
   *
   * @param {number} start
   * @param {number} end
   * @returns string
   */
  betweenNumeric(start, end) {
    return this.attribute + `は${start}~${end}文字で入力してください`;
  }

  /**
   *
   * @returns validation message to countain alphabet and numberic
   */
  containAlphaNumeric() {
    return this.attribute + `は英数字の両方を含む必要があります。`;
  }

  /**
   *
   * @returns validation message to countain alphabet and numberic
   */
  onlyAlpheNumericSymbol() {
    return this.attribute + `は半角英数字で入力してください。`;
  }

  /**
   *
   * @returns validation message katakana
   */
  katakana() {
    return this.attribute + `は全角カタカナで入力してください。`;
  }

  /**
   *
   * @returns validation message email
   */
  email() {
    return `メールアドレスの形式に誤りがあります。`;
  }

  /**
   *
   * @returns validation confirm password
   */
  confirmedPassword() {
    return `入力パスワードと一致していません。`;
  }
}
