
import moment from "moment";

function japaneseDate(datetime) {
  const date = new Date(datetime);
  const dateFormatted = moment(String(date)).format("YYYY年MM月DD日");

  return dateFormatted;
}

function slashDate(datetime) {
  return moment(datetime).format("YYYY/MM/DD");
}

function slashDateTime(datetime) {
  return moment(datetime).format("YYYY/MM/DD HH:mm:ss");
}

function mysqlDateTime(datetime) {
  const date = new Date(datetime);
  const dateFormatted = moment(String(date)).format(
    "YYYY-MM-DD HH:mm:ss"
  );

  return dateFormatted;
}

function spectifiedFormatDate(datetime, format) {
  const date = new Date(datetime);
  const dateFormatted = moment(String(date)).format(
    format
  );

  return dateFormatted;
}

export {
  japaneseDate,
  slashDate,
  slashDateTime,
  mysqlDateTime,
  spectifiedFormatDate
}
