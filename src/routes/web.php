<?php

use Illuminate\Support\Facades\Route;

Route::prefix('/')->group(function () {
    Route::get('password/reset/complete', 'Auth\ResetPasswordController@resetComplete')->name('password.complete');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

    Route::get('register/confirm/{email}', 'Auth\RegisterController@confirmPage')->name('register.confirm_page');
    Route::get('register/complete', 'Auth\RegisterController@completePage')->name('register.complete_page');

    Route::get('email/verification', 'Auth\SignUpController@emailVerificationView')->name('email.verification_page');
    Route::post('email/verification', 'Auth\SignUpController@sendEmailVerification')->name('email.send_verification');

    Route::get('search', 'ProjectController@search')->name('search');
    Route::get('details/{project_id}', 'ProjectController@searchDetails')->name('project.details');
    Route::get('guide', 'UserController@getCommercialTransactionsPage')->name('guide');

    Route::middleware('guest')->group(function () {
        Route::get('login', 'Auth\LoginController@showLoginForm')->name('login.page');
        Route::post('login', 'Auth\LoginController@login')->name('login.post');
        Route::get('/', function () {
            return view('top');
        })->name('home');
        Route::get('top_influencer', 'ProjectController@getRecommendedProjects')->name('top.top_influencer');
        Route::get('top_client', 'ProjectController@getRecommendedProjects')->name('top.top_client');
    });

    Route::get('email/reset/complete', 'Auth\ResetEmailController@resetComplete')->name('email.reset_complete_page');
    Route::get('email/reset/{token}', 'Auth\ResetEmailController@verify')->name('email.reset_page');
    Route::middleware('signed')->group(function () {
        Route::get('register/{email}', 'Auth\RegisterController@infoPage')->name('register.info_page');
        Route::post('register/{email}', 'Auth\RegisterController@confirm')->name('register.confirm');
        Route::patch('register/{email}', 'Auth\RegisterController@setUserInfo')->name('register.complete');
    });

    Route::get('download/project_attachment/{id}', 'DownloadController@downloadProjectAttachment')->name('download.project_attachment');
    Route::get('download/message_attachment/{id}', 'DownloadController@downloadMessageAttachment')->name('download.message_attachment');
    Route::get('faq', 'FaqController@index')->name('faq.faq_page');
    Route::get('faq/search', 'FaqController@search')->name('faq.search');
    Route::get('faq/{title_id}', 'FaqController@searchTitleId')->name('faq.search_title_id');

    Route::middleware('auth')->group(function () {
        Route::prefix('project')->group(function () {
            Route::prefix('recruitment')->group(function () {
                Route::get('/', 'ProjectController@storeForm')->name('project.store.form');
                Route::post('/', 'ProjectController@storeFormPost')->name('project.store.form.post');
                Route::get('confirm', 'ProjectController@storeConfirm')->name('project.store.confirm');
                Route::post('confirm', 'ProjectController@storeConfirmPost')->name('project.store.confirm.post');
                Route::get('complete', 'ProjectController@storeComplete')->name('project.store.complete');
            });
        });
        Route::middleware('applied.user')->prefix('project/{project_id}')->group(function () {
            Route::get('application', 'ProjectUserController@projectUserApplication')->name('project.application');
            Route::post('user_confirmation', 'ProjectUserController@projectUserconfirmation')->name('project.confirmation');
            Route::get('confirmation', 'ProjectUserController@showProjectUserConfirmation')->name('project.confirmation.page');
        });
        Route::get('project/{project_id}/complete', 'ProjectUserController@showProjectUserComplete')->name('project.done');

        Route::prefix('project/edit/{project_id}')->group(function () {
            Route::get('application', 'ProjectUserController@workUserApplication')->name('project.edit.application');
            Route::get('confirmation', 'ProjectUserController@showWorkUserConfirmation')->name('project.edit.confirmation.page');
            Route::post('user_confirmation', 'ProjectUserController@workUserConfirmation')->name('project.edit.confirmation');
            Route::get('complete', 'ProjectUserController@showWorkUserComplete')->name('project.edit.done');
        });

        Route::prefix('project/edit')->group(function () {
            Route::post('decline', 'ProjectUserController@declineApplication')->name('project.edit.decline');
            Route::post('update', 'ProjectUserController@updateApplication')->name('project.edit.update');
        });

        Route::prefix('mypage')->group(function () {
            Route::get('/', 'UserController@myPageDetails')->name('mypage');
            Route::get('influencer', 'UserController@myPageInfluencer')->name('mypage.influencer');
            Route::get('client', 'UserController@myPageClient')->name('mypage.client');
            Route::get('work_management', 'ProjectUserController@getWorkManagement')->name('mypage.work_management');
            Route::get('project_management', 'ProjectController@projectManagementPage')->name('mypage.project_management');
            Route::get('edit_information', 'UserController@myPageEditInformation')->name('mypage.edit_information');
            Route::prefix('project_management')->group(function () {
                Route::middleware('has.client.evaluation')->prefix('checking/{project_user_id}')->group(function () {
                    Route::get('review', 'ProjectManagementController@checking')->name('project_management.checking.input');
                    Route::post('confirm_evaluation', 'ProjectManagementController@confirmEvaluation')->name('project_management.client.confirmation');
                    Route::get('confirmation', 'ProjectManagementController@confirmation')->name('project_management.checking.confirmation');
                    Route::post('submit', 'ProjectManagementController@updateProjectUserClientEvaluation')->name('project_management.checking.store');
                });
                Route::middleware('project.selection.auth')->prefix('selection')->group(function () {
                    Route::get('form/{project_id}', 'SelectionController@form')->name('mypage.project_management.selection.form');
                    Route::get('payment/{project_id}', 'SelectionController@payment')->name('mypage.project_management.selection.payment');
                    Route::get('done/{project_id}', 'SelectionController@done')->name('mypage.project_management.selection.done');
                    Route::post('confirm', 'SelectionController@confirm')->name('mypage.project_management.selection.confirm');
                    Route::post('complete', 'SelectionController@complete')->name('mypage.project_management.selection.complete');
                });
                Route::get('requesting/{project_user_id}', 'ProjectManagementController@requestingUser')->name('project_management.requesting');
                Route::get('complete/{project_user_id}', 'ProjectManagementController@completeClientEvaluation')->name('project_management.checking.done');
                Route::get('done/{project_user_id}', 'ProjectManagementController@showDoneProjectUser')->name('project_management.done');
            });

            Route::prefix('work_management')->group(function () {
                Route::get('details/{project_id?}', 'ProjectUserController@workManagementDetails')->name('workmanagement.details');
                Route::middleware('has.influencer.evaluation')->prefix('requesting/{project_id}')->group(function () {
                    Route::get('review', 'WorkManagementController@checking')->name('work_management.review');
                    Route::post('confirm_evaluation', 'WorkManagementController@confirmEvaluation')->name('work_management.influencer.confirmation');
                    Route::get('confirmation', 'WorkManagementController@confirmation')->name('work_management.requesting.confirmation');
                    Route::post('submit', 'WorkManagementController@updateProjectUserInfluencerEvaluation')->name('work_management.requesting.store');
                });

                Route::get('complete/{project_user_id}', 'WorkManagementController@completeInfluencerEvaluation')->name('work_management.done');
                Route::get('checking/{project_id}', 'WorkManagementController@showCheckingEvaluation')->name('work_management.checking');
                Route::get('done/{project_id}', 'WorkManagementController@showInfluencerEvaluation')->name('work_management.complete');
            });
        });

        Route::get('payment', 'PaymentManagementController@influencerPaymentList')->name('payment.list');
        Route::get('logout', 'Auth\LoginController@logout')->name('logout');

        Route::get('project_form/{any}', function () {
            return view('index');
        });
        Route::get('information', function () {
            return view('index');
        });

        Route::get('information', 'InformationController@index')->name('information.index');
        Route::get('information/{info_id}', 'InformationController@showInformation')->name('information.show_details');

        Route::get('notifications', 'NotificationController@index')->name('notification.list');
        Route::get('project_messages', 'ChatController@showMessages')->name('chat.messages');

        Route::middleware('authorize.message')->get('project_messages/{project_user_id}', 'ChatController@fetchMessages')->name('chat.room');
        Route::get('project_messages/redirect/{project_id}', 'ChatController@getProjectUserId')->name('chat.room.project');

        Route::prefix('profile')->group(function () {
            Route::get('/{uuid}/influencer', 'UserController@getInfluencerProfile')->name('profile.influencer');
            Route::get('/{uuid}/client', 'UserController@getClientProfile')->name('profile.client');
        });

        Route::get('profile/{uuid}', function () {
            return view('index');
        })->name('user.profile');
    });
    Route::get('project/{any}/{id}', function () {
        return view('index');
    });
});
