<?php

use Illuminate\Support\Facades\Route;

Route::post('store', 'UserController@register')->name('user.store');
Route::get('pwreset', 'UserController@resetPassword');
Route::get('genre_list', 'GenreController@getGenre');
Route::get('search_details', 'ProjectController@projectDetails');
Route::get('search_details/project_application', 'ProjectController@confirmProjectApplication');
Route::get('init', 'UserController@init');

Route::middleware('guest')->group(function () {
    Route::post('auth', 'UserController@login');
});

Route::middleware('user')->group(function () {
    Route::post('logout', 'UserController@logout');
    Route::post('email/reset', 'Api\Auth\ResetEmailController@sendResetEmail');
    Route::get('rates', 'ProjectController@rates');
    Route::get('user/{id}', 'UserController@getProjectApplicant');
    Route::get('reporting_user', 'ProjectUserController@getReportingUser');
    Route::post('project_form', 'ProjectController@store');
    Route::get('project_list/{params}', 'ProjectController@projectManagement');
    Route::post('client_evaluation', 'ProjectUserController@updateClientRating');
    Route::get('bank_list', 'BankAccountController@getBanks');
    Route::get('branch_list', 'BankAccountController@getBranch');
    Route::post('update-user-informations', 'UserController@updateUserInformations');
    Route::post('update-user-password', 'UserController@updateUserPassword');
    Route::post('check-self-promo', 'ProjectUserController@checkSelfPromo');
    Route::post('check-report-details', 'ProjectUserController@checkReportDetails');
    Route::get('project-users-status-count', 'ProjectUserController@getProjectUserCount');
    Route::get('get-project', 'ProjectController@getProject');
    Route::post('get-application-status-count', 'ProjectUserController@getApplicationStatusCount');
    Route::get('get-profile/{uuid}', 'UserController@getUserByUuid'); // get user by uuid
    Route::post('get-other-project-users', 'ProjectUserController@getOtherProjectUsers');
    Route::post('get-project-user', 'ProjectUserController@getProjectUsers');
    Route::get('get-project-user-details', 'ProjectUserController@getProjectUserDetails');
    Route::post('get-completed-project-user-details', 'ProjectUserController@getCompletedProjectUsers');
    Route::post('update-application', 'ProjectUserController@updateApplication');
    Route::get('get-user-influencer-details', 'UserController@influencerDetails');
    Route::get('get-user-client-details', 'UserController@clientDetails');
    Route::post('register_project_user', 'ProjectUserController@store')->name('store.project.user');
    Route::get('update_notification', 'NotificationController@readNotification')->name('notification.read');
    Route::post('update_client_review', 'ProjectManagementController@updateClientReview')->name('update.client.review');
    Route::post('user/update', 'UserController@updateProfile');
    Route::post('selection/stripe_payment', 'SelectionController@stripe')->name('project.selection.payment.stripe');
    Route::post('update_email_reject', 'UserController@updateEmailReject');
    Route::get('mypage/project_management', 'ProjectController@projectManagementPage');

    Route::prefix('sns_info')->group(function () {
        Route::get('/', 'SnsInfoController@listSnsOfUser')->name('snsinfo.list');
        Route::post('/', 'SnsInfoController@storeSnsInfo')->name('snsinfo.store');
        Route::patch('/', 'SnsInfoController@updateSnsInfo')->name('snsinfo.update');
        Route::delete('/', 'SnsInfoController@deleteSnsInfo')->name('snsinfo.delete');
    });

    Route::resources([
        'get-informations' => 'InformationController',
        'users' => 'UserController',
        'user-categories' => 'UserCategoryController',
        'bank-account' => 'BankAccountController',
        'project-user' => 'ProjectUserController',
    ]);

    Route::get('project_messages_all/{project_user_id?}', 'ChatController@fetchAllUserConversation');
    Route::get('seen_messages/{project_user_id}', 'ChatController@seenMessages');
    Route::get('project_messages/{project_user_id}', 'ChatController@fetchMessagesApi');
    Route::post('project_messages', 'ChatController@sendMessage');
});
