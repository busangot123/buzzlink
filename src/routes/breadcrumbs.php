<?php

use Diglactic\Breadcrumbs\Breadcrumbs;
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;

Breadcrumbs::for('home', function (BreadcrumbTrail $trail) {
    $trail->push('TOP', route('mypage'));
});

Breadcrumbs::for('project_details', function (BreadcrumbTrail $trail, $project_title) {
    $trail->parent('home');
    $trail->push('案件をさがす', route('search'));
    $trail->push($project_title);
});

Breadcrumbs::for('project_details_search', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push('案件をさがす', route('search'));
});

Breadcrumbs::for('profile', function (BreadcrumbTrail $trail, $user_name) {
    $trail->parent('home');
    $trail->push($user_name);
});

Breadcrumbs::for('management_details', function (BreadcrumbTrail $trail, $route) {
    $trail->push('仕事管理を表示する', $route);
});

Breadcrumbs::for('project_application', function (BreadcrumbTrail $trail, $project_title) {
    $trail->push('仕事管理へ戻る', route('mypage.project_management'));
    $trail->push('案件をさがす', route('search'));
    $trail->push($project_title);
});

Breadcrumbs::for('project_application_edit', function (BreadcrumbTrail $trail, $project_title, $route) {
    $trail->push('仕事管理へ戻る', route('mypage.work_management'));
    $trail->push($project_title, $route);
});
