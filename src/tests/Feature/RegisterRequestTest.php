<?php

namespace Tests\Feature;

use App\Http\Requests\RegisterRequest;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Tests\TestCase;

/**
 * @internal
 * @coversNothing
 */
class RegisterRequestTest extends TestCase
{
    use WithFaker;

    /**
     * A basic test example.
     *
     * @dataProvider dataproviderKatakana
     *
     * @param mixed $item
     * @param mixed $data
     * @param mixed $expect
     */
    public function testkatakana($item, $data, $expect)
    {
        $request = new RegisterRequest();
        $rules = Arr::only($request->rules(), 'full_name_kana');

        $dataList = [$item => $data];
        $validator = Validator::make($dataList, $rules);
        $result = $validator->passes();
        $this->assertEquals($expect, $result);
    }

    public function dataproviderKatakana()
    {
        for ($i = 0; $i < 10; ++$i) {
            $katakana_word = '';
            foreach (range(1, 50) as $u) {
                $lead = 'e3';
                $middle = mt_rand(82, 83);

                switch ($middle) {
                    case 82:
                        $follow = dechex(mt_rand(161, 191));

                        break;

                    case 83:
                        $follow = dechex(mt_rand(128, 182));

                        break;
                }

                $katakana_word .= hex2bin($lead.$middle.$follow);
            }
            $targetWord[] = $katakana_word;
        }

        return [
            'ランダムカタカナ1' => ['full_name_kana', $targetWord[0], true],
            'ランダムカタカナ2' => ['full_name_kana', $targetWord[1], true],
            'ランダムカタカナ3' => ['full_name_kana', $targetWord[2], true],
            'include Half width' => ['full_name_kana', 'ハンカク スペース', true],
            'include Full width' => ['full_name_kana', 'ゼンカク　スペース', true],
            'All Half width' => ['full_name_kana', '      ', false],
            'All Full width' => ['full_name_kana', '　　　　　　　', false],
        ];
    }

    /**
     * A basic test example.
     *
     * @dataProvider dataproviderPassword
     *
     * @param mixed $item
     * @param mixed $data
     * @param mixed $expect
     */
    public function testPassword($item, $data, $expect)
    {
        $request = new RegisterRequest();
        $rules = Arr::only($request->rules(), 'password');

        $dataList = [$item => $data];
        $validator = Validator::make($dataList, $rules);
        $result = $validator->passes();
        $this->assertEquals($expect, $result);
    }

    public function dataproviderPassword()
    {
        return [
            'valid password' => ['password', 'password1', true],
            'alpha_numeric_symbol' => ['password', 'password1-==!@', true],
            'alpha_symbol without numeric' => ['password', 'password-==!@', false],
            'only symbol' => ['password', '-------==!@', false],
            'without number password' => ['password', 'passworddd', false],
            'only number password' => ['password', '1234567890', false],
            'only numeric_symbol' => ['password', '1234567890!=@?', false],
        ];
    }

    /**
     * A basic test example.
     *
     * @dataProvider dataproviderValidRequest
     */
    public function testRegisterRequest(array $keys, array $values, bool $expect)
    {
        $request = new RegisterRequest();
        $dataList = array_combine($keys, $values);
        $rules = $request->rules();
        $validator = Validator::make($dataList, $rules);
        $result = $validator->passes();

        $this->assertEquals($expect, $result);
    }

    public function dataproviderValidRequest()
    {
        return [
            'valid register' => [
                ['name', 'password', 'full_name', 'full_name_kana', 'phone_number', 'default_role'],
                ['testuser', 'password01', 'てすとゆーざー', 'テストユーザー', '00000000000', 1],
                true,
            ],
            'invalid register null name' => [
                ['name', 'password', 'full_name', 'full_name_kana', 'phone_number', 'default_role'],
                ['', 'password01', 'てすとゆーざー', 'テストユーザー', '00000000000', 1],
                false,
            ],
        ];
    }
}
