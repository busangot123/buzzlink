<?php

namespace Tests\Feature;

use App\Http\Controllers\Auth\ResetPasswordController;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use ReflectionClass;
use Tests\TestCase;

/**
 * @internal
 * @coversNothing
 */
class PasswordRequestTest extends TestCase
{
    /**
     * @dataProvider dataproviderPassword
     *
     * @param mixed $item
     * @param mixed $data
     * @param mixed $expect
     */
    public function testPassword($item, $data, $expect)
    {
        // use protected validation method
        $reset_password_controller = new ResetPasswordController();
        $reflection = new ReflectionClass($reset_password_controller);
        $method = $reflection->getMethod('rules');
        $method->setAccessible(true);

        $rules = Arr::only($method->invoke($reset_password_controller), 'password');
        $dataList = array_combine($item, $data);
        $validator = Validator::make($dataList, $rules);
        $result = $validator->passes();
        $this->assertEquals($expect, $result);
    }

    public function dataproviderPassword()
    {
        return [
            'null password' => [['password', 'password_confirmation'], [null, null], false],
            'valid password' => [['password', 'password_confirmation'], ['password1', 'password1'], true],
            'valid password alpha_numeric_symbol' => [['password', 'password_confirmation'], ['password1!=@', 'password1!=@'], true],
            'different password_confirmation' => [['password', 'password_confirmation'], ['password1', 'different1'], false],
            'without number password' => [['password', 'password_confirmation'], ['passworddd', 'passworddd'], false],
            'only number password' => [['password', 'password_confirmation'], ['1234567890', '1234567890'], false],
            'only symbol password' => [['password', 'password_confirmation'], ['@!!!!=-*())', '@!!!!=-*())'], false],
            'include space password' => [['password', 'password_confirmation'], ['password 1', 'password 1'], false],
        ];
    }
}
