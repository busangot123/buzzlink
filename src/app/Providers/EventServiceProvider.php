<?php

namespace App\Providers;

use App\Events\ApplicationDeadlineExpiredEvent;
use App\Events\BatchDeadlineExpired;
use App\Events\BatchDeadlineReminder;
use App\Events\BatchMessageUnreadReminder;
use App\Events\CompleteEvaluation;
use App\Events\InfluencerEvaluation;
use App\Events\ProjectRecruitmentComplete;
use App\Events\ProjectSelection;
use App\Events\ProjectUserApplied;
use App\Listeners\SendApplicationDeadlineExpiredNotification;
use App\Listeners\SendCheckingNotification;
use App\Listeners\SendCompletionDeadlineExpiredNotification;
use App\Listeners\SendCompletionDeadlineReminderNotification;
use App\Listeners\SendInfluencerCompleteReviewNotification;
use App\Listeners\SendMessageUnreadReminder;
use App\Listeners\SendPasswordResetCompleteNotification;
use App\Listeners\SendProjectRecruitmentCompleteNotification;
use App\Listeners\SendProjectSelectionNotification;
use App\Listeners\SendProjectUserAppliedNotification;
use App\Listeners\SendRegisteredNotification;
use App\Listeners\SendReportDeadlineExpiredNotification;
use App\Listeners\SendReportDeadlineReminderNotification;
use App\Listeners\SendSelectionExpiredNotification;
use App\Listeners\SendSelectionReminderNotification;
use App\Models\FacebookAccounts;
use App\Models\InstagramAccounts;
use App\Models\TiktokAccounts;
use App\Models\TwitterAccounts;
use App\Models\YoutubeAccounts;
use App\Observers\AuthorObserver;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendRegisteredNotification::class,
        ],
        PasswordReset::class => [
            SendPasswordResetCompleteNotification::class,
        ],
        ProjectRecruitmentComplete::class => [
            SendProjectRecruitmentCompleteNotification::class,
        ],
        ProjectUserApplied::class => [
            SendProjectUserAppliedNotification::class,
        ],
        CompleteEvaluation::class => [
            SendCheckingNotification::class,
        ],
        InfluencerEvaluation::class => [
            SendInfluencerCompleteReviewNotification::class,
        ],
        ProjectSelection::class => [
            SendProjectSelectionNotification::class,
        ],
        ApplicationDeadlineExpiredEvent::class => [
            SendApplicationDeadlineExpiredNotification::class,
        ],
        BatchDeadlineExpired::class => [
            SendSelectionExpiredNotification::class,
            SendReportDeadlineExpiredNotification::class,
            SendCompletionDeadlineExpiredNotification::class,
        ],
        BatchDeadlineReminder::class => [
            SendSelectionReminderNotification::class,
            SendReportDeadlineReminderNotification::class,
            SendCompletionDeadlineReminderNotification::class,
        ],
        BatchMessageUnreadReminder::class => [
            SendMessageUnreadReminder::class,
        ],
    ];

    /**
     * Register any events for your application.
     */
    public function boot()
    {
        YoutubeAccounts::observe(AuthorObserver::class);
        TiktokAccounts::observe(AuthorObserver::class);
        TwitterAccounts::observe(AuthorObserver::class);
        FacebookAccounts::observe(AuthorObserver::class);
        InstagramAccounts::observe(AuthorObserver::class);
    }
}
