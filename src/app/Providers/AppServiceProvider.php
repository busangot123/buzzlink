<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Monolog\Processor\UidProcessor;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register()
    {
        $this->app->singleton(UidProcessor::class, function ($app) {
            return new UidProcessor(10);
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
    }
}
