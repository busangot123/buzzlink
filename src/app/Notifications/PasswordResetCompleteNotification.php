<?php

namespace App\Notifications;

use App\Helpers\Constant;
use App\Models\MailTemplate;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PasswordResetCompleteNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $template = Constant::PASSWORD_RESET_SUCCESS;
    public $user;
    /**
     * Create a new notification instance.
     *
     * @param mixed $notifiable
     */

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $template = MailTemplate::findByTemplateId($this->template);
        $mail = new MailMessage();
        $mail->from(config('mail.from.address'), config('mail.from.name'));
        $mail->subject($template->name);

        $email = $notifiable->getEmailForPasswordReset();
        $username = User::where('email', $email)->first()->name;
        $login_url = route('login.page');
        $app_url = config('app.url');

        $output = preg_replace_callback(
            '~\{\{(.*?)\}\}~',
            function ($key) use ($username, $app_url, $login_url) {
                $email_variable['username'] = $username;
                $email_variable['login_url'] = $login_url;
                $email_variable['app_url'] = $app_url;

                return $email_variable[$key[1]];
            },
            htmlspecialchars_decode($template->content)
        );
        $mail->view('mails.email_template', ['template' => $output]);

        return $mail;
    }
}
