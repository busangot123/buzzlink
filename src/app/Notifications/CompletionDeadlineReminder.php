<?php

namespace App\Notifications;

use App\Helpers\Constant;
use App\Helpers\MailFormat;
use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class CompletionDeadlineReminder extends Notification implements ShouldQueue
{
    use Queueable;
    private $project;
    private $days_left;
    private $project_user;
    private $template = Constant::REVIEW_DEADLINE_REMIND;

    /**
     * Create a new notification instance.
     *
     * @param mixed $project
     * @param mixed $project_user
     * @param mixed $days_left
     */
    public function __construct($project, $project_user, $days_left)
    {
        $this->project = $project;
        $this->days_left = $days_left;
        $this->project_user = $project_user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail_variables = [
            'username' => $notifiable->name,
            'influencer_name' => $this->project_user->user->name,
            'project_url' => route('project_management.checking.input', ['project_user_id' => $this->project_user->id]),
            'project_title' => $this->project->title,
            'completion_deadline' => japaneseDateFormatAddDays($this->project->report_deadline_at, 14),
            'remaining_days' => '残り'.$this->days_left.'日',
        ];

        $template = MailTemplate::findByTemplateId($this->template);
        $mail = new MailMessage();
        $mail->from(config('mail.from.address'), config('mail.from.name'));
        $mail->subject(MailFormat::decodeTemplate($mail_variables, $template->name));
        $output = MailFormat::decodeTemplate($mail_variables, $template->content);
        $mail->view('mails.email_template', ['template' => $output]);

        $log_message = "Project ID:{$this->project->id}, title:{$this->project->title} sent completion deadline reminder via batch.";
        Log::info($log_message);

        return $mail;
    }
}
