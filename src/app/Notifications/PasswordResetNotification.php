<?php

namespace App\Notifications;

use App\Helpers\Constant;
use App\Models\MailTemplate;
use App\Models\User;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PasswordResetNotification extends ResetPassword implements ShouldQueue
{
    use Queueable;

    public $template = Constant::PASSWORD_RESET;
    public $user;
    /**
     * Create a new notification instance.
     *
     * @param mixed $notifiable
     */

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $template = MailTemplate::findByTemplateId($this->template);
        $mail = new MailMessage();
        $mail->from(config('mail.from.address'), config('mail.from.name'));
        $mail->subject($template->name);

        $email = $notifiable->getEmailForPasswordReset();
        $username = User::where('email', $email)->first()->name;
        $reset_url = route('password.reset', ['token' => $this->token, 'email' => $email]);
        $app_url = config('app.url');

        $output = preg_replace_callback(
            '~\{\{(.*?)\}\}~',
            function ($key) use ($username, $email, $app_url, $reset_url) {
                $email_variable['username'] = $username;
                $email_variable['reset_url'] = $reset_url;
                $email_variable['email'] = $email;
                $email_variable['app_url'] = $app_url;

                return $email_variable[$key[1]];
            },
            htmlspecialchars_decode($template->content)
        );
        $mail->view('mails.email_template', ['template' => $output]);

        return $mail;
    }
}
