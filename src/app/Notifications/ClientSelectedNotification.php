<?php

namespace App\Notifications;

use App\Helpers\Constant;
use App\Helpers\MailFormat;
use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ClientSelectedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $data = [];
    public $template = Constant::SELECTION_DONE_CLIENT;

    /**
     * Create a new notification instance.
     *
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $project = $this->data->project;
        $project_user = $this->data->project_user;
        $mail_variables = [
            'username' => auth()->user()->name,
            'project_name' => $project->title,
            'influencer_name' => $project_user ? $project_user->user->name : '',
            'project_path' => route('project.details', ['project_id' => $project->id, 'limit' => Constant::LIST_LIMIT]),
            'report_deadline' => japaneseDateFormat($project->report_deadline_at),
            'checking_deadline' => japaneseDateFormatAddDays($project->report_deadline_at, 14),
            'host' => config('app.url'),
        ];

        $template = MailTemplate::findByTemplateId($this->template);
        $mail = new MailMessage();
        $mail->from(config('mail.from.address'), config('mail.from.name'));
        $mail->subject(MailFormat::decodeTemplate($mail_variables, $template->name));

        $output = MailFormat::decodeTemplate($mail_variables, $template->content);
        $mail->view('mails.email_template', ['template' => $output]);

        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
        ];
    }
}
