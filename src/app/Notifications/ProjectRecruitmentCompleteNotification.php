<?php

namespace App\Notifications;

use App\Helpers\Constant;
use App\Helpers\MailFormat;
use App\Models\MailTemplate;
use App\Models\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ProjectRecruitmentCompleteNotification extends Notification implements ShouldQueue
{
    use Queueable;
    private $template_id = Constant::PROJECT_REGISTER;
    private $mail_data;

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail(Project $notifiable)
    {
        $data = $this->getEmailData($notifiable);
        $template = MailTemplate::findByTemplateId($this->template_id);
        $mail = new MailMessage();
        $mail->from(config('mail.from.address'), config('mail.from.name'));
        $mail->subject(MailFormat::decodeTemplate($data, $template->name));

        $output = MailFormat::decodeTemplate($data, $template->content);
        $mail->view('mails.email_template', ['template' => $output]);

        return $mail;
    }

    private function getEmailData(Project $project): array
    {
        return [
            'client_name' => $project->user->name,
            'project_title' => $project->title,
            'project_url' => route('project.details', $project->id),
            'project_application_deadline_date' => japaneseDateFormat($project->application_deadline_at),
            'project_selection_deadline_date' => japaneseDateFormat($project->selection_deadline_at),
            'app_url' => config('app.url'),
        ];
    }
}
