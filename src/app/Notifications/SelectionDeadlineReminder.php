<?php

namespace App\Notifications;

use App\Helpers\Constant;
use App\Helpers\MailFormat;
use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class SelectionDeadlineReminder extends Notification implements ShouldQueue
{
    use Queueable;
    private $project;
    private $days_left;
    private $template = Constant::SELECTION_DEADLINE_REMIND;

    /**
     * Create a new notification instance.
     *
     * @param mixed $project
     * @param mixed $days_left
     */
    public function __construct($project, $days_left)
    {
        $this->project = $project;
        $this->days_left = $days_left;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail_variables = [
            'username' => $notifiable->name,
            'project_url' => route('mypage.project_management.selection.form', ['project_id' => $this->project->id]),
            'project_title' => $this->project->title,
            'selection_date' => japaneseDateFormat($this->project->selection_deadline_at),
            'remaining_days' => '残り'.$this->days_left.'日',
        ];
        Log::info($mail_variables['remaining_days']);
        $template = MailTemplate::findByTemplateId($this->template);
        $mail = new MailMessage();
        $mail->from(config('mail.from.address'), config('mail.from.name'));
        $mail->subject(MailFormat::decodeTemplate($mail_variables, $template->name));
        $output = MailFormat::decodeTemplate($mail_variables, $template->content);
        $mail->view('mails.email_template', ['template' => $output]);

        $log_message = "Project ID:{$this->project->id}, title:{$this->project->title} sent selection deadline reminder via batch.";
        Log::info($log_message);

        return $mail;
    }
}
