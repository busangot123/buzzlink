<?php

namespace App\Notifications;

use App\Helpers\Constant;
use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AlreadyEmailVerifiedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $user;
    public $template = Constant::USER_ALREADY_EMAIL_VERIFIED;

    /**
     * Create a new notification instance.
     *
     * @param mixed $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $template = MailTemplate::findByTemplateId($this->template);
        $mail = new MailMessage();
        $mail->from(config('mail.from.address'), config('mail.from.name'));
        $mail->subject($template->name);

        $user = $this->user;
        $login_url = route('login.page');
        $password_reset_url = route('password.request');
        $app_url = config('app.url');

        $output = preg_replace_callback(
            '~\{\{(.*?)\}\}~',
            function ($key) use ($user, $app_url, $login_url, $password_reset_url) {
                $email_variable['login_url'] = $login_url;
                $email_variable['email'] = $user->email;
                $email_variable['app_url'] = $app_url;
                $email_variable['password_reset_url'] = $password_reset_url;

                return $email_variable[$key[1]];
            },
            htmlspecialchars_decode($template->content)
        );
        $mail->view('mails.email_template', ['template' => $output]);

        return $mail;
    }
}
