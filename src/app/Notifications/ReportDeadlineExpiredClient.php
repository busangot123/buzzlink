<?php

namespace App\Notifications;

use App\Helpers\Constant;
use App\Helpers\MailFormat;
use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class ReportDeadlineExpiredClient extends Notification implements ShouldQueue
{
    use Queueable;
    private $project;
    private $project_user;
    private $template = Constant::REPORT_DEADLINE_END_CLIENT;

    /**
     * Create a new notification instance.
     *
     * @param mixed $project
     * @param mixed $project_user
     */
    public function __construct($project, $project_user)
    {
        $this->project = $project;
        $this->project_user = $project_user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail_variables = [
            'username' => $notifiable->name,
            'influencer_name' => $this->project_user->user->name,
            'project_url' => route('project_management.requesting', ['project_user_id' => $this->project_user->id]),
            'project_title' => $this->project->title,
            'report_deadline' => japaneseDateFormat($this->project->report_deadline_at),
        ];

        $template = MailTemplate::findByTemplateId($this->template);
        $mail = new MailMessage();
        $mail->from(config('mail.from.address'), config('mail.from.name'));
        $mail->subject(MailFormat::decodeTemplate($mail_variables, $template->name));
        $output = MailFormat::decodeTemplate($mail_variables, $template->content);
        $mail->view('mails.email_template', ['template' => $output]);

        $log_message = "Project ID:{$this->project->id}, title:{$this->project->title} sent report deadline reminder via batch.";
        Log::info($log_message);

        return $mail;
    }
}
