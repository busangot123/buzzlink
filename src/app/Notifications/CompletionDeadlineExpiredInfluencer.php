<?php

namespace App\Notifications;

use App\Helpers\Constant;
use App\Helpers\MailFormat;
use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Log;

class CompletionDeadlineExpiredInfluencer extends Notification implements ShouldQueue
{
    use Queueable;
    private $project;
    private $template = Constant::REVIEW_DEADLINE_END_INFLUENCER;

    /**
     * Create a new notification instance.
     *
     * @param mixed $project
     */
    public function __construct($project)
    {
        $this->project = $project;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail_variables = [
            'username' => $notifiable->name,
            'client_name' => $this->project->user->name,
            'project_url' => route('work_management.complete', ['project_id' => $this->project->id]),
            'project_title' => $this->project->title,
        ];

        $template = MailTemplate::findByTemplateId($this->template);
        $mail = new MailMessage();
        $mail->from(config('mail.from.address'), config('mail.from.name'));
        $mail->subject(MailFormat::decodeTemplate($mail_variables, $template->name));
        $output = MailFormat::decodeTemplate($mail_variables, $template->content);
        $mail->view('mails.email_template', ['template' => $output]);

        $log_message = "Project ID:{$this->project->id}, title:{$this->project->title} sent completion deadline reminder via batch.";
        Log::info($log_message);

        return $mail;
    }
}
