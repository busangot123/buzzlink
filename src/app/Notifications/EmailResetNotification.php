<?php

namespace App\Notifications;

use App\Helpers\Constant;
use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EmailResetNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $template = Constant::CHANGE_EMAIL;
    public $token;

    /**
     * Create a notification instance.
     *
     * @param string $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param App\Models\EmailReset $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $template = MailTemplate::findByTemplateId($this->template);
        $mail = new MailMessage();
        $mail->from(config('mail.from.address'), config('mail.from.name'));
        $mail->subject($template->name);

        $url = route('email.reset_page', ['token' => $this->token, 'email' => $notifiable->email]);
        $app_url = config('app.url');

        $output = preg_replace_callback(
            '~\{\{(.*?)\}\}~',
            function ($key) use ($notifiable, $app_url, $url) {
                $email_variable['email_reset_url'] = $url;
                $email_variable['email'] = $notifiable->new_email;
                $email_variable['username'] = $notifiable->name;
                $email_variable['app_url'] = $app_url;

                return $email_variable[$key[1]];
            },
            htmlspecialchars_decode($template->content)
        );
        $mail->view('mails.email_template', ['template' => $output]);

        return $mail;
    }
}
