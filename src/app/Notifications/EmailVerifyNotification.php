<?php

namespace App\Notifications;

use App\Helpers\Constant;
use App\Models\MailTemplate;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\URL;

class EmailVerifyNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $template = Constant::USER_ACTIVATION;

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param object $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $template = MailTemplate::findByTemplateId($this->template);
        $mail = new MailMessage();
        $mail->from(config('mail.from.address'), config('mail.from.name'));
        $mail->subject($template->name);
        $email = $notifiable->routeNotificationFor('mail');

        $url = $this->getVerifyUrl($email);
        $app_url = config('app.url');

        $output = preg_replace_callback(
            '~\{\{(.*?)\}\}~',
            function ($key) use ($email, $app_url, $url) {
                $email_variable['verify_url'] = $url;
                $email_variable['email'] = $email;
                $email_variable['app_url'] = $app_url;

                return $email_variable[$key[1]];
            },
            htmlspecialchars_decode($template->content)
        );
        $mail->view('mails.email_template', ['template' => $output]);

        return $mail;
    }

    /**
     * create signed url  register/uuid?expires=xxx&signature=xxxx.
     *
     * @param [type] $user
     * @param mixed  $email
     *
     * @return string
     */
    protected function getVerifyUrl($email)
    {
        return URL::temporarySignedRoute(
            'register.info_page',
            now()->addDays(),
            ['email' => $email]
        );
    }
}
