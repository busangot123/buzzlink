<?php

namespace App\Rules;

use App\Models\Project;
use Illuminate\Contracts\Validation\Rule;

class CheckProjectMaximumSelection implements Rule
{
    public $project_id;

    /**
     * Create a new rule instance.
     *
     * @param mixed $project_id
     */
    public function __construct($project_id)
    {
        $this->project_id = $project_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $project = Project::find($this->project_id);
        if (count($value) > $project->request_people_number) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Maximum selection exceeded';
    }
}
