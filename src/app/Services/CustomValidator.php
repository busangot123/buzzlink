<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Arr;

class CustomValidator extends \Illuminate\Validation\Validator
{
    public function validateContainAlphaNumeric($attribute, $value, $parameters)
    {
        return preg_match('/\\A(?=.*?[a-zA-Z])(?=.*?\\d).+\\z/', $value);
    }

    public function validateAlphaNumericSymbol($attribute, $value, $parameters)
    {
        // return preg_match("/\A[a-zA-Z0-9!-/:-@¥[-`{-~]+\z/", $value);
        return preg_match('/\\A[a-zA-Z0-9\\!-\\/\\:-\\@\\¥\\[-\\`\\{-\\~]+\\z/', $value);
    }

    public function validateKatakana($attribute, $value, $parameters)
    {
        return preg_match('/\\A[ァ-ヶー 　・]+\\z/u', $value);
    }

    /**
     * for full width space.
     *
     * @param mixed $attribute
     * @param mixed $value
     * @param mixed $parameters
     */
    public function validateDoubleSpace($attribute, $value, $parameters)
    {
        $strip = trim(mb_convert_kana($value, 's', 'UTF-8'));

        if (empty($strip)) {
            return false;
        }

        return true;
    }

    public function validateReportDeadline($attribute, $value, $parameters, $validator)
    {
        $parameters = $parameters;
        $current_date = Carbon::now();

        $application_deadline_option = Arr::get($validator->getData(), $parameters[0], null);
        $selection_deadline_at = $current_date->copy()->addDays($application_deadline_option + 12);

        $report_deadline = Carbon::parse($value);

        return $report_deadline->gt($selection_deadline_at);
    }

    public function validateArraySumMax($attribute, $value, $parameters, $validator)
    {
        $target = Arr::get($validator->getData(), $parameters[0], null);
        $countTarget = $target ? sizeof($target) : 0;
        $countValue = sizeof($value);
        $max = $parameters[1];
        $value = $value;

        return $max >= $countTarget + $countValue;
    }

    public function validateGenresMax($attribute, $value, $parameters, $validator): bool
    {
        $genres_count = sizeof($value);
        $other_category = Arr::get($validator->getData(), $parameters[0], null);
        $max = $parameters[1];

        if ($other_category) {
            ++$genres_count;
        }

        return $max >= $genres_count;
    }
}
