<?php

namespace App\Services;

use App\Events\ProjectRecruitmentComplete;
use App\Exceptions\Project\OverDayException;
use App\Helpers\Constant;
use App\Http\Requests\ProjectStoreRequest;
use App\Models\Project;
use App\SharedService\FileSharedService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ProjectService
{
    private $upload_file;

    public function __construct(FileSharedService $upload_file)
    {
        $this->upload_file = $upload_file;
    }

    /**
     * upload files, get uploaded_files name and path. create upload_files_index.
     *
     * @param array $input
     */
    public function storeFormPost(ProjectStoreRequest $request): array
    {
        $input = $request->getValidatedAttributes();
        $session = $request->session()->get('project.store');
        // uploaded_files[0=>[name: path:],1=>[name: path:],2=>[name: path:]] from server session
        // uploaded_files_index[0=>0,1=>2] from input hidden
        $uploaded_files_diff = [];
        if (isset($session['uploaded_files'])) {
            $uploaded_files = $session['uploaded_files'];
            if (isset($input['uploaded_files_index'])) {
                $uploaded_index = $input['uploaded_files_index'];
            } else {
                $uploaded_index = [];
            }

            // get only those that exist in upload_files_index
            foreach ($uploaded_index as $index) {
                array_push($uploaded_files_diff, $uploaded_files[$index]);
            }
        }

        if (isset($input['attachments'])) {
            $uploaded_files = $this->upload_file->uploadFiles($input['attachments'], 'project_tmp');
            $uploaded_files_diff = array_merge($uploaded_files_diff, $uploaded_files);
        }
        // add unique value to upload_files
        foreach ($uploaded_files_diff as $index => &$value) {
            $value = array_merge($value, ['value' => $index]);
        }

        $input['attachments'] = array_fill(0, 5, null);
        $input['uploaded_files'] = $uploaded_files_diff;

        return $input;
    }

    public function storeConfirmPost(Project $project, array $genres, array $attachments): Project
    {
        $application_start_at = new Carbon($project->application_start_at);
        if ($this->isOverDay($application_start_at)) {
            throw new OverDayException();
        }

        $application_deadline_at = $application_start_at->copy()->addDays($project->application_deadline_at + 5);

        $report_deadline_carbon = Carbon::parse($project->report_deadline_at);
        $report_deadline_carbon->hour($application_start_at->hour);
        $report_deadline_carbon->minute($application_start_at->minute);
        $report_deadline_carbon->second($application_start_at->second);

        $project->report_deadline_at = $report_deadline_carbon;

        $project->client_user_id = auth()->user()->id;
        $project->status_code = Constant::PROJECT_STATUS['RECRUITING'];
        $project->application_deadline_at = $application_deadline_at;
        $project->selection_deadline_at = $application_deadline_at->copy()->addDays(7);

        DB::transaction(function () use ($project, $genres, $attachments) {
            $project->save();
            $project->projectCategory()->saveMany($genres);

            // override file path from tmp to project_attachments
            if ($attachments) {
                foreach ($attachments as $attachment) {
                    $attachment->path = $this->upload_file->moveFileDirectory($attachment->getAttributes()['path'], 'project_attachments');
                }

                $project->projectAttachment()->saveMany($attachments);
            }
        });
        event(new ProjectRecruitmentComplete($project));

        return $project;
    }

    private function isOverDay(Carbon $application_start_at): bool
    {
        $nextDay = Carbon::now()->addHours(24);

        return $application_start_at > $nextDay;
    }
}
