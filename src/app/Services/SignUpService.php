<?php

namespace App\Services;

use App\Models\User;
use App\Notifications\EmailVerifyNotification;
use Illuminate\Support\Facades\Notification;

class SignUpService
{
    private $email;

    public function __construct(string $email)
    {
        $this->email = $email;
    }

    /**
     * send register email that verification or verified.
     */
    public function sendRegisterEmail()
    {
        $user = User::where('email', $this->email)->first();
        if ($user) {
            if ($user->email_verified_at) {
                $user->sendAlreadyEmailVerifiedNotification($user);

                return;
            }
        } else {
            $user = $this->create($this->email);
        }
        Notification::route('mail', $this->email)
            ->notify(new EmailVerifyNotification())
        ;
    }

    /**
     * Create a new user before registration.
     *
     * @param array $data
     * @param mixed $email
     *
     * @return \App\Models\User
     */
    private function create($email)
    {
        return new User([
            'email' => $email,
        ]);
    }
}
