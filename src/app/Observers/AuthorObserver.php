<?php

namespace App\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AuthorObserver
{
    public function creating(Model $model)
    {
        if (Auth::user()) {
            $model->created_user = Auth::user()->id;
        } else {
            $model->created_user = 'system';
        }
    }

    public function updating(Model $model)
    {
        if (Auth::user()) {
            $model->updated_user = Auth::user()->id;
        } else {
            $model->updated_user = 'system';
        }
    }

    public function saving(Model $model)
    {
        if (Auth::user()) {
            $model->updated_user = Auth::user()->id;
        } else {
            $model->updated_user = 'system';
        }
    }
}
