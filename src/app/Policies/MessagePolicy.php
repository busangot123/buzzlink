<?php

namespace App\Policies;

use App\Models\Message;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class MessagePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     */
    public function __construct()
    {
    }

    public function view(User $user, Message $message): Response
    {
        $sent_user_id = $message->sent_user;
        $received_user_id = $message->received_user;

        $validUserFlg = in_array($user->id, [
            $sent_user_id,
            $received_user_id,
        ], true);

        if (!$validUserFlg) {
            return $this->deny();
        }

        return $this->allow();
    }
}
