<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BatchDeadlineExpired
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public $project;

    /**
     * Create a new event instance.
     *
     * @param mixed $project
     */
    public function __construct($project)
    {
        $this->project = $project;
    }
}
