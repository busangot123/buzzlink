<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ProjectUserApplied
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public $data;
    public $influencer;
    public $project_title;
    public $client;

    /**
     * Create a new event instance.
     *
     * @param mixed $data
     * @param mixed $influencer
     * @param mixed $client_id
     * @param mixed $project_title
     * @param mixed $client
     */
    public function __construct($data, $influencer, $client, $project_title)
    {
        $this->data = $data;
        $this->project_title = $project_title;
        $this->influencer = $influencer;
        $this->client = $client;
    }
}
