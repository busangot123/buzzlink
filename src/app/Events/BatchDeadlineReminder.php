<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BatchDeadlineReminder
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;
    public $project;
    public $days_left;

    /**
     * Create a new event instance.
     *
     * @param mixed $project
     * @param mixed $days_left
     */
    public function __construct($project, $days_left)
    {
        $this->project = $project;
        $this->days_left = $days_left;
    }
}
