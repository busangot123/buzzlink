<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BatchMessageUnreadReminder
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;
    public $message;

    /**
     * Create a new event instance.
     *
     * @param mixed $message
     */
    public function __construct($message)
    {
        $this->message = $message;
    }
}
