<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CompleteEvaluation
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public $data;
    public $project_user;
    public $balance;
    public $client;

    /**
     * Create a new event instance.
     *
     * @param mixed $data
     * @param mixed $influencer
     * @param mixed $balance
     * @param mixed $client
     * @param mixed $project_user
     */
    public function __construct($data, $project_user, $client, $balance)
    {
        $this->data = $data;
        $this->project_user = $project_user;
        $this->client = $client;
        $this->balance = $balance;
    }
}
