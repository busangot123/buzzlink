<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ProjectSelection
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public $project;
    public $project_user;

    /**
     * Create a new event instance.
     *
     * @param mixed $project
     * @param mixed $project_user
     */
    public function __construct($project, $project_user)
    {
        $this->project = $project;
        $this->project_user = $project_user;
    }
}
