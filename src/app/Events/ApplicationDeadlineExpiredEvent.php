<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ApplicationDeadlineExpiredEvent
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public $projects;

    /**
     * Create a new event instance.
     *
     * @param mixed $projects
     */
    public function __construct($projects)
    {
        $this->projects = $projects;
    }
}
