<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class InfluencerEvaluation
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;
    public $data;
    public $influencer;
    public $client;

    /**
     * Create a new event instance.
     *
     * @param mixed $data
     * @param mixed $influencer
     * @param mixed $client
     */
    public function __construct($data, $influencer, $client)
    {
        $this->data = $data;
        $this->influencer = $influencer;
        $this->client = $client;
    }
}
