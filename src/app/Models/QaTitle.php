<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QaTitle extends Model
{
    use HasFactory;

    protected $table = 'qa_titles';
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(QaCategory::class, 'qa_category_id', 'id');
    }

    public function contents()
    {
        return $this->hasMany(QaContent::class, 'qa_title_id', 'id')->orderBy('id', 'ASC');
    }

    public function getTitleName($title_id)
    {
        return self::where('id', $title_id)->pluck('title')->first();
    }

    public function getTitlesWithContents($keyword)
    {
        return self::whereHas('contents', function ($query) use ($keyword) {
            $query->where('question', 'LIKE', '%'.$keyword.'%')
                ->orWhere('answer', 'LIKE', '%'.$keyword.'%')
            ;
        })->with('contents', function ($query) use ($keyword) {
            $query->where('question', 'LIKE', '%'.$keyword.'%')
                ->orWhere('answer', 'LIKE', '%'.$keyword.'%')
            ;
        })->get();
    }
}
