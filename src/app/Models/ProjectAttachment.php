<?php

namespace App\Models;

use App\Traits\AuthorObservable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ProjectAttachment extends Model
{
    use HasFactory;
    use AuthorObservable;

    protected $guarded = [
        'id', 'project_id', 'created_user', 'updated_user', 'created_at', 'updated_at',
    ];

    public function getPathAttribute($value)
    {
        /** @var \Illuminate\Filesystem\FilesystemManager $disk */
        $disk = Storage::disk('s3_public');

        return $disk->url($value);
    }
}
