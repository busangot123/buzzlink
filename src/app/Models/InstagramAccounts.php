<?php

namespace App\Models;

use App\Helpers\Constant;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InstagramAccounts extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'instagram_id',
        'url',
        'followers',
        'posts',
        'created_user',
        'updated_user',
    ];

    protected $appends = ['type', 'follower_label'];

    public function getTypeAttribute()
    {
        return 'instagram';
    }

    public function getFollowerLabelAttribute()
    {
        return Constant::SNS_FOLLOWERS_OPTIONS[$this->followers];
    }

    public static function userAccounts($user_id)
    {
        return self::where('user_id', $user_id)->orderBy('id')->get(['id', 'instagram_id as sns_id', 'url', 'followers', 'posts']);
    }
}
