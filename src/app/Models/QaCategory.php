<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QaCategory extends Model
{
    use HasFactory;

    protected $table = 'qa_categories';
    protected $guarded = [];

    public function titles()
    {
        return $this->hasMany(QaTitle::class, 'qa_category_id', 'id')->orderBy('id', 'ASC');
    }

    public function getCategoriesWithTitles()
    {
        return self::with('titles')->orderBy('id', 'ASC')->get();
    }
}
