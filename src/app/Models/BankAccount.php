<?php

namespace App\Models;

use App\Traits\AuthorObservable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    use HasFactory;
    use AuthorObservable;

    protected $fillable = [
        'influencer_user_id',
        'code',
        'branch_code',
        'account_type',
        'account_number',
        'account_name',
        'is_auto_pay',
        'account_checked_at',
        'created_user',
        'updated_user',
    ];
}
