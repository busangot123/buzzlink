<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QaContent extends Model
{
    use HasFactory;

    protected $table = 'qa_contents';
    protected $guarded = [];

    public function title()
    {
        return $this->belongsTo(QaTitle::class, 'qa_title_id', 'id')->orderBy('id', 'ASC');
    }

    public function getContentByTitleId($id)
    {
        return self::where('qa_title_id', $id)->get();
    }
}
