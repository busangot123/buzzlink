<?php

namespace App\Models;

use App\Helpers\Constant;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class YoutubeAccounts extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'channel_id',
        'url',
        'followers',
        'views',
        'videos',
        'created_user',
        'updated_user',
    ];

    protected $appends = ['type', 'follower_label'];

    public function getTypeAttribute()
    {
        return 'youtube';
    }

    public function getFollowerLabelAttribute()
    {
        return Constant::SNS_FOLLOWERS_OPTIONS[$this->followers];
    }

    public static function userAccounts($user_id)
    {
        return self::where('user_id', $user_id)->orderBy('id')->get(['id', 'channel_id as sns_id', 'url', 'followers', 'views', 'videos']);
    }
}
