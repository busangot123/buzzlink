<?php

namespace App\Models;

use App\Traits\AuthorObservable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RewardBalances extends Model
{
    use HasFactory;
    use AuthorObservable;

    protected $table = 'reward_balances';
    protected $fillable = ['user_id', 'balance'];
}
