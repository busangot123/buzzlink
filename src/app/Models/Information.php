<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    use HasFactory;
    protected $table = 'informations';
    protected $privateKey = 'id';

    protected $fillable = [
        'notification_at',
        'important_flg',
        'publish_date',
        'close_date',
        'title',
        'contents',
        'is_published',
        'display_position',
    ];

    public function scopePublishedInformations($q)
    {
        $q->where('is_published', 1)
            ->whereRaw('(now() between coalesce(cast(publish_date as date),"1900-01-01") and coalesce(cast(close_date as date), now()))')
        ;

        return $q;
    }

    public function scopeDisplayPosition($q)
    {
        return $q->orderByRaw('CASE WHEN `display_position` > 0 THEN `display_position` ELSE "null" END ASC')->orderBy('notification_at', 'desc');
    }

    public function scopeOrderByUpdatedAt($q)
    {
        return $q->orderBy('updated_at', 'desc');
    }
}
