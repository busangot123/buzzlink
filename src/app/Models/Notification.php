<?php

namespace App\Models;

use App\Traits\AuthorObservable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;
    use AuthorObservable;

    protected $table = 'notifications';
    protected $fillable = [
        'user_id',
        'notification_at',
        'notification_contents',
        'destination_url',
    ];

    public function scopeUserNotifications()
    {
        return self::where('user_id', auth()->user()->id);
    }
}
