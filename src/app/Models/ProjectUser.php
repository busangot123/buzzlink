<?php

namespace App\Models;

use App\Helpers\Constant;
use App\Helpers\ProjectFormater;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProjectUser extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id',
        'influencer_user_id',
        'application_status',
        'self_promotion',
        'applied_at',
        'client_eva',
        'client_comment',
        'influencer_eva',
        'influencer_comment',
        'sns_url',
        'reported_at',
        'evaluated_at',
        'created_user',
        'updated_user',
    ];

    protected $appends = ['status_label', 'status_name', 'sns_frame_url', 'sns_type', 'parse_reported_at_in_japanese', 'project_time_details', 'project_status_label'];

    public function users()
    {
        return $this->hasMany(User::class, 'id', 'influencer_user_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'influencer_user_id', 'id');
    }

    public function hasManyGenres()
    {
        return $this->hasMany(UserCategory::class, 'user_id', 'influencer_user_id');
    }

    public function hasManyProjects()
    {
        return $this->hasMany(Project::class, 'id', 'project_id');
    }

    public function projectCategory()
    {
        return $this->hasMany(ProjectCategory::class, 'project_id', 'project_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'id', 'project_user_id');
    }

    public function getParseReportedAtInJapaneseAttribute()
    {
        return $this->reported_at ? Carbon::parse($this->reported_at)->format('Y年n月j日') : null;
    }

    public function getPublishedDateInJapaneseAttribute()
    {
        return $this->created_at ? Carbon::parse($this->created_at)->format('Y年n月j日') : null;
    }

    public function scopeCountApplied($q)
    {
        return $q->where('influencer_user_id', auth()->user()->id)->where('application_status', Constant::PROJECT_APPLICATION_STATUS['APPLIED'])->count();
    }

    public function scopeCountSelection($q)
    {
        return $q->where('influencer_user_id', auth()->user()->id)->where('application_status', Constant::PROJECT_APPLICATION_STATUS['SELECTION'])->count();
    }

    public function scopeCountWorking($q)
    {
        return $q->where('influencer_user_id', auth()->user()->id)->where('application_status', Constant::PROJECT_APPLICATION_STATUS['WORKING'])->count();
    }

    public function scopeCountChecking($q)
    {
        return $q->where('influencer_user_id', auth()->user()->id)->where('application_status', Constant::PROJECT_APPLICATION_STATUS['CHECKING'])->count();
    }

    public function scopeCountDone($q)
    {
        return $q->where('influencer_user_id', auth()->user()->id)->where('application_status', Constant::PROJECT_APPLICATION_STATUS['DONE'])->count();
    }

    public function scopeCountMissing($q)
    {
        return $q->where('influencer_user_id', auth()->user()->id)->where('application_status', Constant::PROJECT_APPLICATION_STATUS['MISSING'])->count();
    }

    public static function getProjectUserWithProject($id)
    {
        return self::whereId($id)->with(['user', 'project' => function ($q) {
            $q->with('user');
        }])->first();
    }

    public function getStatusNameAttribute()
    {
        if (1 == $this->application_status) {
            return '募集終了まで';
        }
        if (2 == $this->application_status) {
            return '選定終了まで';
        }
        if (3 == $this->application_status) {
            return '完了報告期限まで';
        }
        if (4 == $this->application_status) {
            return '結果確認終了まで';
        }
    }

    public function getStatusLabelAttribute()
    {
        return Constant::PROJECT_USER_STATUS_LABEL[$this->application_status];
    }

    public function getProjectStatusLabelAttribute()
    {
        if (in_array($this->application_status, array_keys(Constant::PROJECT_STATUS_LABEL))) {
            return Constant::PROJECT_STATUS_LABEL[$this->application_status];
        }

        return null;
    }

    public function getSnsTypeAttribute()
    {
        $type = explode('/', $this->sns_url);
        $sns_type = !$this->sns_url ?: explode('.', $type[Constant::SNS_PARTS['HOSTING']])[1];

        return 'com' == $sns_type ? 'twitter' : $sns_type;
    }

    public function getSnsFrameUrlAttribute()
    {
        $sns_parts = explode('/', $this->sns_url);
        if ($this->sns_url) {
            switch ($this->sns_type) {
                case 'instagram':
                    return 'https://'.$sns_parts[Constant::SNS_PARTS['HOSTING']].'/'.$sns_parts[Constant::SNS_PARTS['PARAMS']].'/'.$sns_parts[Constant::SNS_PARTS['POST_ID']].'/embed';

                    break;

                case 'youtube':
                    return 'https://'.$sns_parts[Constant::SNS_PARTS['HOSTING']].'/embed/'.explode('=', $sns_parts[Constant::SNS_PARTS['PARAMS']])[Constant::SNS_PARTS['PROTOCOL']];

                    break;

                case 'facebook':
                    return $this->sns_url;

                    break;

                case 'tiktok':
                    return 'https://'.$sns_parts[Constant::SNS_PARTS['HOSTING']].'/embed/'.$sns_parts[Constant::SNS_PARTS['POST_LINK']];

                    break;

                default:
                    return null;

                    break;
            }
        }
    }

    public function getProjectTimeDetailsAttribute()
    {
        $date = '';

        switch ($this->application_status) {
            case Constant::PROJECT_APPLICATION_STATUS['APPLIED']:
                $date = Carbon::parse($this->project()->first()->application_deadline_at);

                break;

            case Constant::PROJECT_APPLICATION_STATUS['SELECTION']:
                $date = Carbon::parse($this->project()->first()->selection_deadline_at);

                break;

            case Constant::PROJECT_APPLICATION_STATUS['WORKING']:
                $date = Carbon::parse($this->project()->first()->report_deadline_at);

                break;

            case Constant::PROJECT_APPLICATION_STATUS['CHECKING']:
                $date = Carbon::parse($this->project()->first()->report_deadline_at)->addDays(14);

                break;

            default:
                return null;
        }

        $hours_left = ProjectFormater::getProjectDurationFromNow($date);

        return [
            'hours_left' => $hours_left['duration'],
            'hours_left_sign' => $hours_left['sign'],
            'deadline_at' => '' == $date ? null : $date->format('n月j日').'まで',
            'published_at' => Carbon::parse($this->project()->first()->created_at)->format('Y年n月j日'),
        ];
    }

    public static function getClientProjectUserCountPerStatus($client_project_ids, $status)
    {
        return self::whereIn('project_id', $client_project_ids)->where('application_status', $status)->get()->count();
    }

    /**
     * Get project users available for project selection.
     *
     * @param int $project_id
     *
     * @return array
     */
    public static function getProjectUsersAvailableInSelection($project_id)
    {
        $project_users = self::where('project_id', $project_id)
            ->where('application_status', Constant::PROJECT_APPLICATION_STATUS['SELECTION'])
            ->with('user', function ($q) {
                $q->with(['hasManyGenres', 'projects', 'hasManyProjectUsers']);
            })
            ->get()
        ;

        return $project_users;
    }
}
