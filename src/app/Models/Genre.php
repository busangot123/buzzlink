<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    use HasFactory;

    public static function getGenreName(int $id): string
    {
        $genre = self::findOrFail($id);

        return $genre->name;
    }
}
