<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
    use HasFactory;
    protected $table = 'fee_rate';

    /**
     * Get the total payments for project selection process.
     *
     * @param array $selected_users
     * @param int   $reward
     *
     * @return object
     */
    public static function computeSelectionPayment($selected_users, $reward)
    {
        $fee = self::find(1);
        $total_payment = $reward * count($selected_users);
        $payment = new \stdClass();
        $payment->total = $total_payment < $fee->min_rate ? $fee->min_rate : $total_payment;
        $payment->total_with_fee = $payment->total * ($fee->fee_rate / 100);
        $payment->total_with_tax = ceil(($payment->total + $payment->total_with_fee) * (Tax::find(1)->tax_rate / 100));
        $payment->total_to_pay = ceil($payment->total + $payment->total_with_fee + $payment->total_with_tax);

        return $payment;
    }
}
