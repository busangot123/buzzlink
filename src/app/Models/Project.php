<?php

namespace App\Models;

use App\Helpers\Constant;
use App\Helpers\ProjectFormater;
use App\Notifications\ProjectRecruitmentCompleteNotification;
use App\Traits\AuthorObservable;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Project extends Model
{
    use HasFactory;
    use SoftDeletes;
    use AuthorObservable;
    use Notifiable;
    protected $table = 'projects';
    protected $primaryKey = 'id';
    protected $softDelete = true;

    protected $guarded = [
        'id',
    ];

    protected $appends = ['parse_created_at_in_japanese', 'is_already_applied_user', 'is_applied_user', 'applied_user_count', 'contacted_user_count'];
    private $until_selection = 7;

    public function user()
    {
        return $this->belongsTo(User::class, 'client_user_id', 'id');
    }

    public function projectCategory()
    {
        return $this->hasMany(ProjectCategory::class, 'project_id', 'id');
    }

    public function hasManyProjectUsers()
    {
        return $this->hasMany(ProjectUser::class, 'project_id', 'id');
    }

    public function hasManyUserCategory()
    {
        return $this->hasMany(UserCategory::class, 'user_id', 'client_user_id');
    }

    public function getParseCreatedAtInJapaneseAttribute()
    {
        return Carbon::parse($this->attributes['created_at'])->format('Y年n月j日');
    }

    public function projectAttachment()
    {
        return $this->hasMany(ProjectAttachment::class, 'project_id', 'id');
    }

    public function routeNotificationForMail($notification)
    {
        return $this->user->email;
    }

    public function sendProjectRecruitmentCompleteNotification()
    {
        $this->notify(new ProjectRecruitmentCompleteNotification());
    }

    /**
     * convert datetime from datetime format 'Y-m-d h:i:s'
     * TODO: restore this code after delete projectformater.
     *
     * @param string $value
     * @param mixed  $id
     *
     * @return Carbon
     */
    // public function setReportDeadlineAtAttribute($value)
    // {
    //     $this->attributes['report_deadline_at'] = Carbon::parse($value);
    // }

    public static function getProject($id)
    {
        $project = self::where(['id' => $id, 'deleted_at' => null])
            ->with('user', function ($q) {
                $q->with(['hasManyGenres', 'projects', 'hasManyProjectUsers']);
            })
            ->with('projectCategory')
            ->with('projectAttachment')
            ->with('hasManyProjectUsers', function ($q) use ($id) {
                if (url()->current() == route('workmanagement.details', ['project_id' => $id])) {
                    $q->where('influencer_user_id', '!=', auth()->user()->id)->where('project_id', $id)->whereIn(
                        'application_status',
                        Constant::PROJECT_APPLICATION_STATUS['RECORD'],
                    );
                } else {
                    $q->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['RECORD'])
                        ->where('project_id', $id)
                    ;
                }
            })->first();

        return $project;
    }

    public static function getProjectList($case)
    {
        $project = self::where('client_user_id', auth()->user()->id)->with('user')->with('hasManyProjectUsers')->with('projectCategory')->orderByDesc('created_at');
        $project_user = ProjectUser::leftJoin('projects', 'projects.id', 'project_users.project_id')->with('user')
            ->select('*', 'project_users.id')
            ->with('projectCategory')
            ->where('client_user_id', auth()->user()->id)
            ->orderByDesc('projects.created_at')
        ;

        switch ($case) {
            case 'recruiting':
                $data = $project->where('status_code', Constant::PROJECT_STATUS['RECRUITING']);

                break;

            case 'selection':
                $data = $project->where('status_code', Constant::PROJECT_STATUS['SELECTION']);

                break;

            case 'completed':
                $data = $project_user->where('application_status', Constant::PROJECT_APPLICATION_STATUS['WORKING']);

                break;

            case 'checking':
                $data = $project_user->where('application_status', Constant::PROJECT_APPLICATION_STATUS['CHECKING']);

                break;

            case 'done':
                $data = $project_user->where('application_status', Constant::PROJECT_APPLICATION_STATUS['DONE']);

                break;

            case 'expired':
                $data = $project->where('status_code', Constant::PROJECT_STATUS['EXPIRED']);

                break;

            default:
                $data = [];

                break;
        }

        return $data->paginate(Constant::PAGINATION_COUNT);
    }

    public static function searchResult($request)
    {
        $project = self::with('user')->with('projectCategory');
        $category = $request->category;
        if ($request->keyword) {
            $project->where('title', 'like', '%'.$request->keyword.'%')
                ->orWhere('content', 'like', '%'.$request->keyword.'%')
            ;
        }

        if ($category) {
            $project->whereHas('projectCategory', function ($query) use ($category) {
                $query->where('name', $category);
            });
        }

        if ($request->sns) {
            if (is_array($request->sns)) {
                $sns_code_list = $request->sns;
            } else {
                $sns_code_list = explode(',', $request->sns);
            }

            $project->where(function ($query) use ($sns_code_list) {
                foreach ($sns_code_list as $key => $param) {
                    if (0 == $key) {
                        $query->where('post_sns_code', 'like', '%'.$param.'%');
                    } else {
                        $query->orWhere('post_sns_code', 'like', '%'.$param.'%');
                    }
                }
            });
        }

        if ($request->reward_min) {
            $project->where('reward_amount', '>=', $request->reward_min);
        }

        if ($request->reward_max) {
            $project->where('reward_amount', '<=', $request->reward_max);
        }

        if ($request->application_deadline) {
            $project->where('application_deadline_at', '>=', $request->application_deadline);
        }
        $application_date_query = 'CASE WHEN `application_deadline_at` >= now()';

        switch ($request->sort) {
            case 'lowest_time':
                $project->orderByRaw($application_date_query."THEN `application_deadline_at` ELSE '9999-01-01' END ASC");

                break;

            case 'highest_price':
                $project->orderByRaw($application_date_query.'THEN `reward_amount` END DESC');

                break;

            case 'big_to_small':
                $project->withCount('hasManyProjectUsers')
                    ->orderByRaw($application_date_query.'THEN `has_many_project_users_count` END DESC')
                ;

                break;

            default:
                $project->orderByRaw($application_date_query.'THEN `application_deadline_at` ELSE `created_at` END DESC');
        }

        return $project->paginate(Constant::PAGINATION_COUNT);
    }

    public static function getClientProjectsCountPerStatus($status)
    {
        return self::where('client_user_id', auth()->user()->id)->where('status_code', $status)->get()->count();
    }

    public function getIsAppliedUserAttribute()
    {
        if (Auth::check()) {
            return ProjectUser::where([
                'project_id' => $this->id,
                'influencer_user_id' => auth()->user()->id,
                ['application_status', '!=', Constant::PROJECT_APPLICATION_STATUS['COMPLETED']],
                ['application_status', '!=', Constant::PROJECT_APPLICATION_STATUS['MISSING']],
            ])->exists();
        }

        return false;
    }

    public function getIsAlreadyAppliedUserAttribute()
    {
        if (Auth::check()) {
            return ProjectUser::where([
                'project_id' => $this->id,
                'influencer_user_id' => auth()->user()->id,
                'application_status' => Constant::PROJECT_APPLICATION_STATUS['MISSING'],
            ])->exists();
        }

        return false;
    }

    public function getAppliedUserCountAttribute()
    {
        return ProjectUser::where('project_id', $this->id)
            ->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['RECORD'])
            ->count()
        ;
    }

    public function getContactedUserCountAttribute()
    {
        return ProjectUser::where('project_id', $this->id)
            ->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['CONTACT'])
            ->count()
        ;
    }

    public function getStatusNameAttribute()
    {
        if (1 == $this->status_code) {
            return '募集終了まで';
        }
        if (2 == $this->status_code) {
            return '選定終了まで';
        }
        if (3 == $this->status_code) {
            return '完了報告期限まで';
        }
        if (4 == $this->status_code) {
            return '結果確認終了まで';
        }
    }

    public static function getRecommendedProjects()
    {
        $project = Project::where('application_deadline_at', '>', NOW())
            ->orderBy('created_at', 'DESC')->with('projectCategory', function ($q) {
                $q->select('genres.icon', 'project_categories.*')->leftJoin('genres', 'genres.name', 'project_categories.name');
            })
            ->get()
            ->take(10)
        ;

        return $project;
    }

    public static function getBatchCompletionDeadline($date, $reminder = null)
    {
        if ($reminder) {
            $checking = self::whereDate('report_deadline_at', $date);
        } else {
            $checking = self::whereDate('report_deadline_at', '<', $date);
        }
        $checking->where('status_code', Constant::PROJECT_STATUS['CHECKING'])
            ->with('hasManyProjectUsers', function ($q) {
                $q->where('application_status', Constant::PROJECT_APPLICATION_STATUS['CHECKING']);
            })
        ;

        return $checking->get();
    }

    public static function getBatchReportDeadline($date, $reminder = null)
    {
        if ($reminder) {
            $reporting = self::whereDate('report_deadline_at', $date);
        } else {
            $reporting = self::whereDate('report_deadline_at', '<', $date);
        }
        $reporting->where('status_code', Constant::PROJECT_STATUS['COMPLETED'])
            ->with('hasManyProjectUsers', function ($q) {
                $q->where('application_status', Constant::PROJECT_APPLICATION_STATUS['WORKING']);
            })
        ;

        return $reporting->get();
    }

    public static function getBatchSelectionDeadline($date, $reminder = null)
    {
        if ($reminder) {
            $selection = self::whereDate('selection_deadline_at', $date);
        } else {
            $selection = self::whereDate('selection_deadline_at', '<', $date);
        }

        $selection->where('status_code', Constant::PROJECT_STATUS['SELECTION'])
            ->with('hasManyProjectUsers', function ($q) {
                $q->where('application_status', Constant::PROJECT_APPLICATION_STATUS['SELECTION']);
            })
        ;

        return $selection->get();
    }
}
