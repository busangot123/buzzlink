<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MailTemplate extends Model
{
    use HasFactory;

    protected $table = 'mail_template';

    public static function findByTemplateId($id)
    {
        return self::where('template_id', $id)->first();
    }
}
