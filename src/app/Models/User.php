<?php

namespace App\Models;

use App\Helpers\Constant;
use App\Http\Controllers\SnsInfoController;
use App\Notifications\AlreadyEmailVerifiedNotification;
use App\Notifications\ApplicationDeadlineExpiredWithoutSelectionNotification;
use App\Notifications\ApplicationDeadlineExpiredWithSelectionNotification;
use App\Notifications\BankChangesNotification;
use App\Notifications\ClientSelectedNotification;
use App\Notifications\CompletionDeadlineExpiredClient;
use App\Notifications\CompletionDeadlineExpiredInfluencer;
use App\Notifications\CompletionDeadlineReminder;
use App\Notifications\DeclineInfluencerNotification;
use App\Notifications\DoneCheckingClientNotification;
use App\Notifications\DoneCheckingInfluencerNotification;
use App\Notifications\DoneCheckingInfluencerPaymentNotification;
use App\Notifications\InfluencerReviewToClientNotification;
use App\Notifications\InfluencerReviewToInfluencerNotification;
use App\Notifications\MessageUnreadReminder;
use App\Notifications\PasswordResetCompleteNotification;
use App\Notifications\PasswordResetNotification;
use App\Notifications\ProjectApplicationNotification;
use App\Notifications\ProjectUserAppliedNotification;
use App\Notifications\RegisteredNotification;
use App\Notifications\ReportDeadlineExpiredClient;
use App\Notifications\ReportDeadlineExpiredInfluencer;
use App\Notifications\ReportDeadlineReminder;
use App\Notifications\SelectedInfluencerNotification;
use App\Notifications\SelectionDeadlineExpiredClient;
use App\Notifications\SelectionDeadlineExpiredInfluencer;
use App\Notifications\SelectionDeadlineReminder;
use App\Traits\AuthorObservable;
use App\Traits\FileAccessor;
use Auth;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable implements MustVerifyEmail, CanResetPasswordContract
{
    use HasFactory;
    use Notifiable;
    use SoftDeletes;
    use AuthorObservable;
    use FileAccessor;
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $softDelete = true;

    protected $guarded = [
        'id',
    ];

    protected $appends = ['age', 'sex_text', 'client_rate', 'influencer_rate', 'completed_project', 'completion_rate', 'users_in_contact', 'client_project_post_count', 'client_project_user_count', 'client_order_rate', 'sns_list', 'client_rate_count', 'influencer_rate_count'];
    /**
     * The attributes that should be hidden for arrays.`.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function routeNotificationForMail($notification)
    {
        return $this->email;
    }

    public function messages()
    {
        return $this->hasMany(Message::class, 'sent_user', 'id');
    }

    public function projects()
    {
        return $this->hasMany(Project::class, 'client_user_id', 'id');
    }

    public function hasManyProjectUsers()
    {
        return $this->hasMany(ProjectUser::class, 'influencer_user_id', 'id');
    }

    public function prefectureData()
    {
        return $this->belongsTo(Prefecture::class, 'prefecture', 'id');
    }

    public function bankAccount()
    {
        return $this->belongsTo(BankAccount::class, 'id', 'influencer_user_id');
    }

    public function hasManyGenres()
    {
        return $this->hasMany(UserCategory::class);
    }

    public static function getUsers()
    {
        return self::where('id', '!=', auth()->user()->id)->get();
    }

    public function sendEmailWhenApplicationDeadlineExpired($project)
    {
        if ($project->hasManyProjectUsers()->exists()) {
            $this->notify(new ApplicationDeadlineExpiredWithSelectionNotification($project));
        } else {
            $this->notify(new ApplicationDeadlineExpiredWithoutSelectionNotification($project));
        }
    }

    public function sendSelectionDeadlineReminder($project, $days_left)
    {
        if (is_null($this->email_reject_setting)) {
            $this->notify(new SelectionDeadlineReminder($project, $days_left));
        }
    }

    public function sendSelectionDeadlineExpiredClient($project)
    {
        $this->notify(new SelectionDeadlineExpiredClient($project));
    }

    public function sendSelectionDeadlineExpiredInfluencer($project)
    {
        $this->notify(new SelectionDeadlineExpiredInfluencer($project));
    }

    public function sendReportDeadlineReminder($project, $days_left)
    {
        if (is_null($this->email_reject_setting)) {
            $this->notify(new ReportDeadlineReminder($project, $days_left));
        }
    }

    public function sendReportDeadlineExpiredClient($project, $project_user)
    {
        $this->notify(new ReportDeadlineExpiredClient($project, $project_user));
    }

    public function sendReportDeadlineExpiredInfluencer($project)
    {
        $this->notify(new ReportDeadlineExpiredInfluencer($project));
    }

    public function sendCompletionDeadlineReminder($project, $project_user, $days_left)
    {
        if (is_null($this->email_reject_setting)) {
            $this->notify(new CompletionDeadlineReminder($project, $project_user, $days_left));
        }
    }

    public function sendCompletionDeadlineExpiredClient($project, $project_user)
    {
        $this->notify(new CompletionDeadlineExpiredClient($project, $project_user));
    }

    public function sendCompletionDeadlineExpiredInfluencer($project)
    {
        $this->notify(new CompletionDeadlineExpiredInfluencer($project));
    }

    public function SendMessageUnreadReminder($message)
    {
        if (is_null($this->email_reject_setting)) {
            $this->notify(new MessageUnreadReminder($message));
        }
    }

    public function getClientProjectPostCountAttribute()
    {
        return Auth::check() ? Project::where('client_user_id', auth()->user()->id)->get()->count() : 0;
    }

    public function getClientProjectUserCountAttribute()
    {
        $client_project_ids = Auth::check() ? Project::where('client_user_id', auth()->user()->id)->get()->pluck('id')->toArray() : [];

        return ProjectUser::whereIn('project_id', $client_project_ids)
            ->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['CONTACT'])
            ->get()
            ->count()
        ;
    }

    public function clientProjectIds()
    {
        return Project::where('client_user_id', $this->id)->get()->pluck('id')->toArray();
    }

    public function getClientOrderRateAttribute()
    {
        $all_project_user_count = ProjectUser::whereIn('project_id', $this->clientProjectIds())
            ->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['RECORD'])->count();
        if ($all_project_user_count <= 0) {
            return 0;
        }

        return round(($this->client_project_user_count / $all_project_user_count) * 100, 2);
    }

    public function getUsersInContactAttribute()
    {
        return $this->hasManyProjectUsers()
            ->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['CONTACT'])->count();
    }

    public function getCompletedProjectAttribute()
    {
        return $this->hasManyProjectUsers()
            ->where('application_status', Constant::PROJECT_APPLICATION_STATUS['DONE'])
            ->count()
        ;
    }

    public function getCompletionRateAttribute()
    {
        $total = 0 == $this->completed_project ? 0 : $this->completed_project / $this->users_in_contact;

        return round(($total * 100), 2);
    }

    public function getAvatarImageAttribute($value): string
    {
        /** @var \Illuminate\Filesystem\FilesystemManager $disk */
        $disk = Storage::disk('s3_public');
        if (!$value) {
            // default avatar image
            return '/assets/user.png';
        }

        return $disk->url($value);
    }

    public function getAgeAttribute()
    {
        $age = Carbon::parse($this->birthday)->age;
        if ($age < 10) {
            return null;
        }
        $split = str_split($age);
        $base = count($split) > 2 ? ($split[0].$split[1]) * 10 : $split[0] * 10;

        return $split[1] < 6 ? $base.' 代前半' : $base.' 代後半';
    }

    public function getClientRateCountAttribute()
    {
        return ProjectUser::whereIn('project_id', $this->clientProjectIds())
            ->where('influencer_eva', '!=', '')
            ->where('influencer_eva', '!=', null)
            ->count()
        ;
    }

    public function getInfluencerRateCountAttribute()
    {
        return $this->hasManyProjectUsers()
            ->where('client_eva', '!=', '')
            ->where('client_eva', '!=', null)
            ->count()
        ;
    }

    //// getting the user client rate
    public function getClientRateAttribute()
    {
        $data = ProjectUser::whereIn('project_id', $this->clientProjectIds())
            ->where('influencer_eva', '!=', '')
            ->where('influencer_eva', '!=', null)
            ->get(['id', 'influencer_eva'])
        ;

        return $this->calculateClientRate($data);
    }

    //// getting the user influencer rate
    public function getInfluencerRateAttribute()
    {
        $data = $this->hasManyProjectUsers()
            ->where('client_eva', '!=', '')
            ->where('client_eva', '!=', null)
            ->get(['id', 'client_eva'])
        ;

        return $this->calculateInfluencerRate($data);
    }

    public function deleteChildData()
    {
        $this->hasManyGenres()->delete();
    }

    public function sendAlreadyEmailVerifiedNotification($user)
    {
        $this->notify(new AlreadyEmailVerifiedNotification($user));
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordResetNotification($token));
    }

    public function sendPasswordResetCompleteNotification()
    {
        $this->notify(new PasswordResetCompleteNotification());
    }

    public function sendRegisteredNotification()
    {
        $this->notify(new RegisteredNotification());
    }

    public function sendClientSelectedNotification($data)
    {
        $this->notify(new ClientSelectedNotification($data));
    }

    public function sendSelectedInfluencerNotification($data)
    {
        $this->notify(new SelectedInfluencerNotification($data));
    }

    public function sendDeclineInfluencerNotification($data)
    {
        $this->notify(new DeclineInfluencerNotification($data));
    }

    public function sendProjectUserAppliedNotification($data)
    {
        $this->notify(new ProjectUserAppliedNotification($data));
    }

    public function sendProjectApplicationNotification($data)
    {
        if (is_null($this->email_reject_setting)) {
            $this->notify(new ProjectApplicationNotification($data));
        }
    }

    public function sendDoneCheckingClientNotification($data)
    {
        $this->notify(new DoneCheckingClientNotification($data));
    }

    public function sendDoneCheckingInfluencerNotification($data)
    {
        $this->notify(new DoneCheckingInfluencerNotification($data));
    }

    public function sendDoneCheckingInfluencerPaymentNotification($data)
    {
        $this->notify(new DoneCheckingInfluencerPaymentNotification($data));
    }

    public function sendInfluencerReviewToInfluencerNotification($data)
    {
        $this->notify(new InfluencerReviewToInfluencerNotification($data));
    }

    public function sendInfluencerReviewToClientNotification($data)
    {
        $this->notify(new InfluencerReviewToClientNotification($data));
    }

    public function sendBankChangesNotification($data)
    {
        $this->notify(new BankChangesNotification($data));
    }

    /**
     * get user who has not been email verified yet.
     *
     * @param string $uuid
     *
     * @return mixed user
     */
    public static function getNotEmailVerifiedUser($uuid): mixed
    {
        return self::where('uuid', $uuid)->where('email_verified_at', null)->firstOrFail();
    }

    public function getSexTextAttribute()
    {
        if (1 == $this->sex) {
            return '男性';
        }
        if (2 == $this->sex) {
            return '女性';
        }
        if (9 == $this->sex) {
            return '公開しない';
        }
    }

    public function getSnsListAttribute()
    {
        return SnsInfoController::listSnsOfUser($this->id);
    }

    ///calculate the average of client rate
    ///return hyphen if no rate
    private function calculateClientRate($data)
    {
        $sum = $data->map(function ($e) {
            return 1 == $e->influencer_eva ? 5 : 1;
        })->toArray();

        return 0 != count($sum) ? number_format(array_sum($sum) / count($sum), 1) : '-';
    }

    ///calculate the average of influencer rate
    ///return hyphen if no rate
    private function calculateInfluencerRate($data)
    {
        $sum = $data->map(function ($e) {
            return 1 == $e->client_eva ? 5 : 1;
        })->toArray();

        return 0 != count($sum) ? number_format(array_sum($sum) / count($sum), 1) : '-';
    }
}
