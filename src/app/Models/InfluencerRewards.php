<?php

namespace App\Models;

use App\Traits\AuthorObservable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InfluencerRewards extends Model
{
    use HasFactory;
    use AuthorObservable;

    protected $table = 'influencer_rewards';
    protected $appends = ['project_id', 'project_title'];
    protected $fillable = [
        'project_user_id',
        'get_reward_at',
        'reward_amount',
        'closing_month',
        'closing_processing_date',
    ];

    public function getProjectIdAttribute()
    {
        $project_user = ProjectUser::whereId($this->project_user_id)->with('project')->first();

        return $project_user->project->id;
    }

    public function getProjectTitleAttribute()
    {
        $project_user = ProjectUser::whereId($this->project_user_id)->with('project')->first();

        return $project_user->project->title;
    }
}
