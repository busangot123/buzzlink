<?php

namespace App\Models;

use App\Notifications\EmailResetNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class EmailReset extends Model
{
    use Notifiable;

    public const UPDATED_AT = null;

    protected $fillable = [
        'email',
        'name',
        'new_email',
        'token',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sendEmailResetNotification($token)
    {
        $this->notify(new EmailResetNotification($token));
    }

    /**
     * send email to new email address.
     *
     * @param \Illuminate\Notifications\Notification $notification
     *
     * @return string
     */
    public function routeNotificationForMail($notification)
    {
        return $this->new_email;
    }

    public static function createNewToken()
    {
        return hash_hmac('sha256', Str::random(40), config('app.key'));
    }

    public function exists($user, $token)
    {
        $record = self::where(
            'email',
            $user->email
        )->first();
        $dummy = $this->tokenExpired(($record->created_at));

        return $record
            && !$this->tokenExpired($record->created_at)
            && Hash::check($token, $record->token);
    }

    /**
     * update user email address, delete token.
     *
     * @param [type] $user
     *
     * @return bool
     */
    public function updateEmail($user)
    {
        $updated_user =
            DB::transaction(function () use ($user) {
                $user->email_verified_at = Carbon::now();
                $user->email = $this->new_email;

                $user->save();
                $this->delete();

                return $user;
            });

        return $updated_user ? true : false;
    }

    /**
     * Determine if the token has expired.
     *
     * @param string $createdAt
     *
     * @return bool
     */
    protected function tokenExpired($createdAt)
    {
        return Carbon::parse($createdAt)->addSeconds(config('auth.passwords.users.expire') * 60)->isPast();
    }
}
