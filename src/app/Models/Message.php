<?php

namespace App\Models;

use App\Helpers\Constant;
use App\Traits\AuthorObservable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Message extends Model
{
    use AuthorObservable;

    protected $table = 'messages';
    protected $primaryKey = 'id';

    protected $guarded = [
        'id', 'created_user', 'updated_user', 'created_at', 'updated_at',
    ];

    public function receivedUser()
    {
        return $this->belongsTo(User::class, 'received_user', 'id');
    }

    public function sentUser()
    {
        return $this->belongsTo(User::class, 'sent_user', 'id');
    }

    public function projectUser()
    {
        return $this->belongsTo(ProjectUser::class, 'project_user_id', 'id');
    }

    public function getAttachmentPathAttribute($value): string
    {
        /** @var \Illuminate\Filesystem\FilesystemManager $disk */
        $disk = Storage::disk('s3_private');

        return $disk->url($value);
    }

    public static function getMessages($project_user_id)
    {
        $project_user = ProjectUser::whereId($project_user_id)->with('project', function ($q) {
            $q->with('user');
        })->first();

        $conversation_ids[] = auth()->user()->id;
        if ($project_user->influencer_user_id == auth()->user()->id) {
            $conversation_ids[] = $project_user->project->user->id;
        } else {
            $conversation_ids[] = $project_user->influencer_user_id;
        }

        return self::whereIn('sent_user', $conversation_ids)
            ->whereIn('received_user', $conversation_ids)
            ->where('project_user_id', $project_user_id)
            ->with('sentUser')
            ->get()
        ;
    }

    public static function getMessageNotificaiton()
    {
        return self::where('received_user', auth()->user()->id)
            ->where('is_read', Constant::NOT_READ)
            ->with('sentUser')
            ->orderByDesc('created_at')
            ->get()
        ;
    }

    public static function getAllUserMessages($id)
    {
        $messages = self::select('sent_user', 'received_user', 'project_user_id', 'is_read')
            ->where('sent_user', auth()->user()->id)
            ->orWhere('received_user', auth()->user()->id)
            ->with('receivedUser')
            ->with('sentUser')
            ->with('projectUser', function ($q) {
                $q->with('project');
            })
            ->orderByDesc('created_at')
            ->get()
        ;

        return $messages;
    }

    public static function updateIsRead($project_user_id)
    {
        self::where('project_user_id', $project_user_id)
            ->where('sent_user', '!=', auth()->user()->id)
            ->update(['is_read' => Constant::READ])
        ;
    }
}
