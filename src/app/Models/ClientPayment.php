<?php

namespace App\Models;

use App\Traits\AuthorObservable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientPayment extends Model
{
    use HasFactory;
    use AuthorObservable;

    protected $appends = ['project_title'];

    public function getProjectTitleAttribute()
    {
        return Project::whereId($this->project_id)->first()->title;
    }

    public static function createClientPayment(int $project_id, object $payment)
    {
        $client_payment = new self();
        $client_payment->project_id = $project_id;
        $client_payment->influencer_cost = $payment->total;
        $client_payment->system_usage_cost = $payment->total_with_fee;
        $client_payment->tax_cost = $payment->total_with_tax;
        $client_payment->total_cost = $payment->total_to_pay;
        $client_payment->save();
    }
}
