<?php

namespace App\Logging;

use Monolog\Formatter\JsonFormatter;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\FormattableHandlerInterface;
use Monolog\Handler\ProcessableHandlerInterface;
use Monolog\Logger;

class BasicFormatter
{
    /**
     * @param Logger $monolog
     */
    public function __invoke($monolog)
    {
        // $formatter = new JsonFormatter();
        // $formatter = new LineFormatter();

        $uidProcessor = app()->make('Monolog\Processor\UidProcessor');

        foreach ($monolog->getHandlers() as $handler) {
            if ($handler instanceof FormattableHandlerInterface) {
                // $handler->setFormatter($formatter);
            }
            if ($handler instanceof ProcessableHandlerInterface) {
                $handler->pushProcessor($uidProcessor);
            }
        }
    }
}
