<?php

namespace App\Logging;

use App\Logging\Processor\UserProcessor;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\FormattableHandlerInterface;
use Monolog\Handler\ProcessableHandlerInterface;
use Monolog\Logger;
use Monolog\Processor\WebProcessor;

class RequestFormatter
{
    /**
     * @param Logger $monolog
     */
    public function __invoke($monolog)
    {
        $formatter = new JsonFormatter();

        $uidProcessor = app()->make('Monolog\Processor\UidProcessor');

        foreach ($monolog->getHandlers() as $handler) {
            if ($handler instanceof FormattableHandlerInterface) {
                $handler->setFormatter($formatter);
            }
            if ($handler instanceof ProcessableHandlerInterface) {
                $handler->pushProcessor(new WebProcessor());
                $handler->pushProcessor($uidProcessor);
                $handler->pushProcessor(new UserProcessor());
            }
        }
    }
}
