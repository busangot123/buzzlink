<?php

namespace App\Logging\Processor;

use Illuminate\Support\Facades\Auth;
use Monolog\Processor\ProcessorInterface;

class UserProcessor implements ProcessorInterface
{
    public function __invoke(array $record)
    {
        if (!Auth::check()) {
            return $record;
        }

        $record['extra']['user_id'] = auth()->user()->id;

        return $record;
    }
}
