<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ApplicationDeadlineExpired::class,
        Commands\SelectionDeadlineExpired::class,
        Commands\ReportDeadlineExpired::class,
        Commands\CompletionDeadlineExpired::class,
        Commands\MessageReminder::class,
        Commands\ProjectUserStatus::class,
    ];

    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('batch:application_deadline_expired')->daily()->withoutOverlapping()->appendOutputTo(storage_path('logs/application_deadline_expired.log'));
        $schedule->command('batch:selection_deadline_expired')->daily()->withoutOverlapping()->appendOutputTo(storage_path('logs/selection_deadline_expired.log'));
        $schedule->command('batch:report_deadline_expired')->daily()->withoutOverlapping()->appendOutputTo(storage_path('logs/report_deadline_expired.log'));
        $schedule->command('batch:completion_deadline_expired')->daily()->withoutOverlapping()->appendOutputTo(storage_path('logs/completion_deadline_expired.log'));
        $schedule->command('batch:message_reminder')->everyFiveMinutes()->withoutOverlapping()->appendOutputTo(storage_path('logs/message_reminder.log'));
        $schedule->command('batch:project_user_status')->daily()->withoutOverlapping()->appendOutputTo(storage_path('logs/project_user_status.log'));
    }

    /**
     * Register the commands for the application.
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
