<?php

namespace App\Console\Commands;

use App\Events\BatchMessageUnreadReminder;
use App\Models\Message;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class MessageReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'batch:message_reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email to user to remind of the unread messages';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info('Message Reminder batch is running...');

        try {
            $now = Carbon::now();
            $messages = Message::where([
                'is_read' => false,
                'notified_at' => null,
            ])->get();

            if (count($messages) > 0) {
                $already_sent_email = [];
                foreach ($messages as $message) {
                    $created_date = Carbon::parse($message->created_at);
                    if ($created_date->diffInHours($now, false) >= 1) {
                        if (!in_array($message->project_user_id, $already_sent_email)) {
                            $already_sent_email[] = $message->project_user_id;
                            event(new BatchMessageUnreadReminder($message));
                        }
                        Message::findOrFail($message->id)->update(['notified_at' => $now]);
                    }
                }
            }
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        }
        Log::info('Message Reminder batch done successfully...');
    }
}
