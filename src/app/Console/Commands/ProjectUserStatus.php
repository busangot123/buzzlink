<?php

namespace App\Console\Commands;

use App\Helpers\Constant;
use App\Models\Project;
use App\Models\ProjectUser;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ProjectUserStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'batch:project_user_status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update work management status';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info('Project User status update is running...');

        try {
            $now = Carbon::now()->subDays(14);
            $projects = Project::where('report_deadline_at', '<', $now)
                ->where('status_code', Constant::PROJECT_STATUS['CHECKING'])
                ->get()
            ;

            if (count($projects) > 0) {
                foreach ($projects as $project) {
                    $project->status_code = Constant::PROJECT_STATUS['DONE'];
                    $project->update();
                    ProjectUser::where('project_id', $project->id)
                        ->where('application_status', Constant::PROJECT_APPLICATION_STATUS['CHECKING'])
                        ->update(
                            [
                                'application_status' => Constant::PROJECT_APPLICATION_STATUS['DONE'],
                                'client_eva' => true,
                                'client_comment' => Constant::DEFAULT_CLIENT_COMMENT,
                            ]
                        )
                    ;
                }
            }
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        }
        Log::info('Project User status successfully updated...');
    }
}
