<?php

namespace App\Console\Commands;

use App\Events\BatchDeadlineExpired;
use App\Events\BatchDeadlineReminder;
use App\Models\Project;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SelectionDeadlineExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'batch:selection_deadline_expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email when selection deadline expired';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info('Selection deadline expired batch is running...');

        try {
            $now = Carbon::now();
            $projects = Project::getBatchSelectionDeadline($now);
            $projects_day_deadline = Project::getBatchSelectionDeadline($now->addDays(1), true);
            $projects_days_deadline = Project::getBatchSelectionDeadline($now->addDays(2), true);
            $projects_week_deadline = Project::getBatchSelectionDeadline($now->addDays(4), true);

            if (count($projects) > 0) {
                foreach ($projects as $project) {
                    event(new BatchDeadlineExpired($project));
                }
            } elseif (count($projects_day_deadline) > 0) {
                foreach ($projects_day_deadline as $project_day) {
                    event(new BatchDeadlineReminder($project_day, 1));
                }
            } elseif (count($projects_days_deadline) > 0) {
                foreach ($projects_days_deadline as $project_days) {
                    event(new BatchDeadlineReminder($project_days, 3));
                }
            } else {
                foreach ($projects_week_deadline as $project_week) {
                    event(new BatchDeadlineReminder($project_week, 7));
                }
            }
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        }
        Log::info('Selection deadline expired batch done successfully...');
    }
}
