<?php

namespace App\Console\Commands;

use App\Events\ApplicationDeadlineExpiredEvent;
use App\Helpers\Constant;
use App\Models\Project;
use App\Models\ProjectUser;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ApplicationDeadlineExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'batch:application_deadline_expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email when application deadline expired';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info('Application deadline expired batch is running...');

        try {
            $now = Carbon::now();
            $projects = Project::where('application_deadline_at', '<', $now)
                ->where('status_code', Constant::PROJECT_STATUS['RECRUITING'])
                ->get()
            ;

            if (count($projects) > 0) {
                foreach ($projects as $project) {
                    if ($project->hasManyProjectUsers()->exists()) {
                        $project->status_code = Constant::PROJECT_STATUS['SELECTION'];
                        ProjectUser::where('project_id', $project->id)
                            ->where('application_status', Constant::PROJECT_APPLICATION_STATUS['APPLIED'])
                            ->update(['application_status' => Constant::PROJECT_APPLICATION_STATUS['SELECTION']])
                        ;
                    } else {
                        $project->status_code = Constant::PROJECT_STATUS['EXPIRED'];
                    }
                    $project->update();
                }
                event(new ApplicationDeadlineExpiredEvent($projects));
            }

            $projects_on_selection = Project::where('selection_deadline_at', '<', $now)
                ->where('status_code', Constant::PROJECT_STATUS['SELECTION'])
                ->with('hasManyProjectUsers', function ($q) {
                    $q->where('application_status', Constant::PROJECT_APPLICATION_STATUS['SELECTION']);
                })
                ->get()
            ;

            if (count($projects_on_selection) > 0) {
                foreach ($projects_on_selection as $select_project) {
                    $select_project->status_code = Constant::PROJECT_STATUS['EXPIRED'];
                    $select_project->update();

                    foreach ($select_project->hasManyProjectUsers as $project_user) {
                        $project_user->application_status = Constant::PROJECT_APPLICATION_STATUS['COMPLETED'];
                        if ($project_user->update()) {
                            event(new ProjectSelection($select_project, $project_user));
                        }
                    }
                }
            }
        } catch (\Throwable $th) {
            Log::debug($th->getMessage());
        }
        Log::info('Application deadline expired batch done successfully...');
    }
}
