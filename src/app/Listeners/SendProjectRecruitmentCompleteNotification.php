<?php

namespace App\Listeners;

use App\Events\ProjectRecruitmentComplete;
use App\Models\Project;

class SendProjectRecruitmentCompleteNotification
{
    public function handle(ProjectRecruitmentComplete $event)
    {
        if ($event->data instanceof Project) {
            $project = $event->data;
            $project->sendProjectRecruitmentCompleteNotification();
        }
    }
}
