<?php

namespace App\Listeners;

use App\Events\BatchMessageUnreadReminder;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMessageUnreadReminder implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param object $event
     */
    public function handle(BatchMessageUnreadReminder $event)
    {
        $event->message->receivedUser->SendMessageUnreadReminder($event->message);
    }
}
