<?php

namespace App\Listeners;

use App\Events\BatchDeadlineReminder;
use App\Helpers\Constant;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendCompletionDeadlineReminderNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param object $event
     */
    public function handle(BatchDeadlineReminder $event)
    {
        if ($event->project->status_code == Constant::PROJECT_STATUS['CHECKING']) {
            foreach ($event->project->hasManyProjectUsers as $project_user) {
                Notification::create([
                    'user_id' => $event->project->user->id,
                    'notification_at' => Carbon::now()->toDateTimeString(),
                    'notification_contents' => '[残り'.$event->days_left.'日]['.$event->project->title.']['.$project_user->user->name.']'.Constant::NOTIFICATION_CONTENTS['BATCH_COMPLETION_REMINDER'],
                    'destination_url' => route('project_management.checking.input', ['project_user_id' => $project_user->id]),
                ]);

                $event->project->user->sendCompletionDeadlineReminder($event->project, $project_user, $event->days_left);
            }
        }
    }
}
