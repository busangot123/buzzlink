<?php

namespace App\Listeners;

use App\Events\ProjectSelection;
use App\Helpers\Constant;
use App\Models\Notification;
use App\Models\User;
use Carbon\Carbon;

class SendProjectSelectionNotification
{
    /**
     * Handle the event.
     */
    public function handle(ProjectSelection $event)
    {
        if ($event->project_user->user instanceof User) {
            switch ($event->project_user->application_status) {
                case Constant::PROJECT_APPLICATION_STATUS['COMPLETED']:
                    Notification::create([
                        'user_id' => $event->project_user->influencer_user_id,
                        'notification_at' => Carbon::now()->toDateTimeString(),
                        'notification_contents' => '['.$event->project->title.']'.Constant::NOTIFICATION_CONTENTS['NOT_SELECTED'],
                        'destination_url' => route('workmanagement.details', ['project_id' => $event->project->id]),
                    ]);
                    $event->project_user->user->sendDeclineInfluencerNotification($event);

                    break;

                case Constant::PROJECT_APPLICATION_STATUS['WORKING']:
                    Notification::create([
                        'user_id' => $event->project->user->id,
                        'notification_at' => Carbon::now()->toDateTimeString(),
                        'notification_contents' => '['.$event->project->title.']'.Constant::NOTIFICATION_CONTENTS['SELECTED'],
                        'destination_url' => route('project_management.requesting', ['project_user_id' => $event->project_user->id]),
                    ]);

                    Notification::create([
                        'user_id' => $event->project_user->influencer_user_id,
                        'notification_at' => Carbon::now()->toDateTimeString(),
                        'notification_contents' => '['.$event->project->title.']'.Constant::NOTIFICATION_CONTENTS['SELECTED'],
                        'destination_url' => route('work_management.review', ['project_id' => $event->project->id]),
                    ]);

                    $event->project_user->user->sendSelectedInfluencerNotification($event);
                    $event->project->user->sendClientSelectedNotification($event);

                    break;

                default:
                    break;
            }
        }
    }
}
