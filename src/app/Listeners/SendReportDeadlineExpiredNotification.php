<?php

namespace App\Listeners;

use App\Events\BatchDeadlineExpired;
use App\Helpers\Constant;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReportDeadlineExpiredNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param object $event
     */
    public function handle(BatchDeadlineExpired $event)
    {
        if ($event->project->status_code == Constant::PROJECT_STATUS['COMPLETED']) {
            foreach ($event->project->hasManyProjectUsers as $project_user) {
                $reject_template_id = explode(',', $project_user->notified_mail_template_id);
                if (!in_array(Constant::REPORT_DEADLINE_END_INFLUENCER, $reject_template_id)) {
                    array_push($reject_template_id, Constant::REPORT_DEADLINE_END_INFLUENCER);
                    $project_user->notified_mail_template_id = $project_user->notified_mail_template_id ? implode(',', $reject_template_id) : Constant::REPORT_DEADLINE_END_INFLUENCER;
                    $project_user->update();

                    Notification::create([
                        'user_id' => $event->project->user->id,
                        'notification_at' => Carbon::now()->toDateTimeString(),
                        'notification_contents' => '[期限切れ]['.$event->project->title.']['.$project_user->user->name.']'.Constant::NOTIFICATION_CONTENTS['BATCH_REPORTING'],
                        'destination_url' => route('project_management.requesting', ['project_user_id' => $project_user->id]),
                    ]);

                    Notification::create([
                        'user_id' => $project_user->user->id,
                        'notification_at' => Carbon::now()->toDateTimeString(),
                        'notification_contents' => '[期限切れ]['.$event->project->title.']'.Constant::NOTIFICATION_CONTENTS['BATCH_REPORTING'],
                        'destination_url' => route('work_management.review', ['project_id' => $event->project->id]),
                    ]);

                    $event->project->user->sendReportDeadlineExpiredClient($event->project, $project_user);
                    $project_user->user->sendReportDeadlineExpiredInfluencer($event->project);
                }
            }
        }
    }
}
