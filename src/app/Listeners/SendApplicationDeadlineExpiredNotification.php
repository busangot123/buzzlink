<?php

namespace App\Listeners;

use App\Events\ApplicationDeadlineExpiredEvent;
use App\Helpers\Constant;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendApplicationDeadlineExpiredNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param object $event
     */
    public function handle(ApplicationDeadlineExpiredEvent $event)
    {
        foreach ($event->projects as $project) {
            $notify_data = [
                'user_id' => $project->user->id,
                'notification_at' => Carbon::now()->toDateTimeString(),
            ];

            if ($project->hasManyProjectUsers()->exists()) {
                $notify_data['notification_contents'] = '['.$project->title.']'.Constant::NOTIFICATION_CONTENTS['BATCH_APPLICATION'];
                $notify_data['destination_url'] = route('mypage.project_management.selection.form', ['project_id' => $project->id]);
            } else {
                $notify_data['notification_contents'] = '['.$project->title.']'.Constant::NOTIFICATION_CONTENTS['BATCH_NO_APPLICATION'];
                $notify_data['destination_url'] = route('project.details', ['project_id' => $project->id]);
            }

            Notification::create($notify_data);
            $project->user->sendEmailWhenApplicationDeadlineExpired($project);
        }
    }
}
