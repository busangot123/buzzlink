<?php

namespace App\Listeners;

use App\Events\ProjectUserApplied;
use App\Helpers\Constant;
use App\Models\Notification;
use Carbon\Carbon;

class SendProjectUserAppliedNotification
{
    /**
     * Handle the event.
     *
     * @param object $event
     */
    public function handle(ProjectUserApplied $event)
    {
        Notification::create([
            'user_id' => $event->client->id,
            'notification_at' => Carbon::now()->toDateTimeString(),
            'notification_contents' => '['.$event->project_title.']'.Constant::NOTIFICATION_CONTENTS['APPLY_INFLUENCER'],
            'destination_url' => $event->data['project_path'],
        ]);

        $event->influencer->sendProjectUserAppliedNotification($event->data);
        $event->client->sendProjectApplicationNotification($event->data);
    }
}
