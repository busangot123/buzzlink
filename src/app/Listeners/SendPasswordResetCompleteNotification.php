<?php

namespace App\Listeners;

use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;

class SendPasswordResetCompleteNotification
{
    /**
     * Handle the event.
     *
     * @param \Illuminate\Auth\Events\Registered $event
     */
    public function handle(PasswordReset $event)
    {
        if ($event->user instanceof User) {
            $event->user->sendPasswordResetCompleteNotification();
        }
    }
}
