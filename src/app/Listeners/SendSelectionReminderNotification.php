<?php

namespace App\Listeners;

use App\Events\BatchDeadlineReminder;
use App\Helpers\Constant;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSelectionReminderNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param object $event
     */
    public function handle(BatchDeadlineReminder $event)
    {
        if ($event->project->status_code == Constant::PROJECT_STATUS['SELECTION']) {
            Notification::create([
                'user_id' => $event->project->user->id,
                'notification_at' => Carbon::now()->toDateTimeString(),
                'notification_contents' => '[残り'.$event->days_left.'日]['.$event->project->title.']'.Constant::NOTIFICATION_CONTENTS['BATCH_APPLICATION'],
                'destination_url' => route('mypage.project_management.selection.form', ['project_id' => $event->project->id]),
            ]);

            $event->project->user->sendSelectionDeadlineReminder($event->project, $event->days_left);
        }
    }
}
