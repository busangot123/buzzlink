<?php

namespace App\Listeners;

use App\Events\BatchDeadlineExpired;
use App\Helpers\Constant;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendSelectionExpiredNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param object $event
     */
    public function handle(BatchDeadlineExpired $event)
    {
        if ($event->project->status_code == Constant::PROJECT_STATUS['SELECTION']) {
            Notification::create([
                'user_id' => $event->project->user->id,
                'notification_at' => Carbon::now()->toDateTimeString(),
                'notification_contents' => '['.$event->project->title.']'.Constant::NOTIFICATION_CONTENTS['BATCH_SELECTION_CLIENT'],
                'destination_url' => route('mypage.project_management.selection.form', ['project_id' => $event->project->id]),
            ]);

            $event->project->user->sendSelectionDeadlineExpiredClient($event->project);
            foreach ($event->project->hasManyProjectUsers as $project_user) {
                $reject_template_id = explode(',', $project_user->notified_mail_template_id);
                if (!in_array(Constant::SELECTION_DEADLINE_END_INFLUENCER, $reject_template_id)) {
                    $project_user->notified_mail_template_id = Constant::SELECTION_DEADLINE_END_INFLUENCER;
                    $project_user->update();

                    Notification::create([
                        'user_id' => $project_user->user->id,
                        'notification_at' => Carbon::now()->toDateTimeString(),
                        'notification_contents' => '['.$event->project->title.']'.Constant::NOTIFICATION_CONTENTS['NOT_SELECTED'],
                        'destination_url' => route('workmanagement.details', ['project_id' => $event->project->id]),
                    ]);

                    $project_user->user->sendSelectionDeadlineExpiredInfluencer($event->project);
                }
            }
        }
    }
}
