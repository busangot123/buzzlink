<?php

namespace App\Listeners;

use App\Events\CompleteEvaluation;
use App\Helpers\Constant;
use App\Models\InfluencerRewards;
use App\Models\Notification;
use App\Models\RewardBalances;
use Carbon\Carbon;

class SendCheckingNotification
{
    /**
     * Handle the event.
     *
     * @param object $event
     */
    public function handle(CompleteEvaluation $event)
    {
        $notification_details = [
            'user_id' => $event->project_user->influencer_user_id,
            'notification_at' => Carbon::now()->toDateTimeString(),
            'notification_contents' => '['.$event->data['project_title'].']'.Constant::NOTIFICATION_CONTENTS['DONE_CLIENT_REVIEW'],
            'destination_url' => $event->data['influencer_url'],
        ];

        Notification::create($notification_details);
        $notification_details['destination_url'] = route('payment.list').Constant::REWARD_DETAIL_PARAMS;
        $notification_details['notification_contents'] = '['.$event->data['project_title'].']'.Constant::NOTIFICATION_CONTENTS['INFLUENCER_PAYMENT'];
        Notification::create($notification_details);

        InfluencerRewards::create(
            [
                'project_user_id' => $event->project_user->id,
                'get_reward_at' => now(),
                'reward_amount' => $event->balance,
                'closing_month' => 0,
                'closing_processing_date' => now(),
            ]
        );

        $latest_reward = RewardBalances::where('user_id', $event->project_user->influencer_user_id)
            ->orderByDesc('created_at')
            ->pluck('balance')
            ->first() ?: 0;

        RewardBalances::create(
            [
                'user_id' => $event->project_user->influencer_user_id,
                'balance' => $event->balance + $latest_reward,
            ]
        );

        $event->project_user->user->sendDoneCheckingInfluencerNotification($event->data);
        $event->project_user->user->sendDoneCheckingInfluencerPaymentNotification($event->data);
        $event->client->sendDoneCheckingClientNotification($event->data);
    }
}
