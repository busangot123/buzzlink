<?php

namespace App\Listeners;

use App\Events\InfluencerEvaluation;

class SendInfluencerCompleteReviewNotification
{
    /**
     * Handle the event.
     *
     * @param object $event
     */
    public function handle(InfluencerEvaluation $event)
    {
        $event->influencer->sendInfluencerReviewToInfluencerNotification($event->data);
        $event->client->sendInfluencerReviewToClientNotification($event->data);
    }
}
