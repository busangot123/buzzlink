<?php

namespace App\Listeners;

use App\Events\BatchDeadlineReminder;
use App\Helpers\Constant;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReportDeadlineReminderNotification implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param object $event
     */
    public function handle(BatchDeadlineReminder $event)
    {
        if ($event->project->status_code == Constant::PROJECT_STATUS['COMPLETED']) {
            foreach ($event->project->hasManyProjectUsers as $project_user) {
                Notification::create([
                    'user_id' => $project_user->user->id,
                    'notification_at' => Carbon::now()->toDateTimeString(),
                    'notification_contents' => '[残り'.$event->days_left.'日]['.$event->project->title.']'.Constant::NOTIFICATION_CONTENTS['BATCH_REPORT_REMINDER'],
                    'destination_url' => route('work_management.review', ['project_id' => $event->project->id]),
                ]);

                $project_user->user->sendReportDeadlineReminder($event->project, $event->days_left);
            }
        }
    }
}
