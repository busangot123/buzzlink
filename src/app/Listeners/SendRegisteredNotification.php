<?php

namespace App\Listeners;

use App\Models\User;
use Illuminate\Auth\Events\Registered;

class SendRegisteredNotification
{
    /**
     * Handle the event.
     */
    public function handle(Registered $event)
    {
        if ($event->user instanceof User) {
            $event->user->sendRegisteredNotification();
        }
    }
}
