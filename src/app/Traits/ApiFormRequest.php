<?php

namespace App\Traits;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

/**
 * return json error.
 */
trait ApiFormRequest
{
    protected function failedValidation(Validator $validator)
    {
        $response['status'] = 'failed validation';
        $response['code'] = Response::HTTP_UNPROCESSABLE_ENTITY;
        $response['errors'] = $validator->errors()->toArray();
        $response['errorMessages'] = $validator->errors()->all();

        throw new HttpResponseException(
            response()->json($response, Response::HTTP_UNPROCESSABLE_ENTITY)
        );
    }
}
