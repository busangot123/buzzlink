<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;

trait FileAccessor
{
    private function getPublicFileUrl($file)
    {
        /** @var \Illuminate\Filesystem\FilesystemManager $disk */
        $disk = Storage::disk('s3_public');

        return $disk->url($file);
    }
}
