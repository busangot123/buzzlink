<?php

namespace App\Traits;

use Symfony\Component\HttpFoundation\Response;

trait ApiJson
{
    private function successGetResponse($data = null, $message = 'success get data')
    {
        return response()->json(
            [
                $data,
                'status' => 'success',
                'code' => Response::HTTP_OK,
                'message' => $message,
            ],
            Response::HTTP_OK
        );
    }

    private function successPostResponse($message = 'success post data')
    {
        return response()->json(
            [
                'status' => 'success',
                'code' => Response::HTTP_CREATED,
                'message' => $message,
            ],
            Response::HTTP_CREATED
        );
    }

    private function badRequestResponse($message = 'Bad request')
    {
        return response()->json(
            [
                'status' => 'Bad request',
                'code' => Response::HTTP_BAD_REQUEST,
                'message' => $message,
            ],
            Response::HTTP_BAD_REQUEST
        );
    }

    private function internalServerErrorResponse($message = 'Internal Server Error')
    {
        return response()->json(
            [
                'status' => 'Internal Server Error',
                'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => $message,
            ],
            Response::HTTP_INTERNAL_SERVER_ERROR
        );
    }
}
