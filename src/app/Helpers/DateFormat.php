<?php

use Carbon\Carbon;

if (!function_exists('japaneseDateFormat')) {
    function japaneseDateFormat($date): string
    {
        return Carbon::parse($date)->format('Y年m月d日');
    }
}

if (!function_exists('japaneseDateFormatAddDays')) {
    function japaneseDateFormatAddDays($date, $days): string
    {
        return Carbon::parse($date)->addDays($days)->format('Y年m月d日');
    }
}

if (!function_exists('dateTimeSlashFormat')) {
    function DateTimeSlashFormat($date): string
    {
        return Carbon::parse($date)->format('Y/m/d');
    }
}
