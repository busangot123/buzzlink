<?php

use App\Models\Information;
use App\Models\Message;
use App\Models\Notification;

if (!function_exists('headerMessage')) {
    function headerMessage()
    {
        $messages = Message::where('received_user', Auth::user()->id)
            ->where('is_read', false)
            ->with('sentUser')
            ->orderByDesc('created_at')
            ->get()
        ;

        return [
            'latest' => $messages->groupBy('project_user_id')->map(function ($item) {
                return $item->first();
            })->take(5),
            'count' => count($messages),
        ];
    }
}

if (!function_exists('headerNotification')) {
    function headerNotification()
    {
        $notif = Notification::where([
            'user_id' => Auth::user()->id,
            'is_notification_read' => false,
        ]);

        return [
            'latest' => $notif->latest()->take(5)->get(),
            'count' => $notif->get()->count(),
        ];
    }
}

if (!function_exists('headerInformation')) {
    function headerInformation()
    {
        $info = Information::publishedInformations()->displayPosition()->orderByUpdatedAt()->latest()->take(5)->get();

        return [
            'info' => $info,
            'is_user_info_read' => Auth::user()->fresh()->is_information_read,
        ];
    }
}
