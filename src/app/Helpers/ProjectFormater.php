<?php

namespace App\Helpers;

use App\Models\Project;
use App\Models\ProjectUser;
use Carbon\Carbon;

class ProjectFormater
{
    /**
     * Fetch project details use.
     *
     * @param mixed $data
     * @param mixed $project_id
     */
    public static function projectDetails($project_id)
    {
        $project = Project::getProject($project_id);
        $is_applied_data = ProjectUser::where([
            'project_id' => $project_id,
            'influencer_user_id' => auth()->user()->id,
        ])->first();
        $is_applied = auth()->user() && $is_applied_data && $is_applied_data->application_status != Constant::PROJECT_APPLICATION_STATUS['COMPLETED'] ? true : false;
        $applied_user = ProjectUser::where('project_id', $project->id)
            ->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['RECORD'])
            ->count()
        ;
        $contacted_user = ProjectUser::where('project_id', $project->id)
            ->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['CONTACT'])
            ->count()
        ;

        self::formatProjectData($project);

        return [
            'project' => $project,
            'client_rating' => $project->user->client_rate,
            'is_applied' => $is_applied,
            'applied_user' => $applied_user,
            'contacted_user' => $contacted_user,
        ];
    }

    public static function getProjectDurationFromNow($duration)
    {
        $endOfDay = $duration->endOfDay();

        if (now()->diffInDays($endOfDay, false) <= 0) {
            $hours = now()->diffInHours($endOfDay, false) < 0 ? 0 : now()->diffInHours($endOfDay, false);

            return [
                'duration' => $hours,
                'sign' => 0 == $hours ? '日' : '時間',
            ];
        }

        return [
            'duration' => now()->diffInDays($endOfDay, false),
            'sign' => '日',
        ];
    }

    /**
     * Format project datas for frontend use.
     *
     * @param mixed $data
     */
    public static function formatProjectData($data)
    {
        $client_rating = 0;
        $sns_code_list = [];
        $application_deadline = Carbon::parse($data->application_deadline_at);
        $data->application_hours_left = self::getProjectDurationFromNow($application_deadline);
        $data->application_deadline_at = $application_deadline->format('n月j日');
        $data->application_deadline_inner = $application_deadline->format('Y年n月j日');
        $selection_deadline = Carbon::parse($data->selection_deadline_at);
        $data->selection_hours_left = self::getProjectDurationFromNow($selection_deadline);
        $data->selection_deadline_at = $selection_deadline->format('n月j日');
        $report_deadline = Carbon::parse($data->report_deadline_at);
        $data->report_hours_left = self::getProjectDurationFromNow($report_deadline);
        $checking_deadline = Carbon::parse($data->report_deadline_at)->addDays(14);
        $data->report_deadline_at = $report_deadline->format('n月j日');
        $data->report_deadline_inner = $report_deadline->format('Y年n月j日');
        $data->checking_hours_left = self::getProjectDurationFromNow($checking_deadline);
        $data->checking_deadline_at = $checking_deadline->format('n月j日');
        $data->published = Carbon::parse($data->created_at)->format('Y年n月j日');
        if (!isset($data->hasManyProjectUsers)) {
            $data->completion_date = Carbon::parse($data->evaluated_at)->format('n月j日');
        }
        $data->status = Constant::PROJECT_STATUS_LABEL[$data->status_code];
        foreach (explode(',', $data->post_sns_code) as $code) {
            $sns_code_list[] = Constant::SNS_CODE[$code];
        }
        $data->sns_code_list = $sns_code_list;
    }

    public static function formatProjectDetails($data)
    {
        self::formatProjectData($data);

        $deadline_at = '';

        switch ($data->status_code) {
            case Constant::PROJECT_STATUS['RECRUITING']:
                $hours_left = $data->application_hours_left;
                $deadline_at = $data->application_deadline_at;

                break;

            case Constant::PROJECT_STATUS['SELECTION']:
                $hours_left = $data->selection_hours_left;
                $deadline_at = $data->selection_deadline_at;

                break;

            case Constant::PROJECT_STATUS['COMPLETED']:
                $hours_left = $data->report_hours_left;
                $deadline_at = $data->report_deadline_at;

                break;

            case Constant::PROJECT_STATUS['CHECKING']:
                $hours_left = $data->checking_hours_left;
                $deadline_at = $data->checking_deadline_at;

                break;
        }

        $data->project_time_details = [
            'hours_left' => $hours_left['duration'],
            'hours_left_sign' => $hours_left['sign'],
            'deadline_at' => $deadline_at.'まで',
        ];

        return $data;
    }

    /**
     * Format sns_url to embed into frontend.
     *
     * @param mixed $data
     * @param mixed $sns_url
     */
    public static function getSnsFrameUrl($sns_url)
    {
        $sns_parts = explode('/', $sns_url);
        $domain = 'com' == explode('.', $sns_parts[Constant::SNS_PARTS['HOSTING']])[1] ? 'twitter' : explode('.', $sns_parts[Constant::SNS_PARTS['HOSTING']])[1];

        if ($sns_url) {
            switch ($domain) {
                case 'instagram':
                    $data = [
                        'url' => 'https://'.$sns_parts[Constant::SNS_PARTS['HOSTING']].'/'.$sns_parts[Constant::SNS_PARTS['PARAMS']].'/'.$sns_parts[Constant::SNS_PARTS['POST_ID']].'/embed',
                        'domain' => $domain,
                    ];

                    return $data;

                    break;

                case 'youtube':
                    $data = [
                        'url' => 'https://'.$sns_parts[Constant::SNS_PARTS['HOSTING']].'/embed/'.explode('=', $sns_parts[Constant::SNS_PARTS['PARAMS']])[Constant::SNS_PARTS['PROTOCOL']],
                        'domain' => $domain,
                    ];

                    return $data;

                    break;

                case 'facebook':
                    $data = [
                        'url' => $sns_url,
                        'domain' => $domain,
                    ];

                    return $data;

                    break;

                case 'tiktok':
                    $data = [
                        'url' => 'https://'.$sns_parts[Constant::SNS_PARTS['HOSTING']].'/embed/'.$sns_parts[Constant::SNS_PARTS['POST_LINK']],
                        'domain' => $domain,
                    ];

                    return $data;

                    break;

                case 'twitter':
                    $data = [
                        'url' => $sns_url,
                        'domain' => $domain,
                    ];

                    return $data;

                    break;

                default:
                    return null;

                    break;
            }
        }
    }
}
