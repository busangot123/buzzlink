<?php

namespace App\Helpers;

use Carbon\Carbon;

class Utils
{
    /**
     * Convert date or datetime to japanese date format.
     *
     * @param mixed $date
     *
     * @return date format
     */
    public static function japaneseDateFormat($date)
    {
        return Carbon::parse($date)->format('Y年m月d日');
    }

    /**
     * Convert date or datetime to japanese datetime format.
     *
     * @param mixed $date
     *
     * @return datetime format
     */
    public static function japaneseDateTimeFormat($date)
    {
        return Carbon::parse($date)->format('Y年m月d日 H時i分');
    }

    /**
     * remove time in the date data.
     *
     * @param mixed $date
     *
     * @return datetime format
     */
    public static function DateTimeFormat($date)
    {
        return Carbon::parse($date)->format('Y/m/d');
    }

    /**
     * get route to display on mypage: client, same as project management.
     */
    public static function ProjectRoutePerStatus(object $record): string
    {
        $status = is_null($record->status_code) ? $record->application_status : $record->status_code;

        switch ($status) {
            case Constant::PROJECT_STATUS['RECRUITING']:
                return route('project.details', ['project_id' => $record->id, 'limit' => 50, 'management' => 'true']);

            case Constant::PROJECT_STATUS['SELECTION']:
                return route('mypage.project_management.selection.form', ['project_id' => $record->id]);

            case Constant::PROJECT_APPLICATION_STATUS['WORKING']:
                return route('project_management.requesting', ['project_user_id' => $record->id]);

            case Constant::PROJECT_APPLICATION_STATUS['CHECKING']:
                return route('project_management.checking.input', ['project_user_id' => $record->id]);
        }
    }

    /**
     * get route to display mypage: influencer, same as work management.
     */
    public static function ProjectUserRoutePerStatus(object $record): string
    {
        switch ($record->application_status) {
            case Constant::PROJECT_APPLICATION_STATUS['APPLIED']:
                return route('workmanagement.details', ['project_id' => $record->project->id]);

            case Constant::PROJECT_APPLICATION_STATUS['SELECTION']:
                return route('workmanagement.details', ['project_id' => $record->project->id]);

            case Constant::PROJECT_APPLICATION_STATUS['WORKING']:
                return route('work_management.review', ['project_id' => $record->project->id]);

            case Constant::PROJECT_APPLICATION_STATUS['CHECKING']:
                return route('work_management.checking', ['project_id' => $record->project->id]);
        }
    }
}
