<?php

namespace App\Helpers;

class MailFormat
{
    public static function decodeTemplate(array $data, string $template)
    {
        return preg_replace_callback(
            '~\{\{(.*?)\}\}~',
            function ($key) use ($data) {
                $email_variables = collect($data)->map(function ($value) {
                    return $value;
                });

                return $email_variables[$key[1]];
            },
            htmlspecialchars_decode($template)
        );
    }
}
