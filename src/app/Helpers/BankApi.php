<?php

namespace App\Helpers;

class BankApi
{
    public static function bankCodeJpApi($url): object
    {
        $client = new \GuzzleHttp\Client();

        return $client->request('GET', $url, [
            'query' => ['apiKey' => config('app.bank_api_key')],
        ]);
    }
}
