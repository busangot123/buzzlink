<?php

namespace App\Helpers;

class Constant
{
    public const STATUS = [
        'SUCCESS' => 200,
        'UNAUTHORIZE' => 401,
    ];

    public const CURRENCY = [
        'JPY' => 'jpy',
    ];

    public const PAGINATION_COUNT = 30;
    public const LIST_LIMIT = 50;

    public const CREDENTIALS = ['email', 'password'];
    public const UUID = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    public const USER_ACTIVATION = 1;
    public const USER_ALREADY_EMAIL_VERIFIED = 2;
    public const REGISTRATION_SUCCESS = 3;
    public const PASSWORD_RESET = 4;
    public const PASSWORD_RESET_SUCCESS = 5;
    public const PASSWORD_RESET_SUCCESS_USER_INFO = 6;
    public const CHANGE_EMAIL = 7;
    public const BANK_ACCOUNT = 8;
    public const PROJECT_REGISTER = 9;
    public const INFLUENCER_APPLY = 10;
    public const CLIENT_APPLY = 11;
    public const APPLICATION_DEADLINE_END_APPLIED = 12;
    public const APPLICATION_DEADLINE_END_APPLIED_NONE = 13;
    public const SELECTION_DEADLINE_REMIND = 14;
    public const SELECTION_DEADLINE_END_CLIENT = 15;
    public const SELECTION_DEADLINE_END_INFLUENCER = 16;
    public const SELECTION_DONE_CLIENT = 17;
    public const SELECTION_DONE_INFLUENCER = 18;
    public const SELECTION_DONE_INFLUENCER_UNSELECTED = 19;
    public const REPORT_DEADLINE_REMIND = 20;
    public const REPORT_DEADLINE_END_INFLUENCER = 21;
    public const REPORT_DEADLINE_END_CLIENT = 22;
    public const REPORT_DONE_INFLUENCER = 23;
    public const REPORT_DONE_CLIENT = 24;
    public const REVIEW_DEADLINE_REMIND = 25;
    public const REVIEW_DEADLINE_END_CLIENT = 26;
    public const REVIEW_DEADLINE_END_INFLUENCER = 27;
    public const REVIEW_DONE_CLIENT = 28;
    public const REVIEW_DONE_INFLUENCER = 29;
    public const REVIEW_DONE_INFLUENCER_PAYMENT = 30;
    public const MESSAGE_RECEIVED_REMIND = 31;
    public const INFORMATION_FROM_ADMIN = 32;

    public const NOT_READ = 0;
    public const READ = 1;
    public const CAN_BE_REJECT_EMAIL = '11,14,20,25,31,32';
    public const DEFAULT_CLIENT_COMMENT = 'コメントはありません';

    public const NOTIFICATION_CONTENTS = [
        'DONE_CLIENT_REVIEW' => 'の結果確認完了のお知らせ。',
        'SELECTED' => 'の選定結果のお知らせ',
        'NOT_SELECTED' => 'の選定結果のお知らせ。',
        'INFLUENCER_PAYMENT' => 'のタスクが全て完了しました。獲得した報酬は、以下よりご確認ください。',
        'APPLY_INFLUENCER' => 'に新しい応募がありました。',
        'BATCH_APPLICATION' => 'を依頼するインフルエンサーを選定してください。',
        'BATCH_NO_APPLICATION' => 'の募集期間が終了しました。インフルエンサーからの応募はありませんでした。',
        'BATCH_SELECTION_CLIENT' => 'の選定期間が終了しました。',
        'BATCH_REPORT_REMINDER' => 'の完了報告をお願いします。',
        'BATCH_REPORTING' => 'の完了報告について',
        'BATCH_COMPLETION_REMINDER' => 'の結果確認をお願いします',
        'BATCH_COMPLETION_CLIENT' => 'の結果確認について',
        'BATCH_COMPLETION_INFLUENCER' => 'の結果確認完了について',
    ];

    public const PROJECT_STATUS_LABEL = [
        1 => '募集中',
        2 => '選定中',
        3 => '依頼中',
        4 => '結果確認中',
        5 => '完了',
        9 => '募集終了',
    ];

    public const PROJECT_USER_STATUS_LABEL = [
        1 => '応募中',
        2 => '選定中',
        3 => '作業中',
        4 => '結果確認中',
        5 => '完了',
        6 => '選定漏れ',
        9 => '応募取消',
    ];

    public const SNS_PARTS = [
        'PROTOCOL' => 1,
        'HOSTING' => 2,
        'PARAMS' => 3,
        'POST_ID' => 4,
        'POST_LINK' => 5,
    ];

    public const PROJECT_PARAMETER = 5;

    public const PROJECT_STATUS = [
        'RECRUITING' => 1,
        'SELECTION' => 2,
        'COMPLETED' => 3,
        'CHECKING' => 4,
        'DONE' => 5,
        'EXPIRED' => 9,
    ];

    public const USER_ROLE_STATUS = [
        'CLIENT' => 1,
        'INFLUENCER' => 2,
    ];

    public const USER_ROLE = [
        'CLIENT' => 'client',
        'INFLUENCER' => 'influencer',
    ];

    public const PROJECT_APPLICATION_STATUS = [
        'APPLIED' => 1,
        'CONTACT' => [3, 4, 5],
        'SELECTION' => 2,
        'WORKING' => 3,
        'CHECKING' => 4,
        'DONE' => 5,
        'COMPLETED' => 6,
        'MISSING' => 9,
        'COMPLETION_RATE' => [4, 5],
        'RECORD' => [1, 2, 3, 4, 5, 6],
        'MY_PAGE' => [1, 2, 3, 4],
        'MY_PAGE_CLIENT' => [1, 2],
        'MY_PAGE_INFLUENCER' => [3, 4],
    ];

    public const SNS_CODE = [
        '-',
        'youtube',
        'instagram',
        'facebook',
        'twitter',
        'tiktok',
    ];

    public const SNS_FOLLOWERS_OPTIONS = [
        1 => '1,000未満',
        2 => '1,000以上～5,000未満',
        3 => '5,000以上～10,000未満',
        4 => '10,000以上～50,000未満',
        5 => '50,000以上～100,000未満',
        6 => '100,000以上～1,000,000未満',
        7 => '1,000,000以上',
    ];

    public const REWARD_DETAIL_PARAMS = '#報酬明細';
}
