<?php

namespace App\Helpers;

use File;

class UploadHelper
{
    private $image_path;

    public function __construct($type = '')
    {
        if ('avatar' == $type) {
            $this->image_path = 'images/avatar_images/'.auth()->user()->uuid;
        }
    }

    public function uploadImage($image)
    {
        $file_name = time().'.'.$image->getClientOriginalExtension();
        if (!File::exists(public_path().$this->image_path)) {
            File::makeDirectory(public_path().$this->image_path, $mode = 0777, true, true);
        }
        $image->move($this->image_path, $file_name);

        return $this->image_path.'/'.$file_name;
    }
}
