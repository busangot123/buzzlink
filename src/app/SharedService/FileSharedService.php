<?php

namespace App\SharedService;

use Illuminate\Support\Facades\Storage;

class FileSharedService
{
    private $public_bucket = 's3_public';
    private $private_bucket = 's3_private';

    /**
     * upload files, return uploaded files info.
     *
     * @param mixed $path_name
     *
     * @return array $upload_files
     */
    public function uploadFiles(array $files, $path_name): array
    {
        $upload_files = [];

        $path = '/'.auth()->user()->uuid.'/'.$path_name;
        foreach ($files as $file) {
            $origin_file_name = $file->getClientOriginalName();
            $data = $file;

            $file_path = $this->uploadPublicBucket($path, $data);
            array_push($upload_files, ['name' => $origin_file_name, 'path' => $file_path]);
        }

        return $upload_files;
    }

    public function moveFileDirectory($file, $directory)
    {
        $path = '/'.auth()->user()->uuid.'/'.$directory.'/';
        $new_path = $path.basename($file);
        if (!file_exists($this->public_bucket.$new_path)) {
            Storage::disk($this->public_bucket)->move($file, $new_path);
        }

        return $new_path;
    }

    public function downloadPublicFile(string $path): string
    {
        return Storage::disk($this->public_bucket)->get($path);
    }

    public function downloadPrivateFile(string $path): string
    {
        return Storage::disk($this->private_bucket)->get($path);
    }

    /**
     * upload files to private bucket.
     *
     * @return array [name: string, path: string]
     */
    public function uploadPrivateFiles(array $files, string $path_name): array
    {
        $upload_files = [];

        $path = '/'.auth()->user()->uuid.'/'.$path_name;
        foreach ($files as $file) {
            $origin_file_name = $file->getClientOriginalName();
            $data = $file;

            $file_path = $this->uploadPrivateBucket($path, $data);
            array_push($upload_files, ['name' => $origin_file_name, 'path' => $file_path]);
        }

        return $upload_files;
    }

    public function getPublicFileMimeType(string $path): string
    {
        return Storage::disk($this->public_bucket)->mimeType($path);
    }

    public function getPrivateFileMimeType(string $path): string
    {
        return Storage::disk($this->private_bucket)->mimeType($path);
    }

    /**
     * upload to S3 public bucket.
     *
     * @param string $fileName
     * @param data   $data
     *
     * @return string $file_path
     */
    private function uploadPublicBucket(string $path, $data): string
    {
        return Storage::disk($this->public_bucket)->put($path, $data, 'public');
    }

    /**
     * upload to S3 private bucket.
     *
     * @param data $data
     *
     * @return string file path
     */
    private function uploadPrivateBucket(string $path, $data): string
    {
        return Storage::disk($this->private_bucket)->put($path, $data, 'private');
    }
}
