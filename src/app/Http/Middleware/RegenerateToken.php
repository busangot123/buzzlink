<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RegenerateToken
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ('POST' === $request->method()) {
            $request->session()->regenerateToken();
        }

        return $next($request);
    }
}
