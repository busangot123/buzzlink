<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class NotVerifiedEmail
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $email = $request->route('email');
        if (!User::where('email', $email)->exists()) {
            return $next($request);
        }

        throw new NotFoundHttpException();
    }
}
