<?php

namespace App\Http\Middleware;

use App\Models\User;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->auth) {
            $payload = explode('|', Crypt::decryptString($request->auth));
            $user = User::where(['email_verify_token' => $request->auth, 'email_verified' => 0])->first();
            $change_user = count($payload) > 1 ? User::where('uuid', $payload[1])->first() : null;

            if (Carbon::now() > $payload[0]) {
                return redirect('/expired');
            }
            if (!$user && 1 == count($payload)) {
                return redirect('/user_not_found');
            }
            if (!$change_user && 3 == count($payload)) {
                return redirect('/user_not_found');
            }

            return $next($request);
        }

        return redirect('/');
    }
}
