<?php

namespace App\Http\Middleware;

use App\Helpers\Constant;
use App\Models\ProjectUser;
use Closure;
use Illuminate\Http\Request;

class RedirectIfHasInfluencerEva
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $pending_review = ProjectUser::where([
            'project_id' => $request->project_id,
            'influencer_user_id' => auth()->user()->id,
            'application_status' => Constant::PROJECT_APPLICATION_STATUS['WORKING'],
        ])->exists();

        if ($pending_review) {
            return $next($request);
        }

        return redirect()->route('mypage.work_management');
    }
}
