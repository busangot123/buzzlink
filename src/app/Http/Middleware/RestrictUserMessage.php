<?php

namespace App\Http\Middleware;

use App\Models\ProjectUser;
use Closure;
use Illuminate\Http\Request;

class RestrictUserMessage
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $project_user = ProjectUser::whereId($request->project_user_id)->first();
        if ($project_user) {
            $authorize_influencer = $project_user->influencer_user_id == auth()->user()->id;
            $authorize_client = $project_user->project->user->id == auth()->user()->id;

            if ($authorize_client || $authorize_influencer) {
                return $next($request);
            }
        }

        return redirect()->route('chat.messages');
    }
}
