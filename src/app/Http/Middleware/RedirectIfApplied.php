<?php

namespace App\Http\Middleware;

use App\Helpers\Constant;
use App\Models\ProjectUser;
use Closure;
use Illuminate\Http\Request;

class RedirectIfApplied
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $is_applied = ProjectUser::where([
            'project_id' => $request->project_id,
            'influencer_user_id' => auth()->user()->id,
            ['application_status', '!=', Constant::PROJECT_APPLICATION_STATUS['MISSING']],
        ])->exists();

        if (!$is_applied) {
            return $next($request);
        }

        return redirect()->route('mypage');
    }
}
