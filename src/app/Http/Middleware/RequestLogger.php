<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RequestLogger
{
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    /**
     * Handle an incoming request.
     *
     * @param Closure $next
     * @param mixed   $response
     *
     * @return mixed
     */
    public function terminate(Request $request, $response)
    {
        $this->write($request);
    }

    private function write(Request $request): void
    {
        Log::channel('request')->debug(
            'request',
            [],
        );
    }
}
