<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ProjectSelectionAuth
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (auth()->user()->projects->contains($request->project_id)) {
            return $next($request);
        }
        abort(404);
    }
}
