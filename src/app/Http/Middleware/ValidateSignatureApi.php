
<?php

namespace Illuminate\Routing\Middleware;

use Closure;
use Illuminate\Routing\Exceptions\InvalidSignatureException;

class ValidateSignatureApi
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param null|string              $relative
     *
     * @throws \Illuminate\Routing\Exceptions\InvalidSignatureException
     *
     * @return \Illuminate\Http\Response
     */
    public function handle($request, Closure $next, $relative = null)
    {
        if ($request->hasValidSignature('relative' !== $relative)) {
            return $next($request);
        }

        throw new InvalidSignatureException();
    }
}
