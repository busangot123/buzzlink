<?php

namespace App\Http\Middleware;

use App\Helpers\Constant;
use App\Models\ProjectUser;
use Closure;
use Illuminate\Http\Request;

class RedirectIfHasClientEva
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $pending_review = ProjectUser::where(
            [
                'id' => $request->project_user_id,
                'application_status' => Constant::PROJECT_APPLICATION_STATUS['CHECKING'],
            ]
        )->exists();

        if ($pending_review) {
            return $next($request);
        }

        return redirect()->route('mypage.project_management');
    }
}
