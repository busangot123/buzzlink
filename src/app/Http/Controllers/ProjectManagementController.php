<?php

namespace App\Http\Controllers;

use App\Events\CompleteEvaluation;
use App\Helpers\Constant;
use App\Helpers\ProjectFormater;
use App\Http\Requests\ClientEvaluationRequest;
use App\Models\Project;
use App\Models\ProjectUser;
use Illuminate\Http\Request;
use Session;

class ProjectManagementController extends Controller
{
    public $project_user;
    public $project;

    public function __construct(Request $request)
    {
        if ($request->project_user_id) {
            $project_user = ProjectUser::getProjectUserWithProject($request->project_user_id);
            $project = $project_user->project;
            ProjectFormater::formatProjectData($project);
            $this->project_user = $project_user;
            $this->project = $project;
        }
    }

    public function checking($project_user_id)
    {
        $client_eva = Session::get('client_eva_'.$project_user_id);
        $client_comment = Session::get('client_comment_'.$project_user_id);

        return view('project.checking.review_input')->with([
            'project' => $this->project,
            'project_user' => $this->project_user,
            'client_eva' => $client_eva,
            'client_comment' => $client_comment,
        ]);
    }

    public function confirmation(Request $request)
    {
        $client_eva = Session::get('client_eva_'.$this->project_user->id);
        $client_comment = Session::get('client_comment_'.$this->project_user->id);

        return view('project.checking.confirmation')->with([
            'project' => $this->project,
            'project_user' => $this->project_user,
            'client_eva' => $client_eva,
            'client_comment' => $client_comment,
        ]);
    }

    public function confirmEvaluation(ClientEvaluationRequest $request)
    {
        $request->session()->put('client_comment_'.$this->project_user->id, $request->client_comment);
        $request->session()->put('client_eva_'.$this->project_user->id, $request->client_eva);

        return redirect()->route('project_management.checking.confirmation', ['project_user_id' => $this->project_user->id]);
    }

    public function updateProjectUserClientEvaluation(Request $request)
    {
        $project_user = $this->project_user;
        $project_user->client_eva = $request->client_eva;
        $project_user->client_comment = $request->client_comment;
        $project_user->application_status = Constant::PROJECT_APPLICATION_STATUS['DONE'];
        $project_user->evaluated_at = now();
        $project_user->update();

        $remaining_project_user = ProjectUser::where([
            'project_id' => $this->project->id,
            'application_status' => Constant::PROJECT_APPLICATION_STATUS['CHECKING'],
        ])->get();

        if (0 == count($remaining_project_user)) {
            $project = Project::find($this->project->id);
            $project->status_code = 5;
            $project->update();
        }

        $mail_data = [
            'project_title' => $this->project->title,
            'influencer_name' => $project_user->user->name,
            'client_name' => $this->project->user->name,
            'destination_url' => route('payment.list').Constant::REWARD_DETAIL_PARAMS,
            'influencer_url' => route('work_management.complete', [
                'project_id' => $this->project->id,
            ]),
            'client_url' => route('project_management.done', [
                'project_user_id' => $this->project_user->id,
            ]),
        ];

        CompleteEvaluation::dispatch($mail_data, $project_user, $this->project->user, $this->project->reward_amount);

        return redirect()->route('project_management.checking.done', ['project_user_id' => $this->project_user->id]);
    }

    public function completeClientEvaluation(Request $request)
    {
        Session::forget([
            'client_eva_'.$this->project_user->id,
            'client_comment_'.$this->project_user->id,
        ]);

        return view('project.checking.done');
    }

    public function showDoneProjectUser($project_user_id)
    {
        return view('project.done.report_details')->with([
            'project' => $this->project,
            'project_user' => $this->project_user,
        ]);
    }

    public function requestingUser($project_user_id)
    {
        return view('project.requesting.influencer_application')->with([
            'project' => $this->project,
            'project_user' => $this->project_user,
        ]);
    }
}
