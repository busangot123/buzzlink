<?php

namespace App\Http\Controllers;

use App\Models\QaCategory;
use App\Models\QaContent;
use App\Models\QaTitle;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    /*
    * default view of FAQ
    * @params none
    * @return view with collection of categories and relationships
    */
    public function index()
    {
        $qaCategory = new QaCategory();
        $categories = $qaCategory->getCategoriesWithTitles();

        return view('faq.faq', compact('categories'));
    }

    /*
    * @params keyword for search
    * @return collection
    */
    public function search(Request $request)
    {
        $keyword = $request->keyword;

        $qaCategory = new QaCategory();
        $categories = $qaCategory->getCategoriesWithTitles();

        $qaTitle = new QaTitle();
        $titleWithContents = $qaTitle->getTitlesWithContents($keyword);

        return view('faq.faqSearchResults', compact('categories', 'titleWithContents'));
    }

    /*
    * @params title id
    * @return array of collection and title variable
    */
    public function searchTitleId($title_id)
    {
        $qaTitle = new QaTitle();
        $title = $qaTitle->getTitleName($title_id);

        $qaCategory = new QaCategory();
        $categories = $qaCategory->getCategoriesWithTitles();

        $qaContent = new QaContent();
        $titleWithContents = $qaContent->getContentByTitleId($title_id);

        return view('faq.faqSearchResults', compact('categories', 'titleWithContents', 'title'));
    }
}
