<?php

namespace App\Http\Controllers;

use App\Helpers\BankApi;
use App\Models\BankAccount;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class BankAccountController extends Controller
{
    public function getBanks()
    {
        $bank_list = [];
        $selected_bank = null;
        $response = BankApi::bankCodeJpApi('https://apis.bankcode-jp.com/v1/banks?limit=2000');

        if (Auth::user()->bankAccount) {
            $bank_response = BankApi::bankCodeJpApi('https://apis.bankcode-jp.com/v1/banks/'.Auth::user()->bankAccount->code);
            $selected_bank = json_decode($bank_response->getBody())->code.' '.json_decode($bank_response->getBody())->name;
        }

        foreach (json_decode($response->getBody())->data as $bank) {
            $bank_list[] = $bank->code.' '.$bank->name;
        }

        return response()->json([
            'bank_list' => $bank_list,
            'selected_bank' => $selected_bank,
        ]);
    }

    public function getBranch(Request $request)
    {
        $branch_list = [];
        $selected_branch = null;
        $response = BankApi::bankCodeJpApi('https://apis.bankcode-jp.com/v1/banks/'.$request->bank_code.'/branches');

        if (Auth::user()->bankAccount && Auth::user()->bankAccount->code == $request->bank_code) {
            $branch_response = BankApi::bankCodeJpApi('https://apis.bankcode-jp.com/v1/banks/'.Auth::user()->bankAccount->code.'/branches/'.Auth::user()->bankAccount->branch_code);
            $selected_branch = json_decode($branch_response->getBody())[0]->code.' '.json_decode($branch_response->getBody())[0]->name;
        }

        foreach (json_decode($response->getBody())->data as $branch) {
            $branch_list[] = $branch->code.' '.$branch->name;
        }

        return response()->json([
            'branch_list' => $branch_list,
            'selected_branch' => $selected_branch,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->only([
            'is_auto_pay',
            'account_number',
            'account_type',
            'account_name',
            'code',
            'branch_code',
        ]);
        $data['influencer_user_id'] = auth()->user()->id;
        $data['account_checked_at'] = Carbon::now();
        BankAccount::updateOrCreate(['influencer_user_id' => auth()->user()->id], $data);
        $response = User::whereId(auth()->user()->id)->with(['prefectureData', 'hasManyGenres', 'bankAccount'])->first();

        $email_data = [
            'influencer_name' => auth()->user()->name,
            'bank_edit_url' => route('mypage.edit_information').'#振込先口座情報',
        ];
        $response->sendBankChangesNotification($email_data);

        return $this->response(
            '出金方式が更新されました。',
            $response,
            Response::HTTP_OK
        );
    }
}
