<?php

namespace App\Http\Controllers;

use App\Models\Information;
use App\Models\User;

class InformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        User::find(auth()->user()->id)->update(['is_information_read' => true]);

        $information = Information::publishedInformations()->displayPosition()->orderByUpdatedAt()->get();
        foreach ($information as $info) {
            $info->publish_date = date('d-m-Y', strtotime($info->publish_date));
        }

        return view('information.information', compact('information'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function showInformation($id)
    {
        $information = Information::where('id', $id)->first();
        $title = $information->title;
        $information->publish_date = date('d-m-Y', strtotime($information->publish_date));

        return view('information.information', compact('information', 'title'));
    }
}
