<?php

namespace App\Http\Controllers;

use App\Events\InfluencerEvaluation;
use App\Helpers\Constant;
use App\Helpers\ProjectFormater;
use App\Helpers\Utils;
use App\Http\Requests\InfluencerEvaluationRequest;
use App\Models\ProjectUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Session;

class WorkManagementController extends Controller
{
    public $project;
    public $project_user;
    public $sns_url;
    public $sns_iframe_data;
    public $influencer_eva;
    public $influencer_comment;

    public function setData($project_id)
    {
        $this->project_user = ProjectUser::where([
            'project_id' => $project_id,
            'influencer_user_id' => auth()->user()->id,
        ])->with(['user', 'project'])->first();
        $project = $this->project_user->project;
        ProjectFormater::formatProjectData($project);
        $this->project = $project;

        $this->sns_url = Session::get('sns_url_'.$this->project_user->id);
        $this->sns_iframe_data = $this->sns_url ? ProjectFormater::getSnsFrameUrl($this->sns_url) : null;
        $this->influencer_eva = Session::get('influencer_eva_'.$this->project_user->id);
        $this->influencer_comment = Session::get('influencer_comment_'.$this->project_user->id);
    }

    public function checking($project_id)
    {
        self::setData($project_id);

        return view('work.requesting.review')->with([
            'project' => $this->project,
            'project_user' => $this->project_user,
            'sns_url' => $this->sns_url,
            'sns_iframe_data' => $this->sns_iframe_data,
            'influencer_eva' => $this->influencer_eva,
            'influencer_comment' => $this->influencer_comment,
        ]);
    }

    public function confirmation($project_id)
    {
        self::setData($project_id);

        return view('work.requesting.confirmation')->with([
            'project' => $this->project,
            'project_user' => $this->project_user,
            'sns_url' => $this->sns_url,
            'sns_iframe_data' => $this->sns_iframe_data,
            'influencer_eva' => $this->influencer_eva,
            'influencer_comment' => $this->influencer_comment,
        ]);
    }

    public function confirmEvaluation(InfluencerEvaluationRequest $request)
    {
        $project_user = ProjectUser::where([
            'project_id' => $request->project_id,
            'influencer_user_id' => auth()->user()->id,
        ])->first();
        $request->session()->put('influencer_comment_'.$project_user->id, $request->influencer_comment);
        $request->session()->put('sns_url_'.$project_user->id, $request->sns_url);
        $request->session()->put('influencer_eva_'.$project_user->id, $request->influencer_eva);

        return redirect()->route('work_management.requesting.confirmation', ['project_id' => $request->project_id]);
    }

    public function updateProjectUserInfluencerEvaluation(Request $request)
    {
        self::setData($request->project_id);
        $project_user = $this->project_user;
        $project_user->influencer_eva = $request->influencer_eva;
        $project_user->influencer_comment = $request->influencer_comment;
        $project_user->sns_url = $request->sns_url;
        $project_user->application_status = Constant::PROJECT_APPLICATION_STATUS['CHECKING'];
        $project_user->reported_at = now();
        $project_user->update();

        $mail_data = [
            'project_title' => $this->project->title,
            'report_deadline' => Utils::japaneseDateFormat(Carbon::parse($this->project->getOriginal('report_deadline_at'))->addDays(14)),
            'influencer_name' => $project_user->user->name,
            'client_name' => $this->project->user->name,
            'influencer_url' => route('work_management.checking', [
                'project_id' => $this->project->id,
            ]),
            'client_url' => route('project_management.checking.input', [
                'project_user_id' => $this->project_user->id,
            ]),
        ];

        InfluencerEvaluation::dispatch($mail_data, $project_user->user, $this->project->user);

        return redirect()->route('work_management.done', ['project_user_id' => $request->project_id]);
    }

    public function completeInfluencerEvaluation($project_user_id)
    {
        Session::forget([
            'sns_url_'.$project_user_id,
            'influencer_eva_'.$project_user_id,
            'influencer_comment_'.$project_user_id,
        ]);

        return view('work.requesting.complete');
    }

    public function showCheckingEvaluation($project_id)
    {
        self::setData($project_id);

        return view('work.checking.checking_details', [
            'project' => $this->project,
            'project_user' => $this->project_user,
        ]);
    }

    public function showInfluencerEvaluation($project_id)
    {
        self::setData($project_id);

        return view('work.done.report_details', [
            'project' => $this->project,
            'project_user' => $this->project_user,
        ]);
    }
}
