<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use App\Helpers\ProjectFormater;
use App\Http\Requests\EmailRejectRequest;
use App\Http\Requests\UserChangePasswordRequest;
use App\Http\Requests\UserInformationRequest;
use App\Http\Requests\UserRequest;
use App\Models\Genre;
use App\Models\Prefecture;
use App\Models\Project;
use App\Models\ProjectUser;
use App\Models\RewardBalances;
use App\Models\User;
use App\SharedService\FileSharedService;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    use ThrottlesLogins;
    protected $maxAttempts = 5;
    protected $decayMinutes = 30;

    protected $upload;

    public function __construct(FileSharedService $upload)
    {
        $this->upload = $upload;
    }

    public function init(Request $request)
    {
        if (isset($request->auth)) {
            $token_data = explode('|', Crypt::decryptString($request->auth));
            if (count($token_data) > 1) {
                return response()->json(User::where('uuid', $token_data[1])->first());
            }

            return response()->json(User::where('email_verify_token', $request->auth)->first());
        }

        if (Auth::user()) {
            $response = User::whereId(Auth::user()->id)->with(['prefectureData', 'hasManyGenres', 'bankAccount'])->first();

            return response()->json($response);
        }

        return response()->json(false);
    }

    public function getProjectApplicant($id)
    {
        return Project::getProject($id);
    }

    public function username()
    {
        return Constant::CREDENTIALS[0];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\UserRequest $request
     * @param int                          $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\UserRequest $request
     * @param int                          $id
     *
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(UserRequest $request)
    {
        $data = $request->only([
            'self_introduction',
            'updated_user',
            'is_profile_update',
            'image',
            'file_name',
        ]);

        if (isset($data['file_name']) && null != $data['file_name']) {
            $avatar_image = $request->file('image');
            $uploaded_files = $this->upload->uploadFiles([$avatar_image], 'avatar_images');
            $data['avatar_image'] = $uploaded_files[0]['path'];
            unset($data['file_name'], $data['image']);
        }

        User::whereId(auth()->user()->id)->update($data);

        return $this->response(
            'プロフィールが更新されました。',
            $this->userWithRelation(),
            Response::HTTP_OK
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\UserInformationRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function updateUserInformations(UserInformationRequest $request)
    {
        $data = $request->only([
            'id',
            'name',
            'full_name',
            'full_name_kana',
            'phone_number',
            'postcode',
            'default_role',
            'prefecture',
            'address1',
            'address2',
            'sex',
            'birthday',
        ]);

        $data['birthday'] = false == strtotime($data['birthday']) ? null : $data['birthday'];
        User::whereId(auth()->user()->id)->update($data);

        return $this->response(
            'ユーザー情報は更新されました。',
            $this->userWithRelation(),
            Response::HTTP_OK
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\UserChangePasswordRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function updateUserPassword(UserChangePasswordRequest $request)
    {
        $data = $request->only(['password', 'id']);
        $data['password'] = Hash::make($data['password']);
        User::whereId($data['id'])->update($data);

        return $this->response(
            'パスワードが正常に更新されました。',
            $this->userWithRelation(),
            Response::HTTP_OK
        );
    }

    public function getClientProfile($uuid)
    {
        $client = $this->getUserByUuid($uuid);
        $project_id = Project::where('client_user_id', $client->id)
            ->whereIn('status_code', Constant::PROJECT_APPLICATION_STATUS['CONTACT'])
            ->orderBy('id', 'DESC')
            ->get()->map(function ($e) {
                return $e->id;
            });

        $client->client_projects->has_many_project_users_completion_rate_count = $this->getUserRate($client->client_projects->has_many_project_users_corresponded_count, $client->client_projects->has_many_project_users_completion_rate_count);
        $client->project_client_comment = ProjectUser::whereIn('project_id', $project_id)
            ->where('influencer_eva', '!=', null)
            ->orderBy('id', 'DESC')
            ->get()->take(4);

        return view('user.profile.client', compact('client'));
    }

    public function getInfluencerProfile($uuid)
    {
        $influencer = $this->getUserByUuid($uuid);
        $influencer->project_influencer = ProjectUser::with(['project' => function ($q) {
            $q->with('projectCategory');
        }])->whereInfluencerUserId($influencer->id)
            ->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['CONTACT'])
            ->orderBy('id', 'DESC')->get()->take(4);

        $influencer->project_influencer_comment = ProjectUser::whereInfluencerUserId($influencer->id)
            ->where('application_status', Constant::PROJECT_APPLICATION_STATUS['DONE'])
            ->where('client_eva', '!=', null)
            ->orderBy('id', 'DESC')->get()->take(4);

        return view('user.profile.influencer', compact('influencer'));
    }

    public function getUserByUuid($uuid)
    {
        $response = User::whereUuid($uuid)->with([
            'prefectureData', 'hasManyGenres',
            'projects' => function ($q) {
                $q->with(['hasManyUserCategory'])
                    ->whereIn('status_code', Constant::PROJECT_APPLICATION_STATUS['CONTACT'])->orderBy('id', 'DESC')
                    ->take(4)
                ;
            },
        ])
            ->withCount(['projects'])
            ->first()
        ;

        $response->has_many_project_users_recruitment_count = $this->getClientProjectsWithStatus($response->id);
        $response->registration_date = $this->parseDateInJapanese($response->created_at);
        $response->self_introduction = $response->self_introduction ? $response->self_introduction : '登録されていません。';

        $response->client_projects = User::whereUuid($uuid)->with(['hasManyProjectUsers' => function ($q) {
            $q->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['RECORD'])->orderBy('id', 'DESC');
        }])
            ->withCount([
                'hasManyProjectUsers as has_many_project_users_corresponded_count' => function ($q) {
                    $q->where('application_status', Constant::PROJECT_APPLICATION_STATUS['DONE']);
                },
                'hasManyProjectUsers as has_many_project_users_completion_rate_count' => function ($q) {
                    $q->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['COMPLETION_RATE']);
                },
            ])->first();

        return $response;
    }

    public function myPageInfluencer()
    {
        $response = $this->influencerDetails();

        return view('user.mypage.influencer', ['data' => $response]);
    }

    public function myPageClient()
    {
        $response = $this->ClientDetails();

        return view('user.mypage.client', ['data' => $response]);
    }

    public function myPageDetails()
    {
        $default_role = auth()->user()->default_role;

        switch ($default_role) {
            case Constant::USER_ROLE_STATUS['CLIENT']:
                return redirect()->route('mypage.client');

            case Constant::USER_ROLE_STATUS['INFLUENCER']:
                return redirect()->route('mypage.influencer');

            default:
                abort(404);
        }
    }

    public function myPageEditInformation(Request $request)
    {
        $auth = Auth::user();
        $user = User::whereId(Auth::user()->id)->with(['prefectureData', 'hasManyGenres', 'bankAccount'])->first();
        $prefectures = Prefecture::all();
        $genres = Genre::all();

        return view('user.mypage.editInformation', compact('user', 'prefectures', 'genres'));
    }

    public function influencerDetails()
    {
        $project_users_details = User::where('id', auth()->user()->id)
            ->with(['hasManyProjectUsers' => function ($q) {
                $q->with(['project' => function ($q) {
                    $q->with('user:id,uuid,avatar_image,name');
                }, 'projectCategory'])->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['MY_PAGE'])->orderBy('application_status')->latest();
            }])->get()->first();
        $reward_balance = RewardBalances::where('user_id', auth()->user()->id)->latest()->get()->first();

        return [
            'applied_count' => ProjectUser::where('influencer_user_id', auth()->user()->id)
                ->where('application_status', Constant::PROJECT_APPLICATION_STATUS['APPLIED'])->get()->count(),
            'working_count' => ProjectUser::where('influencer_user_id', auth()->user()->id)
                ->where('application_status', Constant::PROJECT_APPLICATION_STATUS['WORKING'])->get()->count(),
            'checking_count' => ProjectUser::where('influencer_user_id', auth()->user()->id)
                ->where('application_status', Constant::PROJECT_APPLICATION_STATUS['CHECKING'])->get()->count(),
            'evaluation_count' => ProjectUser::where('influencer_user_id', auth()->user()->id)
                ->where('client_eva', '!=', null)->get()->count(),
            'client' => User::where('id', auth()->user()->id)->get()->first(),
            'project_users' => $project_users_details,
            'reward_balance' => null == $reward_balance ? 0 : number_format($reward_balance->balance),
        ];
    }

    public function clientDetails()
    {
        $project = Project::where('client_user_id', auth()->user()->id);
        $client_project_ids = $project->get()->pluck('id')->toArray();
        $client = User::where('id', auth()->user()->id);
        $project_users_details = ProjectUser::whereIn('project_id', $client_project_ids)->with('user')
            ->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['MY_PAGE_INFLUENCER'])
            ->get()
        ;
        $project_details = Project::where('client_user_id', auth()->user()->id)->with('user')->with('hasManyProjectUsers')
            ->with('projectCategory')->whereIn('status_code', Constant::PROJECT_APPLICATION_STATUS['MY_PAGE_CLIENT'])
            ->orderByDesc('created_at')->get();

        $response = [
            'recruiting_count' => Project::getClientProjectsCountPerStatus(Constant::PROJECT_STATUS['RECRUITING']),
            'selection_count' => Project::getClientProjectsCountPerStatus(Constant::PROJECT_STATUS['SELECTION']),
            'completed_count' => ProjectUser::getClientProjectUserCountPerStatus($client_project_ids, Constant::PROJECT_APPLICATION_STATUS['WORKING']),
            'checking_count' => ProjectUser::getClientProjectUserCountPerStatus($client_project_ids, Constant::PROJECT_APPLICATION_STATUS['CHECKING']),
            'evaluation_count' => ProjectUser::whereIn('project_id', $client_project_ids)
                ->where('influencer_eva', '!=', null)->get()->count(),
            'client' => $client->get()->first(),
            'project_users' => $project_users_details,
            'project' => collect($project_details)->map(function ($project) {
                return ProjectFormater::formatProjectDetails($project);
            }),
        ];

        return $response;
    }

    public function getClientProjectsWithStatus($id)
    {
        $user_project_id = $this->getClientProjectId($id);

        return ProjectUser::whereIn('project_id', $user_project_id)
            ->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['CONTACT'])->count();
    }

    public function getClientProjectId($id)
    {
        return Project::whereClientUserId($id)->get()->map(function ($e) {
            return $e->id;
        });
    }

    public function getAllClientProjectsCount()
    {
        return ProjectUser::whereIn('project_id', $this->getClientProjectId(Auth::user()->id))
            ->where('application_status', '!=', 9)
            ->count()
        ;
    }

    public function getClientOrderRate()
    {
        $total_project_selected = $this->getClientProjectsWithStatus(Auth::user()->id);
        $total_project_applied = $this->getAllClientProjectsCount();
        if (0 == $total_project_applied) {
            return 0;
        }

        return round(($total_project_selected / $total_project_applied) * 100, 2);
    }

    public function updateEmailReject(EmailRejectRequest $request)
    {
        $user = User::findOrFail(auth()->user()->id);
        $user->email_reject_setting = $request->email_reject_setting ? null : Constant::CAN_BE_REJECT_EMAIL;
        $user->update();

        return Constant::STATUS['SUCCESS'];
    }

    public function getCommercialTransactionsPage()
    {
        return view('commercial_transactions');
    }

    private function parseDateInJapanese($data)
    {
        return Carbon::parse($data)->format('Y年n月j日');
    }

    private function getUserGender($id)
    {
        if (1 == $id) {
            return '男性';
        }
        if (2 == $id) {
            return '女性';
        }
        if (9 == $id) {
            return '公開しない';
        }
    }

    private function getUserRate($divisor, $dividen)
    {
        if ($dividen <= 0) {
            return 0;
        }
        $dividen = 0 == $dividen ? 1 : $dividen;

        return round(($divisor / $dividen) * 100, 2);
    }

    private function userWithRelation()
    {
        return User::whereId(Auth::user()->id)->with(['prefectureData', 'hasManyGenres', 'bankAccount'])->first();
    }
}
