<?php

namespace App\Http\Controllers;

use App\Models\Genre;

class GenreController extends Controller
{
    public function getGenre()
    {
        return Genre::all();
    }
}
