<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use App\Models\MailTemplate;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\View;
use SendGrid;

class MailController extends Controller
{
    public static function sendMail($email, $user, $template, $host)
    {
        if (isset($user->email_verify_token)) {
            $token = $user->email_verify_token;
        } else {
            $token = Crypt::encryptString(Carbon::now()->addDays(1).'|'.$user->uuid);
        }

        $template = MailTemplate::find($template);
        $mail = new SendGrid\Mail\Mail();
        $mail->setFrom(Constant::MAIL_SENDER, Constant::MAIL_SENDER_NAME);
        $mail->addTo($email);
        $mail->setSubject($template->name);
        $output = preg_replace_callback(
            '~\{\{(.*?)\}\}~',
            function ($key) use ($token, $user, $host) {
                $email_variable['host'] = $host;
                $email_variable['token'] = $token;
                $email_variable['email'] = $user->email;
                $email_variable['username'] = $user->name;

                return $email_variable[$key[1]];
            },
            htmlspecialchars_decode($template->content)
        );
        $mail->addContent('text/html', view('mails.email_template')->with('template', $output)->render());
        $sendgrid = new SendGrid(Constant::SENDGRID_API_KEY);

        try {
            $response = $sendgrid->send($mail);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function mailProjectManagement($email, $params, $influencer, $template, $host)
    {
        $template = MailTemplate::find($template);
        $mail = new SendGrid\Mail\Mail();
        $mail->setFrom(Constant::MAIL_SENDER, Constant::MAIL_SENDER_NAME);
        $mail->addTo($email);

        $output = preg_replace_callback(
            '~\{\{(.*?)\}\}~',
            function ($key) use ($influencer, $params, $host) {
                $email_variable['influencer_name'] = $influencer;
                $email_variable['project_path'] = '/details?project_id='.$params['id'].'&status=3';
                $email_variable['influencer_path'] = '/work_management/requesting?project_id='.$params['id'];
                $email_variable['not_selected_path'] = '/work_management/unselected?project_id='.$params['id'];
                $email_variable['username'] = $params['user']['name'];
                $email_variable['project_name'] = $params['title'];
                $email_variable['report_deadline'] = Carbon::parse($params['report_deadline_at'])->format('Y年n月j日');
                $email_variable['checking_deadline'] = Carbon::parse($params['report_deadline_at'])->addDays(14)->format('Y年n月j日');
                $email_variable['host'] = $host;

                return $email_variable[$key[1]];
            },
            htmlspecialchars_decode($template->content)
        );
        $subject = preg_replace_callback(
            '~\{\{(.*?)\}\}~',
            function ($key) use ($params) {
                $email_variable['project_name'] = $params['title'];

                return $email_variable[$key[1]];
            },
            htmlspecialchars_decode($template->name)
        );

        $mail->setSubject($subject);
        $mail->addContent('text/html', view('mails.email_template')->with('template', $output)->render());
        $sendgrid = new SendGrid(Constant::SENDGRID_API_KEY);

        try {
            $response = $sendgrid->send($mail);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function sendEmail($email, $data, $template_id)
    {
        $template = MailTemplate::find($template_id);
        $mail = new SendGrid\Mail\Mail();
        $mail->setFrom(Constant::MAIL_SENDER, Constant::MAIL_SENDER_NAME);
        $mail->setSubject(self::decodeTemplate($data, $template->name));
        $mail->addTo($email);
        $mail->addContent('text/html', view('mails.email_template')->with('template', self::decodeTemplate($data, $template->content))->render());
        $sendgrid = new SendGrid(Constant::SENDGRID_API_KEY);

        try {
            $response = $sendgrid->send($mail);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function decodeTemplate($data, $template)
    {
        return preg_replace_callback(
            '~\{\{(.*?)\}\}~',
            function ($key) use ($data) {
                $email_variables = collect($data)->map(function ($value) {
                    return $value;
                });

                return $email_variables[$key[1]];
            },
            htmlspecialchars_decode($template)
        );
    }
}
