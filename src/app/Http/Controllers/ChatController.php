<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use App\Http\Requests\ChatValidation;
use App\Models\Message;
use App\Models\ProjectUser;
use App\SharedService\FileSharedService;

class ChatController extends Controller
{
    public function fetchAllUserConversation($project_user_id = null)
    {
        $message_users = [];
        $user_id_list = [];
        $conversation_exist = Message::where('project_user_id', $project_user_id)->exists();
        $user_list = Message::getAllUserMessages($project_user_id);

        if (!$conversation_exist && $project_user_id) {
            $project_user = ProjectUser::whereId($project_user_id)->with('project', function ($q) {
                $q->with('user');
            })->first();

            $user_id_list[] = $project_user_id;
            if ($project_user->influencer_user_id == auth()->user()->id) {
                $message_users[] = [
                    'is_read' => 1,
                    'sent_user' => auth()->user(),
                    'received_user' => $project_user->project->user,
                    'project_user' => $project_user,
                ];
            } else {
                $message_users[] = [
                    'is_read' => 1,
                    'sent_user' => auth()->user(),
                    'received_user' => $project_user->user,
                    'project_user' => $project_user,
                ];
            }
        }

        foreach ($user_list as $message_user) {
            if (!in_array($message_user->project_user_id, $user_id_list) && $message_user->received_user != auth()->user()->id) {
                $user_id_list[] = $message_user->project_user_id;
                $message_users[] = [
                    'is_read' => $message_user->sent_user != auth()->user()->id ? $message_user->is_read : 1,
                    'sent_user' => $message_user->sentUser,
                    'received_user' => $message_user->receivedUser,
                    'project_user' => $message_user->projectUser,
                ];
            }

            if (!in_array($message_user->project_user_id, $user_id_list) && $message_user->sent_user != auth()->user()->id) {
                $user_id_list[] = $message_user->project_user_id;
                $message_users[] = [
                    'is_read' => $message_user->is_read,
                    'sent_user' => $message_user->receivedUser,
                    'received_user' => $message_user->sentUser,
                    'project_user' => $message_user->projectUser,
                ];
            }
        }

        return $message_users;
    }

    public function fetchMessageNotif()
    {
        $data = [];
        $project_user_id_list = [];
        $id_list = [];
        $messages = Message::getMessageNotificaiton();
        foreach ($messages as $message) {
            if (!in_array($message->project_user_id, $project_user_id_list)) {
                $project_user_id_list[] = $message->project_user_id;
                $id_list[] = $message->sent_user;
                $data[] = $message;
            }
        }

        return response()->json([
            'id_list' => $id_list,
            'messages' => $data,
        ]);
    }

    public function showMessages()
    {
        $message_users = self::fetchAllUserConversation();

        return view('user.chat.chat_room', compact('message_users'));
    }

    public function fetchMessages($project_user_id)
    {
        Message::updateIsRead($project_user_id);
        $project_user = ProjectUser::whereId($project_user_id)->with(['user', 'project' => function ($q) {
            $q->with('user');
        }])->first();
        $message_users = self::fetchAllUserConversation($project_user_id);
        $active_messages = Message::getMessages($project_user_id);
        if ($project_user->influencer_user_id == auth()->user()->id) {
            $active_chat = [
                'user_id' => $project_user->project->user->id,
                'project_user_id' => $project_user_id,
                'title' => $project_user->project->title,
                'recipient_name' => $project_user->project->user->name,
            ];
        } else {
            $active_chat = [
                'user_id' => $project_user->user->id,
                'project_user_id' => $project_user_id,
                'title' => $project_user->project->title,
                'recipient_name' => $project_user->user->name,
            ];
        }

        return view('user.chat.chat_room', compact('message_users', 'active_messages', 'active_chat'));
    }

    public function fetchMessagesApi($project_user_id)
    {
        return Message::getMessages($project_user_id);
    }

    public function sendMessage(ChatValidation $request, FileSharedService $upload)
    {
        $attachment = null;
        if ($request->attachment) {
            $attachment = $upload->uploadPrivateFiles([$request->attachment], 'message_attachments/'.$request->project_user_id)[0];
        }

        $message = auth()->user()->messages()->create([
            'project_user_id' => $request->project_user_id,
            'content' => $request->message ? $request->message : null,
            'received_user' => $request->receiver,
            'title' => $request->title,
            'attachment_name' => $attachment ? $attachment['name'] : null,
            'attachment_path' => $attachment ? $attachment['path'] : null,
        ]);

        return [
            'message_id' => $message->id,
            'status' => Constant::STATUS['SUCCESS'],
        ];
    }

    public function getProjectUserId($project_id)
    {
        $project_user = ProjectUser::where(
            [
                'project_id' => $project_id,
                'influencer_user_id' => auth()->user()->id,
            ]
        )->first();

        return redirect(route('chat.room', ['project_user_id' => $project_user->id]));
    }

    public function seenMessages($project_user_id)
    {
        Message::updateIsRead($project_user_id);
    }
}
