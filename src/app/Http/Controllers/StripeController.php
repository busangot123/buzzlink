<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Stripe\Exception;
use Stripe\StripeClient;

class StripeController extends Controller
{
    private $stripe;
    private $stripe_customer_id;

    public function __construct()
    {
        $this->stripe = new StripeClient(config('services.stripe.secret_key'));
        $this->createRetrieveCustomer();
    }

    public function createRetrieveCustomer()
    {
        $user = auth()->user();

        try {
            $stripe_customer = null;
            if (empty($user->stripe_customer_id)) {
                $stripe_customer = $this->stripe->customers->create([
                    'name' => $user->full_name,
                    'email' => $user->email,
                    'metadata' => [
                        'uuid' => $user->uuid,
                    ],
                ]);
                $user->stripe_customer_id = $stripe_customer->id;
                $user->save();
            } else {
                $stripe_customer = $this->stripe->customers->retrieve($user->stripe_customer_id);
            }
            $this->stripe_customer_id = $stripe_customer->id;
        } catch (Exception\ApiErrorException $e) {
            Log::error($e->getMessage());
        }
    }

    public function createPaymentIntent($payment, $payment_method_id, $payment_intent_id)
    {
        $intent = null;
        $response = [];

        try {
            if (!is_null($this->stripe_customer_id)) {
                if (empty($payment_intent_id)) {
                    $intent = $this->stripe->paymentIntents->create([
                        'customer' => $this->stripe_customer_id,
                        'payment_method' => $payment_method_id,
                        'amount' => $payment->total_to_pay,
                        'currency' => Constant::CURRENCY['JPY'],
                        'confirmation_method' => 'manual',
                        'confirm' => true,
                    ]);
                } else {
                    $intent = $this->stripe->paymentIntents->retrieve(
                        $payment_intent_id
                    );
                    $intent->confirm();
                }
                $response = $this->paymentIntentResponse($intent);
            } else {
                $response = [
                    'error' => 'Invalid customer id',
                ];
            }
        } catch (Exception\ApiErrorException $e) {
            Log::error($e->getMessage());
            $response = [
                'error' => [
                    'slug' => Str::slug($e->getMessage(), '_'),
                    'message' => $e->getMessage(),
                ],
            ];
        }

        return $response;
    }

    public function confirmPaymentIntent($payment_intent_id)
    {
        try {
            $intent = $this->stripe->paymentIntents->retrieve(
                $payment_intent_id
            );
            $intent->confirm();
        } catch (Exception\ApiErrorException $e) {
            Log::error($e->getMessage());
        }
    }

    private function paymentIntentResponse($intent)
    {
        $response = [
            'payment_intent_id' => $intent->id,
        ];
        if ('requires_action' == $intent->status && 'use_stripe_sdk' == $intent->next_action->type) {
            $response['requires_action'] = true;
            $response['payment_intent_client_secret'] = $intent->client_secret;
        } elseif ('succeeded' == $intent->status) {
            $response['success'] = true;
            $response['message'] = 'Payment is processing';
        } else {
            return ['error' => 'Invalid payment intent status'];
        }

        return $response;
    }
}
