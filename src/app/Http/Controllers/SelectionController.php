<?php

namespace App\Http\Controllers;

use App\Events\ProjectSelection;
use App\Helpers\Constant;
use App\Helpers\ProjectFormater;
use App\Http\Requests\ProjectSelectedUserRequest;
use App\Http\Requests\ProjectSelectionPaymentRequest;
use App\Models\ClientPayment;
use App\Models\Fee;
use App\Models\Project;
use App\Models\ProjectUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SelectionController extends Controller
{
    public function form(Request $request, $project_id)
    {
        $selected_users_array = [];
        if ($request->session()->has('project.selection.selected_users'.$project_id)) {
            $selected_users_array = $request->session()->get('project.selection.selected_users'.$project_id);
        }

        $project = Project::find($project_id);
        $project_data = ProjectFormater::projectDetails($project_id);
        $project_users = ProjectUser::getProjectUsersAvailableInSelection($project_id);

        $post_url = route('mypage.project_management.selection.confirm');

        return view('user.project-management.selection.form', compact('post_url', 'project', 'selected_users_array', 'project_data', 'project_users'));
    }

    public function payment(Request $request, $project_id)
    {
        $selected_users_array = [];
        if (!$request->session()->has('project.selection.selected_users'.$project_id)) {
            return redirect()->route('mypage.project_management.selection.form', $project_id);
        }
        $selected_users_array = $request->session()->get('project.selection.selected_users'.$project_id);

        $project = Project::find($project_id);
        $project_data = ProjectFormater::projectDetails($project_id);
        $project_users = ProjectUser::getProjectUsersAvailableInSelection($project_id);

        $payment = Fee::computeSelectionPayment($selected_users_array, $project->reward_amount);
        $post_url = route('mypage.project_management.selection.complete');

        return view('user.project-management.selection.payment', compact('post_url', 'project', 'selected_users_array', 'payment', 'project_data', 'project_users'));
    }

    public function done(Request $request, $project_id)
    {
        return view('user.project-management.selection.done', compact('project_id'));
    }

    public function confirm(ProjectSelectedUserRequest $request)
    {
        $request->session()->put('project.selection.selected_users'.$request->project_id, $request->selected_users);

        return redirect()->route('mypage.project_management.selection.payment', ['project_id' => $request->project_id]);
    }

    public function stripe(ProjectSelectionPaymentRequest $request)
    {
        $selected_users = $request->session()->get('project.selection.selected_users'.$request->project_id);
        $project = Project::find($request->project_id);

        $stripe = new StripeController();
        $payment = Fee::computeSelectionPayment($selected_users, $project->reward_amount);
        $stripe_payment_intent = $stripe->createPaymentIntent($payment, $request->payment_method_id, $request->payment_intent_id);

        return response()->json($stripe_payment_intent);
    }

    public function complete(ProjectSelectionPaymentRequest $request)
    {
        if (!$request->session()->has('project.selection.selected_users'.$request->project_id)) {
            return redirect()->route('mypage.project_management.selection.form', $request->project_id);
        }

        $selected_users = $request->session()->get('project.selection.selected_users'.$request->project_id);
        $project = Project::find($request->project_id);

        DB::transaction(function () use ($project, $selected_users, $request) {
            $payment = Fee::computeSelectionPayment($selected_users, $project->reward_amount);
            ClientPayment::createClientPayment($project->id, $payment);

            $project->status_code = Constant::PROJECT_STATUS['COMPLETED'];
            foreach ($project->hasManyProjectUsers as $project_user) {
                if (!in_array($project_user->user->uuid, $selected_users)) {
                    $project_user->application_status = Constant::PROJECT_APPLICATION_STATUS['COMPLETED'];
                } else {
                    $project_user->application_status = Constant::PROJECT_APPLICATION_STATUS['WORKING'];
                    $project_user->selected_at = Carbon::now();
                }
                if ($project_user->update()) {
                    event(new ProjectSelection($project, $project_user));
                }
            }
            $project->update();
            $request->session()->forget('project.selection.selected_users'.$request->project_id);
        });

        return redirect()->route('mypage.project_management.selection.done', ['project_id' => $request->project_id]);
    }
}
