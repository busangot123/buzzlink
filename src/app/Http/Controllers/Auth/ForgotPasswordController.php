<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    /**
     * Send a reset link to the given user.
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        $forgotUser = User::where('email', $request->input('email'))->first();

        // email_verified user can password reset
        if ($forgotUser) {
            $response = $this->broker()->sendResetLink(
                $this->credentials($request)
            );
        }

        return view('auth.passwords.emailSent');
    }
}
