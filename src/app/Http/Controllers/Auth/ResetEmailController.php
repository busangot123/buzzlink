<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\EmailReset;
use App\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResetEmailController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/email/reset/complete';

    /**
     * Mark the authenticated user's email address as verified.
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function verify(Request $request)
    {
        $email_reset = EmailReset::where('email', $request->email)->firstOrFail();
        $user = User::where('email', $request->email)->firstOrFail();

        // email duplicate check
        if (User::where('email', $email_reset->new_email)->exists()) {
            abort(404);
        }

        // token check
        if (!$email_reset->exists($user, $request->route('token'))) {
            abort(404);
        }

        if ($email_reset->updateEmail($user)) {
            event(new Verified($user));
            Auth::logout();
        }

        return redirect($this->redirectPath())->with('verified', true);
    }

    protected function resetComplete(Request $request)
    {
        return view('auth.emailResetComplete');
    }
}
