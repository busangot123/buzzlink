<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ResetPasswordController extends Controller
{
    use ResetsPasswords;
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */
    protected $redirectTo = '/password/reset/complete';

    /**
     * check token before show reset form.
     *
     * @param [type] $token
     * @param [type] $email
     *
     * @return bool
     */
    public function checkToken($token, $email)
    {
        $password_reset = DB::table('password_resets')->where('email', $email)->first();

        if ($password_reset && Hash::check($token, $password_reset->token)) {
            $createdAt = Carbon::parse($password_reset->created_at);
            if (!Carbon::now()->greaterThan($createdAt->addMinutes(config('auth.passwords.users.expire')))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request)
    {
        $token = $request->route()->parameter('token');
        if (!$this->checkToken($token, $request->email)) {
            abort(404);
        }

        return view('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|string|confirmed|between:8,32|contain_alpha_numeric|alpha_numeric_symbol',
        ];
    }

    /**
     * Reset the given user's password.
     *
     * @param \Illuminate\Contracts\Auth\CanResetPassword $user
     * @param string                                      $password
     */
    protected function resetPassword($user, $password)
    {
        $this->setUserPassword($user, $password);
        $user->save();

        event(new PasswordReset($user));
        // $this->guard()->login($user);
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param string $response
     */
    protected function sendResetResponse(Request $request, $response)
    {
        // TODO: can not redirect...(redirect path become require auth)
        return view('auth.passwords.complete');
    }

    protected function resetComplete(Request $request)
    {
        return view('auth.passwords.complete');
    }
}
