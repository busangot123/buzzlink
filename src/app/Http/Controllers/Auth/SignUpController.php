<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Services\SignUpService;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SignUpController extends Controller
{
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email:rfc', 'max:255'],
        ]);
    }

    protected function emailVerificationView()
    {
        return view('auth.emailVerification');
    }

    /**
     * Send Verify mail.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function sendEmailVerification(Request $request)
    {
        $this->validator($request->all())->validate();
        $email = $request->input('email');

        $signup_service = new SignUpService($email);
        $signup_service->sendRegisterEmail();

        return view('auth.emailVerificationConfirm', compact('email'));
    }
}
