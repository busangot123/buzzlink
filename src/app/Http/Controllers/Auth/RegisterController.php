<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Constant;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $row_session = 'register_input'; // for saving user input
    protected $encrypted_session = 'register_encrypted'; // for using server

    public function __construct()
    {
        $this->middleware('email.not.verified');
    }

    /**
     * get signed url parameter.
     *
     * @param Request $request['uuid', 'expires', 'signature']
     *
     * @return array params, query
     */
    protected function getSignedUrl(Request $request): array
    {
        return [
            'email' => $request->email,
            'expires' => $request->expires,
            'signature' => $request->signature,
        ];
    }

    protected function infoPage(Request $request)
    {
        $email = $request->route('email');
        $session_input = $request->session()->get($this->row_session);

        // pass signed url(post)
        $post_url = route('register.confirm', $this->getSignedUrl($request));

        return view('auth.register', compact('email', 'session_input', 'post_url'));
    }

    protected function confirm(RegisterRequest $request)
    {
        $input_encrypted = $request->registerAttributes();
        $input = $request->registerAttributesWithoutPassword();

        $input['password_num'] = preg_replace('(.)', '*', $input_encrypted['password']);
        $input['password'] = '';
        $input['re_password'] = '';
        $request->session()->put($this->row_session, $input);

        $input_encrypted['password'] = Hash::make($input_encrypted['password']);
        $request->session()->put($this->encrypted_session, $input_encrypted);

        return redirect()->route('register.confirm_page', $this->getSignedUrl($request));
    }

    protected function confirmPage(Request $request)
    {
        $input = $request->session()->get($this->row_session);
        if (!$input) {
            return redirect()->route('register.info_page', $this->getSignedUrl($request));
        }

        $email = $request->route('email');
        $patch_url = route('register.complete', $this->getSignedUrl($request));

        return view('auth.registerConfirm', compact('email', 'input', 'patch_url'));
    }

    protected function setUserInfo(Request $request)
    {
        $input = $request->session()->get($this->encrypted_session);
        if (!$input) {
            return redirect()->route('register.info_page', $this->getSignedUrl($request));
        }

        $input['uuid'] = 'BLC-'.substr(str_shuffle(Constant::UUID), 0, 6);
        $input['email'] = $request->route('email');
        $input['email_verified_at'] = Carbon::now();
        $user = User::create($input);
        event(new Registered($user));

        // after update succeed, clear both session
        $request->session()->forget([$this->row_session, $this->encrypted_session]);

        return redirect()->route('register.complete_page');
    }

    protected function completePage(Request $request)
    {
        return view('auth.registerComplete');
    }
}
