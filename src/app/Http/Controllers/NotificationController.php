<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = Notification::userNotifications()->orderByDesc('notification_at')->get();
        $unread_ids = Notification::where([
            'user_id' => auth()->user()->id,
            'is_notification_read' => false,
        ])->get()->pluck('id')->toArray();

        if (sizeof($unread_ids) > 0) {
            Notification::whereIn('id', $unread_ids)->update(['is_notification_read' => true]);
        }

        return view('notification.list', compact('notifications'));
    }

    public function readNotification(Request $request)
    {
        $notification = Notification::find($request->id);
        $notification->is_notification_read = true;
        $notification->read_at = now();
        $notification->update();

        return redirect($notification->destination_url);
    }
}
