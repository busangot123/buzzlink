<?php

namespace App\Http\Controllers;

use App\Helpers\Constant;
use App\Helpers\ProjectFormater;
use App\Http\Requests\ProjectStoreRequest;
use App\Models\Fee;
use App\Models\Genre;
use App\Models\Project;
use App\Models\ProjectUser;
use App\Models\Tax;
use App\Services\ProjectService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProjectController extends Controller
{
    protected $project_service;

    public function __construct(ProjectService $project_service)
    {
        $this->project_service = $project_service;
    }

    public function projectDetails(Request $request)
    {
        $project = Project::getProject($request->project_id);

        ProjectFormater::formatProjectData($project);

        return $project;
    }

    public function searchDetails($project_id)
    {
        $project = Project::getProject($project_id);
        ProjectFormater::formatProjectData($project);

        return view('search.details', compact('project'));
    }

    public function confirmProjectApplication(Request $request)
    {
        $project = ProjectUser::where([
            'project_id' => $request->project_id,
            'influencer_user_id' => auth()->user()->id,
        ])->first();

        return $project ? 'true' : 'false';
    }

    public function storeForm(Request $request)
    {
        $session_input = $request->session()->get('project.store');

        $genres = Genre::all();
        $tax_rate = Tax::find(1)->tax_rate;
        $fee_rate = Fee::find(1)->fee_rate;

        return view('project.store.form', compact('session_input', 'genres', 'fee_rate', 'tax_rate'));
    }

    public function storeFormPost(ProjectStoreRequest $request)
    {
        $input = $this->project_service->storeFormPost($request);

        $request->session()->put('project.store', $input);

        return redirect()->route('project.store.confirm');
    }

    public function storeConfirm(Request $request)
    {
        if (!$request->session()->has('project.store')) {
            return redirect()->route('project.store.form');
        }

        $input = $request->session()->get('project.store');

        return view('project.store.confirm', compact('input'));
    }

    public function storeConfirmPost(Request $request)
    {
        if (!$request->session()->has('project.store')) {
            return redirect()->route('project.store.form');
        }

        $input = $request->session()->get('project.store');
        $storeRequest = new ProjectStoreRequest();

        $project = $storeRequest->makeProject($input);
        $genres = $storeRequest->makeProjectCategories($input);
        $attachments = $storeRequest->makeAttachments($input);

        $this->project_service->storeConfirmPost($project, $genres, $attachments);

        $request->session()->forget('project.store');

        return redirect()->route('project.store.complete');
    }

    public function delete(Request $request)
    {
        Project::find($request->id)->delete();

        return response()->json([
            'status' => Constant::STATUS['SUCCESS'],
        ]);
    }

    public function search(Request $request)
    {
        $projects = Project::searchResult($request);
        $categories = Genre::all();
        foreach ($projects->items() as $project) {
            ProjectFormater::formatProjectData($project);
        }

        return view('search.search_result', compact(['projects', 'categories']));
    }

    public static function getRecommendedProjects(Request $request)
    {
        $projects = Project::getRecommendedProjects();
        foreach ($projects as $project) {
            ProjectFormater::formatProjectData($project);
        }
        $categories = Genre::all();
        if ('/top_client' === $request->getPathInfo()) {
            return view('top.top_client', compact(['projects', 'categories']));
        }
        if ('/top_influencer' === $request->getPathInfo()) {
            return view('top.top_influencer', compact(['projects', 'categories']));
        }
    }

    public function rates()
    {
        $fee = Fee::find(1);

        return [
            'fee' => $fee->fee_rate,
            'min_fee' => $fee->min_rate,
            'tax' => Tax::find(1)->tax_rate,
        ];
    }

    public function projectManagementPage(Request $request)
    {
        if ($request->input('page')) {
            return response()->json($this->projectManagement($request->input('status')));
        }

        $data = auth()->user();
        $status_keys = array_keys(Constant::PROJECT_STATUS);
        $status = collect($status_keys)->mapWithKeys(function ($item) {
            $key = strtolower($item);

            return [$key => $this->projectManagement($key)];
        })->toArray();

        return view('user.mypage.project_management', compact('data', 'status'));
    }

    public static function projectManagement($params)
    {
        $projects = Project::getProjectList($params);
        foreach ($projects->items() as $project) {
            ProjectFormater::formatProjectData($project);
        }

        return $projects;
    }

    public function getProject(Request $request)
    {
        $data = $request->all();
        $response = Project::with(['projectAttachment', 'projectCategory', 'user' => function ($q) {
            $q->with('hasManyProjectUsers');
        }])->findOrFail($data['project_id']);
        $response->number_of_applicants = ProjectUser::whereProjectId($data['project_id'])->count();

        return $this->response(
            'Successfully Fetched.',
            $response,
            Response::HTTP_OK
        );
    }

    protected function storeComplete(Request $request)
    {
        return view('project.store.complete');
    }
}
