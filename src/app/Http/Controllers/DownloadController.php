<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\ProjectAttachment;
use App\SharedService\FileSharedService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class DownloadController extends Controller
{
    private $file;

    public function __construct(FileSharedService $file)
    {
        $this->file = $file;
    }

    public function downloadProjectAttachment(Request $request)
    {
        $attachment = ProjectAttachment::findOrFail($request->id);
        $file = $this->file->downloadPublicFile($attachment->getAttributes()['path']);
        $mime_type = $this->file->getPublicFileMimeType($attachment->getAttributes()['path']);

        $headers = [
            'Content-Disposition' => 'attachment; filename="'.$attachment->name.'"',
            'Content-Type' => $mime_type,
        ];

        return Response::make($file, 200, $headers);
    }

    public function downloadMessageAttachment(Request $request)
    {
        $message = Message::whereNotNull('attachment_name')->whereNotNull('attachment_path')->findOrFail($request->id);
        $this->authorize('view', [Message::class, $message]);

        $file = $this->file->downloadPrivateFile($message->getAttributes()['attachment_path']);
        $mime_type = $this->file->getPrivateFileMimeType($message->getAttributes()['attachment_path']);

        $headers = [
            'Content-Disposition' => 'attachment; filename="'.$message->attachment_name.'"',
            'Content-Type' => $mime_type,
        ];

        return Response::make($file, 200, $headers);
    }
}
