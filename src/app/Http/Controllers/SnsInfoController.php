<?php

namespace App\Http\Controllers;

use App\Http\Requests\SnsInfoRequest;
use App\Models\FacebookAccounts;
use App\Models\InstagramAccounts;
use App\Models\TiktokAccounts;
use App\Models\TwitterAccounts;
use App\Models\YoutubeAccounts;
use App\Traits\ApiJson;
use Illuminate\Http\Request;

class SnsInfoController extends Controller
{
    use ApiJson;

    public $sns_ids = [
        'youtube' => 'channel_id',
        'instagram' => 'instagram_id',
        'facebook' => 'facebook_id',
        'twitter' => 'twitter_id',
        'tiktok' => 'tiktok_id',
    ];

    public static function listSnsOfUser($user_id = null)
    {
        $user_id = null == $user_id ? auth()->user()->id : $user_id;

        return array_merge(
            YoutubeAccounts::userAccounts($user_id)->toArray(),
            InstagramAccounts::userAccounts($user_id)->toArray(),
            FacebookAccounts::userAccounts($user_id)->toArray(),
            TwitterAccounts::userAccounts($user_id)->toArray(),
            TiktokAccounts::userAccounts($user_id)->toArray(),
        );
    }

    public function storeSnsInfo(SnsInfoRequest $request)
    {
        $sns_info = $request->all();
        $type = $sns_info['type'];
        $sns_model = $this->getSnsModel($type);
        $SNS_ID = $this->sns_ids[$type];
        $this->selectSnsFields($type, $sns_model, $sns_info);
        $sns_model->{$SNS_ID} = $sns_info['sns_id'];
        $sns_model->user_id = auth()->user()->id;
        $sns_model->url = $sns_info['url'];
        $sns_model->followers = $sns_info['followers'];
        $sns_model->save();

        return $this->successPostResponse(__('response.sns.update'));
    }

    public function updateSnsInfo(SnsInfoRequest $request)
    {
        $sns_info = $request->all();
        $type = $sns_info['type'];
        $SNS_ID = $this->sns_ids[$type];
        $sns_model = $this->getSnsModel($type);
        $sns_model = $sns_model->find($sns_info['id']);
        $this->selectSnsFields($type, $sns_model, $sns_info);
        $sns_model->{$SNS_ID} = $sns_info['sns_id'];
        $sns_model->url = $sns_info['url'];
        $sns_model->followers = $sns_info['followers'];
        $sns_model->save();

        return $this->successPostResponse(__('response.sns.update'));
    }

    public function deleteSnsInfo(Request $request)
    {
        $req = $request->all();
        $type = $req['type'];
        $id = $req['id'];
        $sns_model = $this->getSnsModel($type);
        $sns_model->where('id', $id)->delete();

        return $this->successPostResponse(__('response.sns.update'));
    }

    private function getSnsModel($type)
    {
        $sns_model = [];

        switch ($type) {
            case 'youtube':
                $sns_model = new YoutubeAccounts();

                break;

            case 'instagram':
                $sns_model = new InstagramAccounts();

                break;

            case 'facebook':
                $sns_model = new FacebookAccounts();

                break;

            case 'twitter':
                $sns_model = new TwitterAccounts();

                break;

            case 'tiktok':
                $sns_model = new TiktokAccounts();

                break;
        }

        return $sns_model;
    }

    private function selectSnsFields($type, $sns_model, $sns_info)
    {
        switch ($type) {
            case 'youtube':
                $sns_model->views = $sns_info['views'];
                $sns_model->videos = $sns_info['videos'];

                break;

            case 'facebook':
                $sns_model->likes = $sns_info['likes'];

                break;

            case 'tiktok':
            case 'twitter':
            case 'instagram':
                $sns_model->posts = $sns_info['posts'];

                break;
        }
    }
}
