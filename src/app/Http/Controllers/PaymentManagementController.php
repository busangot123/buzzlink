<?php

namespace App\Http\Controllers;

use App\Models\ClientPayment;
use App\Models\InfluencerRewards;
use App\Models\InfluencerTransfer;
use App\Models\Project;
use App\Models\ProjectUser;
use App\Models\RewardBalances;

class PaymentManagementController extends Controller
{
    public function influencerPaymentList()
    {
        $influencer_rewards_list = [];
        $project_user_ids = ProjectUser::where('influencer_user_id', auth()->user()->id)->get()->pluck('id');
        $project_ids = Project::where('client_user_id', auth()->user()->id)->get()->pluck('id');

        $influencer_rewards_list = InfluencerRewards::whereIn('project_user_id', $project_user_ids)->orderByDesc('get_reward_at')->get();
        $influencer_transfer_list = InfluencerTransfer::where('influencer_user_id', auth()->user()->id)->orderByDesc('bank_transfer_date')->get();
        $client_payment_list = ClientPayment::whereIn('project_id', $project_ids)->orderByDesc('created_at')->get();
        $reward_balance = RewardBalances::where('user_id', auth()->user()->id)->latest()->first();

        return view('user.payment.payment_management', compact('reward_balance', 'influencer_rewards_list', 'influencer_transfer_list', 'client_payment_list'));
    }
}
