<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\EmailReset;
use App\Models\User;
use App\Traits\ApiFormRequest;
use App\Traits\ApiJson;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ResetEmailController extends Controller
{
    use ApiFormRequest;
    use ApiJson;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

    protected function rules()
    {
        return [
            'email' => 'required|string|email:rfc|max:255',
            'password' => 'required|string',
        ];
    }

    /**
     * confirm request password, send reset email to new email address.
     *
     * @return json
     */
    protected function sendResetEmail(Request $request)
    {
        $validator = Validator::make($request->all(), $this->rules());
        $validator->validate();

        // wrong password response
        if (!Hash::check($request->password, auth()->user()->password)) {
            return $this->badRequestResponse(__('auth.password'));
        }

        $new_email = $request->email;
        //  duplicate email response
        if (User::where('email', $new_email)->exists()) {
            return $this->badRequestResponse(__('auth.email.duplicate'));
        }

        $token = EmailReset::createNewToken();
        $user = User::findOrFail(Auth::id());

        $exist_token = EmailReset::where('email', $user->email)->first();
        if ($exist_token) {
            $exist_token->delete();
        }

        $email_reset = EmailReset::create([
            'email' => $user->email,
            'name' => $user->name,
            'new_email' => $new_email,
            'token' => Hash::make($token),
        ]);

        $email_reset->sendEmailResetNotification($token);

        return $this->successPostResponse(__('auth.email.reset'));
    }
}
