<?php

namespace App\Http\Controllers;

use App\Events\ProjectUserApplied;
use App\Helpers\Constant;
use App\Helpers\ProjectFormater;
use App\Http\Requests\ProjectUserSelfPromoRequest;
use App\Models\Project;
use App\Models\ProjectUser;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Session;

class ProjectUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->all();

        return $this->getWorkManagementData($data['page']);
    }

    public function getWorkManagement(Request $request)
    {
        $data = User::whereId(auth()->user()->id)->first();

        $count = $this->getProjectUserCount();
        $projects = $this->getWorkManagementData($request->page);

        return view('user.work-management.workmanagement', compact('data', 'projects', 'count'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project = Project::whereId($request->project_id)->with('user')->first();
        $re_apply_project_user = ProjectUser::where([
            'project_id' => $project->id,
            'influencer_user_id' => auth()->user()->id,
            'application_status' => Constant::PROJECT_APPLICATION_STATUS['MISSING'],
        ])->first();

        if ($re_apply_project_user) {
            $re_apply_project_user->application_status = Constant::PROJECT_APPLICATION_STATUS['APPLIED'];
            $re_apply_project_user->self_promotion = $request->self_promotion;
            $re_apply_project_user->update();
        } else {
            $data = $request->all();
            $data['influencer_user_id'] = auth()->user()->id;
            $data['updated_user'] = auth()->user()->id;
            $data['created_user'] = auth()->user()->id;
            $data['applied_at'] = Carbon::now();
            $project_user = ProjectUser::create($data);
        }

        $mail_data = [
            'client_name' => $project->user->name,
            'influencer_name' => $re_apply_project_user ? $re_apply_project_user->user->name : $project_user->user->name,
            'project_title' => $project->title,
            'influencer_url' => route('workmanagement.details', ['project_id' => $project->id]),
            'project_path' => route('project.details', ['project_id' => $project->id, 'limit' => Constant::LIST_LIMIT]),
            'application_deadline' => japaneseDateFormat($project->application_deadline_at),
            'selection_deadline' => japaneseDateFormat($project->selection_deadline_at),
        ];

        ProjectUserApplied::dispatch($mail_data, auth()->user(), $project->user, $project->title);

        return redirect()->route('project.done', ['project_id' => $project->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $response = ProjectUser::find($id);

        return $this->response(
            'Successfully Fetched.',
            $response,
            Response::HTTP_OK
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $data['reported_at'] = Carbon::now();
        $data['application_status'] = Constant::PROJECT_APPLICATION_STATUS['CHECKING'];
        ProjectUser::whereId($id)->update($data);
        $project_user = ProjectUser::with(['project' => function ($q) {
            $q->with('user');
        }, 'user'])->whereId($id)->first();

        $details = [
            'client_name' => $project_user->project->user->name,
            'influencer_name' => $project_user->user->name,
            'project_title' => $project_user->project->title,
            'influencer_link' => $request->getSchemeAndHttpHost().'/mypage/work_management/',
            'client_link' => $request->getSchemeAndHttpHost().'/mypage/project_management/',
            'report_deadline' => Carbon::parse($project_user->project->report_deadline_at)->format('Y年m月d日'),
            'report_deadline_additional' => Carbon::parse($project_user->project->report_deadline_at)->addDays(14)->format('Y年m月d日'),
            'host' => $request->getSchemeAndHttpHost(),
        ];

        //influencer
        $this->emailSender(
            $project_user->user->id,
            $project_user->user->email,
            $details,
            Constant::EMAIL_TEMPLATE_ID['REPORT_INFLUENCER'],
        );
        //client
        $this->emailSender(
            $project_user->project->user->id,
            $project_user->project->user->email,
            $details,
            Constant::EMAIL_TEMPLATE_ID['REPORT_CLIENT'],
        );

        return $this->response(
            'Successfully Fetched.',
            $data,
            Response::HTTP_OK
        );
    }

    public function declineApplication(Request $request)
    {
        $details = [
            'application_status' => Constant::PROJECT_APPLICATION_STATUS['MISSING'],
        ];

        ProjectUser::where([
            'project_id' => $request->project_id,
            'influencer_user_id' => auth()->user()->id,
        ])->update($details);

        return redirect()->route('mypage.work_management');
    }

    public function updateApplication(ProjectUserSelfPromoRequest $request)
    {
        $data = $request->all();
        $details = [
            'self_promotion' => $data['self_promotion'],
        ];
        ProjectUser::where([
            'project_id' => $data['project_id'],
            'influencer_user_id' => auth()->user()->id,
        ])->update($details);

        return redirect()->route('project.edit.done', ['project_id' => $request->project_id]);
    }

    public function getApplicationStatusCount(Request $request)
    {
        $data = $request->all();
        $applied_count = ProjectUser::whereProjectId($data['project_id'])->whereApplicationStatus(Constant::PROJECT_APPLICATION_STATUS['APPLIED'])->count();
        $contact_count = ProjectUser::whereProjectId($data['project_id'])->whereApplicationStatus(Constant::PROJECT_APPLICATION_STATUS['CONTACT'])->count();
        $response = [
            'contact_count' => $contact_count,
            'applied_count' => $applied_count,
        ];

        return $this->response(
            'Successfully Fetched.',
            $response,
            Response::HTTP_OK
        );
    }

    public function getProjectUserCount()
    {
        return [
            'applied' => ProjectUser::countApplied(),
            'selection' => ProjectUser::countSelection(),
            'working' => ProjectUser::countWorking(),
            'checking' => ProjectUser::countChecking(),
            'done' => ProjectUser::countDone(),
            'missing' => ProjectUser::countMissing(),
        ];
    }

    public function getOtherProjectUsers($project_id)
    {
        $response = ProjectUser::with(['user' => function ($q) {
            $q->with(['hasManyGenres'])
                ->withCount([
                    'hasManyProjectUsers as has_many_project_users_corresponded_count' => function ($q) {
                        $q->where('application_status', Constant::PROJECT_APPLICATION_STATUS['DONE']);
                    },
                    'hasManyProjectUsers as has_many_project_users_completion_rate_count' => function ($q) {
                        $q->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['COMPLETION_RATE']);
                    },
                ])
            ;
        }])
            ->where('influencer_user_id', '!=', auth()->user()->id)
            ->where('project_id', $project_id)->get();

        return $response;
    }

    public function getProjectUsers($project_id)
    {
        $response = ProjectUser::where([
            'project_id' => $project_id,
            'influencer_user_id' => auth()->user()->id,
        ])->with([
            'user' => function ($q) {
                $q->with(['hasManyGenres', 'projects', 'hasManyProjectUsers' => function ($q) {
                    $q->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['CONTACT']);
                }]);
            },
            'project',
        ])->withCount(['hasManyProjects as has_many_project_counts'])->first();

        $response->influencer_rate = User::withCount([
            'hasManyProjectUsers as project_user_count',
            'hasManyProjectUsers as has_many_project_users_corresponded_count' => function ($q) {
                $q->where('application_status', Constant::PROJECT_APPLICATION_STATUS['DONE']);
            },
            'hasManyProjectUsers as has_many_project_users_completion_rate_count' => function ($q) {
                $q->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['COMPLETION_RATE']);
            },
        ])
            ->whereId(auth()->user()->id)
            ->first()
        ;

        return $response;
    }

    public function getProjectUserDetails(Request $request)
    {
        $data = $request->all();
        $response = ProjectUser::whereId($data['project_id'])
            ->whereInfluencerUserId(auth()->user()->id)
            ->with([
                'user' => function ($q) {
                    $q->with(['hasManyGenres']);
                },
                'project' => function ($q) {
                    $q->with(['projectCategory', 'user', 'projectAttachment', 'hasManyProjectUsers' => function ($q) {
                        $q->with('user', function ($q) {
                            $q->with('hasManyGenres');
                        });
                    }]);
                },
            ])
            ->first()
        ;
        self::formatDates($response->project);

        return $this->response(
            'Successfully Fetched.',
            $response,
            Response::HTTP_OK
        );
    }

    public function getCompletedProjectUsers(Request $request)
    {
        $data = $request->all();
        $response = ProjectUser::with(['user' => function ($q) {
            $q->with(['hasManyGenres'])
                ->withCount([
                    'hasManyProjectUsers as has_many_project_users_corresponded_count' => function ($q) {
                        $q->where('application_status', Constant::PROJECT_APPLICATION_STATUS['DONE']);
                    },
                    'hasManyProjectUsers as has_many_project_users_completion_rate_count' => function ($q) {
                        $q->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['COMPLETION_RATE']);
                    },
                ])
            ;
        }])
            ->where('application_status', Constant::PROJECT_APPLICATION_STATUS['COMPLETED'])
            ->where('influencer_user_id', '!=', auth()->user()->id)
            ->whereProjectId($this->getUserProjectId($data['project_id']))->get();

        return $this->response(
            'Successfully Fetched.',
            $response,
            Response::HTTP_OK
        );
    }

    public function formatDates($data)
    {
        $application_deadline = Carbon::parse($data->application_deadline_at);
        $data->application_hours_left = now()->diffInHours($application_deadline, false);
        $data->application_deadline_at = $application_deadline->format('n月j日');
        $selection_deadline = Carbon::parse($data->selection_at);
        $data->selection_hours_left = now()->diffInHours($selection_deadline, false);
        $data->selection_deadline_at = $selection_deadline->format('n月j日');
        $report_deadline = Carbon::parse($data->report_deadline_at);
        $data->report_hours_left = now()->diffInHours($report_deadline, false);
        $checking_deadline = Carbon::parse($data->report_deadline_at)->addDays(14);
        $data->report_deadline_at = $report_deadline->format('n月j日');
        $data->checking_hours_left = now()->diffInHours($checking_deadline, false);
        $data->checking_deadline_at = $report_deadline->format('n月j日');
        $data->published = Carbon::parse($data->created_at)->format('Y年n月j日');
        if (!isset($data->hasManyProjectUsers)) {
            $data->completion_date = Carbon::parse($data->evaluated_at)->format('n月j日');
        }
    }

    public function getReportingUser(Request $request)
    {
        $user = ProjectUser::where(['project_id' => $request->project_id, 'influencer_user_id' => $request->user_id])
            ->with('user', function ($q) {
                $q->with(['hasManyGenres', 'projects', 'hasManyProjectUsers' => function ($q) {
                    $q->whereIn('application_status', Constant::PROJECT_APPLICATION_STATUS['CONTACT']);
                }]);
            })
            ->first()
        ;
        if ($user->sns_url) {
            $sns_parts = explode('/', $user->sns_url);
            $user->sns_type = $sns_parts[Constant::SNS_PARTS['HOSTING']];

            switch ($sns_parts[Constant::SNS_PARTS['HOSTING']]) {
                case 'www.instagram.com':
                    $user->sns_frame_url = 'https://'.$sns_parts[Constant::SNS_PARTS['HOSTING']].'/'.$sns_parts[Constant::SNS_PARTS['PARAMS']].'/'.$sns_parts[Constant::SNS_PARTS['POST_ID']].'/embed';

                    break;

                case 'www.youtube.com':
                    $user->sns_frame_url = 'https://'.$sns_parts[Constant::SNS_PARTS['HOSTING']].'/embed/'.explode('=', $sns_parts[Constant::SNS_PARTS['PARAMS']])[Constant::SNS_PARTS['PROTOCOL']];

                    break;

                case 'twitter.com':
                    $user->sns_frame_url = 'https://twitframe.com/show?url='.$user->sns_url;

                    break;

                case 'www.facebook.com':
                    $user->sns_frame_url = $user->sns_url;

                    break;

                case 'www.tiktok.com':
                    $user->sns_frame_url = 'https://'.$sns_parts[Constant::SNS_PARTS['HOSTING']].'/embed/'.$sns_parts[Constant::SNS_PARTS['POST_LINK']];

                    break;

                default:
                    $user->sns_frame_url = '';

                    break;
            }
        }

        return $user;
    }

    public function updateClientRating(Request $request)
    {
        $user = ProjectUser::find($request->id);
        $user->client_eva = $request->rate;
        $user->client_comment = $request->comments;
        $user->evaluated_at = now();
        $user->application_status = Constant::PROJECT_APPLICATION_STATUS['DONE'];
        $user->update();

        return response()->json([
            'status' => Constant::STATUS['SUCCESS'],
        ]);
    }

    public function workUserApplication($project_id)
    {
        $project = Project::getProject($project_id);
        ProjectFormater::formatProjectData($project);
        $self_promotion = ProjectUser::where([
            'project_id' => $project_id,
            'influencer_user_id' => auth()->user()->id,
        ])->get()->pluck('self_promotion')->first();

        Session::put('edit_self_promotion_'.$project_id, $self_promotion);

        return view('project_application.edit.application', compact('project', 'self_promotion'));
    }

    public function showWorkUserConfirmation($project_id)
    {
        $project = Project::getProject($project_id);
        ProjectFormater::formatProjectData($project);
        $self_promotion = Session::get('edit_self_promotion_'.$project_id);

        return view('project_application.edit.confirmation', compact('project', 'self_promotion'));
    }

    public function workUserConfirmation(ProjectUserSelfPromoRequest $request)
    {
        $request->session()->put('edit_self_promotion_'.$request->project_id, $request->self_promotion);

        return redirect()->route('project.edit.confirmation.page', ['project_id' => $request->project_id]);
    }

    public function showWorkUserComplete($project_id)
    {
        Session::forget('edit_self_promotion_'.$project_id);

        return view('project_application.edit.done');
    }

    public function projectUserApplication($project_id)
    {
        $project = Project::getProject($project_id);
        ProjectFormater::formatProjectData($project);
        $self_promotion = Session::get('self_promotion_'.$project_id);

        return view('project_application.application', compact('project', 'self_promotion'));
    }

    public function showProjectUserConfirmation($project_id)
    {
        $project = Project::getProject($project_id);
        ProjectFormater::formatProjectData($project);
        $self_promotion = Session::get('self_promotion_'.$project_id);

        return view('project_application.confirmation', compact('project', 'self_promotion'));
    }

    public function showProjectUserComplete($project_id)
    {
        Session::forget('self_promotion_'.$project_id);

        return view('project_application.done', ['project_id' => $project_id]);
    }

    public function projectUserConfirmation(ProjectUserSelfPromoRequest $request)
    {
        $request->session()->put('self_promotion_'.$request->project_id, $request->self_promotion);

        return redirect()->route('project.confirmation.page', ['project_id' => $request->project_id]);
    }

    public function checkReportDetails(ProjectUserReportDetailsRequest $request)
    {
        return $request->all();
    }

    public function emailSender($id, $email, $data, $template_id)
    {
        $user = User::findOrFail($id);
        $user->sendEmailVerifyNotification($email, $data, $template_id);
    }

    public function workManagementDetails($project_id)
    {
        $project_users = $this->getProjectUsers($project_id);
        $work_details = [
            'project_users' => $project_users,
        ];

        $project = Project::getProject($project_id);
        ProjectFormater::formatProjectData($project);

        return view('user.apply-project.project_application_detail', compact('project', 'project_users', 'work_details'));
    }

    private function getWorkManagementData($page)
    {
        $statuses = [
            'applied' => Constant::PROJECT_APPLICATION_STATUS['APPLIED'],
            'selection' => Constant::PROJECT_APPLICATION_STATUS['SELECTION'],
            'working' => Constant::PROJECT_APPLICATION_STATUS['WORKING'],
            'checking' => Constant::PROJECT_APPLICATION_STATUS['CHECKING'],
            'done' => Constant::PROJECT_APPLICATION_STATUS['DONE'],
            'missing' => Constant::PROJECT_APPLICATION_STATUS['COMPLETED'],
        ];

        return collect($statuses)->map(function ($item) use ($page) {
            return ProjectUser::select('project_users.*')
                ->leftJoin('projects', 'project_users.project_id', '=', 'projects.id')
                ->with(['project' => function ($q) {
                    $q->with(['user', 'projectCategory']);
                }, 'user'])
                ->where([
                    'influencer_user_id' => auth()->user()->id,
                    'application_status' => $item,
                ])
                ->orderByDesc('projects.created_at')
                ->paginate(Constant::PAGINATION_COUNT, ['*'], 'page', $page)
            ;
        });
    }

    private function getUserProjectId($id)
    {
        return ProjectUser::find($id)->project_id;
    }
}
