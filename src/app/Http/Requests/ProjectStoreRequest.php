<?php

namespace App\Http\Requests;

use App\Models\Genre;
use App\Models\Project;
use App\Models\ProjectAttachment;
use App\Models\ProjectCategory;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class ProjectStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'genres' => 'required_if:other_category,==,null|array|genres_max:other_category,3',
            'genres.*' => 'required|integer|distinct|min:1', //TODO: include count of genres
            'other_category' => 'nullable|string|max:20',
            'title' => 'required|string|max:255',
            'content' => 'required|string|max:2500',
            'post_sns_code' => 'integer|between:1,5',
            'notification_url' => 'nullable|string|max:200',
            'reward_amount' => 'required|numeric|min:5000', // TODO: get Constant
            'request_people_number' => 'required|integer|between:1,10',
            'application_deadline_at' => 'required|integer|between:0,9',
            'report_deadline_at' => 'required|date_format:Y-m-d H:i:s|report_deadline:application_deadline_at',
            'attachments' => 'array|array_sum_max:uploaded_files_index,5',
            'attachments.*' => 'required|file|max:10000',
            'uploaded_files_index' => 'array|array_sum_max:attachments,5',
            'uploaded_files_index.*' => 'required|integer|distinct|max:5',
        ];
    }

    public function getValidatedAttributes(): array
    {
        return Arr::add($this->validated(), 'application_start_at', Carbon::now());
    }

    /**
     * return project model.
     *
     * @param mixed $session
     */
    public function makeProject($session): Project
    {
        $projectAttibute = Arr::only(
            $session,
            [
                'title',
                'content',
                'post_sns_code',
                'notification_url',
                'reward_amount',
                'request_people_number',
                'application_start_at',
                'application_deadline_at',
                'report_deadline_at',
            ]
        );

        return new Project($projectAttibute);
    }

    /**
     * return projectCategory models array.
     *
     * @param mixed $session
     */
    public function makeProjectCategories($session): array
    {
        $project_categories = [];
        if (isset($session['genres'])) {
            foreach ($session['genres'] as $genre) {
                $project_category = new ProjectCategory([
                    'name' => Genre::getGenreName($genre),
                ]);

                array_push($project_categories, $project_category);
            }
        }

        if (isset($session['other_category'])) {
            $other_category = new ProjectCategory([
                'name' => $session['other_category'],
            ]);
            array_push($project_categories, $other_category);
        }

        return $project_categories;
    }

    public function makeAttachments($session): array
    {
        $attachments = [];
        foreach ($session['uploaded_files'] as $file) {
            $attachment = new ProjectAttachment([
                'name' => $file['name'],
                'path' => $file['path'],
            ]);

            array_push($attachments, $attachment);
        }

        return $attachments;
    }
}
