<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserInformationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $now = date('Y-m-d');

        return [
            'name' => 'required|double_space|string|max:100',
            'full_name' => 'required|double_space|string|max:64',
            'full_name_kana' => 'required|double_space|string|max:64|katakana',
            'phone_number' => 'required|numeric|digits_between:1,15',
            'default_role' => 'required|digits_between:0,1',
            'birthday' => 'nullable|date|before:'.$now,
        ];
    }
}
