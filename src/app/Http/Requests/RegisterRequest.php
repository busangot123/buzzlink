<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|double_space|string|max:100',
            'password' => 'required|string|between:8,32|contain_alpha_numeric|alpha_numeric_symbol',
            'full_name' => 'required|double_space|string|max:64',
            'full_name_kana' => 'required|double_space|string|max:64|katakana',
            'phone_number' => 'required|numeric|digits_between:1,15',
            'default_role' => 'required|numeric|between:1,2',
        ];
    }

    /**
     * return register info for server.
     *
     * @return array request
     */
    public function registerAttributes()
    {
        return $this->only([
            'name',
            'password',
            'full_name',
            'full_name_kana',
            'phone_number',
            'default_role',
        ]);
    }

    /**
     * return register info for input.
     *
     * @return array request
     */
    public function registerAttributesWithoutPassword()
    {
        return $this->only([
            'name',
            'full_name',
            'full_name_kana',
            'phone_number',
            'default_role',
            'checkbox',
        ]);
    }
}
