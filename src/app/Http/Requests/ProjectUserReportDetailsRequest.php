<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectUserReportDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'sns_url.required' => 'Sns url は必須です。',
            'influencer_eva.required' => 'Influencer evaluation は必須です。',
            'influencer_comment.required' => 'influencer comment は必須です。',
            'sns_url.url' => 'Sns url format is invalid',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sns_url' => 'required|url',
            'influencer_eva' => 'required|numeric',
            'influencer_comment' => 'required|max:2500',
        ];
    }
}
