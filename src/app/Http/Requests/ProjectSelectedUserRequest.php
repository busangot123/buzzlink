<?php

namespace App\Http\Requests;

use App\Rules\CheckProjectMaximumSelection;
use Illuminate\Foundation\Http\FormRequest;

class ProjectSelectedUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => 'required|integer|exists:projects,id',
            'selected_users' => ['required', 'array', 'min:1', new CheckProjectMaximumSelection($this->project_id)],
            'selected_users.*' => 'required|string|distinct|exists:users,uuid',
        ];
    }
}
