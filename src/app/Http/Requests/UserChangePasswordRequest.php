<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class UserChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'password_error.required' => 'Current password is incorrect',
            'current_password.required' => '現在のパスワードは必須です。',
            'password.required' => '新しいパスワードは必須です。',
            'confirm_new_password.required' => '新しいパスワードを再入力は必須です。',
            'password.same' => '登録パスワードと一致していません。',
            'confirm_new_password.same' => '登録パスワードと一致していません。',
            'password.min' => 'New password must be atleast 8 characters',
            'confirm_new_password.min' => 'Confirm new password must be atleast 8 characters',
            'password.max' => 'パスワードは8~32文字で入力してください。',
            'confirm_new_password.max' => 'パスワードは8~32文字で入力してください。',
            'password.regex' => 'パスワードは英数字の両方を含む必要があります。',
            'confirm_new_password.regex' => 'パスワードは英数字の両方を含む必要があります。',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'current_password' => 'required',
            'password' => 'min:8|max:32|required|regex:/^(?=.*?[aA-zZ])(?=.*?[0-9])/|same:confirm_new_password',
            'confirm_new_password' => 'min:8|max:32|regex:/^(?=.*?[aA-zZ])(?=.*?[0-9])/|required|same:password',
        ];
        if (null != $this->request->get('current_password')) {
            if (!Hash::check($this->request->get('current_password'), auth()->user()->password)) {
                $rules['password_error'] = 'required';
            }
        }

        return $rules;
    }
}
