<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SnsInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required|string|in:youtube,instagram,facebook,tiktok,twitter',
            'sns_id' => ['nullable', 'regex:/^[a-zA-Z0-9`~!@#\$%\^&\*\(\)-_=\+\[\{\}\]\\\|;:\'",<\.>\/\?]+$/'],
            'url' => 'nullable|url',
            'followers' => 'required|integer',
            'views' => 'nullable|integer',
            'videos' => 'nullable|integer',
            'likes' => 'nullable|integer',
            'posts' => 'nullable|integer',
        ];
    }
}
