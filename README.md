# Containers 
- php-apache-8.0.1
- mysql5.7
- mailhog
- schemaspy
- phpMyAdmin

# How to set up docker enviroment

go to docker directory 
```
cd  ./docker
```

Start up docker containers  
(At this time, schemaspy generates DB Scheme from buzzlink database.  
 If you want to regenerate schemaspy, execute docker-compose up command again
)
```
docker-compose up -d
```

edit .env along the docker-compos file env variables  

# enable queue database 

you should use queue when you test sending email.
The environment valiable should be set in .env like below 

```
QUEUE_CONNECTION=database
```

"database"  use queue in database.
"sync"  doesn't use queue. 

# how to use minio 

When docker-compose up, mino creates bucket "public-bucket" and "private-bucket". 
And set read:write permission to both buckets 

you should copy your .env file from .env.example 
```
AWS_ACCESS_KEY_ID=minio
AWS_SECRET_ACCESS_KEY=minio123
AWS_DEFAULT_REGION=us-east-1
AWS_PUBLIC_BUCKET=public-file
AWS_PRIVATE_BUCKET=private-file
AWS_URL_LOCAL=http://localhost:9090
AWS_URL_DOCKER=http://minio:9000

AWS_LOCAL_PUBLIC_URL="${AWS_URL_LOCAL}/${AWS_PUBLIC_BUCKET}"

AWS_PUBLIC_URL="${AWS_URL_DOCKER}/${AWS_PUBLIC_BUCKET}"
AWS_PRIVATE_URL="${AWS_URL_DOCKER}/${AWS_PRIVATE_BUCKET}"
BUCKET_ENDPOINT=false
USE_PATH_STYLE_ENDPOINT=true
```

And execute config:cache. 

you can access minio admin page using localhost:9090.
id=minio,  key=minio123



# access port 
- php-apache localhost:80
- phpMyAdmin localhost:8080