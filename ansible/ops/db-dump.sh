#!/bin/bash
while getopts e: OPT
do
    case $OPT in
        e)  ENV=${OPTARG}
            ;;
    esac
done

db_dump_bucket="buzzlink-$ENV-db-dump"


case ${ENV} in
  "stg")
    DB_USER=$STG_DB_USER
    DB_PASSWORD=$STG_DB_PASSWORD
    DB_HOST=$STG_DB_HOST
    DB_NAME=$STG_DB_NAME
    ;;
  "prd")
    DB_USER=$PRD_DB_USER
    DB_PASSWORD=$PRD_DB_PASSWORD
    DB_HOST=$PRD_DB_HOST
    DB_NAME=$PRD_DB_NAME
    ;;
  *)
  echo "stg, prd以外の値が入力されました。プログラムを終了します。"
  exit
  ;;
esac

echo "${ENV}環境のDBバックアップを開始します。"

mysqldump -u $DB_USER -p$DB_PASSWORD -h $DB_HOST $DB_NAME  --skip-column-statistics --no-tablespaces > /tmp/${ENV}_mysqldump_`date +%y%m%d`.sql
tar czf /tmp/${ENV}_mysqldump_`date +%y%m%d`.tar.gz /tmp/${ENV}_mysqldump_`date +%y%m%d`.sql

aws s3 cp /tmp/${ENV}_mysqldump_`date +%y%m%d`.tar.gz "s3://$db_dump_bucket"

echo "バックアップが完了しました。"